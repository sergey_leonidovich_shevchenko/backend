#!/bin/bash

clear && set -eux
rm -rf ./var/cache/*

RANDOM_STRING=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

# Отобразить сообщение
function addLogMessage {
    if [[ ! -f ./var/projectSetupLog ]]; then
        touch ./var/projectSetupLog && chmod 777 ./var/projectSetupLog
    fi
    echo -e "\n>>>>>>>>>>>> "${1}" <<<<<<<<<<<<\n"
    echo "[$(date)] ${1}" >> ./var/projectSetupLog
}

# Спарсить количество из var_dump вывода
function parseCount {
    SQL="${1}"
    IS_ISSET=$(                             \
        echo ${SQL}                         \
        | awk -F "string '" '{print $2}'    \
        | awk -F "' \(length=" '{print $1}' \
        | tr -d '[:space:]'                 \
    )
}

function isIsset {
    # TODO: Попробовать научить доктринку форматировать вывод не в стиле php var_dump()
    parseCount "$(php bin/console doctrine:query:sql "${1}")"
}


# Создание и наполнение переменных окружений в проект
function createAndFillEnvironment {
    addLogMessage 'Создание и наполнение переменных окружений в проект...'

    MYSQL_CREDENTIAL=
    if [[ ! -z ${MYSQL_USER} ]]; then
        MYSQL_CREDENTIAL=${MYSQL_USER}
        if [[ ! -z ${MYSQL_PASSWORD} ]]; then
            MYSQL_CREDENTIAL="${MYSQL_CREDENTIAL}:${MYSQL_PASSWORD}"
        fi
        if [[ ! -z ${MYSQL_CREDENTIAL} ]]; then
            MYSQL_CREDENTIAL="${MYSQL_CREDENTIAL}@"
        fi
    fi
    addLogMessage "MYSQL_CREDENTIAL: ${MYSQL_CREDENTIAL}"

    MAILER_CREDENTIAL=
    if [[ ! -z ${MAILER_USER} ]]; then
        MAILER_CREDENTIAL=${MAILER_USER}
        if [[ ! -z ${MAILER_PASSWORD} ]]; then
            MAILER_CREDENTIAL="${MAILER_USER}:${MAILER_PASSWORD}"
        fi
        if [[ ! -z ${MAILER_CREDENTIAL} ]]; then
            MAILER_CREDENTIAL="${MAILER_CREDENTIAL}@"
        fi
    fi
    addLogMessage "MAILER_CREDENTIAL: ${MAILER_CREDENTIAL}"

    for ENV_FILENAME in './.env.local' './.env.test.local'
    do
        cp ./.env ${ENV_FILENAME} && chmod 777 ${ENV_FILENAME}
        sed -i "s/^APP_ENV=/APP_ENV=${APP_ENV}/" ${ENV_FILENAME}
        sed -i "s/^APP_SECRET=/APP_SECRET=${RANDOM_STRING}/" ${ENV_FILENAME}
        sed -i "s,^DATABASE_URL=,DATABASE_URL=mysql://${MYSQL_CREDENTIAL}ibs-mysql:3306/${MYSQL_DATABASE}," ${ENV_FILENAME}
        sed -i "s/^JWT_PASSPHRASE=/JWT_PASSPHRASE=${RANDOM_STRING}/" ${ENV_FILENAME}
        sed -i "s/^LOCALE=/LOCALE=ru_Ru/" ${ENV_FILENAME}
        sed -i "s,^MAILER_TRANSPORT=,MAILER_TRANSPORT=${MAILER_TRANSPORT}," ${ENV_FILENAME}
        sed -i "s,^MAILER_HOST=,MAILER_HOST=${MAILER_HOST}," ${ENV_FILENAME}
        sed -i "s,^MAILER_PORT=,MAILER_PORT=${MAILER_PORT}," ${ENV_FILENAME}
        sed -i "s,^MAILER_USER=,MAILER_USER=${MAILER_USER}," ${ENV_FILENAME}
        sed -i "s,^MAILER_PASSWORD=,MAILER_PASSWORD=${MAILER_PASSWORD}," ${ENV_FILENAME}
        sed -i "s,^MAILER_URL=,MAILER_URL=${MAILER_TRANSPORT}://${MAILER_CREDENTIAL}${MAILER_HOST}:${MAILER_PORT}," ${ENV_FILENAME}
        sed -i "s,^MAILER_DSN=,MAILER_DSN=${MAILER_TRANSPORT}://${MAILER_CREDENTIAL}${MAILER_HOST}:${MAILER_PORT}," ${ENV_FILENAME}
        sed -i "s/^MYSQL_HOST=/MYSQL_HOST=ibs-mysql/" ${ENV_FILENAME}
        sed -i "s/^MYSQL_PORT=/MYSQL_PORT=3306/" ${ENV_FILENAME}
        sed -i "s/^MYSQL_ROOT_PASSWORD=/MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}/" ${ENV_FILENAME}
        sed -i "s/^MYSQL_DATABASE=/MYSQL_DATABASE=${MYSQL_DATABASE}/" ${ENV_FILENAME}
        sed -i "s/^MYSQL_USER=/MYSQL_USER=${MYSQL_USER}/" ${ENV_FILENAME}
        sed -i "s/^MYSQL_PASSWORD=/MYSQL_PASSWORD=${MYSQL_PASSWORD}/" ${ENV_FILENAME}
        sed -i "s/^NGINX_HOST_HTTP_PORT=/NGINX_HOST_HTTP_PORT=${NGINX_HOST_HTTP_PORT}/" ${ENV_FILENAME}
        sed -i "s/^NGINX_HOST_HTTPS_PORT=/NGINX_HOST_HTTPS_PORT=${NGINX_HOST_HTTPS_PORT}/" ${ENV_FILENAME}
        sed -i "s/^WORKSPACE_IP=/WORKSPACE_IP=ibs-mysql/" ${ENV_FILENAME}
    done
    source ./.env.local
    
    # Заменяем переменные на значения в конфигурациях (названия и их значения берем из env)
    addLogMessage 'Заменяем переменные на значения в конфигурациях (названия и их значения берем из env)...'
    addLogMessage "/usr/local/etc/php/conf.d/xdebug.ini OLD ==> $(grep remote_host /usr/local/etc/php/conf.d/xdebug.ini)"
    IFS=$'\n'
    for ENV_VARIABLE in $(env | grep IBS)
    do
        ENV_VARIABLE_NAME=$(echo "${ENV_VARIABLE}" | cut -f1 -d=)
        ENV_VARIABLE_VALUE=$(echo "${ENV_VARIABLE}" | cut -f2 -d=)
        sed -i "s/\${${ENV_VARIABLE_NAME}}/${ENV_VARIABLE_VALUE}/g" /usr/local/etc/php/conf.d/xdebug.ini
    done
    addLogMessage "/usr/local/etc/php/conf.d/xdebug.ini NEW ==> $(grep remote_host /usr/local/etc/php/conf.d/xdebug.ini)"
}

# Установка проекта
function installProject {
    addLogMessage "Начало установки проекта..."
    cp ./phpunit.xml.dist ./phpunit.xml
}

# Генерация ключей
function generateKeys {
    addLogMessage 'Генерация JWT ключей авторизации...'
    openssl genrsa -out ./config/jwt/private.pem -aes256 -passout pass:${RANDOM_STRING} 4096
    openssl rsa -pubout -in ./config/jwt/private.pem -out ./config/jwt/public.pem -passin pass:${RANDOM_STRING}
    chmod 644 ./config/jwt/private.pem

    addLogMessage 'Добавление GRPC сертификата...'
    rm -rf ./config/grpc/root.cert
    echo "${GRPC_CERTIFICATE}" | sed -E "s/\\\n/\n/g" > ./config/grpc/root.cert
    chmod 644 ./config/grpc/root.cert
}

# Composer
function composerDependence {
    addLogMessage 'Подтягиваем composer зависимости...'
    composer install -n
    chmod 777 -R ./vendor/
}

# Doctrine
function doctrineSchema {
    addLogMessage 'Обновление таблиц в БД по схемам сущностей...'
    php ./bin/console doctrine:schema:update --force
}

# Создаем начальных пользователей в системе
function createUsers {
    addLogMessage 'Создаем пользователей...'

    ### super-admin ###
    IS_ISSET=
    isIsset 'SELECT COUNT(`id`) FROM `user` WHERE `username` = "super-admin";'
    echo "super-admin IS_ISSET: ${IS_ISSET}"
    if [[ ${IS_ISSET} != '1' ]]; then
        php bin/console fos:user:create super-admin super-admin@example.com super-admin-password --super-admin
        addLogMessage 'Пользователь admin:admin-password с ролью ROLE_SUPER_ADMIN успешно создан.'
    else
        addLogMessage 'Пользователь admin:admin-password с ролью ROLE_SUPER_ADMIN уже существует.'
    fi

    ### admin ###
    IS_ISSET=
    isIsset 'SELECT COUNT(`id`) FROM `user` WHERE `username` = "admin";'
    echo "admin IS_ISSET: ${IS_ISSET}"
    if [[ ${IS_ISSET} != 1 ]]; then
        php bin/console fos:user:create admin admin@example.com admin-password
        php ./bin/console fos:user:promote admin ROLE_ADMIN
        addLogMessage 'Пользователь admin:admin-password с ролью ROLE_ADMIN успешно создан.'
    else
        addLogMessage 'Пользователь admin:admin-password с ролью ROLE_ADMIN уже существует.'
    fi

    ### test ###
    IS_ISSET=
    isIsset 'SELECT COUNT(`id`) FROM `user` WHERE `username` = "test";'
    echo "test IS_ISSET: ${IS_ISSET}"
    if [[ ${IS_ISSET} != 1 ]]; then
        php bin/console fos:user:create test test@example.com test-password
        addLogMessage 'Пользователь test:test-password успешно создан, активированный, но не имеет ни одной роли.'
    else
        addLogMessage 'Пользователь test:test-password уже существует, активированный, но не имеет ни одной роли.'
    fi

    ### inactive-user ###
    IS_ISSET=
    isIsset 'SELECT COUNT(`id`) FROM `user` WHERE `username` = "inactive-user";'
    echo "inactive-user IS_ISSET: ${IS_ISSET}"
    if [[ ${IS_ISSET} != 1 ]]; then
        php bin/console fos:user:create inactive-user inactive-user@example.com inactive-user-password --inactive
        addLogMessage 'Пользователь inactive-user:inactive-user-password успешно создан, не активирован и не имеет ни одной роли.'
    else
        addLogMessage 'Пользователь inactive-user:inactive-user-password уже существует, не активирован и не имеет ни одной роли.'
    fi
}

# Создаем начальные пользовательские группы в системе
function createUserGroups {
    addLogMessage 'Создаем пользовательские группы с ролями...'
    ### group "GROUP_TEST"
    isIsset 'SELECT COUNT(`id`) FROM `user_group` WHERE `name` = "GROUP_TEST";'
    if [[ ${IS_ISSET} != 1 ]]; then
        php bin/console api:entity:user-group:create GROUP_TEST        \
            --name_rus="Тестовая группа"                                \
            --description="Данная группа нужна для системы тестирования" \
            --no-interaction
        addLogMessage 'Пользовательская группа "test" успешно создана и не имеет ни одной роли.'
    else
        addLogMessage 'Пользовательская группа "test" уже существует.'
    fi
}

function finishInstall {
    touch ./var/projectIsInstalled && chmod 777 ./var/projectIsInstalled
    echo "[$(date)] The project has been successfully installed!" > ./var/projectIsInstalled
}

function runTests {
    chmod 777 ./bin/phpunit.phar
    php ./bin/phpunit.phar --stop-on-error --stop-on-failure --process-isolation --configuration ./phpunit.xml ./tests/
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Если проект не установлен, то устанавливаем его
if [[ ! -f ./var/projectIsInstalled ]]; then
    installProject
fi

generateKeys
createAndFillEnvironment
composerDependence
doctrineSchema
createUsers
createUserGroups
finishInstall

if [[ ! -z "${RUN_TEST_AFTER_INSTALL_PROJECT}" ]]; then
    echo "RUN_TEST_AFTER_INSTALL_PROJECT: ${RUN_TEST_AFTER_INSTALL_PROJECT}"
    addLogMessage 'Запуск тестов...'
    runTests
fi

addLogMessage 'Запускаем php-fpm...'
exec "${@}"

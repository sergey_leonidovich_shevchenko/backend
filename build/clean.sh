#!/bin/bash

#########################################
# Очищает результат установки проекта   #
# Удаляет контейнеры с приложениями ibs #
# (полезен при переустановки проекта)   #
# Запускать в контексте корня проекта!  #
#########################################

set -x

docker stop $(docker container ls -f name=ibs-nginx -f name=ibs-php-fpm -f name=ibs-mysql -f name=ibs-mailhog -f name=ibs-blackfire -aq) 2> /dev/null

docker rm $(docker container ls -f name=ibs-nginx -f name=ibs-php-fpm -f name=ibs-mysql -f name=ibs-mailhog -f name=ibs-blackfire -aq) 2> /dev/null
docker volume rm -f name=ibs-mysql-data
docker network rm -f name=ibs-backend-network

docker container prune -f
docker volume prune -f
docker network prune -f
docker image prune -f

docker image rm -f $(docker images "*ibs-nginx" -q)
docker image rm -f $(docker images "*ibs-php-fpm" -q)
docker image rm -f $(docker images "*ibs-mysql" -q)
docker image rm -f $(docker images "*ibs-mailhog" -q)
docker image rm -f $(docker images "*ibs-blackfire" -q)

docker container ls -f name=ibs-nginx -f name=ibs-php-fpm -f name=ibs-mysql -f name=ibs-mailhog -f name=ibs-blackfire
docker network ls -f name=ibs-nginx -f name=ibs-php-fpm -f name=ibs-mysql -f name=ibs-mailhog -f name=ibs-blackfire
docker volume ls -f name=ibs-nginx -f name=ibs-php-fpm -f name=ibs-mysql -f name=ibs-mailhog -f name=ibs-blackfire
docker images "*ibs-nginx"
docker images "*ibs-php-fpm"
docker images "*ibs-mysql"
docker images "*ibs-mailhog"
docker images "*ibs-blackfire"

rm -fv ./var/projectSetupLog ./var/projectIsInstalled 2> /dev/null

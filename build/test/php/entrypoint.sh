#!/bin/bash

set -eux

touch ./var/projectSetupLog && chmod 777 ./var/projectSetupLog

function addLogMessage {
    echo -e "\n>>>>>>>>>>>> $1 <<<<<<<<<<<<\n"
    echo "[$(date)] $1" >> ./var/projectSetupLog
}

function generateRandomString {
    RANDOM_STRING=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
}

# Если проект не установлен, то устанавливаем его
if [[ ! -f ./var/projectIsInstalled ]]; then
    addLogMessage "Начало установки проекта..."

    addLogMessage 'Генерация ключей авторизации...'
    generateRandomString
    openssl genrsa -out ./config/jwt/private.pem -aes256 -passout pass:${RANDOM_STRING} 4096
    openssl rsa -pubout -in ./config/jwt/private.pem -out ./config/jwt/public.pem -passin pass:${RANDOM_STRING}
    chmod 644 ./config/jwt/private.pem
    cp ./phpunit.xml.dist ./phpunit.xml && chmod 777 ./phpunit.xml


    addLogMessage 'Создание и наполнение переменных окружений в проект...'
    # Application
    cp ./.env ./.env.local
    chmod 777 ./.env.local
    sed -i "s/APP_ENV=/APP_ENV=test/" ./.env.local
    sed -i "s/APP_SECRET=/APP_SECRET=${RANDOM_STRING}/" ./.env.local
    sed -i "s/LOCALE=/LOCALE=ru_Ru/" ./.env.local
    sed -i "s,TIMEZONE=,TIMEZONE=Europe/Moscow," ./.env.local
    sed -i "s/JWT_PASSPHRASE=/JWT_PASSPHRASE=${RANDOM_STRING}/" ./.env.local
    sed -i "s/WORKSPACE_IP=/WORKSPACE_IP=127.0.0.1/" ./.env.local
    sed -i "s/MYSQL_HOST=/MYSQL_HOST=ibs-mysql/" ./.env.local
    sed -i "s/MYSQL_PORT=/MYSQL_PORT=3306/" ./.env.local
    sed -i "s/MYSQL_ROOT_PASSWORD=/MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}/" ./.env.local
    sed -i "s/MYSQL_DATABASE=/MYSQL_DATABASE=${MYSQL_DATABASE}/" ./.env.local
    sed -i "s/MYSQL_USER=/MYSQL_USER=${MYSQL_USER}/" ./.env.local
    sed -i "s/MYSQL_PASSWORD=/MYSQL_PASSWORD=${MYSQL_PASSWORD}/" ./.env.local
    # Tests
    cp ./.env.test ./.env.test.local
    chmod 777 ./.env.test.local
    sed -i "s/APP_ENV=/APP_ENV=test/" ./.env.test.local
    sed -i "s/APP_SECRET=/APP_SECRET=${RANDOM_STRING}/" ./.env.test.local
    sed -i "s/JWT_PASSPHRASE=/JWT_PASSPHRASE=${RANDOM_STRING}/" ./.env.test.local
    sed -i "s/LOCALE=/LOCALE=ru_Ru/" ./.env.test.local
    sed -i "s,TIMEZONE=,TIMEZONE=Europe/Moscow," ./.env.test.local
    sed -i "s/WORKSPACE_IP=/WORKSPACE_IP=127.0.0.1/" ./.env.test.local
    sed -i "s/MYSQL_HOST=/MYSQL_HOST=ibs-mysql/" ./.env.test.local
    sed -i "s/MYSQL_PORT=/MYSQL_PORT=3306/" ./.env.test.local
    sed -i "s/MYSQL_ROOT_PASSWORD=/MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}/" ./.env.test.local
    sed -i "s/MYSQL_DATABASE=/MYSQL_DATABASE=${MYSQL_DATABASE}/" ./.env.test.local
    sed -i "s/MYSQL_USER=/MYSQL_USER=${MYSQL_USER}/" ./.env.test.local
    sed -i "s/MYSQL_PASSWORD=/MYSQL_PASSWORD=${MYSQL_PASSWORD}/" ./.env.test.local
    cat ./.env.test.local
    source .env.test.local


    addLogMessage 'Подтягиваем composer зависимости...'
    composer install -n


    addLogMessage 'Обновление таблиц в БД по схемам сущностей...'
    php ./bin/console doctrine:schema:update --force


    addLogMessage 'Создаем пользователей...'

    ### super-admin ###
    # TODO: Попробовать научить доктринку форматировать вывод не в стиле php var_dump()
    USER_ISSET=$(php bin/console doctrine:query:sql "SELECT COUNT(username) FROM user WHERE username = 'super-admin'" \
        | awk -F "string '" '{print $2}'                                                                              \
        | awk -F "' \(length=" '{print $1}'                                                                           \
        | tr -d '[:space:]'                                                                                           \
    )
    if [[ ${USER_ISSET} == 0 ]]; then
        php bin/console fos:user:create super-admin super-admin@example.com super-admin-password --super-admin
        addLogMessage 'Пользователь admin:admin-password с ролью ROLE_SUPER_ADMIN успешно создан.'
    else
        addLogMessage 'Пользователь admin:admin-password с ролью ROLE_SUPER_ADMIN уже существует.'
    fi

    ### admin ###
    USER_ISSET=$(php bin/console doctrine:query:sql "SELECT COUNT(username) FROM user WHERE username = 'admin'" \
        | awk -F "string '" '{print $2}'                                                                        \
        | awk -F "' \(length=" '{print $1}'                                                                     \
        | tr -d '[:space:]'                                                                                     \
    )
    if [[ ${USER_ISSET} == 0 ]]; then
        php bin/console fos:user:create admin admin@example.com admin-password ROLE_ADMIN
        addLogMessage 'Пользователь admin:admin-password с ролью ROLE_ADMIN успешно создан.'
    else
        addLogMessage 'Пользователь admin:admin-password с ролью ROLE_ADMIN уже существует.'
    fi

    ### test ###
    USER_ISSET=$(php bin/console doctrine:query:sql "SELECT COUNT(username) FROM user WHERE username = 'test'" \
        | awk -F "string '" '{print $2}'                                                                       \
        | awk -F "' \(length=" '{print $1}'                                                                    \
        | tr -d '[:space:]'                                                                                    \
    )
    if [[ ${USER_ISSET} == 0 ]]; then
        php bin/console fos:user:create test test@example.com test-password
        addLogMessage 'Пользователь test:test-password успешно создан, активированный, но не имеет ни одной роли.'
    else
        addLogMessage 'Пользователь test:test-password уже существует, активированный, но не имеет ни одной роли.'
    fi

    ### inactive-user ###
    USER_ISSET=$(php bin/console doctrine:query:sql "SELECT COUNT(username) FROM user WHERE username = 'inactive-user'" \
        | awk -F "string '" '{print $2}'                                                                                \
        | awk -F "' \(length=" '{print $1}'                                                                             \
        | tr -d '[:space:]'                                                                                             \
    )
    if [[ ${USER_ISSET} == 0 ]]; then
        php bin/console fos:user:create inactive-user inactive-user@example.com inactive-user-password --inactive
        addLogMessage 'Пользователь inactive-user:inactive-user-password успешно создан, не активирован и не имеет ни одной роли.'
    else
        addLogMessage 'Пользователь inactive-user:inactive-user-password уже существует, не активирован и не имеет ни одной роли.'
    fi

    touch ./var/projectIsInstalled && chmod 777 ./var/projectIsInstalled
fi

addLogMessage 'Запускаем phpunit тесты...'
exec "${@}"

<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\BaseEntityApi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use RuntimeException;

/**
 * Class BaseRepository
 * @package App\Repository
 */
abstract class BaseRepository extends ServiceEntityRepository
{
    /**
     * @param string $entityClassName
     * @param int    $currentId Id текущей сущности (ее не брать в счет при получении из БД)
     * @param array  $params    Параметры для дополнительной фильтрации
     * @return int Количество похожих
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countSimilar(string $entityClassName, int $currentId, array $params = []): int
    {
        $qb = $this->_em->createQueryBuilder();

        $qb = $qb->select($qb->expr()->count('e.id'))
            ->from($entityClassName, 'e')
            ->where('e.id != :id')
            ->setParameter(':id', $currentId);

        foreach ($params as $paramName => $paramValue) {
            $where = sprintf('e.%s = :value', $paramName);
            $qb->andWhere($where)->setParameter(':value', $paramValue);
        }

        $query = $qb->getQuery();
        return (int)$query->getSingleScalarResult();
    }

    /**
     * @param array $params
     * @return int
     */
    public function count($params = []): int
    {
        return (int)parent::count($params);
    }

    /**
     * Получить последнюю добавленную сущность
     * @return BaseEntityApi|null
     * @throws Exception
     */
    public function getLastAdded(): ?BaseEntityApi
    {
        $className = $this->getClassName();
        if (!class_exists($className)) {
            throw new RuntimeException(sprintf('Class %s not found!', $className));
        }
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('e')
            ->from($className, 'e')
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(1);

        try {
            $entity = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // TODO: Logger NonUniqueResultException
            $entity = null;
        }

        return $entity;
    }

    /**
     * Получить Id последнего добавленного брокера
     * @return int|null
     */
    public function getLastAddedId(): ?int
    {
        $className = $this->getClassName();
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('e.id')
            ->from($className, 'e')
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(1);

        try {
            $entityId = $qb->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
        } catch (NonUniqueResultException $e) {
            // TODO: Logger NonUniqueResultException
            $entityId = null;
        }

        return $entityId;
    }

    /**
     * Получить отсортированный список сущностей
     * @param string $entityName     Название сущности
     * @param int    $page           Текущая страница
     * @param int    $elementsOnPage Элементов на странице
     * @return string[]|null
     */
    public function getList(string $entityName, int $page, int $elementsOnPage): ?array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('e')
            ->from($entityName, 'e')
            ->orderBy('e.id', Criteria::ASC)
            ->setFirstResult(--$page * $elementsOnPage)
            ->setMaxResults($elementsOnPage);

        try {
            $result = $qb->getQuery()->getResult();
        } catch (NonUniqueResultException $e) {
            // TODO: Logger NonUniqueResultException
            $result = null;
        }

        return $result;
    }
}

<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\ConsultingCompany;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class ConsultingCompanyRepository
 * @package App\Repository\Api
 */
class ConsultingCompanyRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsultingCompany::class);
    }
}

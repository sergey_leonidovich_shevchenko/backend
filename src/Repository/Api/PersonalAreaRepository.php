<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\PersonalArea;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class PersonalAreaRepository
 * @package App\Repository\Api
 */
class PersonalAreaRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonalArea::class);
    }
}

<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\SalesDepartment;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class SalesDepartmentRepository
 * @package App\Repository\Api
 */
class SalesDepartmentRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalesDepartment::class);
    }
}

<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\Transaction;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class TransactionRepository
 * @package App\Repository\Api
 */
class TransactionRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }
}

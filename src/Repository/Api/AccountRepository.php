<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\Account;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class AccountRepository
 * @package App\Repository\Api
 */
class AccountRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    /**
     * Получить счет по имени
     * @param string $accountName
     * @return Account|null
     */
    public function getByName(string $accountName): ?Account
    {
        return $this->findOneBy(['name' => $accountName]);
    }
}

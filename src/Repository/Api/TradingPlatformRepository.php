<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\TradingPlatform;
use App\Repository\BaseRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class TradingPlatformRepository
 * @package App\Repository\Api
 */
class TradingPlatformRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TradingPlatform::class);
    }

    /**
     * Получить отсортированный список трейдинговых платформ
     * @param string $columnSort
     * @param string $sortType
     * @return mixed|null
     */
    public function getListWithSort($columnSort = 'id', $sortType = Criteria::ASC)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('tp')
            ->from(TradingPlatform::class, 'tp')
            ->orderBy('tp.' . $columnSort, $sortType);

        try {
            $tradingPlatform = $qb->getQuery()->getResult();
        } catch (NonUniqueResultException $e) {
            // TODO: Logger NonUniqueResultException
            $tradingPlatform = null;
        }

        return $tradingPlatform;
    }
}

<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\UserGroup;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class UserGroupRepository
 * @package App\Repository\Api
 */
class UserGroupRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserGroup::class);
    }
}

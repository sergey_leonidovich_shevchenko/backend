<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\User;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class UserRepository
 * @package App\Repository\Api
 */
class UserRepository extends BaseRepository implements UserLoaderInterface
{
    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Получить пользователя по его ID
     * @param int $id ID искомого пользователя
     * @return User|null
     */
    public function loadById(int $id): ?User
    {
        try {
            return $this->createQueryBuilder('u')
                ->where('u.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // TODO: Logger
            return null;
        }
    }

    /**
     * Получить информацию о пользователю по его email
     * @param string $email Email искомого пользователя
     * @return User|null
     */
    public function loadByEmail(string $email): ?User
    {
        try {
            return $this->createQueryBuilder('u')
                ->where('u.email = :email')
                ->setParameter('email', $email)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // TODO: Logger
            return null;
        }
    }

    /**
     * См. $this->loadByUsername()
     * @param string $username The username
     * @return User|null
     */
    public function loadUserByUsername($username): ?User
    {
        return $this->loadByUsername((string)$username);
    }

    /**
     * Получить информацию о пользователю по его логину
     * @param string $username Логин искомого пользователя
     * @return User|null
     */
    public function loadByUsername(string $username): ?User
    {
        try {
            return $this->createQueryBuilder('u')
                ->where('u.username = :username')
                ->setParameter('username', $username)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // TODO: Logger
            return null;
        }
    }

    /**
     * Получить Id последнего добавленного брокера
     * @return int|null
     */
    public function getLastAddedId(): ?int
    {
        $className = $this->getClassName();
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('e.id')
            ->from($className, 'e')
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(1);

        try {
            $entityId = $qb->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
        } catch (NonUniqueResultException $e) {
            // TODO: Logger NonUniqueResultException
            $entityId = null;
        }

        return $entityId;
    }
}

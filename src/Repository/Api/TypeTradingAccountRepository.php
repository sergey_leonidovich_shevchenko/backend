<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\TypeTradingAccount;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class TypeTradingAccountRepository
 * @package App\Repository\Api
 */
class TypeTradingAccountRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeTradingAccount::class);
    }
}

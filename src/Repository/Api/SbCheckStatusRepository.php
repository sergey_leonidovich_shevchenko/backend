<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\SbCheckStatus;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class SbCheckStatusRepository
 * @package App\Repository\Api
 */
class SbCheckStatusRepository extends BaseRepository
{
    /**
     * SbCheckStatusRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SbCheckStatus::class);
    }
}

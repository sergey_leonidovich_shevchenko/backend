<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\Broker;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class BrokerRepository
 * @package App\Repository\Api
 */
class BrokerRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Broker::class);
    }

    /**
     * Получить брокера по имени
     * @param string $brokerName
     * @return Broker|null
     */
    public function getByName(string $brokerName): ?Broker
    {
        return $this->findOneBy(['name' => $brokerName]);
    }
}

<?php declare(strict_types=1);

namespace App\Repository\Api;

use App\Entity\Api\StatusVip;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class StatusVipRepository
 * @package App\Repository\Api
 */
class StatusVipRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatusVip::class);
    }
}

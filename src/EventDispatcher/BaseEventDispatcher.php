<?php declare(strict_types=1);

namespace App\EventDispatcher;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use App\Entity\Api\User;
use JMS\Serializer\SerializerInterface;

/**
 * Class BaseEventDispatcher
 * @package App\EventDispatcher
 */
abstract class BaseEventDispatcher
{
    /** @var ContainerInterface */
    protected $container;

    /** @var string[] Список с ошибками */
    protected $logData = [];

    /** @var Logger|LoggerInterface */
    protected $logger;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var TokenInterface|null */
    protected $token;

    /**
     * BaseSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->serializer = $this->container->get('jms_serializer');

        if ($this->container->has('security.token_storage')) {
            $tokenStorage = $this->container->get('security.token_storage');
            if ($tokenStorage instanceof TokenStorage) {
                $this->setToken($tokenStorage->getToken());
            }
        }

        !empty($_SERVER['REQUEST_ID']) && $this->logData['request_id'] = $_SERVER['REQUEST_ID'];
    }

    /**
     * @return Logger|LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param string $loggerHandlerName
     * @return $this
     */
    public function changeLogger(string $loggerHandlerName): self
    {
        $this->logger = $this->container->get('monolog.logger.' . $loggerHandlerName);
        return $this;
    }

    /**
     * @return TokenInterface|null
     */
    public function getToken(): ?TokenInterface
    {
        return $this->token;
    }

    /**
     * @param TokenInterface|null $token
     * @return $this
     */
    public function setToken(?TokenInterface $token): self
    {
        $this->token = $token;
        return $this;
    }

    /**
     * Заполнить данные о пользователе
     */
    protected function fillIdentity(): void
    {
        /** @var TokenInterface $token */
        if ($token = $this->getToken()) {
            $userId = null;
            if ($token->getUser() instanceof User) {
                $this->logData['identity'] = [
                    'user_id' => $token->getUser()->getId(),
                    'is_auth' => $token->isAuthenticated(),
                ];
            } else {
                $this->logData['identity'] = [
                    'user_id' => null,
                    'is_auth' => false,
                ];
            }
        }
    }
}

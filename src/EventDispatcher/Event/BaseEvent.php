<?php declare(strict_types=1);

namespace App\EventDispatcher\Event;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Interfaces\EntityInterface;

/**
 * Class BaseEvent
 * @package App\Event
 */
abstract class BaseEvent extends Event
{
    /**
     * Изменяемая сущность
     * @var EntityInterface
     */
    protected $entity;

    /**
     * @return EntityInterface|null
     */
    public function getEntity(): ?EntityInterface
    {
        return $this->entity;
    }
}

<?php declare(strict_types=1);

namespace App\EventDispatcher\Event\Api;

use App\EventDispatcher\Event\BaseEvent;
use App\Entity\Interfaces\EntityInterface;

/**
 * Событие при взаимодействии с сущностью
 * Class EntityEvent
 * @package App\Event\Event\Api
 */
class EntityEvent extends BaseEvent
{
    /** @var string Название события возникающее при создании сущности */
    public const EVENT_API_ENTITY_CREATE = 'event.api.entity.create';

    /** @var string Название события возникающее при получении сущности */
    public const EVENT_API_ENTITY_GET = 'event.api.entity.get';

    /** @var string Название события возникающее при получении списка сущностей */
    public const EVENT_API_ENTITY_LIST = 'event.api.entity.list';

    /** @var string Название события возникающее при удалении сущности */
    public const EVENT_API_ENTITY_DELETE = 'event.api.entity.delete';

    /** @var string Название события возникающее при обновлении сущности */
    public const EVENT_API_ENTITY_UPDATE = 'event.api.entity.update';

    /** @var string Название события возникающее при получении списка сущностей */
    public const EVENT_API_ENTITY_TOTAL = 'event.api.entity.total';

    /** @var string Название события возникающее при ошибки во время манипуляции с сущности */
    public const EVENT_API_ENTITY_EXCEPTION = 'event.api.entity.exception';

    /** @var int[] */
    protected $entityIds = [];

    /** @var EntityInterface[] */
    protected $entityList = [];

    /** @var int|null */
    protected $entityTotal;

    /** @var string[] */
    protected $exceptionList = [];

    /**
     * Clear entity list
     */
    public function clearEntities(): void
    {
        $this->entityIds = [];
        $this->entityList = [];
    }

    /**
     * @return EntityInterface|null
     */
    public function getEntity(): ?EntityInterface
    {
        return !empty($this->entityList)
            ? $this->entityList[0]
            : null;
    }

    /**
     * @return EntityInterface[]|null
     */
    public function getEntityList(): ?array
    {
        return $this->entityList;
    }

    /**
     * @return int[]|null
     */
    public function getEntityIds(): ?array
    {
        return $this->entityIds;
    }

    /**
     * @return int|null
     */
    public function getEntityTotal(): ?int
    {
        return $this->entityTotal;
    }

    /**
     * @param int|null $entityTotal
     * @return self
     */
    public function setEntityTotal(?int $entityTotal): self
    {
        $this->entityTotal = $entityTotal;
        return $this;
    }

    /**
     * @param array $entityList
     * @return $this
     */
    public function addEntities($entityList): self
    {
        foreach ($entityList as $entity) {
            $this->addEntity($entity);
        }
        return $this;
    }

    /**
     * @param EntityInterface $entity
     * @return EntityEvent
     */
    public function addEntity(?EntityInterface $entity): self
    {
        if ($entity instanceof EntityInterface) {
            $this->entityList[] = $entity;
            $this->entityIds[] = $entity->getId();
        }

        return $this;
    }

    /**
     * @param int $entityId
     * @return EntityEvent
     */
    public function addEntityId(int $entityId): self
    {
        $this->entityIds[] = $entityId;
        return $this;
    }

    /**
     * @param int[] $ids
     */
    public function addEntityIds(array $ids): void
    {
        foreach ($ids as $id) {
            $this->addEntityId($id);
        }
    }

    /**
     * @param string $exception
     * @return $this
     */
    public function addException(string $exception): self
    {
        $this->exceptionList[] = $exception;
        return $this;
    }

    /**
     * @param string[]|null $exceptionList
     * @return $this
     */
    public function addExceptions(?array $exceptionList): self
    {
        if ($exceptionList) {
            foreach ($exceptionList as $exception) {
                $this->exceptionList[] = $exception;
            }
        }

        return $this;
    }
}

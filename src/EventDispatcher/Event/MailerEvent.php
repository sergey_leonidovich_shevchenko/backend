<?php declare(strict_types=1);

namespace App\EventDispatcher\Event;

use Symfony\Component\Mailer\Mailer;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

/**
 * Событие при запросе
 * Class MailerEvent
 * @package App\EventDispatcher\Api
 */
class MailerEvent extends BaseEvent
{
    /** @var string Событие возникающее при передачи email на отправку */
    public const EVENT_EMAIL_SEND = 'event.email.send';

    /** @var string Событие возникающее во время ошибки при передачи email на отправку */
    public const EVENT_EMAIL_SEND_EXCEPTION = 'event.email.send.exception';

    /** @var TemplatedEmail|null */
    protected $email;

    /** @var Mailer */
    protected $mailer;

    /**
     * @param TemplatedEmail|null $email
     * @param Mailer|null         $mailer
     */
    public function __construct(?TemplatedEmail $email = null, ?Mailer $mailer = null)
    {
        $this->email = $email;
        $this->mailer = $mailer;
    }

    /**
     * @return TemplatedEmail
     */
    public function getEmail(): TemplatedEmail
    {
        return $this->email;
    }

    /**
     * @param TemplatedEmail $email
     * @return self
     */
    public function setEmail(TemplatedEmail $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Mailer
     */
    public function getMailer(): Mailer
    {
        return $this->mailer;
    }

    /**
     * @param Mailer $mailer
     * @return self
     */
    public function setMailer(Mailer $mailer): self
    {
        $this->mailer = $mailer;
        return $this;
    }
}

<?php declare(strict_types=1);

namespace App\EventDispatcher\Event\MT;

use App\EventDispatcher\Event\BaseEvent;
use App\Entity\Api\Account;

/**
 * Class MTEvent
 * @package App\EventDispatcher\Event\MT
 */
class MTEvent extends BaseEvent
{
    /** @var string Название события возникающее при создании пользователя на mt сервере */
    public const EVENT_MT_USER_ADD = 'event.mt.user.add';

    /** @var string Название события возникающее во время ошибки при создании пользователя на mt сервере */
    public const EVENT_MT_USER_ADD_EXCEPTION = 'event.mt.user.add.exception';

    /** @var Account */
    private $_account;

    /** @var string */
    private $_investorPassword;

    /** @var string */
    private $_masterPassword;

    /** @var string */
    private $_message;

    /** @var array|null */
    private $_user;

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->_message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message): self
    {
        $this->_message = $message;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getUser(): ?array
    {
        return $this->_user;
    }

    /**
     * @param array $user
     * @return MTEvent
     */
    public function setUser(array $user): self
    {
        $this->_user = $user;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->_account;
    }

    /**
     * @param Account $account
     * @return MTEvent
     */
    public function setAccount(Account $account): self
    {
        $this->_account = $account;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvestorPassword(): string
    {
        return $this->_investorPassword;
    }

    /**
     * @param string $investorPassword
     * @return MTEvent
     */
    public function setInvestorPassword(string $investorPassword): self
    {
        $this->_investorPassword = $investorPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getMasterPassword(): string
    {
        return $this->_masterPassword;
    }

    /**
     * @param string $masterPassword
     * @return MTEvent
     */
    public function setMasterPassword(string $masterPassword): self
    {
        $this->_masterPassword = $masterPassword;
        return $this;
    }
}

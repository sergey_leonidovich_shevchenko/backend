<?php declare(strict_types=1);

namespace App\EventDispatcher\Event\Security;

use App\Entity\Interfaces\EntityInterface;
use App\Entity\Interfaces\EntityWithRolesInterface;
use FOS\UserBundle\Model\GroupableInterface;
use App\EventDispatcher\Event\BaseEvent;
use Doctrine\Common\Collections\Collection;
use App\Entity\Api\UserGroup;

/**
 * Событие при взаимодействии с сущностью
 * Class PermissionEvent
 * @package App\EventDispatcher\Event\Security
 */
class PermissionEvent extends BaseEvent
{
    // User -> role
    /** @var string Название события возникающее при добавлении списка ролей пользователю */
    public const EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER = 'event.security.permission.add.roles_to_user';

    /** @var string Название события возникающее о время ошибки при добавлении списка ролей пользователю */
    public const EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_EXCEPTION = 'event.security.permission.add.roles_to_user.exception';

    /** @var string Название события возникающее при добавлении списка ролей пользовательской группе */
    public const EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP = 'event.security.permission.add.roles_to_user_group';

    /** @var string Название события возникающее о время ошибки при добавлении списка ролей пользовательской группе */
    public const EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP_EXCEPTION = 'event.security.permission.add.roles_to_user_group.exception';

    /** @var string Название события возникающее при добавлении списка групп ролей пользователю */
    public const EVENT_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER = 'event.permission.add.user_group_to_user';

    /** @var string Название события возникающее о время ошибки при добавлении списка групп ролей пользователю */
    public const EVENT_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER_EXCEPTION = 'event.permission.add.user_group_to_user.exception';

    // Group -> role

    /** @var string Название события возникающее при удалении списка ролей у пользователя */
    public const EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER = 'event.security.permission.delete.role_from_user';

    /** @var string Название события возникающее о время ошибки при удалении списка ролей у пользователя */
    public const EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_EXCEPTION = 'event.security.permission.delete.role_from_user.exception';

    /** @var string Название события возникающее при удалении списка ролей у пользовательской группы */
    public const EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_GROUP = 'event.security.permission.delete.role_from_user_group';

    /** @var string Название события возникающее о время ошибки при удалении списка ролей у пользовательской группы */
    public const EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_GROUP_EXCEPTION = 'event.security.permission.delete.role_from_user_group.exception';

    /** @var string Название события возникающее при удалении списка групп ролей у пользователя */
    public const EVENT_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER = 'event.permission.delete.user_group_to_user';

    /** @var string Название события возникающее о время ошибки при удалении списка групп ролей у пользователя */
    public const EVENT_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER_EXCEPTION = 'event.permission.delete.user_group_to_user.exception';

    // User -> group

    /** @var string Название события возникающее при установки нового списка ролей пользователю */
    public const EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER = 'event.security.permission.set.roles_to_user';

    /** @var string Название события возникающее о время ошибки при установки нового списка ролей пользователю */
    public const EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_EXCEPTION = 'event.security.permission.set.roles_to_user.exception';

    /** @var string Название события возникающее при установки нового списка ролей пользовательской группе */
    public const EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP = 'event.security.permission.set.roles_to_user_group';

    /** @var string Название события возникающее о время ошибки при установки нового списка ролей пользовательской группе */
    public const EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP_EXCEPTION = 'event.security.permission.set.roles_to_user_group.exception';

    /** @var string Название события возникающее при установке нового списка групп ролей пользователю */
    public const EVENT_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER = 'event.permission.set.user_group_to_user';

    /** @var string Название события возникающее о время ошибки при установке нового списка групп ролей пользователю */
    public const EVENT_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER_EXCEPTION = 'event.permission.set.user_group_to_user.exception';

    /** @var string[] */
    protected $exceptionList = [];

    /**
     * @var string Сообщение для лога
     */
    private $message;

    /**
     * Список с новыми группами
     * @var array
     */
    private $newGroups;

    /**
     * Список с новыми ролями
     * @var string[]
     */
    private $newRoles;

    /**
     * Список со старыми группами
     * @var string[]
     */
    private $oldGroups;

    /**
     * Список со старыми ролями
     * @var string[]
     */
    private $oldRoles;

    /**
     * Добавить ошибку в объект события
     * @param string $exception
     * @return $this
     */
    public function addException(string $exception): self
    {
        $this->exceptionList[] = $exception;
        return $this;
    }

    /**
     * Добавить ошибки в объект события
     * @param string[]|null $exceptionList
     * @return $this
     */
    public function addExceptions(?array $exceptionList): self
    {
        if ($exceptionList) {
            foreach ($exceptionList as $exception) {
                $this->exceptionList[] = $exception;
            }
        }

        return $this;
    }

    /**
     * Получить список старых групп ролей
     * @return string[]|null
     */
    public function getNewGroups(): ?array
    {
        return $this->newGroups;
    }

    /**
     * Добавить список с новыми группами
     * @param string[] $oldGroups
     * @return $this
     */
    public function setNewGroups(Collection $oldGroups): self
    {
        $this->oldGroups = $oldGroups;
        return $this;
    }

    /**
     * Получить список старых ролей
     * @return string[]|null
     */
    public function getNewRoles(): ?array
    {
        return $this->newRoles;
    }

    /**
     * Добавить список с новыми ролями
     * @param array $newRoles
     * @return $this
     */
    public function setNewRoles(array $newRoles): self
    {
        $this->newRoles = $newRoles;
        return $this;
    }

    /**
     * Получить список старых групп ролей
     * @return Collection<UserGroup>|null
     */
    public function getOldGroups(): ?Collection
    {
        return $this->oldGroups;
    }

    /**
     * Добавить список со старыми группами
     * @param string[] $oldGroups
     * @return $this
     */
    public function setOldGroups(Collection $oldGroups): self
    {
        $this->oldGroups = $oldGroups;
        return $this;
    }

    /**
     * Получить список старых ролей
     * @return string[]|null
     */
    public function getOldRoles(): ?array
    {
        return $this->oldRoles;
    }

    /**
     * Добавить список со старыми ролями
     * @param array $oldRoles
     * @return $this
     */
    public function setOldRoles(array $oldRoles): self
    {
        $this->oldRoles = $oldRoles;
        return $this;
    }

    /**
     * Добавить сущность с группами
     * @param GroupableInterface $entity
     * @return PermissionEvent
     */
    public function setEntityWithGroups(GroupableInterface $entity): self
    {
        $this->_setEntity($entity);
        return $this;
    }

    /**
     * Добавить сущность с ролями
     * @param EntityWithRolesInterface $entity
     * @return PermissionEvent
     */
    public function setEntityWithRoles(EntityWithRolesInterface $entity): self
    {
        $this->_setEntity($entity);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Добавить сущность в объект события
     * @param object $entity
     */
    private function _setEntity(object $entity): void
    {
        if ($entity instanceof EntityInterface) {
            $this->entity = $entity;
        }
    }
}

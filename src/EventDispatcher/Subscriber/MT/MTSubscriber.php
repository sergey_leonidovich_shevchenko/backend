<?php declare(strict_types=1);

namespace App\EventDispatcher\Subscriber\MT;

use App\EventDispatcher\Event\MT\MTEvent;
use App\EventDispatcher\Subscriber\BaseSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Service\Mailer\AccountMailer;
use App\Entity\Api\Account;
use App\Helper\StringHelper;

/**
 * Class MTSubscriber
 * @package App\EventDispatcher\Subscriber\Security
 */
class MTSubscriber extends BaseSubscriber implements EventSubscriberInterface
{
    /** @var StringHelper */
    private $stringHelper;

    /**
     * RequestSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->stringHelper = $this->container->get(StringHelper::class);

        if ($this->logData === null) {
            $this->logData = [];
        }

        $this->changeLogger('api_entity');
    }

    /**
     * Метод возвращающий список событий на который должен реагировать текущий подписчик
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            MTEvent::EVENT_MT_USER_ADD => [['onUserAdd']],
            MTEvent::EVENT_MT_USER_ADD_EXCEPTION => [['onUserAddException']],
        ];
    }

    //**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**

    /**
     * Событие возникающее при добавление пользователя на mt сервере
     * @param MTEvent $event
     */
    public function onUserAdd(MTEvent $event)
    {
        $this->changeLogger('mt_entity');
        $this->logData['event_name'] = MTEvent::EVENT_MT_USER_ADD;
        $this->fillGeneralData($event);
        $this->logData['user'] = $event->getUser();
        $this->logData['master_password'] = $this->stringHelper->maskAllString($event->getMasterPassword());
        $this->logData['investor_password'] = $this->stringHelper->maskAllString($event->getInvestorPassword());
        $this->logger->info('Запрос на добавление пользователя на mt сервер', $this->logData);

        /** @var Account $account */
        $account = $event->getAccount();
        /** @var AccountMailer $accountMailer */
        $accountMailer = $this->container->get(AccountMailer::class);
        $accountMailer->saveAccount(
            $account,
            $event->getUser(),
            $event->getMasterPassword(), // FIXME: open password in log?
            $event->getInvestorPassword(), // FIXME: open password in log?
        );
    }

    /**
     * Событие возникающее при ошибке во время добавления списка ролей пользователю
     * @param MTEvent $event
     */
    public function onUserAddException(MTEvent $event)
    {
        $this->changeLogger('mt_entity_exception');
        $this->logData['event_name'] = MTEvent::EVENT_MT_USER_ADD_EXCEPTION;
        $this->_fillMessage($event);
        $this->_fillData($event);
        $this->fillGeneralData($event);
        $this->logger->error('Ошибка при добавлении пользователя на mt сервер', $this->logData);
    }

    //**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**

    /**
     * Добавить информацию о сообщении
     * @param MTEvent $event
     */
    private function _fillMessage(MTEvent $event): void
    {
        $event->getMessage() && $this->logData['message'] = $event->getMessage();
    }

    private function _fillData(MTEvent $event)
    {
        $this->logData['user'] = $event->getUser();
    }
}

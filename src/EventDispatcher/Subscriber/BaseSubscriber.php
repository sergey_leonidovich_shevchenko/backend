<?php declare(strict_types=1);

namespace App\EventDispatcher\Subscriber;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Interfaces\EntityInterface;
use App\EventDispatcher\BaseEventDispatcher;

/**
 * Class BaseSubscriber
 * @package App\EventDispatcher\Subscriber
 */
abstract class BaseSubscriber extends BaseEventDispatcher
{
    /**
     * Сгенерировать общую информацию
     * @param Event $event
     */
    protected function fillGeneralData(Event $event): void
    {
        $this->fillIdentity();
        $this->fillBaseEntityInfo($event->getEntity());
    }

    /**
     * Наполнение $this->logData данными о сущности (только ID и название сущности)
     * @param EntityInterface|null $entity
     */
    protected function fillBaseEntityInfo(?EntityInterface $entity): void
    {
        if ($entity instanceof EntityInterface) {
            if (empty($this->logData['entity_info'])) {
                $this->logData['entity_info'] = [];
            }

            $this->logData['entity_info']['entity_name'] = get_class($entity);
            $this->logData['entity_info']['id'] = $entity->getId();
        }
    }

    /**
     * Наполнение $this->logData данными о сущности (все поля сущности)
     * @param EntityInterface|null $entity
     */
    protected function fillFullEntityInfo(?EntityInterface $entity): void
    {
        if ($entity instanceof EntityInterface) {
            if (empty($this->logData['entity_info'])) {
                $this->fillBaseEntityInfo($entity);
            }
            $this->logData['entity_info']['entity'] = $this->serializer->toArray($entity);
        }
    }
}

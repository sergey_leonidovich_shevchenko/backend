<?php

/** @noinspection PhpIllegalStringOffsetInspection */

declare(strict_types=1);

namespace App\EventDispatcher\Subscriber;

use App\EventDispatcher\Event\MailerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\BodyRenderer;
use Symfony\Component\Mime\Address;

/**
 * Class MailerSubscriber
 * @package App\EventDispatcher\Subscriber
 */
class MailerSubscriber extends BaseSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            MailerEvent::EVENT_EMAIL_SEND => [['onEmailSend']],
            MailerEvent::EVENT_EMAIL_SEND_EXCEPTION => [['onEmailSendException']],
        ];
    }

    /**
     * @param MailerEvent $event
     */
    public function onEmailSend(MailerEvent $event): void
    {
        $this->changeLogger('mailer');
        $this->logData['event_name'] = MailerEvent::EVENT_EMAIL_SEND;
        $this->_fillEmailData($event->getEmail());
        $this->fillIdentity();
        $this->logger->info('Email передан на отправку', $this->logData);
    }

    /**
     * @param MailerEvent $event
     */
    public function onEmailSendException(MailerEvent $event): void
    {
        $this->changeLogger('mailer_exception');
        $this->logData['event_name'] = MailerEvent::EVENT_EMAIL_SEND_EXCEPTION;
        $this->_fillEmailData($event->getEmail());
        $this->fillIdentity();
        $this->logger->error('Произошла ошибка при отправки email', $this->logData);
    }

    /**
     * Наполнить данные для лога информацией о письме
     * @param TemplatedEmail|Email|null $email
     */
    private function _fillEmailData(?Email $email): void
    {
        if ($email === null) {
            return;
        }

        if ($email instanceof TemplatedEmail) {
            /** @var BodyRenderer $renderer */
            $renderer = $this->container->get(BodyRenderer::class);
            $renderer->render($email);
        }

        if (!isset($this->logData['email'])) {
            $this->logData['email'] = [];
        }

        $this->logData['email']['attachments'] = $email->getAttachments();
        $this->logData['email']['from'] = $this->_getAddressList($email->getFrom());
        $this->logData['email']['to'] = $this->_getAddressList($email->getTo());
        $this->logData['email']['subject'] = $email->getSubject();

        if ($email instanceof TemplatedEmail) {
            $this->_fillTemplatedEmailData($email);
        } elseif ($email instanceof Email) {
            $this->_fillTextedEmailData($email);
        }
    }

    /**
     * Наполнить данные для лога информацией о шаблонном письме
     * @param TemplatedEmail $templatedEmail
     */
    private function _fillTemplatedEmailData(TemplatedEmail $templatedEmail): void
    {
        $this->logData['email']['html_template'] = $templatedEmail->getHtmlTemplate();
        $this->logData['email']['text_template'] = $templatedEmail->getTextTemplate();
        $this->logData['email']['html_body'] = $templatedEmail->getHtmlBody();

    }

    /**
     * Наполнить данные для лога информацией о текстовом письме
     * @param Email $email
     */
    private function _fillTextedEmailData(Email $email): void
    {
        $this->logData['email']['text_body'] = $email->getTextBody();
    }

    /**
     * Получить массив адресов из списка объектов Address
     * @param array $addressObjectList
     * @return string[]
     */
    private function _getAddressList(array $addressObjectList): array
    {
        $addressList = [];
        foreach ($addressObjectList as $address) {
            /** @var Address $address */
            $addressList[] = $address->getAddress();
        }

        return $addressList;
    }
}

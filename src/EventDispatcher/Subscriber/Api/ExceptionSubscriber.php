<?php declare(strict_types=1);

namespace App\EventDispatcher\Subscriber\Api;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use App\Exception\Api\ApiException;

/**
 * Class ExceptionSubscriber
 * @package App\Event\Api\Subscriber
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $e = $event->getException();
        if (!$e instanceof ApiException) {
            return;
        }

        $apiProblem = $e->getApiProblem();

        $response = new JsonResponse($apiProblem->toArray(), $apiProblem->getStatusCode());
        $response->headers->set('Content-Type', 'application/problem+json');
    }
}

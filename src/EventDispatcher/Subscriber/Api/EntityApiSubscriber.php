<?php declare(strict_types=1);

namespace App\EventDispatcher\Subscriber\Api;

use App\Entity\Interfaces\EntityInterface;
use App\EventDispatcher\Event\Api\EntityEvent;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntitySubscriber
 * @package App\Event\Api\Subscriber
 */
class EntityApiSubscriber extends BaseApiSubscriber implements EventSubscriberInterface
{
    /**
     * RequestSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->changeLogger('api_entity');
    }

    /**
     * Метод возвращающий список событий на который должен реагировать текущий подписчик
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            EntityEvent::EVENT_API_ENTITY_GET => [['onEntityGet']],
            EntityEvent::EVENT_API_ENTITY_LIST => [['onEntityList']],
            EntityEvent::EVENT_API_ENTITY_CREATE => [['onEntityCreate']],
            EntityEvent::EVENT_API_ENTITY_UPDATE => [['onEntityUpdate']],
            EntityEvent::EVENT_API_ENTITY_DELETE => [['onEntityDelete']],
            EntityEvent::EVENT_API_ENTITY_TOTAL => [['onEntityTotal']],
            EntityEvent::EVENT_API_ENTITY_EXCEPTION => [['onEntityException']],
        ];
    }

    //**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**

    /**
     * Событие возникающее после запроса на получение сущности
     * @param EntityEvent $event
     * @throws Exception
     */
    public function onEntityException(EntityEvent $event): void
    {
        $this->changeLogger('api_entity_exception');
        $message = 'Ошибка при взаимодействии с сущностью';
        $this->logData = ['event_name' => EntityEvent::EVENT_API_ENTITY_EXCEPTION];

        $this->fillGeneralData($event);
        $this->logger->error($message, $this->logData);
    }

    /**
     * Событие возникающее после запроса на получение сущности
     * @param EntityEvent $event
     * @throws Exception
     */
    public function onEntityGet(EntityEvent $event): void
    {
        $this->logData = ['event_name' => EntityEvent::EVENT_API_ENTITY_GET];
        $this->fillBaseData($event, 'Запрошена сущность');
    }

    /**
     * Событие возникающее после запроса на получение коллекции сущностей
     * @param EntityEvent $event
     */
    public function onEntityList(EntityEvent $event): void
    {
        $this->logData = ['event_name' => EntityEvent::EVENT_API_ENTITY_LIST];
        $this->fillBaseData($event, 'Запрос на получение списка сущностей');
    }

    /**
     * Событие возникающее после запроса на добавление сущности
     * @param EntityEvent $event
     */
    public function onEntityCreate(EntityEvent $event): void
    {
        $this->logData = ['event_name' => EntityEvent::EVENT_API_ENTITY_CREATE];
        $this->fillBaseData($event, 'Запрос на создание сущности');
    }

    /**
     * Событие возникающее после запроса на обновление сущности
     * @param EntityEvent $event
     */
    public function onEntityUpdate(EntityEvent $event): void
    {
        $this->logData['event_name'] = EntityEvent::EVENT_API_ENTITY_UPDATE;
        $this->fillBaseData($event, 'Запрос на обновление сущности');
    }

    /**
     * Событие возникающее после запроса на удаление сущности
     * @param EntityEvent $event
     */
    public function onEntityDelete(EntityEvent $event): void
    {
        $this->logData = ['event_name' => EntityEvent::EVENT_API_ENTITY_DELETE];
        // true - Логируем всю сущность для INFO лога,
        // что бы потом можно было востановить в случае чего плохого
        $this->fillBaseData($event, 'Запрос на удаление сущности', true);
    }

    /**
     * Событие возникающее после запроса на удаление сущности
     * @param EntityEvent $event
     */
    public function onEntityTotal(EntityEvent $event): void
    {
        $this->logData = ['event_name' => EntityEvent::EVENT_API_ENTITY_TOTAL];
        $this->fillBaseData($event, 'Запрос на получение списка сущностей', true);
    }

    /**
     * @param EntityEvent $event
     * @param string      $message
     * @param bool        $forDebug
     */
    private function fillBaseData(EntityEvent $event, string $message, $forDebug = false): void
    {
        // Info
        $this->fillIdentity();
        $this->fillEntity($event->getEntity(), $forDebug);
        $this->fillEntityList($event->getEntityList(), $forDebug);
        $this->logger->info($message, $this->logData);

        // Debug
        $this->fillEntity($event->getEntity(), true);
        $this->fillEntityList($event->getEntityList(), true);
        $this->logger->debug($message, $this->logData);
    }

    /**
     * Наполнение $this->logData данными о сущности
     * @param EntityInterface|null $entity
     * @param bool                 $forDebug Распечатать ли всю сущность?
     */
    private function fillEntity(?EntityInterface $entity, bool $forDebug = false): void
    {
        if ($entity instanceof EntityInterface) {
            $this->logData['entity_info'] = $forDebug
                ? [ // For debug
                    'entity_name' => get_class($entity),
                    'id' => $entity->getId(),
                    'entity' => $this->serializer->toArray($entity), // For debug
                ]
                : [ // For info
                    'entity_name' => get_class($entity),
                    'id' => $entity->getId(),
                ];
        }
    }

    /**
     * @param array|null $entityList
     * @param bool       $forDebug
     */
    private function fillEntityList(?array $entityList, bool $forDebug = false): void
    {
        $entityDataList = [];
        foreach ($entityList as $entity) {
            $entityDataList[] = $forDebug
                ? [ // For debug
                    'entity_name' => get_class($entity),
                    'id' => $entity->getId(),
                    'entity' => $this->serializer->toArray($entity), // For debug
                ]
                : [ // For info
                    'entity_name' => get_class($entity),
                    'id' => $entity->getId(),
                ];
        }

        if (count($entityDataList)) {
            unset($this->logData['entity_info']);
        }

        $this->logData['entity_list'] = $entityDataList;
    }
}

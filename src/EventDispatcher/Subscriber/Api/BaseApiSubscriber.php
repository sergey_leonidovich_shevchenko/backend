<?php declare(strict_types=1);

namespace App\EventDispatcher\Subscriber\Api;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\EventDispatcher\Subscriber\BaseSubscriber;

/**
 * Class BaseSubscriber
 * @package App\Event\Api\Subscriber
 */
abstract class BaseApiSubscriber extends BaseSubscriber
{
    /** @var SerializerInterface */
    protected $serializer;

    /**
     * RequestSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        if ($this->container->has('jms_serializer')) {
            $this->serializer = $this->container->get('jms_serializer');
        }
    }
}

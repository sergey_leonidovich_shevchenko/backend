<?php declare(strict_types=1);

namespace App\EventDispatcher\Subscriber\Security;

use App\Entity\Api\UserGroup;
use App\EventDispatcher\Event\Security\PermissionEvent;
use App\EventDispatcher\Subscriber\BaseSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PermissionSubscriber
 * @package App\EventDispatcher\Subscriber\Security
 */
class PermissionSubscriber extends BaseSubscriber implements EventSubscriberInterface
{
    /**
     * RequestSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        if ($this->logData === null) {
            $this->logData = [];
        }
    }

    /**
     * Метод возвращающий список событий на который должен реагировать текущий подписчик
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            // User -> role
            PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER => [['onAddRolesToUser']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_EXCEPTION => [['onAddRolesToUserException']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER => [['onDeleteRoleFromUser']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_EXCEPTION => [['onDeleteRoleFromUserException']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER => [['onSetRolesToUser']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_EXCEPTION => [['onSetRolesToUserException']],

            // Group -> role
            PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP => [['onAddRolesToUserGroup']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP_EXCEPTION => [['onAddRolesToUserGroupException']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_GROUP => [['onDeleteRoleFromUserGroup']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_GROUP_EXCEPTION => [['onDeleteRoleFromUserGroupException']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP => [['onSetRolesToUserGroup']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP_EXCEPTION => [['onSetRolesToUserGroupException']],

            // User -> group
            PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER => [['onAddUserGroupToUser']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER_EXCEPTION => [['onAddUserGroupToUserException']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER => [['onDeleteUserGroupFromUser']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER_EXCEPTION => [['onDeleteUserGroupFromUserException']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER => [['onSetUserGroupToUser']],
            PermissionEvent::EVENT_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER_EXCEPTION => [['onSetUserGroupToUserException']],
        ];
    }

    //**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**

    // User -> role

    /**
     * Событие возникающее при добавление списка ролей пользователю
     * @param PermissionEvent $event
     */
    public function onAddRolesToUser(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER;
        $this->_createLoggerInfoForRoles($event);
        $this->logger->info('Запрос на добавление списка ролей пользователю', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время добавления списка ролей пользователю
     * @param PermissionEvent $event
     */
    public function onAddRolesToUserException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission_exception');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_EXCEPTION;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForRoles($event);
        $this->logger->error('Ошибка при запросе на добавление списка ролей пользователю', $this->logData);
    }

    /**
     * Событие возникающее при удалении списка ролей у пользователя
     * @param PermissionEvent $event
     */
    public function onDeleteRoleFromUser(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER;
        $this->_createLoggerInfoForRoles($event);
        $this->logger->info('Запрос на удаление списка ролей у пользователя', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время удаления списка ролей у пользователя
     * @param PermissionEvent $event
     */
    public function onDeleteRoleFromUserException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission_exception');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_EXCEPTION;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForRoles($event);
        $this->logger->error('Ошибка при запросе на удаление списка ролей у пользователя', $this->logData);
    }

    /**
     * Событие возникающее при установки нового списка ролей пользователю
     * @param PermissionEvent $event
     */
    public function onSetRolesToUser(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER;
        $this->_createLoggerInfoForRoles($event);
        $this->logger->info('Запрос на установку нового списка ролей пользователю', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время установки нового списка ролей пользователю
     * @param PermissionEvent $event
     */
    public function onSetRolesToUserException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForRoles($event);
        $this->logger->error('Ошибка при запросе на установку нового списка ролей пользователю', $this->logData);
    }

    // Group -> role // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

    /**
     * Событие возникающее при добавлении списка ролей пользовательской группе
     * @param PermissionEvent $event
     */
    public function onAddRolesToUserGroup(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP;
        $this->_createLoggerInfoForRoles($event);
        $this->logger->info('Запрос на добавление списка ролей пользовательской группе', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время добавления списка ролей пользовательской группе
     * @param PermissionEvent $event
     */
    public function onAddRolesToUserGroupException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission_exception');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP_EXCEPTION;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForRoles($event);
        $this->logger->error('Ошибка при запросе на добавление списка ролей пользовательской группе', $this->logData);
    }

    /**
     * Событие возникающее при удалении списка ролей у пользовательской группы
     * @param PermissionEvent $event
     */
    public function onDeleteRoleFromUserGroup(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_GROUP;
        $this->_createLoggerInfoForRoles($event);
        $this->logger->info('Запрос на удаление списка ролей у пользовательской группы', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время удаления списка ролей у пользовательской группы
     * @param PermissionEvent $event
     */
    public function onDeleteRoleFromUserGroupException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission_exception');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_GROUP_EXCEPTION;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForRoles($event);
        $this->logger->error('Ошибка при запросе на удаление списка ролей у пользовательской группы', $this->logData);
    }

    /**
     * Событие возникающее при установки нового списка ролей пользовательской группе
     * @param PermissionEvent $event
     */
    public function onSetRolesToUserGroup(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP;
        $this->_createLoggerInfoForRoles($event);
        $this->logger->info('Запрос на установку нового списка ролей пользовательской группе', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время установки нового списка ролей пользовательской группе
     * @param PermissionEvent $event
     */
    public function onSetRolesToUserGroupException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission_exception');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP_EXCEPTION;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForRoles($event);
        $this->logger->error('Ошибка при запросе на установку нового списка ролей пользовательской группе', $this->logData);
    }

    // User -> group // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

    /**
     * Событие возникающее при добавлении списка групп ролей пользователю
     * @param PermissionEvent $event
     */
    public function onAddUserGroupToUser(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER;
        $this->_createLoggerInfoForGroups($event);
        $this->logger->info('Запрос на добавление списка групп ролей пользователю', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время добавления списка групп ролей пользователю
     * @param PermissionEvent $event
     */
    public function onAddUserGroupToUserException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission_exception');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER_EXCEPTION;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForGroups($event);
        $this->logger->error('Ошибка при запросе на добавление списка групп ролей пользователю', $this->logData);
    }

    /**
     * Событие возникающее при удалении списка групп ролей у пользователя
     * @param PermissionEvent $event
     */
    public function onDeleteUserGroupFromUser(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER;
        $this->_createLoggerInfoForGroups($event);
        $this->logger->info('Запрос на удаление списка групп ролей у пользователя', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время удалении списка групп ролей у пользователя
     * @param PermissionEvent $event
     */
    public function onDeleteUserGroupFromUserException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission_exception');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER_EXCEPTION;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForGroups($event);
        $this->logger->error('Ошибка при запросе на удаление списка групп ролей у пользователя', $this->logData);
    }

    /**
     * Событие возникающее при установке нового списка групп ролей пользователю
     * @param PermissionEvent $event
     */
    public function onSetUserGroupToUser(PermissionEvent $event)
    {
        $this->changeLogger('security_permission');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER;
        $this->_createLoggerInfoForGroups($event);
        $this->logger->info('Запрос на установку нового списка групп ролей пользователю', $this->logData);
    }

    /**
     * Событие возникающее при ошибке во время установки нового списка групп ролей пользователю
     * @param PermissionEvent $event
     */
    public function onSetUserGroupToUserException(PermissionEvent $event)
    {
        $this->changeLogger('security_permission_exception');
        $this->logData['event_name'] = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER_EXCEPTION;
        $this->_fillMessage($event);
        $this->_createLoggerInfoForGroups($event);
        $this->logger->error('Ошибка при запросе на установку нового списка групп ролей пользователю', $this->logData);
    }

    //**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**

    /**
     * Формирование лога с ролями
     * @param PermissionEvent $event
     */
    private function _createLoggerInfoForRoles(PermissionEvent $event): void
    {
        $this->_addRolesInfo($event);
        $this->fillGeneralData($event);
    }

    /**
     * Формирование лога с группами
     * @param PermissionEvent $event
     */
    private function _createLoggerInfoForGroups(PermissionEvent $event): void
    {
        $this->_addGroupsInfo($event);
        $this->fillGeneralData($event);
    }

    /**
     * Формирование лога с группами и ролями
     * @param PermissionEvent $event
     */
    private function _createLoggerInfoForRolesAndGroups(PermissionEvent $event): void
    {
        $this->_addRolesInfo($event);
        $this->_addGroupsInfo($event);
        $this->fillGeneralData($event);
    }

    /**
     * Добавить информацию о ролях
     * @param PermissionEvent $event
     */
    private function _addRolesInfo(PermissionEvent $event): void
    {
        !empty($event->getOldRoles()) && $this->logData['old_roles'] = $event->getOldRoles();
        !empty($event->getNewRoles()) && $this->logData['new_roles'] = $event->getNewRoles();
    }

    /**
     * Добавить информацию о пользовательских группах
     * @param PermissionEvent $event
     */
    private function _addGroupsInfo(PermissionEvent $event): void
    {
        foreach (['old', 'new'] as $whatGroup) {
            $eventGroupList = $event->{'get' . ucfirst($whatGroup) . 'Groups'}();
            if (!empty($eventGroupList)) {
                if ($eventGroupList[0] instanceof UserGroup) {
                    $groups = [];
                    /** @var UserGroup $group */
                    foreach ($eventGroupList as $group) {
                        $groups[] = $group->__toString();
                    }
                    $this->logData[$whatGroup . 'groups'] = $groups;
                } else {
                    $this->logData[$whatGroup . 'groups'] = $eventGroupList;
                }
            }
        }
    }

    /**
     * Добавить информацию о сообщении
     * @param PermissionEvent $event
     */
    private function _fillMessage(PermissionEvent $event): void
    {
        $event->getMessage() && $this->logData['message'] = $event->getMessage();
    }
}

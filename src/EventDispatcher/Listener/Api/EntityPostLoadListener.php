<?php declare(strict_types=1);

namespace App\EventDispatcher\Listener\Api;

use Doctrine\ORM\Event\LifecycleEventArgs;
use ReflectionClass;
use App\Entity\Interfaces\EntityInterface;
use ReflectionException;
use ReflectionMethod;
use App\EventDispatcher\Listener\BaseListener;
use ReflectionType;

/**
 * Class Entity
 * @package App\EventDispatcher\Listener\Api
 */
class EntityPostLoadListener extends BaseListener
{
    /**
     * Событие возникающее при получение сущности из бд, после ее гидрации
     * @param LifecycleEventArgs $args
     * @throws ReflectionException
     */
    public function postLoad(LifecycleEventArgs $args): void
    {
        /** @var EntityInterface $entity */
        $entity = $args->getEntity();
        if (!$entity instanceof EntityInterface) {
            // TODO: logging
            return;
        }

        //
        try {
            $reflect = new ReflectionClass($entity);
        } catch (ReflectionException $e) {
            // TODO: logging
            return;
        }

        $setterServiceList = self::_getServiceSettersFromEntity($reflect);
        if (!count($setterServiceList)) {
            return;
        }

        $this->_injectServiceToEntity($setterServiceList, $reflect, $entity);
    }

    /**
     * Получение всех сервис-сеттеров
     * @param ReflectionClass $reflect
     * @return array
     */
    private static function _getServiceSettersFromEntity(ReflectionClass $reflect): array
    {
        $methods = $reflect->getMethods(ReflectionMethod::IS_PUBLIC);
        $setters = array_column($methods, 'name');
        return array_filter($setters, static function ($method) {
            return (bool)preg_match('/^set.*Service$/', $method);
        });
    }

    /**
     * Инъекция сервисов в сущности
     * @param array           $setterServiceList
     * @param ReflectionClass $reflect
     * @param EntityInterface $entity
     * @throws ReflectionException
     */
    private function _injectServiceToEntity(array $setterServiceList, ReflectionClass $reflect, EntityInterface $entity): void
    {
        foreach ($setterServiceList as $setterService) {
            // Если сервис уже был инжектирован, то пропускаем его
            $getterService = preg_replace('/^set(.*)$/', 'get$1', $setterService, 1);
            if ($reflect->hasMethod($getterService) && $entity->{$getterService}() !== null) {
                continue;
            }

            $serviceSetMethod = $reflect->getMethod($setterService);
            $params = $serviceSetMethod->getParameters();
            if (count($params)) {
                $param = $params[0];
                /** @var ReflectionType $type */
                $type = $param->getType();
                $serviceClassName = $type->getName();
                $service = $this->container->get($serviceClassName);
                $entity->{$setterService}($service);
            }
        }
    }
}

<?php declare(strict_types=1);

namespace App\EventDispatcher\Listener;

use App\EventDispatcher\BaseEventDispatcher;

/**
 * Class BaseListener
 * @package App\EventDispatcher\Listener
 */
abstract class BaseListener extends BaseEventDispatcher
{
}

<?php declare(strict_types=1);

namespace App\EventDispatcher\Listener\Kernel;

use JMS\Serializer\Serializer;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use App\EventDispatcher\Listener\BaseListener;

/**
 * Class ExceptionListener
 * @package App\EventDispatcher\Listener\Kernel
 */
class ExceptionListener extends BaseListener
{
    /** @var LoggerInterface */
    protected $logger;

    /** @var Serializer */
    protected $serializer;

    /**
     * KernelExceptionListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->logger = $container->get('monolog.logger.kernel_exception');
        $this->serializer = $container->get('jms_serializer');
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        $message = $exception->getMessage();

        // Customize your response object to display the exception details
        $response = new JsonResponse();
        $response->setContent(json_encode([
            'status' => 'error',
            'message' => $message
        ], JSON_THROW_ON_ERROR, 2));

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        // sends the modified response object to the event
        $event->setResponse($response);

        $logData = [
            'message' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'file' => $exception->getFile(),
            'trace' => $exception->getTrace(),
        ];
        !empty($_SERVER['REQUEST_ID']) && $logData['request_id'] = $_SERVER['REQUEST_ID'];
        $this->logger->error($message, $logData);
    }
}

<?php declare(strict_types=1);

namespace App\EventDispatcher\Listener\Kernel;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use App\EventDispatcher\Listener\BaseListener;

/**
 * Class RequestListener
 * @package App\EventDispatcher\Listener\Kernel
 */
class RequestListener extends BaseListener
{
    /**
     * Событие при получении запроса от клиента
     * @param FinishRequestEvent $event
     */
    public function onKernelRequest(FinishRequestEvent $event): void
    {
        $this->changeLogger('kernel_request');
        $this->logData['event_name'] = KernelEvents::REQUEST;

        if (!isset($this->logData['request'])) {
            $this->logData['request'] = [];
        }
        $this->logData['request']['method'] = $event->getRequest()->getMethod();
        $this->logData['request']['format'] = $event->getRequest()->getRequestFormat();
        $this->logData['request']['locale'] = $event->getRequest()->getLocale();
        $this->logData['request']['default_locale'] = $event->getRequest()->getDefaultLocale();
        $this->logData['request']['route'] = $event->getRequest()->attributes->get('_route');
        $this->logData['request']['route_params'] = $event->getRequest()->attributes->get('_route_params');
        $this->logData['request']['firewall_context'] = $event->getRequest()->attributes->get('_firewall_context');
        $this->logData['request']['controller'] = $event->getRequest()->attributes->get('_controller');
        $this->logData['request']['content'] = $event->getRequest()->getContent();
        $this->logData['request']['media_type'] = $event->getRequest()->attributes->get('media_type');
        $this->logData['request']['uri'] = $event->getRequest()->getUri();

        $this->fillIdentity();
        $this->logger->info('Входящий запрос', $this->logData);
    }
}

<?php declare(strict_types=1);

namespace App\EventDispatcher\Listener\Kernel;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use App\EventDispatcher\Listener\BaseListener;

/**
 * Class ResponseListener
 * @package App\EventDispatcher\ListenerKernel
 */
class ResponseListener extends BaseListener
{
    /**
     * Событие отправки ответа пользователю
     * @param ResponseEvent $event
     */
    public function onKernelResponse(ResponseEvent $event): void
    {
        $this->changeLogger('kernel_response');
        $this->logData['event_name'] = KernelEvents::RESPONSE;

        if (!isset($this->logData['response'])) {
            $this->logData['response'] = [];
        }

        $this->logData['response']['content'] = $event->getResponse()->getContent();
        $this->logData['response']['status_code'] = $event->getResponse()->getStatusCode();
        $this->logData['response']['headers'] = $event->getResponse()->headers->all();
        $this->logData['response']['charset'] = $event->getResponse()->getCharset();

        $this->fillIdentity();
        $this->logger->info('Исходящий ответ от сервера', $this->logData);
    }
}

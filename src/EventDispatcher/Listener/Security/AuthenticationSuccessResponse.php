<?php declare(strict_types=1);

namespace App\EventDispatcher\Listener\Security;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use App\EventDispatcher\Listener\BaseListener;

/**
 * Class AuthenticationSuccessResponse
 * @package App\EventDespatcher\Listener\Security
 */
class AuthenticationSuccessResponse extends BaseListener
{
    /**
     * Модернизация ответа после успешного запроса токена авторизации
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event): void
    {
        $event->setData([
            'user' => [
                'id' => $event->getUser()->getId(),
                'username' => $event->getUser()->getUsername(),
                'email' => $event->getUser()->getEmail(),
                'roles' => $event->getUser()->getRoles(),
            ],
            'token' => $event->getData()['token'],
        ]);
    }
}

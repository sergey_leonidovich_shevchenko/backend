<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\SbCheckStatus;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SbCheckStatusForm
 * @package App\Form\Api
 */
class SbCheckStatusForm extends BaseForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class, [
            'invalid_message' => 'Название статуса проверки СБ должно быть строкового типа.',
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => SbCheckStatus::class]);
    }

    /**
     * @param SbCheckStatus    $sbCheckStatus
     * @param AbstractPlatform $platform
     * @return SbCheckStatus|int
     */
    public function convertToDatabaseValue($sbCheckStatus, AbstractPlatform $platform)
    {
        return $sbCheckStatus instanceof SbCheckStatus
            ? $sbCheckStatus->getId()
            : $sbCheckStatus;
    }
}

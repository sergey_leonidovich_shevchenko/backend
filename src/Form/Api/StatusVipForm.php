<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\StatusVip;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\DataTransformer\NumberToLocalizedStringTransformer;

/**
 * Class StatusVipForm
 * @package App\Form\Api
 */
class StatusVipForm extends BaseForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'invalid_message' => 'Название VIP-статуса должна быть строкового типа.',
            ])
            ->add('sum_for_check_status', NumberType::class, [
                'invalid_message' => 'Сумма {{ value }} должна быть целым числом.',
                'scale' => 2,
                'rounding_mode' => NumberToLocalizedStringTransformer::ROUND_HALF_DOWN
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => StatusVip::class]);
    }

    /**
     * @param StatusVip        $statusVip
     * @param AbstractPlatform $platform
     * @return StatusVip|int
     */
    public function convertToDatabaseValue($statusVip, AbstractPlatform $platform)
    {
        return $statusVip instanceof StatusVip
            ? $statusVip->getId()
            : $statusVip;
    }
}

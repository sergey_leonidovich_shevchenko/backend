<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\Broker;
use App\Entity\Api\PersonalArea;
use App\Entity\Api\SbCheckStatus;
use App\Entity\Api\StatusVip;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PersonalAreaForm
 * @package App\Form\Api
 */
class PersonalAreaForm extends BaseForm
{
    /**
     * @param FormBuilder|FormBuilderInterface $builder
     * @param array                            $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('surname', TextType::class, [
                'invalid_message' => 'Фамилия должна быть строкового типа.',
            ])
            ->add('name', TextType::class, [
                'invalid_message' => 'Имя должно быть строкового типа.',
            ])
            ->add('middle_name', TextType::class, [
                'invalid_message' => 'Отчество должно быть строкового типа.',
            ])
            ->add('email', EmailType::class, [
                'invalid_message' => 'Email должен быть строкового типа.',
            ])
            ->add('phone', TextType::class, [
                'invalid_message' => 'Телефон должен быть строкового типа в фомате "+79001112233".',
            ])
            ->add('birthday', DateType::class, [
                'invalid_message' => 'Дата дня рождения должна быть строкового типа в формате "Y-m-d".',
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'yyyy-MM-dd',
                'html5' => true,
                'input_format' => 'Y-m-d',
            ])
            ->add('usdt_tether', TextType::class, [
                'invalid_message' => 'Tether-токен должен быть строкового типа.',
            ])
            ->add('active', TextType::class, [
                'invalid_message' => 'Флаг активности должен быть строкового типа.',
            ])
            ->add('broker', EntityType::class, [
                'class' => Broker::class,
                'choice_label' => 'name',
                'invalid_message' => 'Брокер не найден.',
            ])
            ->add('sb_check_status', EntityType::class, [
                'class' => SbCheckStatus::class,
                'choice_label' => 'name',
                'invalid_message' => 'Статус проверки СБ не найден.',
            ])
            ->add('status_vip', EntityType::class, [
                'class' => StatusVip::class,
                'choice_label' => 'name',
                'invalid_message' => 'Указанный VIP-статус не найден.',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => PersonalArea::class]);
    }

    /**
     * @param PersonalArea     $personalArea
     * @param AbstractPlatform $platform
     * @return PersonalArea|int
     */
    public function convertToDatabaseValue($personalArea, AbstractPlatform $platform)
    {
        return $personalArea instanceof PersonalArea
            ? $personalArea->getId()
            : $personalArea;
    }
}

<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\TypeTradingAccount;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FormTradingAccountForm
 * @package App\Form\Api
 */
class FormTradingAccountForm extends BaseForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', TextType::class, [
                'invalid_message' => 'Код типа торгового счета должен быть строкового типа.',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => TypeTradingAccount::class]);
    }

    /**
     * @param TypeTradingAccount $typeTradingAccount
     * @param AbstractPlatform   $platform
     * @return TypeTradingAccount|int
     */
    public function convertToDatabaseValue($typeTradingAccount, AbstractPlatform $platform)
    {
        return $typeTradingAccount instanceof TypeTradingAccount
            ? $typeTradingAccount->getId()
            : $typeTradingAccount;
    }
}

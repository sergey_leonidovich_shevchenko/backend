<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\ConsultingCompany;
use App\Entity\Api\SalesDepartment;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SalesDepartmentForm
 * @package App\Form\Api
 */
class SalesDepartmentForm extends BaseForm
{
    /**
     * @param FormBuilder|FormBuilderInterface $builder
     * @param array                            $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', TextType::class, [
                'invalid_message' => 'Код департамента продаж должен быть строкового типа.',
            ])
            ->add('consulting_company', EntityType::class, [
                'class' => ConsultingCompany::class,
                'choice_label' => 'name',
                'invalid_message' => 'Консалтинговая компания не найдена.',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => SalesDepartment::class]);
    }

    /**
     * @param SalesDepartment  $salesDepartment
     * @param AbstractPlatform $platform
     * @return SalesDepartment|int
     */
    public function convertToDatabaseValue($salesDepartment, AbstractPlatform $platform)
    {
        return $salesDepartment instanceof SalesDepartment
            ? $salesDepartment->getId()
            : $salesDepartment;
    }
}

<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\Account;
use App\Entity\Api\PersonalArea;
use App\Entity\Api\SalesDepartment;
use App\Entity\Api\TradingPlatform;
use App\Entity\Api\TypeTradingAccount;
use App\Entity\Api\User;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AccountForm
 * @package App\Form\Api
 */
class AccountForm extends BaseForm
{
    /**
     * @param FormBuilder|FormBuilderInterface $builder
     * @param array                            $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'username',
                'invalid_message' => 'Пользователь не найден.',
            ])
            ->add('trading_platform', EntityType::class, [
                'class' => TradingPlatform::class,
                'choice_label' => 'code',
                'invalid_message' => 'Торговая платформа не найдена.',
            ])
            ->add('personal_area', EntityType::class, [
                'class' => PersonalArea::class,
                'choice_label' => 'email',
                'invalid_message' => 'Личный кабинет не найден.',
            ])
            ->add('type_trading_account', EntityType::class, [
                'class' => TypeTradingAccount::class,
                'choice_label' => 'code',
                'invalid_message' => 'Тип торгового счета не найден.',
            ])
            ->add('sales_department', EntityType::class, [
                'class' => SalesDepartment::class,
                'choice_label' => 'code',
                'invalid_message' => 'Департамент продаж не найден.',
            ])
            ->add('source_code', TextType::class, [
                'invalid_message' =>
                    'Не корректный флаг указывающий откуда был создан счет. Допустимые значения: crm, cabinet',
            ])
            ->add('number', IntegerType::class, [
                'invalid_message' => 'Номер счета должен быть целым числом больше нуля.',
            ])
            ->add('leverage', IntegerType::class, [
                'invalid_message' => 'Плечо должно быть целым числом больше нуля.',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => Account::class]);
    }

    /**
     * @param Account          $account
     * @param AbstractPlatform $platform
     * @return Account|int
     */
    public function convertToDatabaseValue($account, AbstractPlatform $platform)
    {
        return $account instanceof Account
            ? $account->getId()
            : $account;
    }
}

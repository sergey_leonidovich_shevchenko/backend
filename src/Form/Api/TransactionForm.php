<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\Account;
use App\Entity\Api\Transaction;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TransactionForm
 * @package App\Form\Api
 */
class TransactionForm extends BaseForm
{
    /**
     * @param FormBuilder|FormBuilderInterface $builder
     * @param array                            $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('account', EntityType::class, [
                'class' => Account::class,
                'invalid_message' => 'Счет не найден.',
            ])
            ->add('outer_system_id', TextType::class, [
                'invalid_message' => 'Не корректный ID во внешней системе.',
            ])
            ->add('reference_id', IntegerType::class, [
                'invalid_message' => 'Не корректный числовой показатель ID.',
            ])
            ->add('livemode', TextType::class, [
                'invalid_message' => 'Не корректный livemode.',
            ])
            ->add('status', TextType::class, [
                'invalid_message' => 'Не корректный статус.',
            ])
            ->add('amount', MoneyType::class, [
                'invalid_message' => 'Не корректная сумма платежа.',
            ])
            ->add('amount_text', TextType::class, [
                'invalid_message' => 'Не корректная текстовая сумма платежа.',
            ])
            ->add('currency_id', TextType::class, [
                'invalid_message' => 'Не корректный ID валюты.',
            ])
            ->add('description', TextType::class, [
                'invalid_message' => 'Не корректное опциональное описание.',
            ])
            ->add('ip', IntegerType::class, [
                'invalid_message' => 'Не корректный IP.',
            ])
            ->add('email', TextType::class, [
                'invalid_message' => 'Не корректный email.',
            ])
            ->add('source', TextType::class, [
                'invalid_message' => 'Не корректный спам от платежек.',
            ])
            ->add('failure_code', TextType::class, [
                'invalid_message' => 'Не корректный код ошибки.',
            ])
            ->add('failure_message', TextType::class, [
                'invalid_message' => 'Не корректный текст ошибки.',
            ])
            ->add('valid_till', TextType::class, [
                'invalid_message' => 'Не корректная конечная дата ожидания оплаты.',
            ])
            ->add('pay_method', TextType::class, [
                'invalid_message' => 'Не корректный метод оплаты.',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => Transaction::class]);
    }

    /**
     * @param Transaction      $transaction
     * @param AbstractPlatform $platform
     * @return Transaction|int
     */
    public function convertToDatabaseValue($transaction, AbstractPlatform $platform)
    {
        return $transaction instanceof Transaction
            ? $transaction->getId()
            : $transaction;
    }
}

<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\UserGroup;
use App\Form\BaseForm;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserGroupForm
 * @package App\Form\Api
 */
class UserGroupForm extends BaseForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название группы',
                'attr' => [
                    'maxlength' => 180, // TODO: Use from EntityManager field length
                ],
                'invalid_message' => 'Название пользовательской группы должно быть строкового типа.',
            ])
            ->add('name_rus', TextType::class, [
                'required' => false,
                'label' => 'Русифицированное название группы',
                'attr' => [
                    'maxlength' => 32, // TODO: Use from EntityManager field length
                ],
                'invalid_message' => 'Русифицированное название пользовательской группы должно быть строкового типа.',
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'Описание группы',
                'invalid_message' => 'Описание пользовательской группы должно быть строкового типа.',
            ])
            ->add('roles', CollectionType::class, [
                'entry_type' => TextType::class,
                'entry_options' => [
                    'label' => false,
                    'invalid_message' => 'Роль пользовательской группы должна быть строкового типа.',
                ],

                // Если установлено значение true, то если нераспознанные элементы будут отправлены в коллекцию,
                // они будут добавлены как новые элементы.
                // Конечный массив будет содержать существующие элементы, а также новый элемент,
                // который был в представленных данных. Смотрите приведенный выше пример для более подробной информации.
                'allow_add' => true,

                // Если установлено значение true, то, если существующий элемент не содержится в представленных данных,
                // он будет правильно отсутствовать в конечном массиве элементов.
                // Это означает, что вы можете реализовать кнопку «удалить» через JavaScript,
                // которая удаляет элемент формы из DOM.
                // Когда пользователь отправляет форму, ее отсутствие в отправленных данных будет означать,
                // что она удалена из окончательного массива.
                'allow_delete' => true,

                // Если вы хотите явно удалить полностью пустые записи коллекции из вашей формы,
                // вы должны установить эту опцию на true.
                // Однако существующие записи коллекции будут удалены, только если у вас включена опция allow_delete.
                // В противном случае пустые значения будут сохранены
                'delete_empty' => true,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class' => UserGroup::class,
        ]);
    }
}

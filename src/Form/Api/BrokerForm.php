<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\Broker;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BrokerForm
 * @package App\Form\Api
 */
class BrokerForm extends BaseForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'invalid_message' => 'Название брокера должно быть строкового типа.',
            ])
            ->add('setting', CollectionType::class, [
                'allow_add' => true,
                'invalid_message' => 'Настройки брокера должны быть объектного типа.',
            ]);

        /* FIXME: Сейчас не проверяем правильные ли приходят идентификаторы сущностей.
                  ПЕРЕДЕЛАТЬ что бы проверял и указывал какой именно не существует */
//            ->add('consulting_company_list', EntityType::class, [
//                'class' => ConsultingCompany::class,
//                'expanded' => true,
//                'multiple' => true,
//                'choice_label' => 'code',
//                'invalid_message' => 'Одина или несколько консалтинговых компаний не найден(ы).',
//            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => Broker::class]);
    }

    /**
     * @param Broker           $broker
     * @param AbstractPlatform $platform
     * @return Broker|int
     */
    public function convertToDatabaseValue($broker, AbstractPlatform $platform)
    {
        return $broker instanceof Broker
            ? $broker->getId()
            : $broker;
    }
}

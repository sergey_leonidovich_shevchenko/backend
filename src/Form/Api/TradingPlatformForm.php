<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\TradingPlatform;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FormType;

/**
 * Class TradingPlatformForm
 * @package App\Form\Api
 */
class TradingPlatformForm extends BaseForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                $builder->create('balance_server', FormType::class, ['by_reference' => true])
                    ->add('server', TextType::class, [
                        'invalid_message' => 'Название сервера баланса должно быть строкового типа.',
                    ])
                    ->add('port', IntegerType::class, [
                        'invalid_message' => 'Порт баланса должен быть числового типа.',
                    ])
                    ->add('DB', TextType::class, [
                        'invalid_message' => 'Название БД баланса должно быть строкового типа.',
                    ])
                    ->add('user', TextType::class, [
                        'invalid_message' => 'Название пользователя баланса должно быть строкового типа.',
                    ])
                    ->add('password', TextType::class, [
                        'invalid_message' => 'Пароль баланса должен быть строкового типа.',
                    ])
            )
            ->add('code', TextType::class, [
                'invalid_message' => 'Код торговой платформы должен быть строкового типа.',
            ])
            ->add('crypt', CheckboxType::class, [
                'invalid_message' => 'crypt', // TODO change value on actual
            ])
            ->add('group', TextType::class, [
                'invalid_message' => 'Группа торговой платформы должна быть строкового типа.',
            ])
            ->add('server', TextType::class, [
                'invalid_message' => 'Название сервера торговой платформы должно быть строкового типа.',
            ])
            ->add('port', IntegerType::class, [
                'invalid_message' => 'Порт должен быть числового типа.',
            ])
            ->add('timeout', IntegerType::class, [
                'invalid_message' => 'Время таймаута должно быть числового типа.',
            ])
            ->add('login', IntegerType::class, [
                'invalid_message' => 'Логин должен быть числового типа.',
            ])
            ->add('password', TextType::class, [
                'invalid_message' => 'Пароль должен быть строкового типа.',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => TradingPlatform::class]);
    }

    /**
     * @param TradingPlatform  $tradingPlatform
     * @param AbstractPlatform $platform
     * @return TradingPlatform|int
     */
    public function convertToDatabaseValue($tradingPlatform, AbstractPlatform $platform)
    {
        return $tradingPlatform instanceof TradingPlatform
            ? $tradingPlatform->getId()
            : $tradingPlatform;
    }
}

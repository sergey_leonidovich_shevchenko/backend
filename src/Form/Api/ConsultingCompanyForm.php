<?php declare(strict_types=1);

namespace App\Form\Api;

use App\Entity\Api\ConsultingCompany;
use App\Form\BaseForm;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ConsultingCompanyForm
 * @package App\Form\Api
 */
class ConsultingCompanyForm extends BaseForm
{
    /**
     * @param FormBuilder|FormBuilderInterface $builder
     * @param array                            $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', TextType::class, [
                'invalid_message' => 'Код консалтинговой компании должен быть строкового типа.',
            ])
            ->add('url', UrlType::class, [
                'invalid_message' => 'Url консалтинговой компании должен быть строкового типа.',
            ]);

        /* FIXME: Сейчас не проверяем правильные ли приходят идентификаторы сущностей.
                  ПЕРЕДЕЛАТЬ что бы проверял и указывал какой именно не существует */
//            ->add('broker_list', CollectionType::class, [
//                'entry_type' => BrokerForm::class,
//                'allow_add' => true,
//                'invalid_message' => 'Один или несколько брокеров не найден(ы).',
//            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(['data_class' => ConsultingCompany::class]);
    }

    /**
     * @param ConsultingCompany $consultingCompany
     * @param AbstractPlatform  $platform
     * @return ConsultingCompany|int
     */
    public function convertToDatabaseValue($consultingCompany, AbstractPlatform $platform)
    {
        return $consultingCompany instanceof ConsultingCompany
            ? $consultingCompany->getId()
            : $consultingCompany;
    }
}

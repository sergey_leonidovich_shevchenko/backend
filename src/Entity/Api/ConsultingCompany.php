<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект консалтинговой компании
 * @ORM\Entity(repositoryClass="App\Repository\Api\ConsultingCompanyRepository")
 * @ORM\Table(options={"comment":"Консалтинговая компания"})
 */
class ConsultingCompany extends BaseEntityApi
{
    use DefaultEntityFields;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Код
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Код"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_]+$/i",
     *     message="В коде консалтинговой компании допустимы только латинские буквы, цыфры и знак подчеркивания. {{ value }}"
     * )
     */
    private $code;

    /**
     * Url
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Url"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Url(message="Url консалтинговой компании не является валидныйм.")
     */
    private $url;

    /**
     * Брокер
     * @var Broker
     * @ORM\ManyToMany(targetEntity="App\Entity\Api\Broker", inversedBy="brokerNewField")
     * @ BrokerConstraint() FIXME: Fix!
     */
    private $brokerList;

    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
        $this->brokerList = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return Collection|Broker[]
     */
    public function getBrokerList(): Collection
    {
        return $this->brokerList;
    }

    /**
     * @param Broker $broker
     * @return $this
     */
    public function addBroker(Broker $broker): self
    {
        if (!$this->brokerList->contains($broker)) {
            $this->brokerList[] = $broker;
        }
        return $this;
    }

    /**
     * @param Broker $broker
     * @return $this
     */
    public function removeBroker(Broker $broker): self
    {
        if ($this->brokerList->contains($broker)) {
            $this->brokerList->removeElement($broker);
        }
        return $this;
    }
}

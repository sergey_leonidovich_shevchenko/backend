<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект типа торгового счета
 * @ORM\Entity(repositoryClass="App\Repository\Api\TypeTradingAccountRepository")
 * @ORM\Table(options={"comment":"Тип торгового счета"})
 */
class TypeTradingAccount extends BaseEntityApi
{
    use DefaultEntityFields;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Код типа торгового счета
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, options={"comment":"Код типа торгового счета"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_]+$/i",
     *     message="В типе торгового счета допустимы только латинские буквы, цыфры и знак подчеркивания. {{ value }}"
     * )
     */
    private $code;

    /**
     * TypeTradingAccount constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }
}

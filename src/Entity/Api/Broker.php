<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект брокера
 * @ORM\Entity(repositoryClass="App\Repository\Api\BrokerRepository")
 * @ORM\Table(options={"comment":"Брокер"})
 */
class Broker extends BaseEntityApi
{
    use DefaultEntityFields;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Название брокера
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, options={"comment":"Название брокера"})
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_-]+$/i",
     *     message="В названии брокера допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
     * )
     */
    private $name;

    /**
     * Настройки брокера
     * @var array
     * @ORM\Column(type="json", options={"comment":"Настройки брокера"})
     */
    private $setting = [];

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Api\ConsultingCompany", mappedBy="brokerList")
     */
    private $consultingCompanyList;

    /**
     * Broker constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
        $this->consultingCompanyList = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getSetting(): ?array
    {
        return $this->setting;
    }

    /**
     * @param array $setting
     * @return $this
     */
    public function setSetting(array $setting): self
    {
        $this->setting = $setting;
        return $this;
    }

    /**
     * @return Collection|ConsultingCompany[]
     */
    public function getConsultingCompanyList(): Collection
    {
        return $this->consultingCompanyList;
    }

    /**
     * @param ConsultingCompany $consultingCompany
     * @return $this
     */
    public function addConsultingCompany(ConsultingCompany $consultingCompany): self
    {
        if (!$this->consultingCompanyList->contains($consultingCompany)) {
            $this->consultingCompanyList[] = $consultingCompany;
            $consultingCompany->addBroker($this);
        }
        return $this;
    }

    /**
     * @param ConsultingCompany $consultingCompany
     * @return $this
     */
    public function removeConsultingCompany(ConsultingCompany $consultingCompany): self
    {
        if ($this->consultingCompanyList->contains($consultingCompany)) {
            $this->consultingCompanyList->removeElement($consultingCompany);
            $consultingCompany->removeBroker($this);
        }
        return $this;
    }
}

<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект консалтинговой компании
 * @ORM\Entity(repositoryClass="App\Repository\Api\SalesDepartmentRepository")
 * @ORM\Table(options={"comment":"Департамент продаж"})
 */
class SalesDepartment extends BaseEntityApi
{
    use DefaultEntityFields;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Код
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Код"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_]+$/i",
     *     message="В коде департамента продаж допустимы только латинские буквы, цыфры и знак подчеркивания. {{ value }}"
     * )
     */
    private $code;

    /**
     * Консалтинговая компания
     * @var ConsultingCompany
     * @ORM\ManyToOne(targetEntity="ConsultingCompany")
     * @ORM\JoinColumn(name="consulting_company_id", referencedColumnName="id", nullable=false)
     * @ ConsultingCompanyConstraint() FIXME: сделать проверку
     * @Serializer\Type(name="integer")
     * @Serializer\Accessor(getter="getConsultingCompanyId")
     */
    private $consultingCompany;

    /**
     * SalesDepartment constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return ConsultingCompany|null
     */
    public function getConsultingCompany(): ?ConsultingCompany
    {
        return $this->consultingCompany;
    }

    /**
     * @param ConsultingCompany $consultingCompany
     * @return $this
     */
    public function setConsultingCompany(ConsultingCompany $consultingCompany): self
    {
        $this->consultingCompany = $consultingCompany;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getConsultingCompanyId(): ?int
    {
        $consultingCompany = $this->getConsultingCompany();
        return $consultingCompany instanceof ConsultingCompany
            ? $consultingCompany->getId()
            : null;
    }
}

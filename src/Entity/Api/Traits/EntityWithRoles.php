<?php declare(strict_types=1);


namespace App\Entity\Api\Traits;

use App\Assert\Constraint\Security as SecurityAssert;
use App\Entity\Api\UserGroup;
use App\Entity\Interfaces\EntityWithGroupsInterface;
use FOS\UserBundle\Model\UserInterface;

/**
 * Trait EntityWithRoles
 * @package App\Entity\Api\Traits
 */
trait EntityWithRoles
{
    /**
     * Список ролей
     * Для манипуляции с этим полем (@see \App\Service\Api\Security\PermissionService)
     * @var string[]
     * @SecurityAssert\RoleConstraint()
     */
    protected $roles;

    /**
     * Добавить роль (старые роли остаются)
     * @param string $addedRole
     * @return self
     */
    public function addRole($addedRole): self
    {
        $addedRole = strtoupper($addedRole);
        if ($addedRole === UserInterface::ROLE_DEFAULT) {
            return $this;
        }

        if ($this->roles === null) {
            $this->roles = [];
        }

        if (!in_array($addedRole, $this->roles, true)) {
            $this->roles[] = $addedRole;
        }

        return $this;
    }

    /**
     * Очистить список ролей
     */
    public function clearRoles(): void
    {
        $this->roles = [];
    }

    /**
     * @param string $role
     * @return bool
     */
    public function hasRole($role): bool
    {
        return in_array($role, $this->getRoles(), true);
    }

    /**
     * Получить список ролей (включай и роли из всех своих групп)
     * @param bool $withGroupRoles Вернуть вместе с ролями групп
     * @return string[]
     */
    public function getRoles($withGroupRoles = false): array
    {
        if (!$this->roles) {
            $this->roles = [];
        }

        // Если у вызываемой сущности есть группы ролей, то пробегаемся по ним
        if ($withGroupRoles) {
            /** @var EntityWithGroupsInterface $this */
            if ($this instanceof EntityWithGroupsInterface) {
                /** @var UserGroup $group */
                foreach ($this->getGroups() as $group) {
                    /** @var string $role */
                    foreach ($group->getRoles() as $role) {
                        if (!in_array($role, $this->roles, true)) {
                            $this->roles[] = $role;
                        }
                    }
                }
            }
        }

        $this->roles[] = 'ROLE_USER';
        $this->roles = array_unique($this->roles);

        return $this->roles;
    }

    /**
     * Установить новые роли (старые удаляются)
     * @param string[] $roles
     * @return self
     */
    public function setRoles(array $roles): self
    {
        $this->roles = [];

        foreach ($roles as $role) {
            if (!in_array($role, $this->roles, true)) {
                $this->roles[] = $role;
            }
        }

        return $this;
    }

    /**
     * Удалить роль
     * @param string $deletingRole
     * @return self
     */
    public function deleteRole(string $deletingRole): self
    {
        $deletingRole = strtoupper($deletingRole);

        foreach ($this->roles as $key => $role) {
            if ($role === $deletingRole) {
                unset($this->roles[$key]);
            }
        }

        return $this;
    }

    /**
     * Удалить роль
     * @param string $role
     * @return self
     */
    public function removeRole($role): self
    {
        $roles = $this->roles;
        foreach ($roles as $k => $r) {
            if ($role !== $r) {
                continue;
            }
            unset($roles[$k]);
        }

        return $this->setRoles($roles);
    }
}

<?php declare(strict_types=1);


namespace App\Entity\Api\Traits;

use DateTime;
use JMS\Serializer\Annotation as Serializer;

trait DefaultEntityFields
{
    /**
     * Дата создания
     * @var DateTime
     * @ORM\Column(
     *     name="created_at",
     *     type="datetime",
     *     columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",
     *     options={"comment":"Дата создания"})
     * @Serializer\ReadOnly()
     */
    protected $createdAt;

    /**
     * Дата обновления
     * @var DateTime
     * @ORM\Column(
     *     name="updated_at",
     *     type="datetime",
     *     columnDefinition="TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
     *     options={"comment":"Дата обновления"})
     * @Serializer\ReadOnly
     */
    protected $updatedAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
}

<?php declare(strict_types=1);


namespace App\Entity\Api\Traits;

use Doctrine\ORM\PersistentCollection;
use App\Entity\Api\User;
use FOS\UserBundle\Model\GroupableInterface;
use FOS\UserBundle\Model\GroupInterface;
use App\Entity\Api\UserGroup;
use RuntimeException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Trait EntityWithGroups
 * @package App\Entity\Api\Traits
 */
trait EntityWithGroups
{
    /**
     * @var PersistentCollection
     */
    protected $groups;

    /**
     * @return PersistentCollection|ArrayCollection
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    /**
     * Установить список групп (старые группы удалятся)
     * @param array $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = array_unique($groups);
    }

    /**
     * @return array
     */
    public function getGroupNames(): array
    {
        $groupNameList = [];
        foreach ($this->groups as $group) {
            /** @var UserGroup $group */
            $groupNameList[] = $group->getName();
        }
        return $groupNameList;
    }

    /**
     * @param $groupNames
     * @return EntityWithGroups
     */
    public function setGroupNames($groupNames): self
    {
        $groupNameList = [];
        foreach ($this->groups as $group) {
            /** @var UserGroup $group */
            $groupNameList[] = $group->getName();
        }
        return $groupNameList;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasGroup($name): bool
    {
        return in_array($name, $this->getGroupNames(), true);
    }

    /**
     * @param UserGroup $addedGroup
     * @return $this|GroupableInterface
     */
    public function addGroup($addedGroup): self
    {
        if (!$addedGroup instanceof UserGroup) {
            throw new RuntimeException(sprintf('%s must be a class instance %s', gettype($addedGroup), UserGroup::class));
        }

        $groups = $this->getGroups();
        $filtered = $groups->filter(static function ($filterGroup) use ($addedGroup) {
            /** @var UserGroup $filterGroup */
            return $filterGroup->getName() === $addedGroup->getName();
        });

        if (!count($filtered)) {
            $groups->add($addedGroup);
        }

        return $this;
    }

    /**
     * @param GroupInterface $groupOnDeletion
     * @return $this|GroupableInterface
     */
    public function deleteGroup($groupOnDeletion): User
    {
        return $this->removeGroup($groupOnDeletion);
    }

    /**
     * @param GroupInterface $groupOnDeletion
     * @return $this|GroupableInterface
     */
    public function removeGroup($groupOnDeletion): User
    {
        $newGroups = [];
        foreach ($this->getGroups() as $group) {
            if ($groupOnDeletion === $group) {
                continue;
            }
            $newGroups[] = $group;
        }
        $this->setGroups($newGroups);

        return $this;
    }

    /**
     * Очистить список ролей
     */
    public function clearGroups(): void
    {
        $this->groups = new ArrayCollection();
    }
}

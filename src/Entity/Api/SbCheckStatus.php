<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект статуса проверки СБ
 * @ORM\Entity(repositoryClass="App\Repository\Api\SbCheckStatusRepository")
 * @ORM\Table(options={"comment":"Статус проверки СБ"})
 */
class SbCheckStatus extends BaseEntityApi
{
    use DefaultEntityFields;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Название статуса проверки СБ
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, options={"comment":"Название статуса проверки СБ"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_-]+$/i",
     *     message="В названии статуса проверки СБ допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
     * )
     */
    private $name;

    /**
     * SbCheckStatus constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}

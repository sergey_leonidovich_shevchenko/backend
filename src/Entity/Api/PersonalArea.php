<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Assert as CustomAssert;
use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект личного кабинета
 * @ORM\Entity(repositoryClass="App\Repository\Api\PersonalAreaRepository")
 * @ORM\Table(options={"comment":"Личный кабинет"})
 */
class PersonalArea extends BaseEntityApi
{
    use DefaultEntityFields;

    public const ACTIVE = 'active';

    public const NOT_ACTIVE = 'not_active';

    public const ACTIVE_LIST = [
        1 => self::ACTIVE,
        2 => self::NOT_ACTIVE,
    ];

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Брокер
     * @var Broker
     * @ORM\ManyToOne(targetEntity="Broker")
     * @ORM\JoinColumn(name="broker_id", referencedColumnName="id", nullable=false)
     * @CustomAssert\Constraint\Api\BrokerConstraint()
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getBrokerId")
     */
    private $broker;

    /**
     * Объект статуса проверки СБ
     * @var SbCheckStatus
     * @ORM\ManyToOne(targetEntity="SbCheckStatus")
     * @ORM\JoinColumn(name="sb_check_status_id", referencedColumnName="id", nullable=false)
     * @CustomAssert\Constraint\Api\SbCheckStatusConstraint()
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getSbCheckStatusId")
     */
    private $sbCheckStatus;

    /**
     * Объект VIP статуса
     * @var StatusVip
     * @ORM\ManyToOne(targetEntity="StatusVip")
     * @ORM\JoinColumn(name="status_vip_id", referencedColumnName="id", nullable=false)
     * @CustomAssert\Constraint\Api\StatusVipConstraint()
     * @Serializer\Accessor(getter="getStatusVipId")
     * @Serializer\Type("integer")
     */
    private $statusVip;

    /**
     * Фамилия
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Фамилия"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_-]+$/i",
     *     message="В фамилии допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
     * )
     */
    private $surname;

    /**
     * Имя
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Имя"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_-]+$/i",
     *     message="В имени допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
     * )
     */
    private $name;

    /**
     * Отчество
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Отчество"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_-]+$/i",
     *     message="В отчестве допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
     * )
     */
    private $middleName;

    /**
     * Email
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, options={"comment":"Email"})
     * @Assert\Email(message="Email ""{{ value }}"" не является валидным.")
     */
    private $email;

    /**
     * Телефон
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, options={"comment":"Телефон"})
     * @Assert\NotBlank
     * @Assert\Regex(
     *     pattern="/^[+]\d{1,4}\d{10}$/i",
     *     message="Номер телефона должен быть в формате +79991112233 где код страны может быть от 1 до 4 цыфр."
     * )
     */
    private $phone;

    /**
     * Дата дня рождения в формате "Y-m-d"
     * @var DateTime
     * @ORM\Column(type="date", options={"comment":"Дата дня рождения"})
     * @Assert\NotBlank
     * @Serializer\Type("DateTime<'Y-m-d'>")
     */
    private $birthday;

    /**
     * @var string
     * @ORM\Column(
     *     name="usdt_tether",
     *     type="string",
     *     length=255,
     *     options={"comment":"Tether — криптовалютный токен, выпущенный компанией Tether Limited"}
     * )
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d]+$/i",
     *     message="В токене допустимы только латинские буквы и цыфры."
     * )
     */
    private $usdtTether;

    /**
     * Флаг указывающий активен ли кабинет
     * @var string|int
     * @ORM\Column(
     *     length=16,
     *     columnDefinition="ENUM('active', 'not_active')",
     *     options={"comment":"Флаг указывающий активен ли кабинет. ENUM('active', 'not_active')"}
     * )
     * @Assert\Choice(choices={"active", "not_active", 1, 2}, message="Выбирите валидное значение.")
     */
    private $active;

    /**
     * PersonalArea constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return $this
     */
    public function setSurname(string $surname): self
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return $this
     */
    public function setMiddleName(string $middleName): self
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * Получить ФИО
     * @return string
     */
    public function getFullName(): string
    {
        $surname = $this->getSurname()
            ? "{$this->getSurname()} "
            : '';

        $name = $this->getName()
            ? "{$this->getName()} "
            : '';

        $middleName = $this->getMiddleName();

        return "{$surname}{$name}{$middleName}";
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getBirthday(): ?DateTime
    {
        return $this->birthday;
    }

    /**
     * @param string|DateTime|null $birthday
     * @return $this
     * @throws Exception
     */
    public function setBirthday($birthday = null): self
    {
        if (is_string($birthday)) {
            [$year, $month, $day] = explode('-', $birthday);
            $birthday = (new DateTime())->setDate((int)$year, (int)$month, (int)$day);
        }
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsdtTether(): ?string
    {
        return $this->usdtTether;
    }

    /**
     * @param string $usdtTether
     * @return $this
     */
    public function setUsdtTether(string $usdtTether): self
    {
        $this->usdtTether = $usdtTether;
        return $this;
    }

    /**
     * @return Broker|null
     */
    public function getBroker(): ?Broker
    {
        return $this->broker;
    }

    /**
     * @param Broker $broker
     * @return $this
     */
    public function setBroker(Broker $broker): self
    {
        $this->broker = $broker;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBrokerId(): ?int
    {
        $broker = $this->getBroker();
        return $broker instanceof Broker
            ? $broker->getId()
            : null;
    }

    /**
     * @return string|null
     */
    public function getActive(): ?string
    {
        return $this->active;
    }

    /**
     * @param int|string $active Флаг активности (int 1 -> Активный, int 2 -> Не активный)
     *                           (также можно использовать "active", "not_active")
     * @return $this
     */
    public function setActive($active): self
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return SbCheckStatus|null
     */
    public function getSbCheckStatus(): ?SbCheckStatus
    {
        return $this->sbCheckStatus;
    }

    /**
     * @param SbCheckStatus $sbCheckStatus
     * @return $this
     */
    public function setSbCheckStatus(SbCheckStatus $sbCheckStatus): self
    {
        $this->sbCheckStatus = $sbCheckStatus;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSbCheckStatusId(): ?int
    {
        $sbCheckStatusId = $this->getSbCheckStatus();
        return $sbCheckStatusId instanceof SbCheckStatus
            ? $sbCheckStatusId->getId()
            : null;
    }

    /**
     * @return StatusVip|null
     */
    public function getStatusVip(): ?StatusVip
    {
        return $this->statusVip;
    }

    /**
     * @param StatusVip $statusVip
     * @return $this
     */
    public function setStatusVip(StatusVip $statusVip): self
    {
        $this->statusVip = $statusVip;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatusVipId(): ?int
    {
        $statusVip = $this->getStatusVip();
        return $statusVip instanceof StatusVip
            ? $statusVip->getId()
            : null;
    }
}

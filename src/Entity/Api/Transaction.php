<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Assert\Constraint\Api\AccountConstraint;
use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JMS\Serializer\Annotation as Serializer;

/**
 * Объект счета
 * @ORM\Entity(repositoryClass="App\Repository\Api\TransactionRepository")
 * @ORM\Table(options={"comment":"Счета"})
 */
class Transaction extends BaseEntityApi
{
    use DefaultEntityFields;

    public const STATUS_SUCCESSFUL = 'successful';

    public const STATUS_FAILED = 'failed';

    public const STATUS_CODE_LIST = [
        self::STATUS_SUCCESSFUL,
        self::STATUS_FAILED,
    ];

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Счет
     * @var Account
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=true)
     * @AccountConstraint
     * @Serializer\Type("integer")
     * @Serializer\Accessor(getter="getAccountId")
     */
    private $account;

    /**
     * ID во внешней системе
     * @ORM\Column(name="outer_system_id", type="string", nullable=true, options={"comment":"ID во внешней системе"})
     * @var string
     */
    private $outerSystemId;

    /**
     * Числовой показатель ID
     * @var int
     * @ORM\Column(
     *     name="reference_id",
     *     type="integer",
     *     nullable=true,
     *     options={"unsigned":true, "comment":"Числовой показатель ID"}
     * )
     */
    private $referenceId;

    /**
     * ???
     * @var string
     * @ORM\Column(name="livemode", type="string", nullable=true, options={"comment":"???"})
     */
    private $livemode;

    /**
     * Статус
     * @var string
     * @ORM\Column(name="status", type="string", nullable=true, options={"comment":"Статус"})
     */
    private $status;

    /**
     * Сумма платежа
     * @var float
     * @ORM\Column(
     *     name="amount", type="decimal",
     *     precision=10, scale=2,
     *     nullable=true, options={"unsigned":true, "comment":"Сумма платежа"})
     */
    private $amount;

    /**
     * Сумма платежа (текст)
     * @var string
     * @ORM\Column(name="amount_text", type="text", nullable=true, options={"comment":"Сумма платежа (текст)"})
     */
    private $amountText;

    /**
     * ???
     * @var string
     * @ORM\Column(name="currency_id", type="string", length=255, nullable=true, options={"comment":"???"})
     */
    private $currencyId;

    /**
     * Опциональное описание
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true, options={"comment":"Опциональное описание"})
     */
    private $description;

    /**
     * IP клиента при оплате
     * @var int
     * @ORM\Column(
     *     name="ip",
     *     type="integer",
     *     nullable=true,
     *     options={"unsigned":"true", "comment":"IP клиента при оплате"}
     * )
     */
    private $ip;

    /**
     * Email
     * @var string
     * @ORM\Column(name="email", type="string", length=255, nullable=true, options={"comment":"Email"})
     */
    private $email;

    /**
     * Спам от платежек
     * @var string
     * @ORM\Column(name="source", type="text", nullable=true, options={"comment":"Спам от платежек"})
     */
    private $source;

    /**
     * Код ошибки
     * @var string
     * @ORM\Column(name="failure_code", type="string", length=255, nullable=true, options={"comment":"Код ошибки"})
     */
    private $failureCode;

    /**
     * Текст ошибки
     * @var string
     * @ORM\Column(name="failure_message", type="text", nullable=true, options={"comment":"Текст ошибки"})
     */
    private $failureMessage;

    /**
     * Будем ждать оплаты до:
     * @var DateTime
     * @ORM\Column(name="valid_till", type="datetime", nullable=true, options={"comment":"Будем ждать оплаты до:"})
     */
    private $validTill;

    /**
     * Метод оплаты
     * @var string
     * @ORM\Column(name="pay_method", type="string", length=255, nullable=true, options={"comment":"Метод оплаты"})
     */
    private $payMethod;

    /**
     * Transaction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getAccountId(): ?int
    {
        $account = $this->getAccount();
        return $account instanceof Account
            ? $account->getId()
            : null;
    }

    /**
     * @return Account|null
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    /**
     * @return string|null
     */
    public function getOuterSystemId(): ?string
    {
        return $this->outerSystemId;
    }

    /**
     * @param string $outerSystemId
     */
    public function setOuterSystemId(string $outerSystemId): void
    {
        $this->outerSystemId = $outerSystemId;
    }

    /**
     * @return int|null
     */
    public function getReferenceId(): ?int
    {
        return $this->referenceId;
    }

    /**
     * @param int $referenceId
     */
    public function setReferenceId(int $referenceId): void
    {
        $this->referenceId = $referenceId;
    }

    /**
     * @return string|null
     */
    public function getLivemode(): ?string
    {
        return $this->livemode;
    }

    /**
     * @param string $livemode
     */
    public function setLivemode(string $livemode): void
    {
        $this->livemode = $livemode;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getAmountText(): ?string
    {
        return $this->amountText;
    }

    /**
     * @param string $amountText
     */
    public function setAmountText(string $amountText): void
    {
        $this->amountText = $amountText;
    }

    /**
     * @return string|null
     */
    public function getCurrencyId(): ?string
    {
        return $this->currencyId;
    }

    /**
     * @param string $currencyId
     */
    public function setCurrencyId(string $currencyId): void
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int|null
     */
    public function getIp(): ?int
    {
        return $this->ip;
    }

    /**
     * @param int $ip
     */
    public function setIp(int $ip): void
    {
        $this->ip = $ip;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string|null
     */
    public function getFailureCode(): ?string
    {
        return $this->failureCode;
    }

    /**
     * @param string $failureCode
     */
    public function setFailureCode(string $failureCode): void
    {
        $this->failureCode = $failureCode;
    }

    /**
     * @return string|null
     */
    public function getFailureMessage(): ?string
    {
        return $this->failureMessage;
    }

    /**
     * @param string $failureMessage
     */
    public function setFailureMessage(string $failureMessage): void
    {
        $this->failureMessage = $failureMessage;
    }

    /**
     * @return DateTime|null
     */
    public function getValidTill(): ?DateTime
    {
        return $this->validTill;
    }

    /**
     * @param DateTime $validTill
     */
    public function setValidTill(DateTime $validTill): void
    {
        $this->validTill = $validTill;
    }

    /**
     * @return string|null
     */
    public function getPayMethod(): ?string
    {
        return $this->payMethod;
    }

    /**
     * @param string $payMethod
     */
    public function setPayMethod(string $payMethod): void
    {
        $this->payMethod = $payMethod;
    }
}

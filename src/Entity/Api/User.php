<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\Api\Traits\EntityWithGroups;
use App\Entity\Api\Traits\EntityWithRoles;
use App\Entity\BaseEntityApi;
use App\Entity\Interfaces\EntityWithGroupsInterface;
use App\Entity\Interfaces\EntityWithRolesInterface;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Exception;
use FOS\UserBundle\Model\UserInterface;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package App\Entity\Api
 * @ORM\Entity(repositoryClass="App\Repository\Api\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseEntityApi implements UserInterface, EntityWithGroupsInterface, EntityWithRolesInterface
{
    use EntityWithRoles;
    use EntityWithGroups;
    use DefaultEntityFields;

    public const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * Random string sent to the user email address in order to verify it.
     * @var string|null
     * @ORM\Column(type="string", length=180, unique=true, nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var string
     * @ORM\Column(type="string", length=180, nullable=false)
     * @Assert\NotBlank(message="Пожалуйста, укажите Ваш email (email).", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=2,
     *     minMessage="Email слишком короткий.",
     *     max=180,
     *     maxMessage="Email слишком длинный.",
     *     groups={"Registration", "Profile"}
     * )
     * @Assert\Email(message="Email в неправильном формате.", groups={"Registration", "Profile"})
     * @Serializer\Groups({"add","get","update"})
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected $emailCanonical;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Serializer\Groups({"add","get","update"})
     */
    protected $enabled;

    /**
     * @var PersistentCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Api\UserGroup")
     * @ORM\JoinTable(
     *     name="user_user_group",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     * @Serializer\Groups({"add","get","update","Default"})
     * @Serializer\Type("array<string>")
     * @Serializer\MaxDepth(5)
     * @Serializer\Accessor(getter="getGroupNames", setter="setGroupNames")
     */
    protected $groups;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @Serializer\Groups({"add","get","update"})
     */
    protected $id;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Groups({"add","get","update"})
     */
    protected $lastLogin;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Serializer\Exclude()
     */
    protected $password;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * Простой пароль. Используется для проверки. Он не должен сохраняться.
     * @var string
     * @Assert\NotBlank(
     *     message="Пожалуйста, укажите пароль (password).",
     *     groups={"Registration", "ResetPassword", "ChangePassword"}
     * )
     * @Assert\Length(
     *     min=2,
     *     minMessage="Пароль слишком короткий.",
     *     groups={"Registration", "ResetPassword", "ChangePassword", "Profile"}
     * )
     */
    protected $plainPassword;

    /**
     * Список ролей
     * Для манипуляции с этим полем (@see \App\Service\Api\Security\PermissionService)
     * @var string[]
     * @ORM\Column(type="json", options={"comment":"Роли доступа"})
     * @SecurityAssert\RoleConstraint()
     * @Serializer\Groups({"add","get","update"})
     * @Serializer\Type("array<string>")
     */
    protected $roles;

    /**
     * The salt to use for hashing.
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $salt;

    /**
     * @var string
     * @ORM\Column(type="string", length=180, nullable=false)
     * @Assert\NotBlank(message="Пожалуйста, укажите логин (username).", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=2,
     *     minMessage="Логин слишком короткий.",
     *     max=180,
     *     maxMessage="Логин слишком длинный.",
     *     groups={"Registration", "Profile"}
     * )
     * @Serializer\Groups({"add","get","update"})
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected $usernameCanonical;

    /**
     * User constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->enabled = false;
        $this->roles = [];
        $this->groups = new ArrayCollection();
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getUsername();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this|UserInterface
     */
    public function setUsername($username): User
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsernameCanonical(): ?string
    {
        return $this->usernameCanonical;
    }

    /**
     * @param string $usernameCanonical
     * @return $this|UserInterface
     */
    public function setUsernameCanonical($usernameCanonical): User
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this|UserInterface
     */
    public function setEmail($email): User
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmailCanonical(): ?string
    {
        return $this->emailCanonical;
    }

    /**
     * @param string $emailCanonical
     * @return $this|UserInterface
     */
    public function setEmailCanonical($emailCanonical): User
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $boolean
     * @return $this|UserInterface
     */
    public function setEnabled($boolean): User
    {
        $this->enabled = (bool)$boolean;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @param string|null $salt
     * @return $this|UserInterface
     */
    public function setSalt($salt): User
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this|UserInterface
     */
    public function setPassword($password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSuperAdmin(): bool
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN);
    }

    /**
     * @param bool $boolean
     * @return $this|UserInterface
     */
    public function setSuperAdmin($boolean): User
    {
        if (true === $boolean) {
            $this->addRole(static::ROLE_SUPER_ADMIN);
        } else {
            $this->removeRole(static::ROLE_SUPER_ADMIN);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $password
     * @return $this|UserInterface
     */
    public function setPlainPassword($password): User
    {
        $this->plainPassword = $password;

        return $this;
    }

    /**
     * Получить время последнего входа в систему
     * @return DateTime|null
     */
    public function getLastLogin(): ?DateTime
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTime|null $time
     * @return $this|UserInterface
     */
    public function setLastLogin(DateTime $time = null): User
    {
        $this->lastLogin = $time;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    /**
     * @param string|null $confirmationToken
     * @return $this|UserInterface
     */
    public function setConfirmationToken($confirmationToken): User
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Получает метку времени, когда пользователь запросил сброс пароля.
     * @return null|DateTime
     */
    public function getPasswordRequestedAt(): ?DateTime
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @param DateTime|null $date
     * @return $this|UserInterface
     */
    public function setPasswordRequestedAt(DateTime $date = null)
    {
        $this->passwordRequestedAt = $date;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            $this->id,
            $this->username,
            $this->usernameCanonical,
            $this->email,
            $this->emailCanonical,
            $this->enabled,
            $this->salt,
            $this->password,
            $this->groups,
            $this->roles,
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized): void
    {
        $data = unserialize($serialized, ['allowed_classes' => true]);

        /**
         * FIXME: ниже какой-то пиздец, как я понял вырезаются не нужные данные для того что бы собрать объект.
         * надо бы разобраться, что здесь приходит после десериализации
         * @see \FOS\UserBundle\Model\User::unserialize()
         */
        // if (15 === count($data)) {
        //     // Unserializing a User object from 1.3.x
        //     unset($data[5], $data[0], $data[6], $data[9], $data[10]);
        //     $data = array_values($data);
        // } elseif (13 === count($data)) {
        //     // Unserializing a User from a dev version somewhere between 2.0-alpha3 and 2.0-beta1
        //     unset($data[4], $data[7], $data[8]);
        //     $data = array_values($data);
        // }

        [
            $this->id,
            $this->username,
            $this->usernameCanonical,
            $this->email,
            $this->emailCanonical,
            $this->enabled,
            $this->salt,
            $this->password,
            $this->groups,
            $this->roles,
        ] = $data;
    }

    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired(): bool
    {
        return true;
    }

    /**
     * @param int $ttl
     * @return bool
     */
    public function isPasswordRequestNonExpired($ttl): bool
    {
        return $this->getPasswordRequestedAt() instanceof DateTime
            && $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
    }
}

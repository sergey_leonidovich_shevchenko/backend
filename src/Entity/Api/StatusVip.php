<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект VIP статуса
 * @ORM\Entity(repositoryClass="App\Repository\Api\StatusVipRepository")
 * @ORM\Table(options={"comment":"VIP статус"})
 */
class StatusVip extends BaseEntityApi
{
    use DefaultEntityFields;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Название VIP статуса
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, options={"comment":"Название VIP статуса"})
     * @Assert\NotBlank
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_-]+$/i",
     *     message="В названии VIP-статуса допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
     * )
     */
    private $name;

    /**
     * Сумма проверки статуса
     * @var float
     * @ORM\Column(type="decimal", precision=9, scale=2, options={"unsigned"=true}, options={"comment":"Сумма проверки статуса"})
     * @Assert\NotBlank
     * @Assert\Regex(
     *     pattern="/^[0-9]{1,9}(\.[0-9]{1,2})?$/i",
     *     message="Сумма проверки статуса должна содержать только дробное число больше нуля."
     * )
     */
    private $sumForCheckStatus;

    /**
     * StatusVip constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSumForCheckStatus(): ?float
    {
        return $this->sumForCheckStatus;
    }

    /**
     * @param float $sumForCheckStatus
     * @return $this
     */
    public function setSumForCheckStatus(float $sumForCheckStatus): self
    {
        $this->sumForCheckStatus = $sumForCheckStatus;

        return $this;
    }
}

<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use App\Service\MT\BalanceService;
use RuntimeException;
use JMS\Serializer\Annotation as Serializer;

/**
 * Объект торговой платформы
 * @ORM\Entity(repositoryClass="App\Repository\Api\TradingPlatformRepository")
 * @ORM\Table(options={"comment":"Торговая платформа"})
 */
class TradingPlatform extends BaseEntityApi
{
    use DefaultEntityFields;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @Serializer\Groups({"get","update","add"})
     */
    protected $id;

    /**
     * Баланс
     * @var float
     * @Serializer\Groups({"get"})
     */
    private $balance;

    /**
     * Массив с доступами на сервер где лежит баланс
     * @var array
     * @ORM\Column(type="json", options={"comment":"Массив с доступами на сервер где лежит баланс"})
     * @Assert\Collection(
     *     fields={
     *         "server"= @Assert\NotBlank(message="Не указано название сервера для получения баланса"),
     *         "port"= @Assert\LessThan(
     *             value="65535",
     *             message="Номер порта не должен превышать 65535 (see https://en.wikipedia.org/wiki/Ephemeral_port)"
     *         ),
     *         "DB"= @Assert\NotBlank(message="Не указана база данных для получения баланса"),
     *         "user"= @Assert\NotBlank(message="Не указан пользователя от сервера для получения баланса"),
     *         "password"= @Assert\NotBlank(message="Не указан пароль от сервера для получения баланса")
     *     },
     *     missingFieldsMessage="Массив с доступами на сервер где лежит баланс не должн быть пустым"
     * )
     * @Serializer\Groups({"update","add"})
     */
    private $balanceServer;

    /**
     * @var BalanceService|null
     * @Serializer\Exclude()
     */
    public $balanceService;

    /**
     * Код торговой платформы
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, options={"comment":"Код торговой платформы"})
     * @Assert\NotBlank(message="Код торговой платформы не должно быть пустым")
     * @Assert\Length(max=255, maxMessage="Максимальное число вводимых символов не должна превышать 255.")
     * @Assert\Regex(
     *     pattern="/^[\w\d_]+$/i",
     *     message="В коде торговой платформы допустимы только латинские буквы, цыфры и знак подчеркивания. {{ value }}"
     * )
     * @Serializer\Groups({"get","update","add"})
     */
    private $code;

    /**
     * Криптовать данные при передачи в MT или нет
     * @var bool
     * @ORM\Column(type="boolean", options={"comment":"Криптовать данные при передачи в MT или нет"})
     * @Serializer\Groups({"get","update","add"})
     */
    private $crypt;

    /**
     * Группа для открытия счёта на MT
     * @var string
     * @ORM\Column(name="`group`", type="string", length=255, options={"comment":"Группа для открытия счёта на MT"})
     * @Assert\NotBlank(message="Группа для открытия счёта на MT не должна быть пустым")
     * @Serializer\Groups({"get","update","add"})
     */
    private $group;

    /**
     * Логин для доступа через API (любое)
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Логин для доступа через API (любое)"})
     * @Assert\NotBlank(message="Логин для доступа через API (любое) не должен быть пустым")
     * @Serializer\Groups({"get","update","add"})
     */
    private $login;

    /**
     * Пароль для доступа через API
     * @var string
     * @ORM\Column(type="string", length=255, options={"comment":"Пароль для доступа через API"})
     * @Assert\NotBlank(message="Пароль для доступа через API не должен быть пустым")
     * @Serializer\Groups({"get","update","add"})
     */
    private $password;

    /**
     * Порт Webgat-a
     * @var int
     * @ORM\Column(type="integer", options={"comment":"Порт Webgat-a"})
     * @Assert\NotBlank(message="Порт Webgat-a не должен быть пустым")
     * @Assert\Regex(pattern="/^\d+$/i", message="Порт должен быть числом.")
     * @Serializer\Groups({"get","update","add"})
     */
    private $port;

    /**
     * Ссылка на Webgate (Для выполнения запросов)
     * @var string
     * @ORM\Column(type="text", options={"comment":"Ссылка на Webgate (Для выполнения запросов)"})
     * @Assert\NotBlank(message="Ссылка на Webgate (Для выполнения запросов) не должно быть пустым")
     * @Serializer\Groups({"get","update","add"})
     */
    private $server;

    /**
     * Время ожидания ответа при запросе через gRPC
     * @var int
     * @ORM\Column(type="integer", options={"comment":"Время ожидания ответа при запросе через gRPC"})
     * @Assert\NotBlank(message="Время ожидания ответа при запросе через gRPC не должно быть пустым")
     * @Serializer\Groups({"get","update","add"})
     */
    private $timeout;

    /**
     * TradingPlatform constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    public function __unserialize(array $data): void
    {
        $this->uploadBalanceFromServer();
    }

    /**
     * Подгрузить баланс с удаленного сервера
     * @return $this
     */
    public function uploadBalanceFromServer(): self
    {
        $balanceServer = $this->getBalanceServer();
        if (empty($balanceServer)) {
            throw new RuntimeException('An error occurred while loading the balancer from the remote server!');
        }

        if ($this->balanceService !== null) {
            $this->balance = $this->balanceService->getBalance([
                'driver' => 'pdo_mysql',
                'host' => $balanceServer['server'],
                'user' => $balanceServer['user'],
                'password' => $balanceServer['password'],
                'port' => $balanceServer['port'],
                'dbname' => $balanceServer['DB'],
            ], $this->getLogin());
        }

        return $this;
    }

    /**
     * @return float|null
     */
    public function getBalance(): ?float
    {
        return $this->balance;
    }

    /**
     * @return array|null
     */
    public function getBalanceServer(): ?array
    {
        return $this->balanceServer;
    }

    /**
     * @param array $balance
     * @return TradingPlatform
     */
    public function setBalanceServer(array $balanceServer): self
    {
        $this->balanceServer = $balanceServer;
        return $this;
    }

    /**
     * @return BalanceService|null
     */
    public function getBalanceService(): ?BalanceService
    {
        return $this->balanceService;
    }

    /**
     * @param BalanceService $balanceService
     * @required
     * @return static
     */
    public function setBalanceService(BalanceService $balanceService): self
    {
        $this->balanceService = $balanceService;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isCrypt(): ?bool
    {
        return $this->crypt;
    }

    /**
     * @param bool $crypt
     * @return TradingPlatform
     */
    public function setCrypt(bool $crypt): self
    {
        $this->crypt = $crypt;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGroup(): ?string
    {
        return $this->group;
    }

    /**
     * @param string $group
     * @return TradingPlatform
     */
    public function setGroup(string $group): self
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return TradingPlatform
     */
    public function setLogin(string $login): self
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return TradingPlatform
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }

    /**
     * @param int $port
     * @return TradingPlatform
     */
    public function setPort(int $port): self
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getServer(): ?string
    {
        return $this->server;
    }

    /**
     * @param string $server
     * @return TradingPlatform
     */
    public function setServer(string $server): self
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTimeout(): ?int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     * @return TradingPlatform
     */
    public function setTimeout(int $timeout): self
    {
        $this->timeout = $timeout;
        return $this;
    }
}

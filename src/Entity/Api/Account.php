<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Assert\Constraint\Api\PersonalAreaConstraint;
use App\Assert\Constraint\Api\SalesDepartmentConstraint;
use App\Assert\Constraint\Api\TradingPlatformConstraint;
use App\Assert\Constraint\Api\TypeTradingAccountConstraint;
use App\Assert\Constraint\Api\UserConstraint;
use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект счета
 * @ORM\Entity(repositoryClass="App\Repository\Api\AccountRepository")
 * @ORM\Table(options={"comment":"Счета"})
 */
class Account extends BaseEntityApi
{
    use DefaultEntityFields;

    public const BASE_LIVERAGE = 100;

    public const SOURCE_CODE_CRM = 'crm';

    public const SOURCE_CODE_CABINET = 'cabinet';

    public const SOURCE_CODE_LIST = [
        1 => self::SOURCE_CODE_CRM,
        2 => self::SOURCE_CODE_CABINET,
    ];

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @Serializer\Groups({"get","add","update"})
     */
    protected $id;

    /**
     * Создатель счета
     * @var User
     * @ORM\ManyToOne(targetEntity="User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @UserConstraint()
     * @Serializer\Type(name="integer")
     * @Serializer\Accessor(getter="getUserId")
     * @Serializer\Groups({"get","add","update"})
     */
    private $user;

    /**
     * Торговая платформа
     * @var TradingPlatform
     * @ORM\ManyToOne(targetEntity="TradingPlatform", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="trading_platform_id", referencedColumnName="id", nullable=false)
     * @TradingPlatformConstraint()
     * @Serializer\Type(name="integer")
     * @Serializer\Accessor(getter="getTradingPlatformId")
     * @Serializer\Groups({"get","add","update"})
     */
    private $tradingPlatform;

    /**
     * Личный кабинет
     * @var PersonalArea
     * @ORM\ManyToOne(targetEntity="PersonalArea", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="personal_area_id", referencedColumnName="id", nullable=false)
     * @PersonalAreaConstraint()
     * @Serializer\Type(name="integer")
     * @Serializer\Accessor(getter="getPersonalAreaId")
     * @Serializer\Groups({"get","add","update"})
     */
    private $personalArea;

    /**
     * Тип торгового счета
     * @var TypeTradingAccount
     * @ORM\ManyToOne(targetEntity="TypeTradingAccount", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="type_trading_account_id", referencedColumnName="id", nullable=false)
     * @TypeTradingAccountConstraint()
     * @Serializer\Type(name="integer")
     * @Serializer\Accessor(getter="getTypeTradingAccountId")
     * @Serializer\Groups({"get","add","update"})
     */
    private $typeTradingAccount;

    /**
     * Департамент продаж
     * @var SalesDepartment
     * @ORM\ManyToOne(targetEntity="SalesDepartment", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="sales_department_id", referencedColumnName="id", nullable=false)
     * @SalesDepartmentConstraint()
     * @Serializer\Type(name="integer")
     * @Serializer\Accessor(getter="getSalesDepartmentId")
     * @Serializer\Groups({"get","add","update"})
     */
    private $salesDepartment;

    /**
     * Номер счета
     * @var int
     * @ORM\Column(type="integer", options={"unsigned"=true}, options={"comment":"Номер счета"})
     * @Assert\NotBlank(message="Номер счета не указан.")
     * @Assert\Positive(message="Номер счета должен содержать только целое число больше нуля.")
     * @Serializer\Groups({"get","add","update"})
     */
    private $number;

    /**
     * Плечо
     * @var int
     * @ORM\Column(type="integer", options={"unsigned"=true}, options={"default": 100, "comment":"Плечо"})
     * @Assert\NotBlank(message="Плечё не указанно.")
     * @Assert\Positive(message="Номер счета должен содержать только целое число больше нуля.")
     * @Serializer\Groups({"get","add","update"})
     */
    private $leverage = 100;

    /**
     * Флаг указывающий откуда был создан счет
     * @var string|int
     * @ORM\Column(
     *     length=16,
     *     columnDefinition="ENUM('crm', 'cabinet')",
     *     options={"comment":"Флаг указывающий откуда был создан счет. ENUM('crm', 'cabinet')"}
     * )
     * @Assert\Choice(
     *     choices={"crm", "cabinet", 1, 2},
     *     message="Нужно указать откуда был создан счет ('crm', 'cabinet').")
     * @Serializer\Groups({"get","add","update"})
     */
    private $sourceCode;

    /**
     * Account constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @return int|null Id of user
     */
    public function getUserId(): ?int
    {
        $user = $this->getUser();
        return $user instanceof User
            ? $user->getId()
            : null;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Account
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTradingPlatformId(): ?int
    {
        $tradingPlatform = $this->getTradingPlatform();
        return $tradingPlatform instanceof TradingPlatform
            ? $tradingPlatform->getId()
            : null;
    }

    /**
     * @return TradingPlatform|null
     */
    public function getTradingPlatform(): ?TradingPlatform
    {
        return $this->tradingPlatform;
    }

    /**
     * @param TradingPlatform $tradingPlatform
     * @return Account
     */
    public function setTradingPlatform(TradingPlatform $tradingPlatform): self
    {
        $this->tradingPlatform = $tradingPlatform;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPersonalAreaId(): ?int
    {
        $personalArea = $this->getPersonalArea();
        return $personalArea instanceof PersonalArea
            ? $personalArea->getId()
            : null;
    }

    /**
     * @return PersonalArea|null
     */
    public function getPersonalArea(): ?PersonalArea
    {
        return $this->personalArea;
    }

    /**
     * @param PersonalArea $personalArea
     * @return Account
     */
    public function setPersonalArea(PersonalArea $personalArea): self
    {
        $this->personalArea = $personalArea;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTypeTradingAccountId(): ?int
    {
        $typeTradingAccount = $this->getTypeTradingAccount();
        return $typeTradingAccount instanceof TypeTradingAccount
            ? $typeTradingAccount->getId()
            : null;
    }

    /**
     * @return TypeTradingAccount|null
     */
    public function getTypeTradingAccount(): ?TypeTradingAccount
    {
        return $this->typeTradingAccount;
    }

    /**
     * @param TypeTradingAccount $typeTradingAccount
     * @return Account
     */
    public function setTypeTradingAccount(TypeTradingAccount $typeTradingAccount): self
    {
        $this->typeTradingAccount = $typeTradingAccount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSalesDepartmentId(): ?int
    {
        $salesDepartment = $this->getSalesDepartment();
        return $salesDepartment instanceof SalesDepartment
            ? $salesDepartment->getId()
            : null;
    }

    /**
     * @return SalesDepartment|null
     */
    public function getSalesDepartment(): ?SalesDepartment
    {
        return $this->salesDepartment;
    }

    /**
     * @param SalesDepartment $salesDepartment
     * @return Account
     */
    public function setSalesDepartment(SalesDepartment $salesDepartment): self
    {
        $this->salesDepartment = $salesDepartment;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return Account
     */
    public function setNumber(int $number): self
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLeverage(): ?int
    {
        return $this->leverage;
    }

    /**
     * @param int $leverage
     * @return Account
     */
    public function setLeverage(int $leverage): self
    {
        $this->leverage = $leverage;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getSourceCode()
    {
        return $this->sourceCode;
    }

    /**
     * @param int|string $sourceCode
     * @return Account
     */
    public function setSourceCode($sourceCode): self
    {
        $this->sourceCode = $sourceCode;
        return $this;
    }
}

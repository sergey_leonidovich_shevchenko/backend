<?php declare(strict_types=1);

namespace App\Entity\Api;

use App\Assert as CustomAssert;
use App\Entity\Interfaces\EntityWithRolesInterface;
use App\Entity\Api\Traits\DefaultEntityFields;
use App\Entity\Api\Traits\EntityWithRoles;
use App\Entity\BaseEntityApi;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserGroup
 * @package App\Entity\Api
 * @ORM\Entity(repositoryClass="App\Repository\Api\UserGroupRepository")
 * @ORM\Table(name="user_group")
 * @CustomAssert\Constraint\Api\UserGroupConstraint(groups={"UpdateUserGroup"})
 */
class UserGroup extends BaseEntityApi implements EntityWithRolesInterface
{
    use EntityWithRoles;
    use DefaultEntityFields;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * Название пользовательской группы
     * @var string
     * @ORM\Column(
     *     type="string",
     *     length=32,
     *     unique=true,
     *     options={"comment":"Название пользовательской группы"})
     * @Assert\NotBlank(message="Пожалуйста, укажите название группы.")
     * @Assert\Length(max=32, maxMessage="Максимальное число вводимых символов не должна превышать {{ limit }}.")
     * @Assert\Regex(
     *     pattern="/^[a-z][0-9a-z_\.]{1,31}$/",
     *     message="В названии присутствуют недопустимые символы. Доступны только маленькие латинские буквы(и начинаться с них), цыфры, точка и знак подчеркивания"
     * )
     */
    protected $name;

    /**
     * Название пользовательской группы на русском
     * @var string
     * @ORM\Column(
     *     type="string",
     *     nullable=true,
     *     length=32,
     *     unique=true,
     *     options={"comment":"Название пользовательской группы на русском"})
     * @Assert\Length(max=32, maxMessage="Максимальное число вводимых символов не должна превышать {{ limit }}.")
     */
    protected $nameRus;

    /**
     * Сумма платежа (текст)
     * @var string
     * @ORM\Column(type="text", nullable=true, options={"comment":"Подробное описание пользовательской роли"})
     */
    protected $description;

    /**
     * Список ролей
     * Для манипуляции с этим полем (@see \App\Service\Api\Security\PermissionService)
     * @var string[]
     * @ORM\Column(type="json", options={"comment":"Роли доступа"})
     * @CustomAssert\Constraint\Security\RoleConstraint()
     */
    protected $roles;

    /**
     * UserGroup constructor.
     * @param array $data
     * @throws Exception
     */
    public function __construct(array $data = [])
    {
        $this->name = $data['name'] ?? [];
        $this->nameRus = $data['name_rus'] ?? null;
        $this->description = $data['description'] ?? null;
        $this->roles = $data['roles'] ?? [];
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UserGroup
     */
    public function setName($name): UserGroup
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNameRus(): ?string
    {
        return $this->nameRus;
    }

    /**
     * @param string|null $nameRus
     * @return UserGroup
     */
    public function setNameRus(?string $nameRus): UserGroup
    {
        $this->nameRus = $nameRus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return UserGroup
     */
    public function setDescription(?string $description): UserGroup
    {
        $this->description = $description;
        return $this;
    }
}

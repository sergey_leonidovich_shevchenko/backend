<?php declare(strict_types=1);


namespace App\Entity\Interfaces;

/**
 * Interface EntityWithRolesInterface
 * Интерфейс указывающий, что у текущей сущность есть роли
 * @package App\Entity\Interfaces
 */
interface EntityWithRolesInterface extends EntityWithPermission
{
    /**
     * Добавить роль
     * @param string $role
     * @return mixed
     */
    public function addRole(string $role);

    /**
     * Очистить список ролей
     * @return mixed
     */
    public function clearRoles();

    /**
     * Удалить роль
     * @param string $role
     * @return mixed
     */
    public function deleteRole(string $role);

    /**
     * Получить список ролей (включай и роли из всех своих групп)
     * @param bool $withGroupRoles Вернуть вместе с ролями групп
     * @return string[]
     */
    public function getRoles($withGroupRoles = true): array;

    /**
     * Проверить имеет ли сущность эту роль
     * @param string $role Роль на проверку
     * @return bool
     */
    public function hasRole(string $role): bool;

    /**
     * Установить список ролей (старые роли удалятся)
     * @param array $roles Список новых ролей
     * @return mixed
     */
    public function setRoles(array $roles);
}

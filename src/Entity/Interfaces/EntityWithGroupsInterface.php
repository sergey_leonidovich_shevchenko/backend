<?php declare(strict_types=1);


namespace App\Entity\Interfaces;

use FOS\UserBundle\Model\GroupableInterface;
use Doctrine\Common\Collections\Collection;

/**
 * Interface EntityWithGroupsInterface
 * Интерфейс указывающий, что у текущей сущность есть группы ролей
 * @package App\Entity\Interfaces
 */
interface EntityWithGroupsInterface extends GroupableInterface, EntityWithPermission
{
    /**
     * Добавить группу
     * @param string $group
     * @return mixed
     */
    public function addGroup($group);

    /**
     * Очистить список групп
     * @return mixed
     */
    public function clearGroups();

    /**
     * Получить список групп
     * @return Collection
     */
    public function getGroups(): Collection;

    /**
     * Проверить имеет ли сущность эту группу
     * @param string $group Группа на проверку
     * @return bool
     */
    public function hasGroup($group): bool;

    /**
     * Установить список групп (старые группы удалятся)
     * @param array $groups Список новых групп
     * @return mixed
     */
    public function setGroups(array $groups);

    /**
     * Удалить группу
     * @param $groups
     * @return $this|GroupableInterface
     */
    public function removeGroup($groups);
}

<?php declare(strict_types=1);


namespace App\Entity\Interfaces;

/**
 * Интерфейс указывающий, что у данной сущности присутствуют хоть какие нибудь пермишены
 * Interface EntityWithPermission
 * @package App\Entity\Interfaces
 */
interface EntityWithPermission
{

}

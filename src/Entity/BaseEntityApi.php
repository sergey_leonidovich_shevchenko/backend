<?php declare(strict_types=1);

namespace App\Entity;

use App\Entity\Interfaces\EntityInterface;

/**
 * Class BaseEntity
 * @package App\Entity
 */
abstract class BaseEntityApi implements EntityInterface
{
}

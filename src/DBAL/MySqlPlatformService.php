<?php declare(strict_types=1);

namespace App\DBAL;

use \Doctrine\DBAL\Platforms\MySqlPlatform;

/**
 * Class MySqlPlatformService
 * @package App\DBAL
 */
class MySqlPlatformService extends MySqlPlatform
{
    /**
     * Отключение создание внешних ключей в БД (тк используются партиционирование)
     * @return false
     */
    public function supportsForeignKeyConstraints(): bool
    {
        return false;
    }

    /**
     * Отключение создание внешних ключей в БД (тк используются партиционирование)
     * @return false
     */
    public function supportsForeignKeyOnUpdate(): bool
    {
        return false;
    }
}

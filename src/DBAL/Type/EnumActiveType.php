<?php declare(strict_types=1);

namespace App\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;

/**
 * Class EnumActiveType
 * @package App\DBAL\Type
 */
class EnumActiveType extends Type
{
    public const NAME = 'enum_active_type';

    public const STATUS_ACTIVE = 'active';

    public const STATUS_NOT_ACTIVE = 'not_active';

    public const STATUS_LIST = [
        self::STATUS_ACTIVE => self::STATUS_ACTIVE,
        self::STATUS_NOT_ACTIVE => self::STATUS_NOT_ACTIVE,
    ];

    /**
     * @param array            $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return sprintf(
            "ENUM('%s', '%s')",
            self::STATUS_ACTIVE,
            self::STATUS_NOT_ACTIVE
        );
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     * @return string
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): string
    {
        return $value;
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        if (!in_array($value, self::STATUS_LIST, true)) {
            throw new InvalidArgumentException('Invalid status');
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @param AbstractPlatform $platform
     * @return bool
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
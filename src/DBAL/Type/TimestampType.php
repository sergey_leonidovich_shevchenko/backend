<?php declare(strict_types=1);

namespace App\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class TimestampType extends Type
{

    const TIMESTAMP_TYPE_NAME = 'timestamp';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     * @param array            $fieldDeclaration The field declaration.
     * @param AbstractPlatform $platform         The currently used database platform.
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "TIMESTAMP";
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * Gets the name of this type.
     * @return string
     * @todo Needed?
     */
    public function getName()
    {
        return self::TIMESTAMP_TYPE_NAME;
    }
}
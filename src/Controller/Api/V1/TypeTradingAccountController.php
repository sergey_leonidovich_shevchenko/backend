<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\TypeTradingAccount;
use App\Exception\EntityException;
use App\Service\Api\TypeTradingAccountService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class TypeTradingAccountController
 * @package App\Controller\Api\V1
 * @Route({"/api/type_trading_account", "/api/v1/type_trading_account"})
 */
class TypeTradingAccountController extends BaseController
{
    /** @var TypeTradingAccountService */
    private $typeTradingAccountService;

    /**
     * TypeTradingAccountController constructor.
     * @param ContainerInterface        $container
     * @param TypeTradingAccountService $typeTradingAccountService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, TypeTradingAccountService $typeTradingAccountService)
    {
        parent::__construct($container);
        $this->typeTradingAccountService = $typeTradingAccountService;
    }

    /**
     * Получить количество типов торговых счетов
     * @IsGranted("ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.type_trading_account.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalTypeTradingAccount = $this->typeTradingAccountService->totalTypeTradingAccount();
        return $this->handleView(View::create(['total' => $totalTypeTradingAccount]));
    }

    /**
     * Получить список типов торговых счетов
     * @IsGranted("ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET")
     * @Route("/", methods={"GET"}, name="route.api.type_trading_account")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.type_trading_account.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $typeTradingAccounts = $this->typeTradingAccountService->getTypeTradingAccountList($page, $countPerPage);
        return $this->handleView($this->view($typeTradingAccounts));
    }

    /**
     * Добавить тип торгового счета
     * @IsGranted("ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_ADD")
     * @Route("/", methods={"POST"}, name="route.api.type_trading_account.add")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $typeTradingAccount = $this->typeTradingAccountService->createTypeTradingAccount($data);
            $view = $this->prepareEntityView($typeTradingAccount, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить тип торгового счета
     * @IsGranted("ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.type_trading_account.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $typeTradingAccount = $this->typeTradingAccountService->get($id);
        if (!$typeTradingAccount) {
            return $this->handleView($this->getNotFoundView(TypeTradingAccount::class, $id));
        }
        return $this->handleView($this->prepareEntityView($typeTradingAccount));
    }

    /**
     * Обновить тип торгового счета
     * @IsGranted("ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.type_trading_account.update")
     * @param Request            $request
     * @param TypeTradingAccount $typeTradingAccount
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, TypeTradingAccount $typeTradingAccount): Response
    {
        try {
            $newTypeTradingAccountData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $typeTradingAccount = $this->typeTradingAccountService->updateTypeTradingAccount($typeTradingAccount, $newTypeTradingAccountData);
            $view = $this->prepareEntityView($typeTradingAccount);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить тип торгового счета
     * @IsGranted("ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.type_trading_account.delete")
     * @param TypeTradingAccount $typeTradingAccount
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteAction(TypeTradingAccount $typeTradingAccount): Response
    {
        $this->typeTradingAccountService->deleteTypeTradingAccount($typeTradingAccount);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\Broker;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Service\Api\BrokerService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BrokerController
 * @package App\Controller\Api\V1
 * @Route({"/api/broker", "/api/v1/broker"})
 */
class BrokerController extends BaseController
{
    /** @var BrokerService */
    private $brokerService;

    /**
     * BrokerController constructor.
     * @param ContainerInterface $container
     * @param BrokerService      $brokerService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, BrokerService $brokerService)
    {
        parent::__construct($container);
        $this->brokerService = $brokerService;
        $this->events[EntityEvent::class] = $this->container->get(EntityEvent::class);
    }

    /**
     * Получить количество брокеров
     * @IsGranted("ROLE_ENTITY_API_BROKER_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.broker.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalBroker = $this->brokerService->totalBroker();
        return $this->handleView(View::create(['total' => $totalBroker]));
    }

    /**
     * Получить список брокеров
     * @IsGranted("ROLE_ENTITY_API_BROKER_GET")
     * @Route("/", methods={"GET"}, name="route.api.broker")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.broker.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $brokerList = $this->brokerService->getBrokerList($page, $countPerPage);
        return $this->handleView($this->view($brokerList));
    }

    /**
     * Добавить брокера
     * @IsGranted("ROLE_ENTITY_API_BROKER_ADD")
     * @Route("/", methods={"POST"}, name="route.api.broker.add")
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $broker = $this->brokerService->createBroker($data);
            $view = $this->prepareEntityView($broker, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить брокера
     * @IsGranted("ROLE_ENTITY_API_BROKER_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.broker.get")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $broker = $this->brokerService->get($id);
        if (!$broker) {
            return $this->handleView($this->getNotFoundView(Broker::class, $id));
        }

        return $this->handleView($this->prepareEntityView($broker));
    }

    /**
     * Обновить брокера
     * @IsGranted("ROLE_ENTITY_API_BROKER_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.broker.update")
     * @param Request $request
     * @param Broker $broker
     * @return Response
     */
    public function updateAction(Request $request, Broker $broker): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $broker = $this->brokerService->updateBroker($broker->getId(), $data);
            $view = $this->prepareEntityView($broker);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить брокера
     * @IsGranted("ROLE_ENTITY_API_BROKER_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.broker.delete")
     * @param Broker $broker
     * @return Response
     */
    public function deleteAction(Broker $broker): Response
    {
        $this->brokerService->deleteBroker($broker);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\SbCheckStatus;
use App\Exception\EntityException;
use App\Service\Api\SbCheckStatusService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class SbCheckStatusController
 * @package App\Controller\Api\V1
 * @Route({"/api/sb_check_status", "/api/v1/sb_check_status"})
 */
class SbCheckStatusController extends BaseController
{
    /** @var SbCheckStatusService */
    private $sbCheckStatusService;

    /**
     * SbCheckStatusController constructor.
     * @param ContainerInterface   $container
     * @param SbCheckStatusService $sbCheckStatusService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, SbCheckStatusService $sbCheckStatusService)
    {
        parent::__construct($container);
        $this->sbCheckStatusService = $sbCheckStatusService;
    }

    /**
     * Получить количество статусов проверки СБ
     * @IsGranted("ROLE_ENTITY_API_SB_CHECK_STATUS_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.sb_check_status.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalSbCheckStatus = $this->sbCheckStatusService->totalSbCheckStatus();
        return $this->handleView(View::create(['total' => $totalSbCheckStatus]));
    }

    /**
     * Получить список статусов проверки СБ
     * @IsGranted("ROLE_ENTITY_API_SB_CHECK_STATUS_GET")
     * @Route("/", methods={"GET"}, name="route.api.sb_check_status")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.sb_check_status.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $sbCheckStatus = $this->sbCheckStatusService->getSbCheckStatusList($page, $countPerPage);
        return $this->handleView($this->view($sbCheckStatus));
    }

    /**
     * Добавить статус проверки СБ
     * @IsGranted("ROLE_ENTITY_API_SB_CHECK_STATUS_ADD")
     * @Route("/", methods={"POST"}, name="route.api.sb_check_status.add")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $sbCheckStatus = $this->sbCheckStatusService->createSbCheckStatus($data);
            $view = $this->prepareEntityView($sbCheckStatus, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить статус проверки СБ
     * @IsGranted("ROLE_ENTITY_API_SB_CHECK_STATUS_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.sb_check_status.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $sbCheckStatus = $this->sbCheckStatusService->get($id);
        if (!$sbCheckStatus) {
            return $this->handleView($this->getNotFoundView(SbCheckStatus::class, $id));
        }
        return $this->handleView($this->prepareEntityView($sbCheckStatus));
    }

    /**
     * Обновить статус проверки СБ
     * @IsGranted("ROLE_ENTITY_API_SB_CHECK_STATUS_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.sb_check_status.update")
     * @param Request       $request
     * @param SbCheckStatus $sbCheckStatus
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, SbCheckStatus $sbCheckStatus): Response
    {
        try {
            $newSbCheckStatusData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $sbCheckStatus = $this->sbCheckStatusService->updateSbCheckStatus($sbCheckStatus, $newSbCheckStatusData);
            $view = $this->prepareEntityView($sbCheckStatus);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить статус проверки СБ
     * @IsGranted("ROLE_ENTITY_API_SB_CHECK_STATUS_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.sb_check_status.delete")
     * @param SbCheckStatus $sbCheckStatus
     * @return Response
     */
    public function deleteAction(SbCheckStatus $sbCheckStatus): Response
    {
        $this->sbCheckStatusService->deleteSbCheckStatus($sbCheckStatus);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

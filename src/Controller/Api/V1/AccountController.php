<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\Account;
use App\Exception\EntityException;
use App\Service\Api\AccountService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;
use JMS\Serializer\SerializationContext;

/**
 * Class AccountController
 * @package App\Controller\Api\V1
 * @Route({"/api/account", "/api/v1/account"})
 */
class AccountController extends BaseController
{
    /** @var AccountService */
    private $accountService;

    /**
     * AccountController constructor.
     * @param ContainerInterface $container
     * @param AccountService     $accountService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, AccountService $accountService)
    {
        parent::__construct($container);
        $this->accountService = $accountService;
    }

    /**
     * Получить количество счетов
     * @IsGranted("ROLE_ENTITY_API_ACCOUNT_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.account.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalAccount = $this->accountService->totalAccount();
        return $this->handleView(View::create(['total' => $totalAccount]));
    }

    /**
     * Получить список счетов
     * @IsGranted("ROLE_ENTITY_API_ACCOUNT_GET")
     * @Route("/", methods={"GET"}, name="route.api.account.list")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.account.list"
     * )
     * @param int|null $page
     * @param int|null $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $accounts = $this->accountService->getAccountList($page, $countPerPage);
        return $this->handleView($this->view($accounts));
    }

    /**
     * Добавить счет
     * @IsGranted("ROLE_ENTITY_API_ACCOUNT_ADD")
     * @Route("/", methods={"POST"}, name="route.api.account.add")
     * @param Request $request
     * @return Response
     * @throws Throwable
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $account = $this->accountService->createAccount($data);
            $context = SerializationContext::create()->setGroups('add');
            $view = $this->prepareEntityView($account, Response::HTTP_CREATED, [
                'context' => $context,
            ]);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить счет
     * @IsGranted("ROLE_ENTITY_API_ACCOUNT_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.account.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $account = $this->accountService->get($id);
        if (!$account) {
            return $this->handleView($this->getNotFoundView(Account::class, $id));
        }

        $context = SerializationContext::create()->setGroups('get');
        return $this->handleView($this->prepareEntityView($account, Response::HTTP_OK, [
            'context' => $context,
        ]));
    }

    /**
     * Обновить счет
     * @IsGranted("ROLE_ENTITY_API_ACCOUNT_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.account.update")
     * @param Request $request
     * @param Account $account
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, Account $account): Response
    {
        try {
            $newAccountData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $account = $this->accountService->updateAccount($account, $newAccountData);
            $context = SerializationContext::create()->setGroups('update');
            $view = $this->prepareEntityView($account, Response::HTTP_OK, [
                'context' => $context,
            ]);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить счет
     * @IsGranted("ROLE_ENTITY_API_ACCOUNT_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.account.delete")
     * @param Account $account
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteAction(Account $account): Response
    {
        $this->accountService->deleteAccount($account);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

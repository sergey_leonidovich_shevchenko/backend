<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\UserGroup;
use App\Exception\EntityException;
use App\Service\Api\UserGroupService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserGroupController
 * @package App\Controller\Api
 * @Route({"/api/user_group", "/api/v1/user_group"})
 */
class UserGroupController extends BaseController
{
    /** @var UserGroupService */
    private $userGroupService;

    /**
     * UserGroupController constructor.
     * @param ContainerInterface $container
     * @param UserGroupService   $userGroupService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, UserGroupService $userGroupService)
    {
        parent::__construct($container);
        $this->userGroupService = $userGroupService;
    }

    /**
     * Получить количество пользователей
     * @IsGranted("ROLE_ENTITY_API_USER_GROUP_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.user_group.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalUserGroup = $this->userGroupService->totalUserGroup();
        return $this->handleView(View::create(['total' => $totalUserGroup]));
    }

    /**
     * Получить список пользователей
     * @IsGranted("ROLE_ENTITY_API_USER_GROUP_GET")
     * @Route("/", methods={"GET"}, name="route.api.user_group")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.userGroup.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $userGroups = $this->userGroupService->getUserGroupList($page, $countPerPage);
        return $this->handleView($this->view($userGroups));
    }

    /**
     * Добавить пользователя
     * @IsGranted("ROLE_ENTITY_API_USER_GROUP_ADD")
     * @Route("/", methods={"POST"}, name="route.api.user_group.add")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $userGroup = $this->userGroupService->createUserGroup($data);
            $view = $this->prepareEntityView($userGroup, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить пользователя
     * @IsGranted("ROLE_ENTITY_API_USER_GROUP_GET")
     * @Route(
     *     path="/{idOrName}",
     *     requirements={"idOrName"="^([1-9][0-9]*)|([A-Z][0-9A-Z_\.]{1,31})$"},
     *     methods={"GET"},
     *     name="route.api.user_group.get",
     * )
     * @param         $idOrName
     * @param Request $request
     * @return Response
     */
    public function getAction($idOrName, Request $request): Response
    {
        $userGroup = $this->userGroupService->getUserGroup($idOrName);
        if (!$userGroup) {
            return $this->handleView(View::create([
                'code' => Response::HTTP_NOT_FOUND,
                'messages' => 'Not Found',
                'entity' => UserGroup::class,
                'idOrName' => $idOrName,
            ], Response::HTTP_NOT_FOUND));
        }
        return $this->handleView($this->prepareEntityView($userGroup));
    }

    /**
     * Обновить пользователя
     * @IsGranted("ROLE_ENTITY_API_USER_GROUP_UPDATE")
     * @Route(
     *     path="/{id}",
     *     requirements={"id"="^[1-9][0-9]*$"},
     *     methods={"PUT"},
     *     name="route.api.user_group.update",
     * )
     * @param Request $request
     * @param UserGroup $userGroup
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, UserGroup $userGroup): Response
    {
        try {
            $newUserGroupData = json_decode($request->getContent(), true, 3, JSON_THROW_ON_ERROR);
            $userGroup = $this->userGroupService->updateUserGroup($userGroup, $newUserGroupData);
            $view = $this->prepareEntityView($userGroup);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить пользовательскую группу по ее Id или названию
     * @IsGranted("ROLE_ENTITY_API_USER_GROUP_DELETE")
     * @Route(
     *     path="/{idOrName}",
     *     methods={"DELETE"},
     *     requirements={"idOrName"="^([1-9][0-9]*)|([a-z][0-9a-z_\.]{1,31})$"},
     *     name="route.api.user_group.delete",
     * )
     * @param int|string $idOrName
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteAction($idOrName): Response
    {
        $userGroup = $this->userGroupService->getUserGroup($idOrName);
        $this->userGroupService->deleteUserGroup($userGroup->getId());
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\TradingPlatform;
use App\Exception\EntityException;
use App\Service\Api\TradingPlatformService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use JMS\Serializer\SerializationContext;

/**
 * Class TradingPlatformController
 * @package App\Controller\Api\V1
 * @Route({"/api/trading_platform", "/api/v1/trading_platform"})
 */
class TradingPlatformController extends BaseController
{
    /** @var TradingPlatformService */
    private $tradingPlatformService;

    /**
     * TradingPlatformController constructor.
     * @param ContainerInterface     $container
     * @param TradingPlatformService $tradingPlatformService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, TradingPlatformService $tradingPlatformService)
    {
        parent::__construct($container);
        $this->tradingPlatformService = $tradingPlatformService;
    }

    /**
     * Получить количество торговых платформ
     * @IsGranted("ROLE_ENTITY_API_TRADING_PLATFORM_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.trading_platform.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalTradingPlatform = $this->tradingPlatformService->totalTradingPlatform();
        return $this->handleView(View::create(['total' => $totalTradingPlatform]));
    }

    /**
     * Получить список торговых платформ
     * @IsGranted("ROLE_ENTITY_API_TRADING_PLATFORM_GET")
     * @Route("/", methods={"GET"}, name="route.api.trading_platform")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.trading_platform.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $tradingPlatforms = $this->tradingPlatformService->getTradingPlatformList($page, $countPerPage);
        $context = SerializationContext::create()->setGroups('get');
        return $this->handleView($this->view($tradingPlatforms));
    }

    /**
     * Добавить торговую платформу
     * @IsGranted("ROLE_ENTITY_API_TRADING_PLATFORM_ADD")
     * @Route("/", methods={"POST"}, name="route.api.trading_platform.add")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 4, JSON_THROW_ON_ERROR);
            $tradingPlatform = $this->tradingPlatformService->createTradingPlatform($data);
            $context = SerializationContext::create()->setGroups('add');
            $view = $this->prepareEntityView($tradingPlatform, Response::HTTP_CREATED, [
                'context' => $context,
            ]);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить торговую платформу
     * @IsGranted("ROLE_ENTITY_API_TRADING_PLATFORM_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.trading_platform.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $tradingPlatform = $this->tradingPlatformService->get($id);
        if (!$tradingPlatform) {
            return $this->handleView($this->getNotFoundView(TradingPlatform::class, $id));
        }
        $context = SerializationContext::create()->setGroups('get');
        return $this->handleView($this->prepareEntityView($tradingPlatform, Response::HTTP_OK, [
            'context' => $context,
        ]));
    }

    /**
     * Обновить торговую платформу
     * @IsGranted("ROLE_ENTITY_API_TRADING_PLATFORM_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.trading_platform.update")
     * @param Request         $request
     * @param TradingPlatform $tradingPlatform
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, TradingPlatform $tradingPlatform): Response
    {
        try {
            $newTradingPlatformData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $tradingPlatform = $this->tradingPlatformService->updateTradingPlatform($tradingPlatform, $newTradingPlatformData);
            $context = SerializationContext::create()->setGroups('update');
            $view = $this->prepareEntityView($tradingPlatform, Response::HTTP_OK, [
                'context' => $context,
            ]);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить торговую платформу
     * @IsGranted("ROLE_ENTITY_API_TRADING_PLATFORM_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.trading_platform.delete")
     * @param TradingPlatform $tradingPlatform
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteAction(TradingPlatform $tradingPlatform): Response
    {
        $this->tradingPlatformService->deleteTradingPlatform($tradingPlatform);
        $context = SerializationContext::create()->setGroups('delete');
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT, [
            'context' => $context,
        ]));
    }
}

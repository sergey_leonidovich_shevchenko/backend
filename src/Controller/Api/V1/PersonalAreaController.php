<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\PersonalArea;
use App\Exception\EntityException;
use App\Service\Api\PersonalAreaService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class PersonalAreaController
 * @package App\Controller\Api\V1
 * @Route({"/api/personal_area", "/api/v1/personal_area"})
 */
class PersonalAreaController extends BaseController
{
    /** @var PersonalAreaService */
    private $personalAreaService;

    /**
     * PersonalAreaController constructor.
     * @param ContainerInterface  $container
     * @param PersonalAreaService $personalAreaService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, PersonalAreaService $personalAreaService)
    {
        parent::__construct($container);
        $this->personalAreaService = $personalAreaService;
    }

    /**
     * Получить количество личных кабинетов
     * @IsGranted("ROLE_ENTITY_API_PERSONAL_AREA_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.personal_area.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalPersonalArea = $this->personalAreaService->totalPersonalArea();
        return $this->handleView(View::create(['total' => $totalPersonalArea]));
    }

    /**
     * Получить список личных кабинетов
     * @IsGranted("ROLE_ENTITY_API_PERSONAL_AREA_GET")
     * @Route("/", methods={"GET"}, name="route.api.personal_area")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.personal_area.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $personalAreas = $this->personalAreaService->getPersonalAreaList($page, $countPerPage);
        return $this->handleView($this->view($personalAreas));
    }

    /**
     * Добавить личный кабинет
     * @IsGranted("ROLE_ENTITY_API_PERSONAL_AREA_ADD")
     * @Route("/", methods={"POST"}, name="route.api.personal_area.add")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $personalArea = $this->personalAreaService->createPersonalArea($data);
            $view = $this->prepareEntityView($personalArea, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->getErrorView($e->getErrorList());
        }

        return $this->handleView($view);
    }

    /**
     * Получить личный кабинет
     * @IsGranted("ROLE_ENTITY_API_PERSONAL_AREA_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.personal_area.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $personalArea = $this->personalAreaService->get($id);
        if (!$personalArea) {
            return $this->handleView($this->getNotFoundView(PersonalArea::class, $id));
        }
        return $this->handleView($this->prepareEntityView($personalArea));
    }

    /**
     * Обновить личный кабинет
     * @IsGranted("ROLE_ENTITY_API_PERSONAL_AREA_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.personal_area.update")
     * @param Request      $request
     * @param PersonalArea $personalArea
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, PersonalArea $personalArea): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $personalArea = $this->personalAreaService->updatePersonalArea($personalArea, $data);
            $view = $this->prepareEntityView($personalArea);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить личный кабинет
     * @IsGranted("ROLE_ENTITY_API_PERSONAL_AREA_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.personal_area.delete")
     * @param PersonalArea $personalArea
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteAction(PersonalArea $personalArea): Response
    {
        $this->personalAreaService->deletePersonalArea($personalArea);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

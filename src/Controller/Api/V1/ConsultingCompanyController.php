<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\ConsultingCompany;
use App\Exception\EntityException;
use App\Service\Api\ConsultingCompanyService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ConsultingCompanyController
 * @package App\Controller\Api\V1
 * @Route({"/api/consulting_company", "/api/v1/consulting_company"})
 */
class ConsultingCompanyController extends BaseController
{
    /** @var ConsultingCompanyService */
    private $consultingCompanyService;

    /**
     * ConsultingCompanyController constructor.
     * @param ContainerInterface       $container
     * @param ConsultingCompanyService $consultingCompanyService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, ConsultingCompanyService $consultingCompanyService)
    {
        parent::__construct($container);
        $this->consultingCompanyService = $consultingCompanyService;
    }

    /**
     * Получить количество консалтинговых компаний
     * @IsGranted("ROLE_ENTITY_API_CONSULTING_COMPANY_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.consulting_company.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalConsultingCompany = $this->consultingCompanyService->totalConsultingCompany();
        return $this->handleView(View::create(['total' => $totalConsultingCompany]));
    }

    /**
     * Получить список консалтинговых компаний
     * @IsGranted("ROLE_ENTITY_API_CONSULTING_COMPANY_GET")
     * @Route("/", methods={"GET"}, name="route.api.consulting_company")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.consulting_company.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $consultingCompany = $this->consultingCompanyService->getConsultingCompanyList($page, $countPerPage);
        return $this->handleView($this->view($consultingCompany));
    }

    /**
     * Добавить консалтинговую компанию
     * @IsGranted("ROLE_ENTITY_API_CONSULTING_COMPANY_ADD")
     * @Route("/", methods={"POST"}, name="route.api.consulting_company.add")
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $consultingCompany = $this->consultingCompanyService->createConsultingCompany($data);
            $view = $this->prepareEntityView($consultingCompany, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить консалтинговую компанию
     * @IsGranted("ROLE_ENTITY_API_CONSULTING_COMPANY_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.consulting_company.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $consultingCompany = $this->consultingCompanyService->get($id);
        if (!$consultingCompany) {
            return $this->handleView($this->getNotFoundView(ConsultingCompany::class, $id));
        }
        return $this->handleView($this->prepareEntityView($consultingCompany));
    }

    /**
     * Обновить консалтинговую компанию
     * @IsGranted("ROLE_ENTITY_API_CONSULTING_COMPANY_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.consulting_company.update")
     * @param Request $request
     * @param ConsultingCompany $consultingCompany
     * @return Response
     */
    public function updateAction(Request $request, ConsultingCompany $consultingCompany): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $consultingCompany = $this->consultingCompanyService->updateConsultingCompany($consultingCompany, $data);
            $view = $this->prepareEntityView($consultingCompany);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить консалтинговую компанию
     * @IsGranted("ROLE_ENTITY_API_CONSULTING_COMPANY_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.consulting_company.delete")
     * @param ConsultingCompany $consultingCompany
     * @return Response
     */
    public function deleteAction(ConsultingCompany $consultingCompany): Response
    {
        $this->consultingCompanyService->deleteConsultingCompany($consultingCompany);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

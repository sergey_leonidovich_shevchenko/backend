<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\SalesDepartment;
use App\Exception\EntityException;
use App\Service\Api\SalesDepartmentService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class SalesDepartmentController
 * @package App\Controller\Api\V1
 * @Route({"/api/sales_department", "/api/v1/sales_department"})
 */
class SalesDepartmentController extends BaseController
{
    /** @var SalesDepartmentService */
    private $salesDepartmentService;

    /**
     * SalesDepartmentController constructor.
     * @param ContainerInterface     $container
     * @param SalesDepartmentService $salesDepartmentService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, SalesDepartmentService $salesDepartmentService)
    {
        parent::__construct($container);
        $this->salesDepartmentService = $salesDepartmentService;
    }

    /**
     * Получить количество департаментов продаж
     * @IsGranted("ROLE_ENTITY_API_SALES_DEPARTMENT_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.sales_department.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalSalesDepartment = $this->salesDepartmentService->totalSalesDepartment();
        return $this->handleView(View::create(['total' => $totalSalesDepartment]));
    }

    /**
     * Получить список департаментов продаж
     * @IsGranted("ROLE_ENTITY_API_SALES_DEPARTMENT_GET")
     * @Route("/", methods={"GET"}, name="route.api.sales_department")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.sales_department.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $salesDepartments = $this->salesDepartmentService->getSalesDepartmentList($page, $countPerPage);
        return $this->handleView($this->view($salesDepartments));
    }

    /**
     * Добавить департамент продаж
     * @IsGranted("ROLE_ENTITY_API_SALES_DEPARTMENT_ADD")
     * @Route("/", methods={"POST"}, name="route.api.sales_department.add")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $salesDepartment = $this->salesDepartmentService->createSalesDepartment($data);
            $view = $this->prepareEntityView($salesDepartment, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить департамент продаж
     * @IsGranted("ROLE_ENTITY_API_SALES_DEPARTMENT_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.sales_department.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $salesDepartment = $this->salesDepartmentService->get($id);
        if (!$salesDepartment) {
            return $this->handleView($this->getNotFoundView(SalesDepartment::class, $id));
        }
        return $this->handleView($this->prepareEntityView($salesDepartment));
    }

    /**
     * Обновить департамент продаж
     * @IsGranted("ROLE_ENTITY_API_SALES_DEPARTMENT_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.sales_department.update")
     * @param Request         $request
     * @param SalesDepartment $salesDepartment
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, SalesDepartment $salesDepartment): Response
    {
        try {
            $newSalesDepartmentData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $salesDepartment = $this->salesDepartmentService->updateSalesDepartment($salesDepartment, $newSalesDepartmentData);
            $view = $this->prepareEntityView($salesDepartment);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить департамент продаж
     * @IsGranted("ROLE_ENTITY_API_SALES_DEPARTMENT_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.sales_department.delete")
     * @param SalesDepartment $salesDepartment
     * @return Response
     */
    public function deleteAction(SalesDepartment $salesDepartment): Response
    {
        $this->salesDepartmentService->deleteSalesDepartment($salesDepartment);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

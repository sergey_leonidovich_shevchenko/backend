<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\StatusVip;
use App\Exception\EntityException;
use App\Service\Api\StatusVipService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class StatusVipController
 * @package App\Controller\Api\V1
 * @Route({"/api/status_vip", "/api/v1/status_vip"})
 */
class StatusVipController extends BaseController
{
    /** @var StatusVipService */
    private $statusVipService;

    /**
     * StatusVipController constructor.
     * @param ContainerInterface $container
     * @param StatusVipService   $statusVipService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, StatusVipService $statusVipService)
    {
        parent::__construct($container);
        $this->statusVipService = $statusVipService;
    }

    /**
     * Получить количество VIP-статусов
     * @IsGranted("ROLE_ENTITY_API_STATUS_VIP_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.status_vip.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalStatusVip = $this->statusVipService->totalStatusVip();
        return $this->handleView(View::create(['total' => $totalStatusVip]));
    }

    /**
     * Получить список VIP-статусов
     * @IsGranted("ROLE_ENTITY_API_STATUS_VIP_GET")
     * @Route("/", methods={"GET"}, name="route.api.status_vip")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.status_vip.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $statusVips = $this->statusVipService->getStatusVipList($page, $countPerPage);
        return $this->handleView($this->view($statusVips));
    }

    /**
     * Добавить VIP-статус
     * @IsGranted("ROLE_ENTITY_API_STATUS_VIP_ADD")
     * @Route("/", methods={"POST"}, name="route.api.status_vip.add")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $statusVip = $this->statusVipService->createStatusVip($data);
            $view = $this->prepareEntityView($statusVip, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить VIP-статус
     * @IsGranted("ROLE_ENTITY_API_STATUS_VIP_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.status_vip.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $statusVip = $this->statusVipService->get($id);
        if (!$statusVip) {
            return $this->handleView($this->getNotFoundView(StatusVip::class, $id));
        }
        return $this->handleView($this->prepareEntityView($statusVip));
    }

    /**
     * Обновить VIP-статус
     * @IsGranted("ROLE_ENTITY_API_STATUS_VIP_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.status_vip.update")
     * @param Request   $request
     * @param StatusVip $statusVip
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, StatusVip $statusVip): Response
    {
        try {
            $newStatusVipData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $statusVip = $this->statusVipService->updateStatusVip($statusVip, $newStatusVipData);
            $view = $this->prepareEntityView($statusVip);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить VIP-статус
     * @IsGranted("ROLE_ENTITY_API_STATUS_VIP_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.status_vip.delete")
     * @param StatusVip $statusVip
     * @return Response
     */
    public function deleteAction(StatusVip $statusVip): Response
    {
        $this->statusVipService->deleteStatusVip($statusVip);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

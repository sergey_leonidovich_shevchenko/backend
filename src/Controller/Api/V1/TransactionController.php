<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\Transaction;
use App\Exception\EntityException;
use App\Service\Api\TransactionService;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class TransactionController
 * @package App\Controller\Api\V1
 * @Route({"/api/transaction", "/api/v1/transaction"})
 */
class TransactionController extends BaseController
{
    /** @var TransactionService */
    private $transactionService;

    /**
     * TransactionController constructor.
     * @param ContainerInterface $container
     * @param TransactionService $transactionService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, TransactionService $transactionService)
    {
        parent::__construct($container);
        $this->transactionService = $transactionService;
    }

    /**
     * Получить количество транзакций
     * @IsGranted("ROLE_ENTITY_API_TRANSACTION_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.transaction.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalTransaction = $this->transactionService->totalTransaction();
        return $this->handleView(View::create(['total' => $totalTransaction]));
    }

    /**
     * Получить список транзакций
     * @IsGranted("ROLE_ENTITY_API_TRANSACTION_GET")
     * @Route("/", methods={"GET"}, name="route.api.transaction")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.transaction.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $transactions = $this->transactionService->getTransactionList($page, $countPerPage);
        return $this->handleView($this->view($transactions));
    }

    /**
     * Добавить транзакцию
     * @IsGranted("ROLE_ENTITY_API_TRANSACTION_ADD")
     * @Route("/", methods={"POST"}, name="route.api.transaction.add")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $transaction = $this->transactionService->createTransaction($data);
            $view = $this->prepareEntityView($transaction, Response::HTTP_CREATED);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить транзакцию
     * @IsGranted("ROLE_ENTITY_API_TRANSACTION_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.transaction.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $transaction = $this->transactionService->get($id);
        if (!$transaction) {
            return $this->handleView($this->getNotFoundView(Transaction::class, $id));
        }
        return $this->handleView($this->prepareEntityView($transaction));
    }

    /**
     * Обновить транзакцию
     * @IsGranted("ROLE_ENTITY_API_TRANSACTION_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.transaction.update")
     * @param Request     $request
     * @param Transaction $transaction
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, Transaction $transaction): Response
    {
        try {
            $newTransactionData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $transaction = $this->transactionService->updateTransaction($transaction, $newTransactionData);
            $view = $this->prepareEntityView($transaction);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить транзакцию
     * @IsGranted("ROLE_ENTITY_API_TRANSACTION_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.transaction.delete")
     * @param Transaction $transaction
     * @return Response
     */
    public function deleteAction(Transaction $transaction): Response
    {
        $this->transactionService->deleteTransaction($transaction);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

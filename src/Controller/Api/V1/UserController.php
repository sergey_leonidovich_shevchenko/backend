<?php declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\BaseController;
use App\Entity\Api\User;
use App\Exception\EntityException;
use App\Service\Api\UserService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializationContext;

/**
 * Class UserController
 * @package App\Controller\Api\V1
 * @Route({"/api/user", "/api/v1/user"})
 */
class UserController extends BaseController
{
    /** @var UserService */
    private $userService;

    /**
     * UserController constructor.
     * @param ContainerInterface $container
     * @param UserService        $userService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, UserService $userService)
    {
        parent::__construct($container);
        $this->userService = $userService;
    }

    /**
     * Получить количество пользователей
     * @IsGranted("ROLE_ENTITY_API_USER_TOTAL")
     * @Route("/total", methods={"GET"}, name="route.api.user.total")
     * @return Response
     */
    public function totalAction(): Response
    {
        $totalUser = $this->userService->totalUser();
        return $this->handleView(View::create(['total' => $totalUser]));
    }

    /**
     * Получить список пользователей
     * @IsGranted("ROLE_ENTITY_API_USER_GET")
     * @Route("/", methods={"GET"}, name="route.api.user")
     * @Route(
     *     path="/page/{page}/count_per_page/{countPerPage}",
     *     requirements={"page"="^[1-9][0-9]*$", "countPerPage"="^[1-9][0-9]*$"},
     *     defaults={"page":1, "countPerPage":25},
     *     methods={"GET"},
     *     name="route.api.user.list"
     * )
     * @param int $page
     * @param int $countPerPage
     * @return Response
     */
    public function listAction(int $page = 1, int $countPerPage = 25): Response
    {
        $users = $this->userService->getUserList($page, $countPerPage);
        return $this->handleView($this->view($users));
    }

    /**
     * Добавить пользователя
     * @IsGranted("ROLE_ENTITY_API_USER_ADD")
     * @Route("/", methods={"POST"}, name="route.api.user.add")
     * @param Request $request
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $user = $this->userService->createUser($data);
            $context = SerializationContext::create()->setGroups('add');
            $view = $this->prepareEntityView($user, Response::HTTP_CREATED, [
                'context' => $context,
            ]);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Получить пользователя
     * @IsGranted("ROLE_ENTITY_API_USER_GET")
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.user.get")
     * @param Request $request
     * @return Response
     */
    public function getAction(Request $request): Response
    {
        $id = (int)$request->get('id');
        $user = $this->userService->get($id);
        if (!$user) {
            return $this->handleView($this->getNotFoundView(User::class, $id));
        }
        $context = SerializationContext::create()->setGroups('get');
        return $this->handleView($this->prepareEntityView($user, Response::HTTP_OK, [
            'context' => $context,
        ]));
    }

    /**
     * Обновить пользователя
     * @IsGranted("ROLE_ENTITY_API_USER_UPDATE")
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.user.update")
     * @param Request $request
     * @param User $user
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAction(Request $request, User $user): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $user = $this->userService->updateUser($user, $data);
            $context = SerializationContext::create()->setGroups('update');
            $view = $this->prepareEntityView($user, Response::HTTP_OK, [
                'context' => $context,
            ]);
        } catch (EntityException $e) {
            $view = $this->view([
                'status' => self::RESPONSE_ERROR,
                'messages' => $e->getErrorList(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Удалить пользователя
     * @IsGranted("ROLE_ENTITY_API_USER_DELETE")
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^[1-9][0-9]*$"}, name="route.api.user.delete")
     * @param User $user
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteAction(User $user): Response
    {
        $this->userService->deleteUser($user);
        return $this->handleView($this->prepareEntityView(null, Response::HTTP_NO_CONTENT));
    }
}

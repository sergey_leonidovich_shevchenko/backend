<?php declare(strict_types=1);


namespace App\Controller;

use App\Entity\Interfaces\EntityInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface BaseControllerInterface
 * @package App\Controller
 */
interface BaseControllerInterface
{
    /**
     * Отправить сущность на вьюху и получить ее типа "success"
     * @param EntityInterface|null $entity
     * @param int|null             $statusCode
     * @return View
     */
    public function prepareEntityView(?EntityInterface $entity = null, ?int $statusCode = Response::HTTP_OK): View;

    /**
     * Отправить список сообщений с шибками на вьюху и получить ее типа "error"
     * @param array $messages
     * @param int   $statusCode
     * @return View
     */
    public function getErrorView($messages = [], ?int $statusCode = Response::HTTP_BAD_REQUEST): View;
}

<?php declare(strict_types=1);

namespace App\Controller\Security;

use App\Controller\BaseController;
use App\Service\Api\Security\PermissionService;
use Exception;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PermissionController
 * @package App\Controller\Security
 * @Route("/security/permission")
 */
class PermissionController extends BaseController
{
    /** @var PermissionService */
    private $permissionService;

    /**
     * PermissionController constructor.
     * @param ContainerInterface $container
     * @param PermissionService  $permissionService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, PermissionService $permissionService)
    {
        parent::__construct($container);
        $this->permissionService = $permissionService;
    }

    /**
     * Добавить список ролей пользователю
     * @IsGranted("ROLE_SECURITY_PERMISSION_ADD_ROLES_TO_USER")
     * @Route(
     *     "/user/{userIdOrName}/role",
     *     requirements={"userIdOrName"="^([1-9][0-9]*)|([a-z][0-9a-z_\.]{1,31})$"},
     *     methods={"PUT"},
     *     name="security.permission.add.roles_to_user",
     *     format="json",
     * )
     * @param int|string $userIdOrName Id или name пользователя
     * @param Request    $request
     * @return Response
     */
    public function addPermissionToUserAction($userIdOrName, Request $request): Response
    {
        $rolesToAdd = $this->_getRolesFromRequest($request);
        try {
            $this->permissionService->addRolesToUser($userIdOrName, $rolesToAdd);
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Удалить список ролей у пользователя
     * @IsGranted("ROLE_SECURITY_PERMISSION_DELETE_ROLES_FROM_USER")
     * @Route(
     *     "/user/{userIdOrName}/role",
     *     requirements={"IdOrName"="^([1-9][0-9]*)|([a-z][0-9a-z_\.]{1,31})$"},
     *     methods={"DELETE"},
     *     name="security.permission.delete.role_from_user",
     *     format="json",
     * )
     * @param int|string $userIdOrName Id или name пользователя
     * @param Request    $request
     * @return Response
     * @throws Exception
     */
    public function deletePermissionFromUserAction($userIdOrName, Request $request): Response
    {
        try {
            $this->permissionService->deleteRolesFromUser($userIdOrName, $this->_getRolesFromRequest($request));
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Установить новый список ролей пользователю
     * @IsGranted("ROLE_SECURITY_PERMISSION_SET_ROLES_TO_USER")
     * @Route(
     *     "/user/{userIdOrName}/role",
     *     requirements={"userIdOrName"="^([1-9][0-9]*)|([a-z][0-9a-z_\.]{1,31})$"},
     *     methods={"POST"},
     *     name="security.permission.set.roles_to_user",
     *     format="json",
     * )
     * @param int|string $userIdOrName Id или name пользователя
     * @param Request    $request
     * @return Response
     * @throws Exception
     */
    public function setPermissionToUserAction($userIdOrName, Request $request): Response
    {
        try {
            $this->permissionService->setRolesToUser($userIdOrName, $this->_getRolesFromRequest($request));
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Добавить список ролей пользовательской группе
     * @IsGranted("ROLE_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP")
     * @Route(
     *     "/user_group/{userGroupIdOrName}/role",
     *     requirements={"groupIdOrName"="^([1-9][0-9]*)|([A-Z][0-9A-Z_\.]{1,31})$"},
     *     methods={"PUT"},
     *     name="security.permission.add.roles_to_user_group",
     *     format="json",
     * )
     * @param int|string $userGroupIdOrName Id или name пользовательской группы
     * @param Request    $request
     * @return Response
     * @throws Exception
     */
    public function addPermissionToUserGroupAction($userGroupIdOrName, Request $request): Response
    {
        try {
            $this->permissionService->addRolesToUserGroup($userGroupIdOrName, $this->_getRolesFromRequest($request));
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Удалить роль у пользовательской группы
     * @IsGranted("ROLE_SECURITY_PERMISSION_DELETE_ROLES_FROM_USER_GROUP")
     * @Route(
     *     "/user_group/{userGroupIdOrName}/role",
     *     requirements={"groupIdOrName"="^([1-9][0-9]*)|([A-Z][0-9A-Z_\.]{1,31})$"},
     *     methods={"DELETE"},
     *     name="security.permission.delete.role_from_user_group",
     *     format="json",
     * )
     * @param int|string $userGroupIdOrName Id или name пользовательской группы
     * @param Request    $request
     * @return Response
     * @throws Exception
     */
    public function deletePermissionFromUserGroupAction($userGroupIdOrName, Request $request): Response
    {
        try {
            $this->permissionService->deleteRolesFromUserGroup($userGroupIdOrName, $this->_getRolesFromRequest($request));
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Установить новый список ролей пользовательской группе
     * @IsGranted("ROLE_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP")
     * @Route(
     *     "/user_group/{userGroupIdOrName}/role",
     *     requirements={"userGroupIdOrName"="^([1-9][0-9]*)|([A-Z][0-9A-Z_\.]{1,31})$"},
     *     methods={"POST"},
     *     name="security.permission.set.roles_to_user_group",
     *     format="json",
     * )
     * @param int|string $userGroupIdOrName Id или name пользовательской группы
     * @param Request    $request
     * @return Response
     * @throws Exception
     */
    public function setPermissionToUserGroupAction($userGroupIdOrName, Request $request): Response
    {
        try {
            $this->permissionService->setRolesToUserGroup($userGroupIdOrName, $this->_getRolesFromRequest($request));
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Добавить пользовательские группы пользователю
     * @IsGranted("ROLE_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER")
     * @Route(
     *     "/user/{userIdOrName}/user_group",
     *     requirements={"userIdOrName"="^([1-9][0-9]*)|([a-z][0-9a-z_\.]{1,31})$"},
     *     methods={"PUT"},
     *     name="security.permission.add.user_group_to_user",
     *     format="json",
     * )
     * @param int|string $userIdOrName Id или name пользователя
     * @param Request    $request
     * @return Response
     * @throws Exception
     */
    public function addUserGroupToUserAction($userIdOrName, Request $request): Response
    {
        try {
            $this->permissionService->addUserGroupToUser($userIdOrName, $this->_getGroupsFromRequest($request));
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Удалить пользовательские группы у пользователя
     * @IsGranted("ROLE_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER")
     * @Route(
     *     "/user/{userIdOrName}/user_group",
     *     requirements={"userIdOrName"="^([1-9][0-9]*)|([a-z][0-9a-z_\.]{1,31})$"},
     *     methods={"DELETE"},
     *     name="security.permission.delete.user_group_to_user",
     *     format="json",
     * )
     * @param int|string $userIdOrName Id или name пользователя
     * @param Request    $request
     * @return Response
     * @throws Exception
     */
    public function deleteUserGroupFromUserAction($userIdOrName, Request $request): Response
    {
        try {
            $this->permissionService->deleteUserGroupFromUser($userIdOrName, $this->_getGroupsFromRequest($request));
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Установить новый список пользовательских групп у пользователя
     * @IsGranted("ROLE_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER")
     * @Route(
     *     "/user/{userIdOrName}/user_group",
     *     requirements={"userIdOrName"="^([1-9][0-9]*)|([a-z][0-9a-z_\.]{1,31})$"},
     *     methods={"POST"},
     *     name="security.permission.set.user_group_to_user",
     *     format="json",
     * )
     * @param int|string $userIdOrName Id или name пользователя
     * @param Request    $request
     * @return Response
     * @throws Exception
     */
    public function setUserGroupToUserAction($userIdOrName, Request $request): Response
    {
        try {
            $this->permissionService->setUserGroupToUser($userIdOrName, $this->_getGroupsFromRequest($request));
            $view = $this->prepareEntityView();
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Получить список ролей на установку с запроса
     * @param Request $request
     * @return mixed
     */
    private function _getGroupsFromRequest(Request $request): array
    {
        $data = json_decode($request->getContent(), true, 3, JSON_THROW_ON_ERROR);
        if (!isset($data['groups'])) {
            throw new RuntimeException('Key "groups" not found in request content.');
        }
        return $data['groups'] ?? [];
    }

    /**
     * Получить список ролей на установку с запроса
     * @param Request $request
     * @return mixed
     */
    private function _getRolesFromRequest(Request $request): array
    {
        $data = json_decode($request->getContent(), true, 3, JSON_THROW_ON_ERROR);
        if (!isset($data['roles'])) {
            throw new RuntimeException('Key "roles" not found in request content.');
        }
        return $data['roles'] ?? [];
    }
}

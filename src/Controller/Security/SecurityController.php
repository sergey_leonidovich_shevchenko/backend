<?php declare(strict_types=1);

namespace App\Controller\Security;

use App\Controller\BaseController;
use App\Service\Api\UserService;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SecurityController
 * @package App\Controller\Security
 * @Route("/security")
 */
class SecurityController extends BaseController
{
    /** @var UserService */
    private $userService;

    /**
     * SecurityController constructor.
     * @param ContainerInterface $container
     * @param UserService        $userService
     * @throws Exception
     */
    public function __construct(ContainerInterface $container, UserService $userService)
    {
        parent::__construct($container);
        $this->userService = $userService;
    }

    /**
     * Получение JWT токена авторизации
     * @Route("/login", name="route.security.security.login",  methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function loginAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $user = $this->userService->createUser($data);
            $view = $this->prepareEntityView($user, Response::HTTP_CREATED);
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }

    /**
     * Регистрация нового пользователя
     * @Route("/register", name="security.register",  methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function registerAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $user = $this->userService->createUser($data);
            $view = $this->prepareEntityView($user, Response::HTTP_CREATED);
        } catch (Exception $e) {
            $view = $this->getErrorView($e->getMessage());
        }

        return $this->handleView($view);
    }
}

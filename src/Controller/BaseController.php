<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Interfaces\EntityInterface;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use JMS\Serializer\Serializer;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use JMS\Serializer\SerializationContext;

/**
 * Class BaseController
 * @package App\Controller
 */
abstract class BaseController extends AbstractFOSRestController implements BaseControllerInterface
{
    /** @var string */
    public const RESPONSE_ERROR = 'error';

    /** @var string */
    public const RESPONSE_SUCCESS = 'success';

    /** @var Container|ContainerInterface */
    protected $container;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /** @var Event[] */
    protected $events = [];

    /** @var ParameterBag|ParameterBagInterface */
    protected $parameterBag;

    /** @var Request */
    protected $request;

    /** @var Serializer */
    protected $serializer;

    /**
     * BaseController constructor.
     * @param ContainerInterface $container
     * @throws Exception
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->eventDispatcher = $container->get('event_dispatcher');
        $this->parameterBag = $this->container->getParameterBag();
        $this->serializer = $container->get('jms_serializer');

        /** @var RequestStack $requestStack */
        $requestStack = $this->container->get('request_stack');
        $this->request = $requestStack->getMasterRequest();
    }

    /**
     * Отправить сущность на вьюху и получить ее типа "success"
     * @param EntityInterface|null $entity
     * @param int                  $statusCode
     * @param array                $options Опции
     * @return View
     */
    public function prepareEntityView(EntityInterface $entity = null, ?int $statusCode = Response::HTTP_OK, $options = []): View
    {
        $responseStatus = self::RESPONSE_SUCCESS;
        if ($entity === null) {
            return View::create(['status' => $responseStatus], $statusCode);
        }

        $context = $options['context'] ?? SerializationContext::create()->setGroups('Default');
        $entityAsArray = $this->serializer->toArray($entity, $context);

        return View::create([
            'status' => $responseStatus,
            'entity_name' => get_class($entity),
            'entity' => $entityAsArray,
        ], $statusCode);
    }

    /**
     * Отправить список сообщений с ошибками на вьюху и получить ее типа "error"
     * @param array $messages
     * @param int   $statusCode
     * @return View
     */
    public function getErrorView($messages = [], ?int $statusCode = Response::HTTP_BAD_REQUEST): View
    {
        return View::create([
            'status' => self::RESPONSE_ERROR,
            'messages' => $messages
        ], $statusCode);
    }

    /**
     * Получить вьюху когда не найдена сущность и получить ее типа "error"
     * @return View
     */
    public function getNotFoundView($entity, $id): View
    {
        return View::create([
            'code' => Response::HTTP_NOT_FOUND,
            'messages' => 'Not Found',
            'entity_name' => $entity,
            'id' => $id,
        ], Response::HTTP_NOT_FOUND);
    }
}

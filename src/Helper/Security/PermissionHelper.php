<?php declare(strict_types=1);

namespace App\Helper\Security;

use App\Helper\BaseHelper;
use RuntimeException;
use App\Repository\Api\UserGroupRepository;
use App\Entity\Api\UserGroup;

/**
 * Class PermissionHelper
 * @package App\Helper\Security
 */
class PermissionHelper extends BaseHelper
{
    public const RETURN_BOOLEAN_IF_ERROR = 'RETURN_BOOLEAN_IF_ERROR';

    public const RETURN_EXCEPTION_IF_ERROR = 'RETURN_EXCEPTION_IF_ERROR';

    /**
     * Проверить, что все роли в переданном списке существуют в системе
     * @param array  $groupsForCheck Список ролей на проверку
     * @param string $returnResult   Что делать если роль не найдена (@see self::RETURN_*_IF_ERROR)
     * @return bool
     */
    public function checkGroupsForExistence(array $groupsForCheck, string $returnResult = self::RETURN_BOOLEAN_IF_ERROR): bool
    {
        if (!empty($groupsForCheck)) {
            /** @var UserGroupRepository $userGroupRepository */
            $userGroupRepository = $this->em->getRepository(UserGroup::class);

            /** @var string[] $groupsForCheck */
            $groupsForCheck = array_unique($groupsForCheck);
            $groupsFound = $userGroupRepository->findBy(['name' => $groupsForCheck]);
            if (count($groupsForCheck) !== count($groupsFound)) {
                foreach ($groupsFound as $key => $groupFound) {
                    if ($keyIsset = array_search($groupsFound, $groupsForCheck, true)) {
                        unset($groupsForCheck[$keyIsset]);
                    }
                }
                if (!empty($groupsForCheck)) {
                    if ($returnResult === self::RETURN_EXCEPTION_IF_ERROR) {
                        $groupsForCheckStr = implode('", "', $groupsForCheck);
                        throw new RuntimeException("Указанных групп нет в системе! [\"$groupsForCheckStr\"]");
                    }
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Проверить, что все роли в переданном списке существуют в системе
     * @param array  $roleList     Список ролей на проверку
     * @param string $returnResult Что делать если роль не найдена (@see self::RETURN_*_IF_ERROR)
     * @return bool
     */
    public function checkRolesForExistence(array $roleList, string $returnResult = self::RETURN_BOOLEAN_IF_ERROR): bool
    {
        if (!empty($roleList)) {
            $roleHierarchy = $this->container->getParameter('security.role_hierarchy.roles');

            /** @var string $role */
            foreach (array_unique($roleList) as $role) {
                if (!array_search_recursive($role, $roleHierarchy)) {
                    if ($returnResult === self::RETURN_EXCEPTION_IF_ERROR) {
                        throw new RuntimeException("Указанной роли \"{$role}\" нет в системе!");
                    }
                    return false;
                }
            }
        }

        return true;
    }
}

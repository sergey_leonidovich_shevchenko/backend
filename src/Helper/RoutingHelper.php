<?php declare(strict_types=1);

namespace App\Helper;

use RuntimeException;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Router;

/**
 * Class RoutingHelper
 * @package App\Helper
 */
class RoutingHelper extends BaseHelper
{
    /**
     * Получить маршрут по его названию
     * @param string $routeName
     * @return string
     */
    public function getPathByName(string $routeName): string
    {
        /** @var $router Router */
        $router = $this->container->get('router');
        /** @var $collection RouteCollection */
        $collection = $router->getRouteCollection();
        $route = $collection->get($routeName);
        if ($route === null) {
            throw new RuntimeException("Route with name \"{$routeName}\" not found!");
        }

        return $route->getPath();
    }
}

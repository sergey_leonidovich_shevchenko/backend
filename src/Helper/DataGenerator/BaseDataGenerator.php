<?php declare(strict_types=1);

namespace App\Helper\DataGenerator;

use App\Helper\BaseHelper;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseDataGenerator
 * @package App\Helper\DataGenerator
 */
abstract class BaseDataGenerator extends BaseHelper
{
    protected const MYSQL_FIELD_DOUBLE_ACCURACY_MAX = 2147483;

    protected const MYSQL_FIELD_DOUBLE_SCALE_MAX = 2;

    /** @var Generator */
    protected $faker;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->faker = Factory::create();
    }
}

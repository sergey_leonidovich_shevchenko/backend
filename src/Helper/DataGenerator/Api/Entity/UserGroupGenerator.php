<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Helper\DataGenerator\BaseDataGenerator;
use App\Entity\Api\{
    User as UserEntity,
    UserGroup as UserGroupEntity,
};

/**
 * Class UserGroupGenerator
 * @package App\Service\Api\DataGenerator\Api\Entity
 */
final class UserGroupGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Пользовательская группа с ролями"
     * @return array
     */
    public function generateData(): array
    {
        return [
            'name' => $this->generateName(),
            'name_rus' => $this->generateName(),
            'description' => $this->generateDescription(),
            'roles' => $this->generateRoles(),
        ];
    }

    /**
     * Сгенерировать сущность "Пользовательская группа с ролями"
     * @return UserGroupEntity
     */
    public function generateEntity(): UserGroupEntity
    {
        /** @var UserGroupEntity $userGroupEntity */
        $userGroupEntity = $this->container->get(UserGroupEntity::class);
        $userGroupEntity->setName($this->generateName());
        $userGroupEntity->setNameRus($this->generateNameRus());
        $userGroupEntity->setDescription($this->generateDescription());
        $userGroupEntity->setRoles($this->generateRoles());
        return $userGroupEntity;
    }

    /**
     * Сгенерировать название
     * @return string
     */
    public function generateName(): string
    {
        return substr(str_replace('.', '', uniqid('name_', true)), 0, 32);
    }

    /**
     * Сгенерировать русское название
     * @return string
     */
    public function generateNameRus(): string
    {
        return substr(str_replace('.', '', uniqid('name_rus_', true)), 0, 32);
    }

    /**
     * Сгенерировать описание
     * @return string
     */
    public function generateDescription(): string
    {
        return $this->faker->realText();
    }

    /**
     * Сгенерировать список ролей
     * @return string[]
     */
    public function generateRoles(): array
    {
        return [
            UserEntity::ROLE_DEFAULT,
        ];
    }
}

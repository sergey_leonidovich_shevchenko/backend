<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Helper\DataGenerator\BaseDataGenerator;
use App\Entity\Api\SbCheckStatus as SbCheckStatusEntity;

/**
 * Class SbCheckStatusGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class SbCheckStatusGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности проверки статуса СБ
     * @return array
     */
    public function generateData(): array
    {
        return [
            'name' => $this->generateName(),
        ];
    }

    /**
     * Сгенерировать сущность "Статус проверки СБ"
     * @return SbCheckStatusEntity
     */
    public function generateEntity(): SbCheckStatusEntity
    {
        /** @var SbCheckStatusEntity $sbCheckStatusEntity */
        $sbCheckStatusEntity = $this->container->get(SbCheckStatusEntity::class);
        $sbCheckStatusEntity->setName($this->generateName());
        return $sbCheckStatusEntity;
    }

    /**
     * Сгенерировать название
     * @return string
     */
    public function generateName(): string
    {
        return str_replace('.', '', uniqid('name_', true));
    }
}

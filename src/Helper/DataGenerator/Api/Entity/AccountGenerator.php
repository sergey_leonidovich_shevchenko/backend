<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Entity\Api\Account as AccountEntity;
use App\Exception\EntityException;
use App\Helper\DataGenerator\BaseDataGenerator;
use App\Service\Api\{
    PersonalAreaService,
    SalesDepartmentService,
    TradingPlatformService,
    TypeTradingAccountService,
    UserService,
};
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Entity\Api\{
    User as UserEntity,
    TradingPlatform as TradingPlatformEntity,
    PersonalArea as PersonalAreaEntity,
    TypeTradingAccount as TypeTradingAccountEntity,
    SalesDepartment as SalesDepartmentEntity,
};
use App\Helper\DataGenerator\Api\Entity\{
    UserGenerator as UserGenerator,
    TradingPlatformGenerator as TradingPlatformGenerator,
    PersonalAreaGenerator as PersonalAreaGenerator,
    TypeTradingAccountGenerator as TypeTradingAccountGenerator,
    SalesDepartmentGenerator as SalesDepartmentGenerator,
};

/**
 * Class AccountGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class AccountGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Счет"
     * @param int $userId
     * @param int $tradingPlatformId
     * @param int $personalAreaId
     * @param int $typeTradingAccountId
     * @param int $salesDepartmentId
     * @return array
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function generateData(
        int $userId = null,
        int $tradingPlatformId = null,
        int $personalAreaId = null,
        int $typeTradingAccountId = null,
        int $salesDepartmentId = null
    ): array
    {
        if (!$userId) {
            $user = $this->_generateUser();
            $userId = $user->getId();
        }

        if (!$tradingPlatformId) {
            $tradingPlatform = $this->_generateTradingPlatform();
            $tradingPlatformId = $tradingPlatform->getId();
        }

        if (!$personalAreaId) {
            $personalArea = $this->_generatePersonalArea();
            $personalAreaId = $personalArea->getId();
        }

        if (!$typeTradingAccountId) {
            $typeTradingAccount = $this->_generateTypeTradingAccount();
            $typeTradingAccountId = $typeTradingAccount->getId();
        }

        if (!$salesDepartmentId) {
            $salesDepartment = $this->_generateSalesDepartment();
            $salesDepartmentId = $salesDepartment->getId();
        }

        return [
            'user' => $userId,
            'trading_platform' => $tradingPlatformId,
            'personal_area' => $personalAreaId,
            'type_trading_account' => $typeTradingAccountId,
            'sales_department' => $salesDepartmentId,
            'source_code' => $this->generateSourceCode(),
            'number' => $this->generateNumber(),
            'leverage' => $this->generateLeverage(),
        ];
    }

    /**
     * Сгенерировать сущность "Счет"
     * @param UserEntity               $user
     * @param TradingPlatformEntity    $tradingPlatform
     * @param PersonalAreaEntity       $personalArea
     * @param TypeTradingAccountEntity $typeTradingAccount
     * @param SalesDepartmentEntity    $salesDepartment
     * @return AccountEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function generateEntity(
        UserEntity $user = null,
        TradingPlatformEntity $tradingPlatform = null,
        PersonalAreaEntity $personalArea = null,
        TypeTradingAccountEntity $typeTradingAccount = null,
        SalesDepartmentEntity $salesDepartment = null
    ): AccountEntity
    {
        if (!$user) {
            $user = $this->_generateUser();
        }

        if (!$tradingPlatform) {
            $tradingPlatform = $this->_generateTradingPlatform();
        }

        if (!$personalArea) {
            $personalArea = $this->_generatePersonalArea();
        }

        if (!$typeTradingAccount) {
            $typeTradingAccount = $this->_generateTypeTradingAccount();
        }

        if (!$salesDepartment) {
            $salesDepartment = $this->_generateSalesDepartment();
        }

        /** @var AccountEntity $accountEntity */
        $accountEntity = $this->container->get(AccountEntity::class);
        $accountEntity->setUser($user);
        $accountEntity->setTradingPlatform($tradingPlatform);
        $accountEntity->setPersonalArea($personalArea);
        $accountEntity->setTypeTradingAccount($typeTradingAccount);
        $accountEntity->setSalesDepartment($salesDepartment);
        $accountEntity->setLeverage($this->generateLeverage());
        $accountEntity->setSourceCode($this->generateSourceCode());
        $accountEntity->setNumber($this->generateNumber());

        return $accountEntity;
    }

    /**
     * Сгенерировать плечо
     * @return int
     */
    public function generateLeverage(): int
    {
        return $this->faker->randomNumber(8);
    }

    /**
     * Сгенерировать номер счета
     * @return int
     */
    public function generateNumber(): int
    {
        return $this->faker->randomNumber(8);
    }

    /**
     * Сгенерировать место откуда был создан счет
     * @return string
     */
    public function generateSourceCode(): string
    {
        return $this->faker->randomElement(AccountEntity::SOURCE_CODE_LIST);
    }

    /**
     * Сгенерировать нового пользователя
     * @return UserEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function _generateUser(): UserEntity
    {
        /** @var UserGenerator $userDataGenerator */
        $userDataGenerator = $this->container->get(UserGenerator::class);
        /** @var UserService $userService */
        $userService = $this->container->get(UserService::class);
        return $userService->createUser($userDataGenerator->generateData());
    }

    /**
     * Сгенерировать новую торговую платформу
     * @return TradingPlatformEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function _generateTradingPlatform(): TradingPlatformEntity
    {
        /** @var TradingPlatformGenerator $tradingPlatformDataGenerator */
        $tradingPlatformDataGenerator = $this->container->get(TradingPlatformGenerator::class);
        /** @var TradingPlatformService $tradingPlatformService */
        $tradingPlatformService = $this->container->get(TradingPlatformService::class);
        return $tradingPlatformService->createTradingPlatform($tradingPlatformDataGenerator->generateData());
    }

    /**
     * Сгенерировать новый личный кабинет
     * @return PersonalAreaEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function _generatePersonalArea(): PersonalAreaEntity
    {
        /** @var PersonalAreaGenerator $personalAreaDataGenerator */
        $personalAreaDataGenerator = $this->container->get(PersonalAreaGenerator::class);
        /** @var PersonalAreaService $personalAreaService */
        $personalAreaService = $this->container->get(PersonalAreaService::class);
        return $personalAreaService->createPersonalArea($personalAreaDataGenerator->generateData());
    }

    /**
     * Сгенерировать новый тип торговой платформы
     * @return TypeTradingAccountEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function _generateTypeTradingAccount(): TypeTradingAccountEntity
    {
        /** @var TypeTradingAccountGenerator $typeTradingAccountDataGenerator */
        $typeTradingAccountDataGenerator = $this->container->get(TypeTradingAccountGenerator::class);
        /** @var TypeTradingAccountService $typeTradingAccountService */
        $typeTradingAccountService = $this->container->get(TypeTradingAccountService::class);
        return $typeTradingAccountService->createTypeTradingAccount($typeTradingAccountDataGenerator->generateData());
    }

    /**
     * Сгенерировать новый департамент продаж
     * @return SalesDepartmentEntity
     * @throws EntityException
     * @throws ORMException
     */
    private function _generateSalesDepartment(): SalesDepartmentEntity
    {
        /** @var SalesDepartmentGenerator $salesDepartmentDataGenerator */
        $salesDepartmentDataGenerator = $this->container->get(SalesDepartmentGenerator::class);
        /** @var SalesDepartmentService $salesDepartmentService */
        $salesDepartmentService = $this->container->get(SalesDepartmentService::class);
        return $salesDepartmentService->createSalesDepartment($salesDepartmentDataGenerator->generateData());
    }
}

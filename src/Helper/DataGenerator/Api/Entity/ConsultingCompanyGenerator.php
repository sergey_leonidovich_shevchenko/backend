<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Helper\DataGenerator\BaseDataGenerator;
use App\Entity\Api\ConsultingCompany as ConsultingCompanyEntity;

/**
 * Class ConsultingCompanyGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class ConsultingCompanyGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Консалтинговая компания"
     * @return array
     */
    public function generateData(): array
    {
        return [
            'broker_list' => [], // FIXME: сделать проверку на список брокеров
            'code' => $this->generateCode(),
            'url' => $this->generateUrl(),
        ];
    }

    /**
     * Сгенерировать сущность "Консалтинговая компания"
     * @return ConsultingCompanyEntity
     */
    public function generateEntity(): ConsultingCompanyEntity
    {
        /** @var ConsultingCompanyEntity $consultingCompanyEntity */
        $consultingCompanyEntity = $this->container->get(ConsultingCompanyEntity::class);
        $consultingCompanyEntity->setCode($this->generateCode());
        $consultingCompanyEntity->setUrl($this->generateUrl());
        return $consultingCompanyEntity;
    }

    /**
     * Сгенерировать код консалтинговой компании
     * @return string|string[]
     */
    public function generateCode()
    {
        return str_replace('.', '', uniqid('code_', true));
    }

    /**
     * Сгенерировать URL
     * @return string
     */
    public function generateUrl(): string
    {
        return $this->faker->url;
    }
}

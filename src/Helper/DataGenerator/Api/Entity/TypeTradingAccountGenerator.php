<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Helper\DataGenerator\BaseDataGenerator;
use App\Entity\Api\TypeTradingAccount as TypeTradingAccountEntity;

/**
 * Class TypeTradingAccountGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class TypeTradingAccountGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Тип торгового счета"
     * @return array
     */
    public function generateData(): array
    {
        return [
            'code' => $this->generateCode(),
        ];
    }

    /**
     * Сгенерировать сущность "Тип торгового счета"
     * @return TypeTradingAccountEntity
     */
    public function generateEntity(): TypeTradingAccountEntity
    {
        /** @var TypeTradingAccountEntity $typeTradingAccountEntity */
        $typeTradingAccountEntity = $this->container->get(TypeTradingAccountEntity::class);
        $typeTradingAccountEntity->setCode($this->generateCode());
        return $typeTradingAccountEntity;
    }

    /**
     * Сгенерировать код
     * @return string
     */
    public function generateCode(): string
    {
        return str_replace('.', '', uniqid('code_', true));
    }
}

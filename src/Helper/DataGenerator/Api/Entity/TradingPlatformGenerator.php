<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Helper\DataGenerator\BaseDataGenerator;
use App\Entity\Api\TradingPlatform as TradingPlatformEntity;

/**
 * Class TradingPlatformGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class TradingPlatformGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Торговая платформа"
     * @return array
     */
    public function generateData(): array
    {
        return [
            'balance_server' => $this->generateBalanceServer(),
            'code' => $this->generateCode(),
            'crypt' => $this->generateCrypt(),
            'group' => $this->generateGroup(),
            'login' => $this->generateLogin(),
            'password' => $this->generatePassword(10),
            'port' => $this->generatePort(),
            'server' => $this->generateServer(),
            'timeout' => $this->generateTimeout(),
        ];
    }

    /**
     * Сгенерировать сущность "Торговая платформа"
     * @return TradingPlatformEntity
     */
    public function generateEntity(): TradingPlatformEntity
    {
        /** @var TradingPlatformEntity $tradingPlatformEntity */
        $tradingPlatformEntity = $this->container->get(TradingPlatformEntity::class);
        $tradingPlatformEntity->setGroup($this->generateGroup());
        $tradingPlatformEntity->setPort($this->generatePort());
        $tradingPlatformEntity->setServer($this->generateServer());
        $tradingPlatformEntity->setCode($this->generateCode());
        $tradingPlatformEntity->setTimeout($this->generateTimeout());
        $tradingPlatformEntity->setPassword($this->generatePassword(10));
        $tradingPlatformEntity->setBalance($this->generateBalanceServer());
        $tradingPlatformEntity->setLogin($this->generateLogin());
        $tradingPlatformEntity->setCrypt($this->generateCrypt());
        return $tradingPlatformEntity;
    }

    /**
     * Сгенерировать группу
     * @return string
     */
    public function generateGroup(): string
    {
        return $this->container->getParameter('mt.user_group.default');
    }

    /**
     * Сгенерировать порт
     * @return int
     */
    public function generatePort(): int
    {
        return $this->faker->numberBetween(1000, 65535);
    }

    /**
     * Сгенерировать сервер
     * @return string
     */
    public function generateServer(): string
    {
        return $this->faker->url;
    }

    /**
     * Сгенерировать таймаут
     * @return int
     */
    public function generateTimeout(): int
    {
        return $this->faker->randomNumber();
    }

    /**
     * Сгенерировать пароль
     * @param int $length Длина пароля
     * @return string
     */
    public function generatePassword(int $length): string
    {
        return $this->faker->password($length);
    }

    /**
     * Сгенерировать массив с параметрами для подключения к серверу для получения баланса
     * @return array
     */
    public function generateBalanceServer(): array
    {
        return [
            'server' => $this->container->getParameter('mt.sql.server.db_host'),
            'DB' => $this->container->getParameter('mt.sql.server.db_name'),
            'user' => $this->container->getParameter('mt.sql.server.db_user'),
            'password' => $this->container->getParameter('mt.sql.server.db_password'),
            'port' => $this->container->getParameter('mt.sql.server.db_port'),
        ];
    }

    /**
     * Сгенерировать логин
     * @return string
     */
    public function generateLogin(): string
    {
        return (string)$this->faker->randomNumber();
    }

    /**
     * Сгенерировать флаг криптовать ли
     * @return bool
     */
    public function generateCrypt(): bool
    {
        return $this->faker->boolean;
    }

    /**
     * Сгенерировать код
     * @return string
     */
    public function generateCode(): string
    {
        return str_replace('.', '', uniqid('code_', true));
    }
}

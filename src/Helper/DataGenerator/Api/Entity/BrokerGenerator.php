<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Helper\DataGenerator\BaseDataGenerator;
use App\Entity\Api\Broker as BrokerEntity;

/**
 * Class BrokerGenerator
 * @package App\Service\Api\DataGenerator\Api\Entity
 */
final class BrokerGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Брокер"
     * @return array
     */
    public function generateData(): array
    {
        return [
            'name' => $this->generateName(),
            'setting' => $this->generateSetting(),
            'consulting_company_list' => [],
        ];
    }

    /**
     * Сгенерировать сущность "Брокер"
     * @return BrokerEntity
     */
    public function generateEntity(): BrokerEntity
    {
        /** @var BrokerEntity $broker */
        $brokerEntity = $this->container->get(BrokerEntity::class);
        $brokerEntity->setName($this->generateName());
        $brokerEntity->setSetting($this->generateSetting());
        return $brokerEntity;
    }

    /**
     * Сгенерировать название брокера
     * @return string|string[]
     */
    public function generateName()
    {
        return str_replace('.', '', uniqid('name_', true));
    }

    /**
     * Сгенерировать настройки для брокера
     * @return array
     */
    public function generateSetting(): array
    {
        return [
            'random_text_1' => $this->faker->words($this->faker->numberBetween(3, 20), true),
            'random_text_2' => $this->faker->words($this->faker->numberBetween(3, 20), true),
            'random_text_3' => $this->faker->words($this->faker->numberBetween(3, 20), true),
        ];
    }
}

<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Helper\DataGenerator\BaseDataGenerator;
use App\Entity\Api\StatusVip as StatusVipEntity;

/**
 * Class StatusVipGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class StatusVipGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности Vip-статус
     * @return array
     */
    public function generateData(): array
    {
        return [
            'name' => $this->generateName(),
            'sum_for_check_status' => $this->generateSumForCheckStatus(),
        ];
    }

    /**
     * Сгенерировать сущность "Статус проверки СБ"
     * @return StatusVipEntity
     */
    public function generateEntity(): StatusVipEntity
    {
        /** @var StatusVipEntity $statusVipEntity */
        $statusVipEntity = $this->container->get(StatusVipEntity::class);
        $statusVipEntity->setName($this->generateName());
        $statusVipEntity->setSumForCheckStatus($this->generateSumForCheckStatus());
        return $statusVipEntity;
    }

    /**
     * Сгенерировать название
     * @return string
     */
    public function generateName(): string
    {
        return str_replace('.', '', uniqid('name_', true));
    }

    /**
     * Сгенерировать сумму проверки статуса
     * @return float
     */
    public function generateSumForCheckStatus(): float
    {
        if (!defined('SOME_FLOAT')) {
            // Если ниже генерируется число 0.0, то "(string)$float === 0" и программа падает
            define('SOME_FLOAT', 1.01);
        }
        $float = $this->faker->randomFloat(self::MYSQL_FIELD_DOUBLE_SCALE_MAX) + SOME_FLOAT;
        [$_, $fractionalPart] = explode('.', (string)$float);
        $wholePart = (string)$this->faker->numberBetween(0, self::MYSQL_FIELD_DOUBLE_ACCURACY_MAX - 1);
        $wholePart = substr($wholePart, 0, self::MYSQL_FIELD_DOUBLE_ACCURACY_MAX - self::MYSQL_FIELD_DOUBLE_SCALE_MAX);
        return (float)"{$wholePart}.{($fractionalPart}";
    }
}

<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Entity\Api\Transaction as TransactionEntity;
use App\Helper\DataGenerator\BaseDataGenerator;
use App\Service\Api\AccountService;
use DateTime;
use Throwable;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Exception\EntityException;
use App\Entity\Api\Account as AccountEntity;

/**
 * Class TransactionGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class TransactionGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности транзакции
     * @param int|null $accountId
     * @return array
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Throwable
     */
    public function generateData(int $accountId = null): array
    {
        if (!$accountId) {
            $account = $this->_generateAccount();
            $accountId = $account->getId();
        }

        return [
            'account' => $accountId,
            'outer_system_id' => $this->generateOuterSystemId(),
            'reference_id' => $this->generateReferenceId(),
            'livemode' => $this->generateLivemode(),
            'status' => $this->generateStatus(),
            'amount' => $this->generateAmount(),
            'amount_text' => $this->generateAmountText(),
            'currency_id' => $this->generateCurrencyId(),
            'description' => $this->generateDescription(),
            'ip' => $this->generateIp(),
            'email' => $this->generateEmail(),
            'source' => $this->generateSource(),
            'failure_code' => $this->generateFailureCode(),
            'failure_message' => $this->generateFailureMessage(),
            'valid_till' => $this->generateValidTill(),
            'pay_method' => $this->generatePayMethod(),
        ];
    }

    /**
     * Сгенерировать сущность "Транзакция"
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Throwable
     */
    public function generateEntity(): TransactionEntity
    {
        /** @var TransactionEntity $transactionEntity */
        $transactionEntity = $this->container->get(TransactionEntity::class);
        $transactionEntity->setAccount($this->_generateAccount());
        $transactionEntity->setOuterSystemId($this->generateOuterSystemId());
        $transactionEntity->setReferenceId($this->generateReferenceId());
        $transactionEntity->setLivemode($this->generateLivemode());
        $transactionEntity->setStatus($this->generateStatus());
        $transactionEntity->setAmount($this->generateAmount());
        $transactionEntity->setAmountText($this->generateAmountText());
        $transactionEntity->setCurrencyId($this->generateCurrencyId());
        $transactionEntity->setDescription($this->generateDescription());
        $transactionEntity->setIp($this->generateIp());
        $transactionEntity->setEmail($this->generateEmail());
        $transactionEntity->setSource($this->generateSource());
        $transactionEntity->setFailureCode($this->generateFailureCode());
        $transactionEntity->setFailureMessage($this->generateFailureMessage());
        $transactionEntity->setValidTill(new DateTime($this->generateValidTill()));
        $transactionEntity->setPayMethod($this->generatePayMethod());
        return $transactionEntity;
    }

    /**
     * Сгенерировать сущность "Счет"
     * @return AccountEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Throwable
     */
    private function _generateAccount(): AccountEntity
    {
        /** @var AccountGenerator $accountDataGenerator */
        $accountDataGenerator = $this->container->get(AccountGenerator::class);
        /** @var AccountService $accountService */
        $accountService = $this->container->get(AccountService::class);
        return $accountService->createAccount($accountDataGenerator->generateData());
    }

    /**
     * Сгенерировать ID во внешней системе
     * @return string
     */
    public function generateOuterSystemId(): string
    {
        return str_replace('.', '', uniqid('outer_system_id_', true));
    }

    /**
     * Сгенерировать числовой показатель ID
     * @return int
     */
    public function generateReferenceId(): int
    {
        return (int)$this->faker->randomNumber();
    }

    /**
     * Сгенерировать livemode
     * @return string
     */
    public function generateLivemode(): string
    {
        return (string)$this->faker->randomElement([0, 1]);
    }

    /**
     * Сгенерировать статус
     * @return string
     */
    public function generateStatus(): string
    {
        return $this->faker->randomElement(TransactionEntity::STATUS_CODE_LIST);
    }

    /**
     * Сгенерировать сумму платежа
     * @return float
     */
    public function generateAmount(): float
    {
        if (!defined('SOME_FLOAT')) {
            // Если ниже генерируется число 0.0, то "(string)$float === 0" и программа падает
            define('SOME_FLOAT', 1.01);
        }
        $float = $this->faker->randomFloat(self::MYSQL_FIELD_DOUBLE_SCALE_MAX) + SOME_FLOAT;
        [$_, $fractionalPart] = explode('.', (string)$float);
        $wholePart = (string)$this->faker->numberBetween(0, self::MYSQL_FIELD_DOUBLE_ACCURACY_MAX - 1);
        $wholePart = substr($wholePart, 0, self::MYSQL_FIELD_DOUBLE_ACCURACY_MAX - self::MYSQL_FIELD_DOUBLE_SCALE_MAX);
        return (float)"{$wholePart}.{($fractionalPart}";
    }

    /**
     * Сгенерировать сумму платежа текстом
     * @return string
     */
    public function generateAmountText(): string
    {
        return $this->faker->text();
    }

    /**
     * Сгенерировать ???
     * @return string
     */
    public function generateCurrencyId(): string
    {
        return str_replace('.', '', uniqid('currency_id_', true));
    }

    /**
     * Сгенерировать описание
     * @return string
     */
    public function generateDescription(): string
    {
        return $this->faker->realText();
    }

    /**
     * Сгенерировать IP
     * @return int
     */
    public function generateIp(): int
    {
        return ip2long($this->faker->ipv4);
    }

    /**
     * Сгенерировать email
     * @return string
     */
    public function generateEmail(): string
    {
        return $this->faker->email;
    }

    /**
     * Сгенерировать спам от платежек
     * @return string
     */
    public function generateSource(): string
    {
        return $this->faker->realText();
    }

    /**
     * Сгенерировать код ошибки
     * @return string
     */
    public function generateFailureCode(): string
    {
        return str_replace('.', '', uniqid('failure_code_', true));
    }

    /**
     * Сгенерировать сообщение ошибки
     * @return string
     */
    public function generateFailureMessage(): string
    {
        return $this->faker->text();
    }

    /**
     * Сгенерировать дату с временем до которого ожидается оплата
     * @return string
     */
    public function generateValidTill(): string
    {
        return $this->faker->dateTime()->format('Y-m-d');
    }

    /**
     * Сгенерировать метод оплаты
     * @return string
     */
    public function generatePayMethod(): string
    {
        return str_replace('.', '', uniqid('pay_method_', true));
    }
}

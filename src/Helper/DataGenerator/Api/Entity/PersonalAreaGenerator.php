<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Entity\Api\PersonalArea as PersonalAreaEntity;
use App\Exception\EntityException;
use App\Helper\DataGenerator\BaseDataGenerator;
use App\Helper\DataGenerator\Api\Entity\{
    BrokerGenerator as BrokerGenerator,
    SbCheckStatusGenerator as SbCheckStatusGenerator,
    StatusVipGenerator as StatusVipGenerator,
};
use App\Service\Api\{
    BrokerService,
    SbCheckStatusService,
    StatusVipService,
};
use Doctrine\ORM\{
    OptimisticLockException,
    ORMException,
};
use App\Entity\Api\{
    SbCheckStatus as SbCheckStatusEntity,
    Broker as BrokerEntity,
    StatusVip as StatusVipEntity
};
use DateTime;

/**
 * Class PersonalAreaGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class PersonalAreaGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Личный кабинет"
     * @param int $brokerId
     * @param int $sbCheckStatusId
     * @param int $statusVipId
     * @return array
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function generateData(?int $brokerId = null, ?int $sbCheckStatusId = null, ?int $statusVipId = null): array
    {
        if (!$brokerId) {
            $broker = $this->_generateBroker();
            $brokerId = $broker->getId();
        }

        if (!$sbCheckStatusId) {
            $sbCheckStatus = $this->_generateSbCheckStatus();
            $sbCheckStatusId = $sbCheckStatus->getId();
        }

        if (!$statusVipId) {
            $statusVip = $this->_generateStatusVip();
            $statusVipId = $statusVip->getId();
        }

        return [
            'broker' => $brokerId,
            'sb_check_status' => $sbCheckStatusId,
            'status_vip' => $statusVipId,
            'surname' => $this->generateSurname(),
            'name' => $this->generateName(),
            'middle_name' => $this->generateMiddleName(),
            'email' => $this->generateEmail(),
            'phone' => $this->generatePhone(),
            'birthday' => $this->generateBirthday(),
            'usdt_tether' => $this->generateUsdtTether(),
            'active' => $this->generateActive(),
        ];
    }

    /**
     * Сгенерировать новую сущность "Личный кабинет"
     * @return PersonalAreaEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function generateEntity(): PersonalAreaEntity
    {
        /** @var PersonalAreaEntity $personalAreaEntity */
        $personalAreaEntity = $this->container->get(PersonalAreaEntity::class);
        $personalAreaEntity->setBroker($this->_generateBroker());
        $personalAreaEntity->setStatusVip($this->_generateStatusVip());
        $personalAreaEntity->setSbCheckStatus($this->_generateSbCheckStatus());
        $personalAreaEntity->setSurname($this->generateSurname());
        $personalAreaEntity->setName($this->generateName());
        $personalAreaEntity->setMiddleName($this->generateMiddleName());
        $personalAreaEntity->setEmail($this->generateEmail());
        $personalAreaEntity->setPhone($this->generatePhone());
        $personalAreaEntity->setBirthday((new DateTime())->setTimestamp($this->generateBirthday()));
        $personalAreaEntity->setUsdtTether($this->generateUsdtTether());
        $personalAreaEntity->setActive($this->generateActive());
        return $personalAreaEntity;
    }

    /**
     * Сгенерировать фамилию
     * @return string
     */
    public function generateSurname(): string
    {
        return str_replace('.', '', uniqid('surname_', true));
    }

    /**
     * Сгенерировать имя
     * @return string
     */
    public function generateName(): string
    {
        return str_replace('.', '', uniqid('name_', true));
    }

    /**
     * Сгенерировать отчество
     * @return string
     */
    public function generateMiddleName(): string
    {
        return str_replace('.', '', uniqid('middle_name_', true));
    }

    /**
     * Сгенерировать email
     * @return string
     */
    public function generateEmail(): string
    {
        return $this->faker->email;
    }

    /**
     * Сгенерировать телефон
     * @return string
     */
    public function generatePhone(): string
    {
        return $this->faker->randomElement([
            '+7' . time(),
            '+9' . time(),
            '+99' . time(),
            '+999' . time(),
            '+9999' . time(),
        ]);
    }

    /**
     * Сгенерировать дату дня рождения
     * @return string
     */
    public function generateBirthday(): string
    {
        return $this->faker->dateTime()->format('Y-m-d');
    }

    /**
     * Сгенерировать Tether-токен
     * @return string
     */
    public function generateUsdtTether(): string
    {
        return str_replace('.', '', uniqid('usdt_tether_', true));
    }

    /**
     * Сгенерировать активность
     * @return string
     */
    public function generateActive(): string
    {
        return $this->faker->randomElement(PersonalAreaEntity::ACTIVE_LIST);
    }

    /**
     * Сгенерировать сущность "Брокер"
     * @return BrokerEntity
     * @throws EntityException
     */
    private function _generateBroker(): BrokerEntity
    {
        /** @var BrokerGenerator $brokerDataGenerator */
        $brokerDataGenerator = $this->container->get(BrokerGenerator::class);
        /** @var BrokerService $brokerService */
        $brokerService = $this->container->get(BrokerService::class);
        return $brokerService->createBroker($brokerDataGenerator->generateData());
    }

    /**
     * Сгенерировать сущность "Статус проверки СБ"
     * @return SbCheckStatusEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function _generateSbCheckStatus(): SbCheckStatusEntity
    {
        /** @var SbCheckStatusGenerator $sbCheckStatusDataGenerator */
        $sbCheckStatusDataGenerator = $this->container->get(SbCheckStatusGenerator::class);
        /** @var SbCheckStatusService $sbCheckStatusService */
        $sbCheckStatusService = $this->container->get(SbCheckStatusService::class);
        return $sbCheckStatusService->createSbCheckStatus($sbCheckStatusDataGenerator->generateData());
    }

    /**
     * Сгенерировать сущность "Vip-статус"
     * @return StatusVipEntity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function _generateStatusVip(): StatusVipEntity
    {
        /** @var StatusVipGenerator $statusVipDataGenerator */
        $statusVipDataGenerator = $this->container->get(StatusVipGenerator::class);
        /** @var StatusVipService $statusVipService */
        $statusVipService = $this->container->get(StatusVipService::class);
        return $statusVipService->createStatusVip($statusVipDataGenerator->generateData());
    }
}

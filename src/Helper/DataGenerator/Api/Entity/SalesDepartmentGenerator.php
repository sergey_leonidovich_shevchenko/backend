<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Exception\EntityException;
use App\Helper\DataGenerator\BaseDataGenerator;
use App\Service\Api\ConsultingCompanyService;
use App\Entity\Api\SalesDepartment as SalesDepartmentEntity;
use App\Helper\DataGenerator\Api\Entity\ConsultingCompanyGenerator as ConsultingCompanyGenerator;
use App\Entity\Api\ConsultingCompany as ConsultingCompanyEntity;

/**
 * Class SalesDepartmentGenerator
 * @package App\Helper\DataGenerator\Api\Entity
 */
final class SalesDepartmentGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Департамент продаж"
     * @param int|null $consultingCompanyId
     * @return array
     * @throws EntityException
     */
    public function generateData(?int $consultingCompanyId = null): array
    {
        if (!$consultingCompanyId) {
            $consultingCompany = $this->_generateConsultingCompany();
            $consultingCompanyId = $consultingCompany->getId();
        }

        return [
            'consulting_company' => $consultingCompanyId,
            'code' => $this->generateCode(),
        ];
    }

    /**
     * Сгенерировать сущность "Департамент продаж"
     * @return SalesDepartmentEntity
     * @throws EntityException
     */
    public function generateEntity(): SalesDepartmentEntity
    {
        /** @var SalesDepartmentEntity $salesDepartmentEntity */
        $salesDepartmentEntity = $this->container->get(SalesDepartmentEntity::class);
        $salesDepartmentEntity->setConsultingCompany($this->_generateConsultingCompany());
        $salesDepartmentEntity->setCode($this->generateCode());
        return $salesDepartmentEntity;
    }

    /**
     * Сгенерировать сущность "Консалтинговая компания"
     * @return ConsultingCompanyEntity
     * @throws EntityException
     */
    private function _generateConsultingCompany(): ConsultingCompanyEntity
    {
        /** @var ConsultingCompanyGenerator $consultingCompanyDataGenerator */
        $consultingCompanyDataGenerator = $this->container->get(ConsultingCompanyGenerator::class);
        /** @var ConsultingCompanyService $consultingCompanyService */
        $consultingCompanyService = $this->container->get(ConsultingCompanyService::class);
        return $consultingCompanyService->createConsultingCompany($consultingCompanyDataGenerator->generateData());
    }

    /**
     * Сгенерировать код
     * @return string
     */
    private function generateCode(): string
    {
        return str_replace('.', '', uniqid('code_', true));
    }
}

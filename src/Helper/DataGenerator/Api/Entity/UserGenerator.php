<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Entity;

use App\Entity\Api\User as UserEntity;
use App\Helper\DataGenerator\BaseDataGenerator;

/**
 * Class UserGeneratorGenerator
 * @package App\Service\Api\DataGenerator\Api\Entity
 */
final class UserGenerator extends BaseDataGenerator
{
    /**
     * Сгенерировать данные для сущности "Пользователь"
     * @return array
     */
    public function generateData(): array
    {
        $username = $this->generateUsername();
        $email = $this->generateEmail();

        return [
            'username' => $username,
            'email' => $email,
            'password' => "{$username}-password",
            'enabled' => $this->generateEnabled(),
            'roles' => $this->generateRoles(),
        ];
    }

    /**
     * Сгенерировать данные для сущности "Пользователь"
     * @return UserEntity
     */
    public function generateEntity(): UserEntity
    {
        $username = $this->generateUsername();
        $email = $this->generateEmail();

        /** @var UserEntity $userEntity */
        $userEntity = $this->container->get(UserEntity::class);
        $userEntity->setUsername($username);
        $userEntity->setUsernameCanonical($username);
        $userEntity->setEmail($email);
        $userEntity->setEmailCanonical($email);
        $userEntity->setEnabled($this->generateEnabled());
        $userEntity->setPassword("{$username}-password");
        $userEntity->setRoles($this->generateRoles());
        return $userEntity;
    }

    /**
     * Сгенерировать имя пользователю
     * @return string
     */
    public function generateUsername(): string
    {
        return str_replace('.', '', uniqid('username_', true));
    }

    /**
     * Сгенерировать
     * @return string
     */
    public function generateEmail(): string
    {
        return $this->faker->email;
    }

    /**
     * Сгенерировать флаг активен ли пользователь в системе или нет
     * @return bool
     */
    public function generateEnabled(): bool
    {
        return true;
    }

    /**
     * Сгенерировать роли
     * @return string[]
     */
    public function generateRoles(): array
    {
        return [
            UserEntity::ROLE_ADMIN,
        ];
    }
}

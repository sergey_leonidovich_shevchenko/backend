<?php declare(strict_types=1);

namespace App\Helper\DataGenerator\Api\Security;

use App\Helper\DataGenerator\BaseDataGenerator;
use Exception;

/**
 * Class GeneratePassword
 * @package App\Helper\DataGenerator\Api\Security
 */
class GeneratePassword extends BaseDataGenerator
{
    /**
     * Сгенерировать пароль со всеми символами. Напр. k&|X+a45*2[
     * @param int $minLength Минимальная длина пароля
     * @param int $maxLength Максимальная длина пароля
     * @return string
     */
    public function generateWithAllSymbols($minLength = 10, $maxLength = 20): string
    {
        $password = '';
        if ($maxLength > 20) {
            while (strlen($password) <= $maxLength) {
                $password .= $this->faker->password();
            }
            $password = substr($password, 0, $maxLength);
        } else {
            $password .= $this->faker->password($minLength, $maxLength);
        }

        return $password;
    }

    /**
     * Сгенерировать пароль только с латинскими буквами и цыфрами. Напр. i4zpO0hMi
     * @param int $minLength Минимальная длина пароля
     * @return string
     * @throws Exception
     */
    public function generateAlphabetPassword($minLength = 8): string
    {
        $alphabet = 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789';
        $pass = []; // remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; // put the length -1 in cache
        for ($i = 0; $i < $minLength; $i++) {
            $n = random_int(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass); // turn the array into a string
    }
}

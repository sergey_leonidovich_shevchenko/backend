<?php declare(strict_types=1);

namespace App\Helper;

/**
 * Class StringHelper
 * @package App\Helper\Security
 */
class StringHelper extends BaseHelper
{
    /**
     * Замаскировать строку
     * @param string $string
     * @return string
     * @example "Unmask string" --> "*************"
     */
    public function maskAllString(string $string): string
    {
        $length = mb_strlen($string);
        return str_repeat('*', $length);
    }
}

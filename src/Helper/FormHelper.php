<?php declare(strict_types=1);

namespace App\Helper;

use Symfony\Component\Form\FormInterface;

/**
 * Class FormHelper
 * @package App\Helper
 */
class FormHelper
{
    /**
     * Получить данные формы в виде ассоциативного массива
     * @param FormInterface $form
     * @return array|null
     */
    public static function getFormDataAsArray(FormInterface $form): ?array
    {
        $data = [];
        foreach ($form as $key => $value) {
            $data[$key] = $value->getData();
        }
        return $data;
    }
}

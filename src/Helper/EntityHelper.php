<?php declare(strict_types=1);

namespace App\Helper;

/**
 * Class EntityHelper
 * @package App\Helper
 */
class EntityHelper
{
    /**
     * Преобразует структуру приватных ключей массива в публичные
     *     (срезает символ подчеркивания у приватных полей)
     *     ['_key1_' => 1, '__key2__' => 2] преобразует в
     *     ['key1_'  => 1, 'key2__'   => 2]
     * @param array $entityData
     * @return array
     */
    public static function privateFieldToPublic(array $entityData): array
    {
        if (!$entityData) {
            return $entityData;
        }

        $prepareEntityData = [];

        foreach ($entityData as $fieldName => $value) {
            $prepareEntityData[ltrim($fieldName, '_')] = $value;
        }

        return $prepareEntityData;
    }
}

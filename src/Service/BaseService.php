<?php declare(strict_types=1);

namespace App\Service;

use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class BaseService
 * @package App\Service
 */
abstract class BaseService
{
    /** @var ContainerInterface */
    protected $container;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /** @var ParameterBagInterface */
    protected $parameterBag;

    /**
     * BaseService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        if ($container === null) {
            throw new RuntimeException('@Container must be loading!');
        }
        $this->container = $container;
        $this->parameterBag = $container->getParameterBag();

        $eventDispatcher = $this->container->get('event_dispatcher');
        if ($eventDispatcher === 'null') {
            throw new RuntimeException('@EventDispatcher must be loading!');
        }
        $this->eventDispatcher = $eventDispatcher;
    }
}

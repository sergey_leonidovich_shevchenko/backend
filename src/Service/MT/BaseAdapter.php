<?php declare(strict_types=1);

namespace App\Service\MT;

use Grpc\UnaryCall;
use App\Service\MT\Exception\NotFoundException;

/**
 * Class BaseAdapter
 * @package App\Service\MT
 */
abstract class BaseAdapter extends RequestService
{
    /**
     * @param UnaryCall $request
     * @return false|mixed
     */
    protected function _getMTResponse(UnaryCall $request)
    {
        [$response, $status] = $request->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return false;
        }

        return $response;
    }
}

<?php declare(strict_types=1);

namespace App\Service\MT;

use App\Service\BaseService;
use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\DBALException;
use PDOStatement;

/**
 * Class BaseMTService
 * @package App\Service\MT
 */
abstract class BaseMTService extends BaseService
{
    /**
     * Выполнить запрос на удаленной БД
     * @param array  $connectionParams
     * @param string $sql
     * @param array  $params
     * @return PDOStatement
     */
    protected function prepareSqlStatement(array $connectionParams, string $sql, array $params = []): PDOStatement
    {
        try {
            /** @var PDOConnection $pdo */
            $pdo = DriverManager::getConnection($connectionParams)->getWrappedConnection();
        } catch (DBALException $e) {
            // TODO: Write to log (ошибка при установлении соединения)
            return null;
        }


        $sth = $pdo->prepare($sql);
        $sth->execute($params);

        return $sth;
    }
}

<?php declare(strict_types=1);

namespace App\Service\MT;

use PDO;

/**
 * Class BalanceService
 * @package App\Service\MT
 */
class BalanceService extends BaseMTService
{
    /**
     * Получить баланс для определенного счета
     * @param array  $connectionParams
     * @param string $login
     * @return array|null
     */
    public function getBalance(array $connectionParams, string $login): ?string
    {
        $sql = '
            SELECT `t`.`balance` 
            FROM `last_account_money_flow_update` AS `t`
            WHERE `t`.`login` = :login
            LIMIT 1;
        ';
        $sth = $this->prepareSqlStatement($connectionParams, $sql, [':login' => $login]);

        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (array_key_exists('balance', $result)) {
            return $result['balance'];
        }

        return null;
    }
}

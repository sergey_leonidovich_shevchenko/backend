<?php declare(strict_types=1);

namespace App\Service\MT;

use App\Service\BaseService;
use App\Service\MT\Adapter\AccountAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Entity\Api\Account;
use App\Service\MT\Adapter\UserAdapter;
use RuntimeException;
use App\Entity\Api\TradingPlatform;
use App\Helper\DataGenerator\Api\Security\GeneratePassword;
use App\Entity\Api\PersonalArea;
use Exception;
use App\Exception\GrpcException;
use App\EventDispatcher\Event\MT\MTEvent;
use Mt5ManagerApi\UserEventResponse;
use Generator;

/**
 * Class AccountService
 * @package App\Service\MT
 */
class AccountService extends BaseService
{
    // всегда 4278190080 (HEX -> 0xFF000000)
    public const DEFAULT_COLOR = 4278190080;

    public const USER_RIGHTS_BY_DEFAULT = self::USER_RIGHT_ENABLED
    | self::USER_RIGHT_PASSWORD
    | self::USER_RIGHT_TRAILING
    | self::USER_RIGHT_EXPERT
    | self::USER_RIGHT_API
    | self::USER_RIGHT_REPORTS;

    protected const USER_RIGHT_API = 0x0000000000000080;

    protected const USER_RIGHT_ENABLED = 0x0000000000000001;

    protected const USER_RIGHT_EXPERT = 0x0000000000000040;

    protected const USER_RIGHT_PASSWORD = 0x0000000000000002;

    protected const USER_RIGHT_REPORTS = 0x0000000000000100;

    // всегда по умолчанию следующий набор прав (суммируется по через | )

    protected const USER_RIGHT_TRAILING = 0x0000000000000020;

    /** @var AccountAdapter */
    private $accountAdapter;

    /** @var UserAdapter */
    private $userAdapter;

    /**
     * AccountService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->userAdapter = $this->container->get(UserAdapter::class);
    }

    /**
     * Сохранить создание счета на mt сервере
     * @param Account $account
     * @return bool
     * @throws GrpcException
     * @throws Exception
     */
    public function saveAccount(Account $account): bool
    {
        /** @var TradingPlatform $tradingPlatform */
        $tradingPlatform = $account->getTradingPlatform();

        /** @var MTEvent $event */
        $event = $this->container->get(MTEvent::class);
        $event->setAccount($account);

        /** @var GeneratePassword $generatePasswordHelper */
        $generatePasswordHelper = $this->container->get(GeneratePassword::class);
        $masterPassword = $generatePasswordHelper->generateWithAllSymbols();
        $investorPassword = $generatePasswordHelper->generateWithAllSymbols();
        $event->setMasterPassword($masterPassword);
        $event->setInvestorPassword($investorPassword);

        /** @var array $user */
        $user = $this->_fillUser($account);

        try {
            $stream = $this->userAdapter->subscribeToEvents();
            $this->userAdapter->add($user, $masterPassword, $investorPassword);
            /** @var Generator $responseGenerator */
            $responseGenerator = $stream->responses();
            foreach ($responseGenerator as $response) {
                /** @var UserEventResponse $response */
                if ($response instanceof UserEventResponse) {
                    if ($response->getUser()->getName() === $user['name']) {
                        $user = $response->getUser();
                        $user = json_decode($user->serializeToJsonString(), true, 512, JSON_THROW_ON_ERROR);
                        $event->setUser($user);
                        $tradingPlatform->setLogin($user['login']);
                        $stream->cancel();
                        break;
                    }
                }
            }
        } catch (Exception $e) {
            throw new GrpcException($this->container, MTEvent::EVENT_MT_USER_ADD_EXCEPTION, $e, [
                'user' => $user,
            ]);
        }

        $this->eventDispatcher->dispatch($event, MTEvent::EVENT_MT_USER_ADD);

        return true;
    }

    /**
     * Собрать структуру пользователя и наполнить данными для отправки на mt сервер
     * @param Account $account
     * @return array
     */
    private function _fillUser(Account $account): array
    {
        $tradingPlatform = $account->getTradingPlatform();
        if (!$tradingPlatform instanceof TradingPlatform) {
            throw new RuntimeException('$tradingPlatform is not instanceof App\\Entity\\Api\\TradingPlatform!');
        }

        $personalArea = $account->getPersonalArea();
        if (!$personalArea instanceof PersonalArea) {
            throw new RuntimeException('$personalArea is not instanceof App\\Entity\\Api\\PersonalArea!');
        }

        return [
            // берётся из сущности платформы (@see TradingPlatform::groups)
            'group' => $tradingPlatform->getGroup() ?? $this->parameterBag->get('mt.user_group.default'),
            'color' => self::DEFAULT_COLOR,
            'rights' => self::USER_RIGHTS_BY_DEFAULT,
            // берётся из  сущности "Личные  кабинеты" обычное ФИО
            'name' => $personalArea->getFullName(),
            // опционально, по умолчанию пусто
            'country' => null,
            // по умолчанию 0
            'language' => null ?? 0,
            // по умолчанию пусто
            'city' => null,
            // по умолчанию пусто
            'address' => null,
            // по умолчанию пусто
            'phone' => null,
            // по умолчанию пусто
            'email' => null,
            // по умолчанию пусто
            'comment' => null,
            // по умолчанию пусто
            'phonePassword' => null,
            // берётся из настроек платформы (обязательное поле)
            'leverage' => 100,
            // берётся поле "code" из сущности "Торговые платформы"
            'leadCampaign' => null,
            // берётся поле "ID" из сущности "Личные  кабинеты"
            'id' => null,
        ];
    }
}

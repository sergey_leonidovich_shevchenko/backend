<?php declare(strict_types=1);

namespace App\Service\MT\Adapter;

use App\Service\MT\Exception\NotFoundException;
use DateTime;
use Google\Protobuf\Timestamp;
use Grpc\ServerStreamingCall;
use Mt5ManagerApi\Mt5UserManagerApiServerClient;
use Mt5ManagerApi\User;
use Mt5ManagerApi\UserAddRequest;
use Mt5ManagerApi\UserDeleteRequest;
use Mt5ManagerApi\UserGetGroupRequest;
use Mt5ManagerApi\UserGetGroupResponse;
use Mt5ManagerApi\UserGetLoginsRequest;
use Mt5ManagerApi\UserGetLoginsResponse;
use Mt5ManagerApi\UserGetRequest;
use Mt5ManagerApi\UserGetResponse;
use Mt5ManagerApi\UserUpdateRequest;
use stdClass;
use App\Service\MT\BaseAdapter;

/**
 * Получение информации о пользователях
 * Class UserAdapter
 * @package App\Service\MT\Adapter
 */
class UserAdapter extends BaseAdapter
{
    /**
     * Получение списка логинов пользователей для указанной группы
     * @param string $groupName Название группы
     * @param string $serverId  Название сервера
     * @return array Массив логинов группы
     */
    public function getLoginList(string $groupName, string $serverId = ''): array
    {
        /** @var Mt5UserManagerApiServerClient $client */
        /** @var UserGetLoginsResponse $userGetLoginsResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5UserManagerApiServerClient::class);
        $args = new UserGetLoginsRequest([
            'group' => $groupName,
            'serverId' => $serverId,
        ]);
        $getUserRequest = $client->GetLogins($args, $this->getMetadata());
        [$userGetLoginsResponse, $status] = $getUserRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $userList = json_decode($userGetLoginsResponse->serializeToJsonString(), true);

        return $userList['logins'];
    }

    /**
     * Получение информации о пользователях для указанных логинов
     * @param string[] $loginList Список числовых логинов
     * @param string   $serverId  Название сервера
     * @return array Информация о пользователях (массив объектов типа 'user')
     */
    public function get(array $loginList, string $serverId = ''): array
    {
        /** @var Mt5UserManagerApiServerClient $client */
        /** @var UserGetResponse $response */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5UserManagerApiServerClient::class);
        $args = new UserGetRequest([
            'logins' => $loginList,
            'serverId' => $serverId,
        ]);
        $request = $client->Get($args, $this->getMetadata());
        $response = $this->_getMTResponse($request);
        $userList = json_decode($response->serializeToJsonString(), true, 512, JSON_THROW_ON_ERROR);

        return $userList['users'];
    }

    /**
     * Добавление нового пользователя на торговом сервере
     * @param array  $user             Данные о добавляемом пользователе
     * @param string $masterPassword   Пароль
     * @param string $investorPassword Пароль инвестора
     * @param string $serverId         Название сервера
     * @return bool Статус добавления пользователя
     */
    public function add(array $user, string $masterPassword, string $investorPassword, string $serverId = ''): bool
    {
        /** @var Mt5UserManagerApiServerClient $client */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5UserManagerApiServerClient::class);
        $args = new UserAddRequest([
            'user' => new User($user),
            'masterPassword' => $masterPassword,
            'investorPassword' => $investorPassword,
            'serverId' => $serverId,
        ]);
        $request = $client->Add($args, $this->getMetadata());

        return !($this->_getMTResponse($request) === false);
    }

    /**
     * Обновление информации о пользователях на торговом сервере
     * @param array[] $userList Данные о изменяемом пользователе
     * @param string  $serverId Название сервера
     * @return bool Статус обновления пользователей
     */
    public function update(array $userList, string $serverId = ''): bool
    {
        /** @var Mt5UserManagerApiServerClient $client */
        /** @var stdClass $status */
        $this->prepareUserBeforeSend($userList);
        $client = $this->getClient(Mt5UserManagerApiServerClient::class);
        $args = new UserUpdateRequest([
            'users' => $userList,
            'serverId' => $serverId,
        ]);
        $request = $client->Update($args, $this->getMetadata());

        return !($this->_getMTResponse($request) === false);
    }

    /**
     * Приведение полей пользователя к нужному формату перед отправкой на сервер
     * @param array[] $userList
     */
    private function prepareUserBeforeSend(array &$userList): void
    {
        $userList = array_map(static function (array $user) {
            if ($user['registrationTime']) {
                $datetime = new DateTime($user['registrationTime']);
                $user['registrationTime'] = new Timestamp(['seconds' => $datetime->getTimestamp()]);
            }

            if ($user['lastAccessTime']) {
                $datetime = new DateTime($user['lastAccessTime']);
                $user['lastAccessTime'] = new Timestamp(['seconds' => $datetime->getTimestamp()]);
            }

            if ($user['lastPasswordChange']) {
                $datetime = new DateTime($user['lastPasswordChange']);
                $user['lastPasswordChange'] = new Timestamp(['seconds' => $datetime->getTimestamp()]);
            }

            return new User($user);
        }, $userList);
    }

    /**
     * Удаление пользователей на торговом сервере
     * @param string[] $userLogins Числовые логины удаляемых пользователей
     * @param string   $serverId   Название сервера
     * @return bool Статус удаления пользователей
     */
    public function delete(array $userLogins, string $serverId = ''): bool
    {
        /** @var Mt5UserManagerApiServerClient $client */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5UserManagerApiServerClient::class);
        $args = new UserDeleteRequest([
            'logins' => $userLogins,
            'serverId' => $serverId,
        ]);
        $request = $client->Delete($args, $this->getMetadata());
        return !($this->_getMTResponse($request) === false);
    }

    /**
     * Получение имени группы для указанного номера логина
     * @param string $userLogin Числовой логин пользователя
     * @param string $serverId  Название сервера
     * @return string|null Название группы
     */
    public function getGroup(string $userLogin, string $serverId = ''): ?string
    {
        /** @var Mt5UserManagerApiServerClient $client */
        /** @var UserGetGroupResponse $response */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5UserManagerApiServerClient::class);
        $args = new UserGetGroupRequest([
            'login' => $userLogin,
            'serverId' => $serverId,
        ]);
        $request = $client->GetGroup($args, $this->getMetadata());
        $response = $this->_getMTResponse($request);
        $groupName = json_decode($response->serializeToJsonString(), true, 512, JSON_THROW_ON_ERROR);

        return $groupName['group'];
    }

    /**
     * Открытие потока событий пользователей на торговом сервере
     * @return ServerStreamingCall Открывается поток, в котором будут появляться объекты с полем result типа
     *                             'userEvent'.
     */
    public function subscribeToEvents(): ServerStreamingCall
    {
        /** @var Mt5UserManagerApiServerClient $client */
        /** @var ServerStreamingCall $subscribeToEvents */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5UserManagerApiServerClient::class);

        return $client->SubscribeToEvents($this->getGPBEmpty(), $this->getMetadata());
    }
}

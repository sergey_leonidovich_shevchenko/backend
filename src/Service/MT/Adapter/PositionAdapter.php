<?php declare(strict_types=1);

namespace App\Service\MT\Adapter;

use App\Service\MT\Exception\NotFoundException;
use App\Service\MT\RequestService;
use Mt5ManagerApi\Mt5TradeManagerApiServerClient;
use Mt5ManagerApi\Position;
use Mt5ManagerApi\PositionDeleteRequest;
use Mt5ManagerApi\PositionsGetByGroupRequest;
use Mt5ManagerApi\PositionsGetByLoginRequest;
use Mt5ManagerApi\PositionsGetRequest;
use Mt5ManagerApi\PositionsGetResponse;
use Mt5ManagerApi\PositionUpdateRequest;
use stdClass;

/**
 * Получение информации о позициях
 * Class PositionAdapter
 * @package App\Service\MT\Adapter
 */
class PositionAdapter extends RequestService
{
    /**
     * Получение информации о позициях по указанным номерам
     * @param string[] $positionIds Числовые номера позиций
     * @param string   $serverId    Название сервера
     * @return array Массив объектов типа 'position'
     */
    public function get(array $positionIds, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var PositionsGetResponse $positionsGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new PositionsGetRequest([
            'positionIds' => $positionIds,
            'serverId' => $serverId,
        ]);
        $positionsGetRequest = $client->GetPositions($args, $this->getMetadata());
        [$positionsGetResponse, $status] = $positionsGetRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $positionList = json_decode($positionsGetResponse->serializeToJsonString(), true);

        if (!empty($positionList)) {
            return $positionList['positions'];
        }

        return [];
    }

    /**
     * Получение информации о позициях для указанной маски групп
     * @param string $groupMask Маска групп
     * @param string $serverId  Название сервера
     * @return array Массив объекта типа 'position'
     */
    public function getByGroup(string $groupMask, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var PositionsGetResponse $positionsGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new PositionsGetByGroupRequest([
            'groupMask' => $groupMask,
            'serverId' => $serverId,
        ]);
        $positionsGetByLoginRequest = $client->GetPositionsByGroup($args, $this->getMetadata());
        [$positionsGetResponse, $status] = $positionsGetByLoginRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $positionList = json_decode($positionsGetResponse->serializeToJsonString(), true);

        if (!empty($positionList)) {
            return $positionList['positions'];
        }

        return [];
    }

    /**
     * Получение информации о позициях для указанного логина
     * @param string $login    Числовой логин пользователя
     * @param string $serverId Название сервера
     * @return array Массив объекта типа 'position'
     */
    public function getByLogin(string $login, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var PositionsGetResponse $positionsGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new PositionsGetByLoginRequest([
            'login' => $login,
            'serverId' => $serverId,
        ]);
        $positionsGetByLoginRequest = $client->GetPositionsByLogin($args, $this->getMetadata());
        [$positionsGetResponse, $status] = $positionsGetByLoginRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $positionList = json_decode($positionsGetResponse->serializeToJsonString(), true);

        if (!empty($positionList)) {
            return $positionList['positions'];
        }

        return [];
    }

    /**
     * Установка свойств позиции
     * @param array $position Информация о позиции (тип 'position')
     * @return bool Статус обновления
     */
    public function update(array $position): bool
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new PositionUpdateRequest(['position' => $this->preparePositionBeforeSend($position)]);
        $positionUpdateRequest = $client->UpdatePosition($args, $this->getMetadata());
        [$_, $status] = $positionUpdateRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param array $position
     * @return Position
     */
    private function preparePositionBeforeSend(array $position): Position
    {
//        $position['action'] = constant('Mt5ManagerApi\\Position\\PositionAction::' . $position['action']);
//        $position['creationTime'] = new Timestamp(['seconds' => (new DateTime($position['creationTime']))->getTimestamp()]);
        return new Position($position);
    }

    /**
     * Удаление позиции
     * @param string $positionId Числовой идентификатор позиции
     * @param string $login      Числовой логин пользователя
     * @param string $serverId   Название сервера
     * @return bool Статус обновления
     */
    public function delete(string $positionId, string $login, string $serverId = ''): bool
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new PositionDeleteRequest([
            'positionId' => $positionId,
            'login' => $login,
            'serverId' => $serverId,
        ]);
        $positionDeleteRequest = $client->DeletePosition($args, $this->getMetadata());
        [$_, $status] = $positionDeleteRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return false;
        }

        return true;
    }
}

<?php declare(strict_types=1);

namespace App\Service\MT\Adapter;

use App\Service\MT\Exception\NotFoundException;
use App\Service\MT\RequestService;
use DateTime;
use Exception;
use Google\Protobuf\Timestamp;
use Mt5ManagerApi\Deal;
use Mt5ManagerApi\DealDeleteRequest;
use Mt5ManagerApi\DealsGetByLoginRequest;
use Mt5ManagerApi\DealsGetRequest;
use Mt5ManagerApi\DealsGetResponse;
use Mt5ManagerApi\DealUpdateRequest;
use Mt5ManagerApi\Mt5TradeManagerApiServerClient;
use stdClass;

/**
 * Получение информации о сделках
 * Class DealAdapter
 * @package App\Service\MT\Adapter
 */
class DealAdapter extends RequestService
{
    /**
     * Получение информации об сделках по указанным номерам
     * @param string[] $dealIds  Числовые номера сделок
     * @param string   $serverId Название сервера
     * @return array Массив объектов типа 'deal
     */
    public function get(array $dealIds, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var DealsGetResponse $dealsGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new DealsGetRequest([
            'dealIds' => $dealIds,
            'serverId' => $serverId,
        ]);
        $dealsGetRequest = $client->GetDeals($args, $this->getMetadata());
        [$dealsGetResponse, $status] = $dealsGetRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $deals = json_decode($dealsGetResponse->serializeToJsonString(), true);

        if (!empty($deals)) {
            return $deals['deals'];
        }

        return [];
    }

    /**
     * Получение информации о сделках для указанного логина
     * @param string        $login    Числовой логин клиента
     * @param DateTime|null $from     Дата с которой нужно вести поиск
     * @param DateTime|null $to       Дата по которой нужно вести поиск
     * @param string        $serverId Название сервера
     * @return array Массив объекта типа 'deal'
     */
    public function getByLogin(string $login, DateTime $from, DateTime $to, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var DealsGetResponse $dealsGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new DealsGetByLoginRequest([
            'login' => $login,
            'from' => $this->prepareDateBeforeSend($from),
            'to' => $this->prepareDateBeforeSend($to),
            'serverId' => $serverId,
        ]);
        $dealsGetByLoginRequest = $client->GetDealsByLogin($args, $this->getMetadata());
        [$dealsGetResponse, $status] = $dealsGetByLoginRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        return json_decode($dealsGetResponse->serializeToJsonString(), true);
    }

    /**
     * Приведение даты к нужному формату перед отправкой на сервер
     * @param DateTime|null $date
     * @return Timestamp|null
     */
    private function prepareDateBeforeSend(?DateTime $date): ?Timestamp
    {
        if (!$date) {
            return null;
        }

        return new Timestamp(['seconds' => $date->getTimestamp()]);
    }

    /**
     * Установка свойств сделки
     * @param array $deal Информация о сделке (тип 'deal')
     * @return bool Статус обновления
     * @throws Exception
     */
    public function update(array $deal): bool
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new DealUpdateRequest(['deal' => $this->prepareDealBeforeSend($deal)]);
        $dealUpdateRequest = $client->UpdateDeal($args, $this->getMetadata());
        [$_, $status] = $dealUpdateRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param array $deal
     * @return Deal
     * @throws Exception
     */
    private function prepareDealBeforeSend(array $deal): Deal
    {
        $deal['action'] = constant('Mt5ManagerApi\\Deal\\DealAction::' . $deal['action']);
        $deal['reason'] = constant('Mt5ManagerApi\\Deal\\DealReason::' . $deal['reason']);
        $deal['creationTime'] = new Timestamp(['seconds' => (new DateTime($deal['creationTime']))->getTimestamp()]);
        return new Deal($deal);
    }

    /**
     * Удаление сделки
     * @param string $dealId   Числовой идентификатор сделки
     * @param string $login    Числовой логин пользователя
     * @param string $serverId Название сервера
     * @return bool Статус обновления
     */
    public function delete(string $dealId, string $login = null, string $serverId = ''): bool
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new DealDeleteRequest([
            'dealId' => $dealId,
            'login' => $login,
            'serverId' => $serverId,
        ]);
        $dealDeleteRequest = $client->DeleteDeal($args, $this->getMetadata());
        [$_, $status] = $dealDeleteRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return false;
        }

        return true;
    }
}

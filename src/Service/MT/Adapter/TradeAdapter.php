<?php declare(strict_types=1);

namespace App\Service\MT\Adapter;

use App\Service\MT\Exception\NotFoundException;
use App\Service\MT\RequestService;
use Grpc\ServerStreamingCall;
use Mt5ManagerApi\Mt5TradeManagerApiServerClient;
use Mt5ManagerApi\Request;
use Mt5ManagerApi\RequestExecuteRequest;
use Mt5ManagerApi\UserGetLoginsResponse;
use stdClass;

/**
 * Получение информации о торгах
 * Class TradeAdapter
 * @package App\Service\MT\Request
 */
class TradeAdapter extends RequestService
{
    /**
     * Проведение балансовой операции
     * @param array $request
     * @return array Массив
     */
    public function execute(array $request): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var UserGetLoginsResponse $userGetLoginsResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new RequestExecuteRequest(['request' => new Request($request)]);
        $getUserRequest = $client->RequestExecute($args, $this->getMetadata());
        [$userGetLoginsResponse, $status] = $getUserRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $userList = json_decode($userGetLoginsResponse->serializeToJsonString(), true);
        if (empty($userList)) {
            return [];
        }

        return $userList['logins'];
    }

    /**
     * Открытие потока торговых событий на торговом сервере
     * @return ServerStreamingCall Открывается поток,
     *                             в котором будут появляться объекты с полем result типа 'tradeEvent'.
     */
    public function events(): ServerStreamingCall
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var ServerStreamingCall $subscribeToEvents */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);

        return $client->SubscribeToEvents($this->getGPBEmpty(), $this->getMetadata());
    }
}

<?php declare(strict_types=1);

namespace App\Service\MT\Adapter;

use App\Service\MT\Exception\NotFoundException;
use App\Service\MT\RequestService;
use Mt5ManagerApi\Mt5TradeManagerApiServerClient;
use Mt5ManagerApi\Order;
use Mt5ManagerApi\OrderDeleteRequest;
use Mt5ManagerApi\OrdersGetByGroupRequest;
use Mt5ManagerApi\OrdersGetByLoginRequest;
use Mt5ManagerApi\OrdersGetRequest;
use Mt5ManagerApi\OrdersGetResponse;
use Mt5ManagerApi\OrderUpdateRequest;
use stdClass;

/**
 * Получение информации о заказах
 * Class OrderAdapter
 * @package App\Service\MT\Adapter
 */
class OrderAdapter extends RequestService
{
    /**
     * Получение информации о заказах по указанным номерам
     * @param string[] $orderIds Числовые номера заказов
     * @param string   $serverId Название сервера
     * @return array Массив объектов типа 'order'
     */
    public function get(array $orderIds, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var OrdersGetResponse $ordersGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new OrdersGetRequest([
            'orderIds' => $orderIds,
            'serverId' => $serverId,
        ]);
        $ordersGetRequest = $client->GetOrders($args, $this->getMetadata());
        [$ordersGetResponse, $status] = $ordersGetRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $orderList = json_decode($ordersGetResponse->serializeToJsonString(), true);

        if (!empty($orderList)) {
            return $orderList['orders'];
        }

        return [];
    }

    /**
     * Получение информации о заказах для указанной маски групп
     * @param string $groupMask Маска групп
     * @param string $serverId  Название сервера
     * @return array Массив объекта типа 'order'
     */
    public function getByGroup(string $groupMask, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var OrdersGetResponse $ordersGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new OrdersGetByGroupRequest([
            'groupMask' => $groupMask,
            'serverId' => $serverId,
        ]);
        $ordersGetByLoginRequest = $client->GetOrdersByGroup($args, $this->getMetadata());
        [$ordersGetResponse, $status] = $ordersGetByLoginRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $orderList = json_decode($ordersGetResponse->serializeToJsonString(), true);

        if (!empty($orderList)) {
            return $orderList['orders'];
        }

        return [];
    }

    /**
     * Получение информации о заказах для указанного логина
     * @param string $login    Числовой логин пользователя
     * @param string $serverId Название сервера
     * @return array Массив объекта типа 'order'
     */
    public function getByLogin(string $login, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var OrdersGetResponse $ordersGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new OrdersGetByLoginRequest([
            'login' => $login,
            'serverId' => $serverId,
        ]);
        $ordersGetByLoginRequest = $client->GetOrdersByLogin($args, $this->getMetadata());
        [$ordersGetResponse, $status] = $ordersGetByLoginRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $orderList = json_decode($ordersGetResponse->serializeToJsonString(), true);

        if (!empty($orderList)) {
            return $orderList['orders'];
        }

        return [];
    }

    /**
     * Установка свойств заказа
     * @param array $order Информация о заказе (тип 'order')
     * @return bool Статус обновления
     */
    public function update(array $order): bool
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new OrderUpdateRequest(['order' => $this->prepareOrderBeforeSend($order)]);
        $orderUpdateRequest = $client->UpdateOrder($args, $this->getMetadata());
        [$_, $status] = $orderUpdateRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param array $order
     * @return Order
     */
    private function prepareOrderBeforeSend(array $order): Order
    {
//        $order['action'] = constant('Mt5ManagerApi\\Order\\OrderAction::' . $order['action']);
//        $order['creationTime'] = new Timestamp(['seconds' => (new DateTime($order['creationTime']))->getTimestamp()]);
        return new Order($order);
    }

    /**
     * Удаление заказа
     * @param string $orderId  Числовой идентификатор заказа
     * @param string $login    Числовой логин пользователя
     * @param string $serverId Название сервера
     * @return bool Статус обновления
     */
    public function delete(string $orderId, string $login, string $serverId = ''): bool
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new OrderDeleteRequest([
            'orderId' => $orderId,
            'login' => $login,
            'serverId' => $serverId,
        ]);
        $orderDeleteRequest = $client->DeleteOrder($args, $this->getMetadata());
        [$_, $status] = $orderDeleteRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return false;
        }

        return true;
    }
}

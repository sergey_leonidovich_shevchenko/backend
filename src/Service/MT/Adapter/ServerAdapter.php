<?php declare(strict_types=1);

namespace App\Service\MT\Adapter;

use App\Service\MT\Exception\NotFoundException;
use App\Service\MT\RequestService;
use Mt5ManagerApi\ServerGetLoginsRequest;
use Mt5ManagerApi\ServerGetServerByLoginRequest;
use Mt5ManagerApi\ServerGetServersResponse;
use Mt5ManagerApi\ServerManagerApiServerClient;
use stdClass;

/**
 * Получение информации о серверах
 * Class ServerAdapter
 * @package App\Service\MT\Adapter
 */
class ServerAdapter extends RequestService
{
    /**
     * Получение списка МТ5 серверов
     * @return array Массив серверов
     */
    public function getServerList(): array
    {
        /** @var ServerManagerApiServerClient $client */
        /** @var ServerGetServersResponse $serverGetServersResponse */
        /** @var stdClass $status */
        $client = $this->getClient(ServerManagerApiServerClient::class);
        $getServersRequest = $client->GetServers($this->getGPBEmpty(), $this->getMetadata());
        [$serverGetServersResponse, $status] = $getServersRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $serverList = json_decode($serverGetServersResponse->serializeToJsonString(), true);

        return $serverList['servers'];
    }

    /**
     * Получение списка интервалов логинов для указанного сервера МТ
     * @param string $serverName Идентификатор (название) сервера МТ5
     * @return array Список интервалов логинов
     */
    public function getLoginIntervalList(string $serverName): array
    {
        /** @var ServerManagerApiServerClient $client */
        /** @var ServerGetLoginsRequest $serverGetLoginsResponse */
        /** @var stdClass $status */
        $client = $this->getClient(ServerManagerApiServerClient::class);
        $args = new ServerGetLoginsRequest(['serverId' => $serverName]);
        $getLoginsRequest = $client->GetLogins($args, $this->getMetadata());
        [$serverGetLoginsResponse, $status] = $getLoginsRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $loginIntervals = json_decode($serverGetLoginsResponse->serializeToJsonString(), true);

        return $loginIntervals['loginIntervals'];
    }

    /**
     * Получение списка МТ5 серверов по его логин-счету
     * @param string $serverLogin Числовой логин-счет сервера
     * @return string|null Идентификатор (имя) сервера МТ5, к которому принадлежит логин
     */
    public function getServerByLogin(string $serverLogin): ?string
    {
        /** @var ServerManagerApiServerClient $client */
        /** @var ServerGetServerByLoginRequest $serverGetLoginsResponse */
        /** @var stdClass $status */
        $client = $this->getClient(ServerManagerApiServerClient::class);
        $args = new ServerGetServerByLoginRequest(['login' => $serverLogin]);
        $getServersRequest = $client->GetServerByLogin($args, $this->getMetadata());
        [$serverGetLoginsResponse, $status] = $getServersRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return null;
        }

        $serverId = json_decode($serverGetLoginsResponse->serializeToJsonString(), true);

        return $serverId['serverId'];
    }
}

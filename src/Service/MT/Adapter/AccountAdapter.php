<?php declare(strict_types=1);

namespace App\Service\MT\Adapter;

use App\Service\MT\Exception\NotFoundException;
use App\Service\MT\RequestService;
use Mt5ManagerApi\AccountsGetRequest;
use Mt5ManagerApi\AccountsGetResponse;
use Mt5ManagerApi\BalanceOperationRequest;
use Mt5ManagerApi\BalanceOperationResponse;
use Mt5ManagerApi\Mt5TradeManagerApiServerClient;
use stdClass;

/**
 * Получение информации о счетах
 * Class AccountAdapter
 * @package App\Service\MT\Request
 */
class AccountAdapter extends RequestService
{
    /**
     * Получение информации о торговых счетах
     * @param string[] $loginList Массив числовых логинов клиентов
     * @param string   $serverId  Название сервера
     * @return array Массив данных о клиентах (тип 'account')
     */
    public function get(array $loginList, string $serverId = ''): array
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var AccountsGetResponse $accountsGetResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new AccountsGetRequest([
            'logins' => $loginList,
            'serverId' => $serverId,
        ]);
        $accountGetRequest = $client->GetAccounts($args, $this->getMetadata());
        [$accountsGetResponse, $status] = $accountGetRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return [];
        }

        $accounts = json_decode($accountsGetResponse->serializeToJsonString(), true);

        return $accounts['accounts'];
    }

    /**
     * Проведение балансовой операции
     * @param string $login    Числовой логин клиента
     * @param float  $valueSum Сумма
     * @param string $comment  Комментарий
     * @param string $serverId Название сервера
     * @return string|null ID сделки
     */
    public function balance(string $login, float $valueSum, string $comment = '', string $serverId = ''): ?string
    {
        /** @var Mt5TradeManagerApiServerClient $client */
        /** @var BalanceOperationResponse $balanceOperationResponse */
        /** @var stdClass $status */
        $client = $this->getClient(Mt5TradeManagerApiServerClient::class);
        $args = new BalanceOperationRequest([
            'login' => $login,
            'value' => $valueSum,
            'type' => BalanceOperationRequest\BalanceOperation::Balance,
            'comment' => $comment,
            'serverId' => $serverId,
        ]);
        $balanceOperationRequest = $client->BalanceOperation($args, $this->getMetadata());
        [$balanceOperationResponse, $status] = $balanceOperationRequest->wait();
        try {
            $this->checkErrors($status);
        } catch (NotFoundException $e) {
            return null;
        }

        $balance = json_decode($balanceOperationResponse->serializeToJsonString(), true);

        return $balance['dealId'];
    }
}

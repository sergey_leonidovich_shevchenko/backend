<?php declare(strict_types=1);

namespace App\Service\MT;

use App\Service\BaseService;
use App\Service\MT\Exception\NotFoundException;
use Google\Protobuf\GPBEmpty;
use Grpc\BaseStub;
use RuntimeException;
use stdClass;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RequestService
 * @package App\Service\MT
 */
class RequestService extends BaseService
{
    /** @var GRPCConnection */
    protected $connection;

    /**
     * RequestService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->connection = new GRPCConnection($container, $this->eventDispatcher);
    }

    /**
     * @return GPBEmpty
     */
    protected function getGPBEmpty(): GPBEmpty
    {
        return $this->connection->getGPBEmpty();
    }

    /**
     * @return array
     */
    protected function getMetadata(): array
    {
        $token = $this->connection->getUserLogin()->getToken();
        return [
            'auth-token' => [$token],
        ];
    }

    /**
     * Получить клиент
     * @param string $clientClassName
     * @return BaseStub
     */
    protected function getClient(string $clientClassName): BaseStub
    {
        return new $clientClassName(
            $this->connection->getServerHost(),
            $this->connection->getChannelOpts(),
            $this->connection->getChannel()
        );
    }

    /**
     * Проверка запроса на ошибки
     * @param stdClass $status
     * @throws NotFoundException
     */
    protected function checkErrors(stdClass $status): void
    {
        switch ($status->code) {
            case GRPCConnection::STATUS_OK:
                break;

            case GRPCConnection::STATUS_NOT_FOUND:
                throw new NotFoundException($status->details);
                break;

            default:
                throw new RuntimeException('Failed! ' . json_encode($status, JSON_THROW_ON_ERROR, 512));
        }
    }
}

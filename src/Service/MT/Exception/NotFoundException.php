<?php declare(strict_types=1);

namespace App\Service\MT\Exception;

use Exception;

/**
 * Class NotFoundException
 * @package App\Service\MT\Exception
 */
class NotFoundException extends Exception
{

}

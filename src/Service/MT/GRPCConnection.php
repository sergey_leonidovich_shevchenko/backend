<?php declare(strict_types=1);

namespace App\Service\MT;

use App\Service\BaseService;
use Google\Protobuf\GPBEmpty;
use Grpc\Channel;
use Grpc\ChannelCredentials;
use Mt5ManagerApi\AuthenticationApiServerClient;
use Mt5ManagerApi\LoginResponse;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class GRPCConnection
 * @package App\Service\MT
 */
class GRPCConnection extends BaseService
{
    /**
     * Not an error; returned on success.
     */
    const STATUS_OK = 0;

    /**
     * The operation was cancelled (typically by the caller).
     */
    const STATUS_CANCELLED = 1;

    /**
     * Unknown error. An example of where this error may be returned is if a
     * Status value received from another address space belongs to an error-space
     * that is not known in this address space. Also errors raised by APIs that
     * do not return enough error information may be converted to this error.
     */
    const STATUS_UNKNOWN = 2;

    /**
     * Client specified an invalid argument. Note that this differs from
     * FAILED_PRECONDITION. INVALID_ARGUMENT indicates arguments that are
     * problematic regardless of the state of the system (e.g., a malformed file name).
     */
    const STATUS_INVALID_ARGUMENT = 3;

    /**
     * Deadline expired before operation could complete. For operations that
     * change the state of the system, this error may be returned even if the
     * operation has completed successfully. For example, a successful response
     * from a server could have been delayed long enough for the deadline to expire.
     */
    const STATUS_DEADLINE_EXCEEDED = 4;

    /**
     * Some requested entity (e.g., file or directory) was not found.
     */
    const STATUS_NOT_FOUND = 5;

    /**
     * Some entity that we attempted to create (e.g., file or directory) already exists.
     */
    const STATUS_ALREADY_EXISTS = 6;

    /**
     * The caller does not have permission to execute the specified operation.
     * PERMISSION_DENIED must not be used for rejections caused by exhausting
     * some resource (use RESOURCE_EXHAUSTED instead for those errors).
     * PERMISSION_DENIED must not be used if the caller can not be identified
     * (use UNAUTHENTICATED instead for those errors).
     */
    const STATUS_PERMISSION_DENIED = 7;

    /**
     * The request does not have valid authentication credentials for the operation.
     */
    const STATUS_UNAUTHENTICATED = 16;

    /**
     * Some resource has been exhausted, perhaps a per-user quota, or perhaps the
     * entire file system is out of space.
     */
    const STATUS_RESOURCE_EXHAUSTED = 8;

    /**
     * Operation was rejected because the system is not in a state required for
     * the operation's execution. For example, directory to be deleted may be
     * non-empty, an rmdir operation is applied to a non-directory, etc.
     * A litmus test that may help a service implementor in deciding
     * between FAILED_PRECONDITION, ABORTED, and UNAVAILABLE:
     *  (a) Use UNAVAILABLE if the client can retry just the failing call.
     *  (b) Use ABORTED if the client should retry at a higher-level
     *      (e.g., restarting a read-modify-write sequence).
     *  (c) Use FAILED_PRECONDITION if the client should not retry until
     *      the system state has been explicitly fixed. E.g., if an "rmdir"
     *      fails because the directory is non-empty, FAILED_PRECONDITION
     *      should be returned since the client should not retry unless
     *      they have first fixed up the directory by deleting files from it.
     *  (d) Use FAILED_PRECONDITION if the client performs conditional
     *      REST Get/Update/Delete on a resource and the resource on the
     *      server does not match the condition. E.g., conflicting
     *      read-modify-write on the same resource.
     */
    const STATUS_FAILED_PRECONDITION = 9;

    /**
     * The operation was aborted, typically due to a concurrency issue like
     * sequencer check failures, transaction aborts, etc.
     * See litmus test above for deciding between FAILED_PRECONDITION, ABORTED and UNAVAILABLE.
     */
    const STATUS_ABORTED = 10;

    /**
     * Operation was attempted past the valid range. E.g., seeking or reading
     * past end of file.
     *
     * Unlike INVALID_ARGUMENT, this error indicates a problem that may be fixed
     * if the system state changes. For example, a 32-bit file system will
     * generate INVALID_ARGUMENT if asked to read at an offset that is not in the
     * range [0,2^32-1], but it will generate OUT_OF_RANGE if asked to read from
     * an offset past the current file size.
     *
     * There is a fair bit of overlap between FAILED_PRECONDITION and
     * OUT_OF_RANGE. We recommend using OUT_OF_RANGE (the more specific error)
     * when it applies so that callers who are iterating through a space can
     * easily look for an OUT_OF_RANGE error to detect when they are done.
     */
    const STATUS_OUT_OF_RANGE = 11;

    /**
     * Operation is not implemented or not supported/enabled in this service.
     */
    const STATUS_UNIMPLEMENTED = 12;

    /**
     * Internal errors. Means some invariants expected by underlying System has
     * been broken. If you see one of these errors, Something is very broken.
     */
    const STATUS_INTERNAL = 13;

    /**
     * The service is currently unavailable. This is a most likely a transient
     * condition and may be corrected by retrying with a backoff. Note that it is
     * not always safe to retry non-idempotent operations.
     *
     * \warning Although data MIGHT not have been transmitted when this
     * status occurs, there is NOT A GUARANTEE that the server has not seen
     * anything. So in general it is unsafe to retry on this status code
     * if the call is non-idempotent.
     *
     * See litmus test above for deciding between FAILED_PRECONDITION, ABORTED,
     * and UNAVAILABLE.
     */
    const STATUS_UNAVAILABLE = 14;

    /**
     * Unrecoverable data loss or corruption.
     */
    const STATUS_DATA_LOSS = 15;

    /**
     * Force users to include a default branch
     */
    const STATUS_DO_NOT_USE = -1;

    /**
     * Statuses list
     */
    const STATUS_LIST = [
        self::STATUS_OK,
        self::STATUS_CANCELLED,
        self::STATUS_UNKNOWN,
        self::STATUS_INVALID_ARGUMENT,
        self::STATUS_DEADLINE_EXCEEDED,
        self::STATUS_NOT_FOUND,
        self::STATUS_ALREADY_EXISTS,
        self::STATUS_PERMISSION_DENIED,
        self::STATUS_UNAUTHENTICATED,
        self::STATUS_RESOURCE_EXHAUSTED,
        self::STATUS_FAILED_PRECONDITION,
        self::STATUS_ABORTED,
        self::STATUS_OUT_OF_RANGE,
        self::STATUS_UNIMPLEMENTED,
        self::STATUS_INTERNAL,
        self::STATUS_UNAVAILABLE,
        self::STATUS_DATA_LOSS,
        self::STATUS_DO_NOT_USE,
    ];

    /** @var string Host on GRPC server */
    private $serverHost;

    /** @var GPBEmpty */
    private $GPBEmpty;

    /** @var Channel */
    private $channel;

    /** @var array Options for GRPC channel */
    private $channelOpts;

    /** @var LoginResponse */
    private $userLogin;

    /**
     * ConnectService constructor.
     * @param ContainerInterface       $container
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(ContainerInterface $container, EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct($container, $eventDispatcher);

        $this->initConfiguration();
        $this->createGPBEmpty();
        $this->createChannel();
        $this->auth();
    }

    /**
     * Init configurations
     */
    private function initConfiguration()
    {
        $this->serverHost = $this->parameterBag->get('mt.connection.gprc.host');
        if (!$this->serverHost) {
            throw new RuntimeException('Не корректно указан GRPC сервер!');
        }

        $resourceCert = file_get_contents($this->parameterBag->get('mt.connection.gprc.ssl_root'));
        $this->channelOpts = [
            'grpc.ssl_target_name_override' => $this->parameterBag->get('mt.connection.gprc.ssl_target'),
            'grpc.default_authority' => $this->parameterBag->get('mt.connection.gprc.default_authority'),
            'grpc.primary_user_agent' => 'Php client',
            'credentials' => ChannelCredentials::createSsl($resourceCert),
        ];
    }

    /**
     * Create GPB empty
     */
    private function createGPBEmpty()
    {
        $this->GPBEmpty = new GPBEmpty();
    }

    /**
     * Create GRPC channel
     */
    private function createChannel()
    {
        $this->channel = new Channel($this->serverHost, $this->channelOpts);
    }

    /**
     * Auth on GRPC server
     */
    private function auth()
    {
        $user = $this->parameterBag->get('mt.connection.gprc.user');
        if (!$user) {
            throw new RuntimeException('Не указан пользователь от GRPC сервера!');
        }
        $password = $this->parameterBag->get('mt.connection.gprc.password');
        if (!$password) {
            throw new RuntimeException('Не указан пароль от GRPC сервера!');
        }
        $authClient = new AuthenticationApiServerClient($this->serverHost, $this->channelOpts, $this->channel);
        $loginRequest = $authClient->Login($this->GPBEmpty, [
            'auth-user' => [$user],
            'auth-password' => [$password]
        ]);
        [$this->userLogin, $status] = $loginRequest->wait();
        if ($status->code !== 0) {
            throw new RuntimeException('GRPC login failed! ' . json_encode($status));
        }
    }

    /**
     * @return mixed
     */
    public function getServerHost(): string
    {
        return $this->serverHost;
    }

    /**
     * @param mixed $serverHost
     * @return GRPCConnection
     */
    public function setServerHost($serverHost): self
    {
        $this->serverHost = $serverHost;
        return $this;
    }

    /**
     * @return GPBEmpty
     */
    public function getGPBEmpty(): GPBEmpty
    {
        return $this->GPBEmpty;
    }

    /**
     * @param GPBEmpty $GPBEmpty
     * @return GRPCConnection
     */
    public function setGPBEmpty(GPBEmpty $GPBEmpty): self
    {
        $this->GPBEmpty = $GPBEmpty;
        return $this;
    }

    /**
     * @return Channel
     */
    public function getChannel(): Channel
    {
        return $this->channel;
    }

    /**
     * @param Channel $channel
     * @return GRPCConnection
     */
    public function setChannel(Channel $channel): self
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * @return array
     */
    public function getChannelOpts(): array
    {
        return $this->channelOpts;
    }

    /**
     * @param array $channelOpts
     * @return GRPCConnection
     */
    public function setChannelOpts(array $channelOpts): self
    {
        $this->channelOpts = $channelOpts;
        return $this;
    }

    /**
     * @return LoginResponse
     */
    public function getUserLogin(): LoginResponse
    {
        return $this->userLogin;
    }

    /**
     * @param LoginResponse $userLogin
     * @return GRPCConnection
     */
    public function setUserLogin(LoginResponse $userLogin): self
    {
        $this->userLogin = $userLogin;
        return $this;
    }
}

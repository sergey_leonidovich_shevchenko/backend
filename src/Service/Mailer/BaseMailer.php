<?php declare(strict_types=1);

namespace App\Service\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Mailer\Mailer;
use App\EventDispatcher\Event\MailerEvent;

/**
 * Class BaseMailer
 * @package App\Service\Mailer
 */
abstract class BaseMailer
{
    /** @var ContainerInterface */
    protected $container;

    /** @var Email */
    protected $email;

    /** @var Mailer */
    protected $mailer;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * AccountMailer constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->eventDispatcher = $this->container->get('event_dispatcher');
        $this->email = $this->container->get(TemplatedEmail::class);
        $this->email->from(new Address($this->container->getParameter('mailer.sender')));
        $this->mailer = $this->container->get(Mailer::class);
    }

    /**
     * @param string $email
     * @return self
     */
    public function to(string $email): self
    {
        $this->email->to(new Address($email));
        return $this;
    }

    /**
     * @param string[] $emailList
     * @return self
     */
    public function toList(array $emailList): self
    {
        $toAddresses = [];
        foreach ($emailList as $email) {
            $toAddresses[] = new Address($email);
        }
        $this->email->to($toAddresses);
        return $this;
    }

    /**
     * @return bool
     */
    public function send(): bool
    {
        /** @var MailerEvent $event */
        $event = $this->container->get(MailerEvent::class);
        $event->setMailer($this->mailer);
        $event->setEmail($this->email);

        try {
            $this->mailer->send($this->email);
            $this->eventDispatcher->dispatch($event, MailerEvent::EVENT_EMAIL_SEND);
            return true;
        } catch (TransportExceptionInterface $e) {
            $this->eventDispatcher->dispatch($event, MailerEvent::EVENT_EMAIL_SEND_EXCEPTION);
            return false;
        }
    }
}

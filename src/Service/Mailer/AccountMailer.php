<?php declare(strict_types=1);

namespace App\Service\Mailer;

use App\Entity\Api\Account;
use App\Entity\Api\PersonalArea;

/**
 * Class AccountMailer
 * @package App\Service\Mailer
 */
class AccountMailer extends BaseMailer
{
    /**
     * @param Account $account
     * @param array   $user
     * @param string  $masterPassword
     * @param string  $investorPassword
     * @return bool
     */
    public function saveAccount(Account $account, array $user, string $masterPassword, string $investorPassword): bool
    {
        $this->email->htmlTemplate('emails/account/save-account.html.twig');
        $this->email->context([
            'name' => $user['name'],
            'login' => $user['login'],
            'masterPassword' => $masterPassword,
            'investorPassword' => $investorPassword,
        ]);

        /** @var PersonalArea $personalArea */
        $personalArea = $account->getPersonalArea();
        $this->to($personalArea->getEmail());

        return $this->send();
    }
}

<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\UserGroup;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\UserGroupForm;
use App\Repository\Api\UserGroupRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use RuntimeException;

/**
 * Class UserGroupService
 * @package App\Service\Api
 */
final class UserGroupService extends BaseApiService
{
    /**
     * Создать пользовательскую группу
     * @param array $userGroupData Необязательные данные
     * @return UserGroup объект
     * @throws EntityException
     * @throws Exception
     */
    public function createUserGroup(array $userGroupData): UserGroup
    {
        $userGroup = new UserGroup($userGroupData);
        $this->formSubmitData(UserGroupForm::class, $userGroup, $userGroupData, [
            'form_options' => [
                'validation_groups' => ['CreateUserGroup'],
            ],
        ]);

        $this->em->persist($userGroup);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $userGroup);

        return $userGroup;
    }

    /**
     * Получить пользовательскую группу по ее Id или названию
     * @param int|string $idOrName Id пользовательской группы или название
     * @return UserGroup|null
     */
    public function getUserGroup($idOrName): ?UserGroup
    {
        return is_numeric($idOrName)
            ? $this->getUserGroupById((int)$idOrName)
            : $this->getUserGroupByName($idOrName);
    }

    /**
     * Получить пользовательскую группу по ее ID
     * @param int $userGroupId ID группы
     * @return UserGroup|null
     */
    public function getUserGroupById(int $userGroupId): ?UserGroup
    {
        /** @var UserGroup|null $userGroup */
        /** @var UserGroupRepository $userGroupRepository */
        $userGroupRepository = $this->em->getRepository(UserGroup::class);
        $userGroup = $userGroupRepository->find($userGroupId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $userGroup);
        return $userGroup;
    }

    /**
     * Получить пользовательскую группу по ее названию
     * @param string $userGroupName Название группы
     * @return UserGroup|null
     */
    public function getUserGroupByName(string $userGroupName): ?UserGroup
    {
        /** @var UserGroup|null $userGroup */
        /** @var UserGroupRepository $userGroupRepository */
        $userGroupRepository = $this->em->getRepository(UserGroup::class);
        $userGroup = $userGroupRepository->findOneBy(['name' => $userGroupName]);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $userGroup);
        return $userGroup;
    }

    /**
     * Получить список пользовательских групп
     * @param int $page
     * @param int $countPerPage
     * @return UserGroup[]|null
     */
    public function getUserGroupList(int $page, int $countPerPage = 25): ?array
    {
        /** @var UserGroupRepository $userGroupRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(UserGroup::class);
        $userGroupRepository = $this->em->getRepository(UserGroup::class);
        $userGroupList = $userGroupRepository->getList(UserGroup::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $userGroupList);

        return $userGroupList;
    }

    /**
     * @param UserGroup|int $userGroup
     * @param array         $newUserGroupData
     * @return UserGroup|null
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateUserGroup($userGroup, array $newUserGroupData): ?UserGroup
    {
        if (is_int($userGroup)) {
            $userGroup = $this->getUserGroupById((int)$userGroup);
        }

        if (!$userGroup) {
            throw new RuntimeException('Updated user group not found on system!');
        }

        $currentUserGroupData = $this->serializer->toArray($userGroup);
        $newUserGroupData = array_merge($currentUserGroupData, $newUserGroupData);

        $this->formSubmitData(UserGroupForm::class, $userGroup, $newUserGroupData, [
            'form_options' => [
                'validation_groups' => ['UpdateUserGroup'],
            ],
        ]);

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $userGroup);

        return $userGroup;
    }

    /**
     * Удалить пользовательскую группу по ее Id или названию
     * @param $userGroupIdOrName
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUserGroup($userGroupIdOrName): void
    {
        is_numeric($userGroupIdOrName)
            ? $this->deleteUserGroupById($userGroupIdOrName)
            : $this->deleteUserGroupByName($userGroupIdOrName);
    }

    /**
     * Удаление пользовательской группы по ее ID
     * @param int $userGroupId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUserGroupById(int $userGroupId): void
    {
        $userGroupRepository = $this->em->getRepository(UserGroup::class);
        $userGroup = $userGroupRepository->find($userGroupId);
        if (!$userGroup instanceof UserGroup) {
            throw new RuntimeException("User group with ID: {$userGroupId} not found!");
        }
        $this->_delete($userGroup);
    }

    /**
     * Удаление пользовательской группы
     * @param UserGroup $userGroup
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function _delete(UserGroup $userGroup): void
    {
        $userGroupClone = clone $userGroup;
        $this->em->remove($userGroup);
        $this->em->flush();

        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $userGroupClone);
    }

    /**
     * Удаление пользовательской группы по ее названию
     * @param string $userGroupName
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUserGroupByName(string $userGroupName): void
    {
        $userGroupRepository = $this->em->getRepository(UserGroup::class);
        $userGroup = $userGroupRepository->findOneBy(['name' => $userGroupName]);
        if (!$userGroup instanceof UserGroup) {
            throw new RuntimeException("User group with ID: {$userGroupName} not found!");
        }
        $this->_delete($userGroup);
    }

    /**
     * Получить количество пользовательских групп
     * @return int
     */
    public function totalUserGroup(): int
    {
        /** @var UserGroupRepository $userGroupRepository */
        $userGroupRepository = $this->em->getRepository(UserGroup::class);
        $total = $userGroupRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\AccessToken;
use App\Entity\Api\AuthCode;
use App\Entity\Api\RefreshToken;
use App\Entity\Api\User;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\UserForm;
use App\Repository\Api\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Form\FormError;

/**
 * Class UserService
 * @package App\Service\Api
 */
final class UserService extends BaseApiService
{
    /**
     * Получить список пользователей
     * @param int $page
     * @param int $countPerPage
     * @return User[]|null
     */
    public function getUserList(int $page, int $countPerPage = 25): ?array
    {
        /** @var UserRepository $userRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(User::class);
        $userRepository = $this->em->getRepository(User::class);
        $userList = $userRepository->getList(User::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $userList);
        return $userList;
    }

    /**
     * Создать нового пользователя
     * @param $userData
     * @return User
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createUser($userData): User
    {
        $user = new User();
        $form = $this->form->create(UserForm::class, $user);
        $form->submit($userData);

        $user->setEmail($user->getEmail() ?? $user->getUsername());
        if (empty($user->getEmail())) {
            $error = new FormError('Email пользователя обязателен для заполнения.');
            $form->addError($error);
        }
        if (empty($user->getUsername())) {
            $user->setUsername($userData['email'] ?? $userData['username']);
            $user->setUsernameCanonical($userData['email'] ?? $userData['username']);
        }
        if (!$user->getEmail() && $user->getUsername()) {
            $user->setEmail($user->getUsername());
        }
        if (!$user->getPlainPassword() && $user->getPassword()) {
            $user->setPlainPassword($user->getPassword());
        }
        $user->setPassword(null);

        $user->setEnabled((bool)($userData['enabled'] ?? false));

        if (!empty($userData['roles'])) {
            foreach ($userData['roles'] as $role) {
                $user->addRole($role);
            }
        }

        $isRegistered = (bool)$this->em->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);
        if ($isRegistered) {
            $msg = 'Пользователь с таким логином уже зарегистрирован в системе. ';
            $msg .= 'Пожалуйста введите другой логин.';
            $error = new FormError($msg);
            $form->addError($error);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($user);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $user);

        return $user;
    }

    /**
     * Получить пользователя по ID
     * @param int $userId
     * @return User|null
     */
    public function get(int $userId): ?User
    {
        /** @var User|null $user */
        /** @var UserRepository $userRepository */
        $userRepository = $this->em->getRepository(User::class);
        $user = $userRepository->find($userId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $user);
        return $user;
    }

    /**
     * Обновление информации о пользователе
     * @param User  $user
     * @param array $newUserData
     * @return User
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateUser(User $user, array $newUserData): User
    {
        $currentUserData = $this->serializer->toArray($user);

        $form = $this->form->create(UserForm::class, $user);
        $newUserData = array_merge($currentUserData, $newUserData);
        $form->submit($newUserData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $user);

        return $user;
    }

    /**
     * Удаление пользователя
     * @param User $user
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUser(User $user): void
    {
        $accessTokenRepository = $this->em->getRepository(AccessToken::class);
        $accessTokenList = $accessTokenRepository->findBy(['user' => $user]);
        foreach ($accessTokenList as $accessToken) {
            $this->em->remove($accessToken);
        }

        $refreshTokenRepository = $this->em->getRepository(RefreshToken::class);
        $refreshTokenList = $refreshTokenRepository->findBy(['user' => $user]);
        foreach ($refreshTokenList as $refreshToken) {
            $this->em->remove($refreshToken);
        }

        $authCodeRepository = $this->em->getRepository(AuthCode::class);
        $authCodeList = $authCodeRepository->findBy(['user' => $user]);
        foreach ($authCodeList as $authCode) {
            $this->em->remove($authCode);
        }

        $userClone = clone $user;
        $this->em->remove($user);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $userClone);
    }

    /**
     * Получить количество пользователей
     * @return int
     */
    public function totalUser(): int
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->em->getRepository(User::class);
        $total = $userRepository->count([]);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

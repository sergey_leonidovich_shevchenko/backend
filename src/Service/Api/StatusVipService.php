<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\StatusVip;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\StatusVipForm;
use App\Repository\Api\StatusVipRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class StatusVipService
 * @package App\Service\Api
 */
final class StatusVipService extends BaseApiService
{
    /**
     * Получить список VIP-статусов
     * @param int $page
     * @param int $countPerPage
     * @return StatusVip[]|null
     */
    public function getStatusVipList(int $page, int $countPerPage = 25): ?array
    {
        /** @var StatusVipRepository $statusVipRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(StatusVip::class);
        $statusVipRepository = $this->em->getRepository(StatusVip::class);
        $statusVipList = $statusVipRepository->getList(StatusVip::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $statusVipList);
        return $statusVipList;
    }

    /**
     * Создать новый VIP-статус
     * @param array $statusVipData
     * @return StatusVip
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createStatusVip(array $statusVipData): StatusVip
    {
        $statusVip = new StatusVip();
        $form = $this->form->create(StatusVipForm::class, $statusVip);
        $form->submit($statusVipData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($statusVip);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $statusVip);

        return $statusVip;
    }

    /**
     * Получить Vip-статус по ID
     * @param int $statusVipId
     * @return StatusVip|null
     */
    public function get(int $statusVipId): ?StatusVip
    {
        /** @var StatusVip $statusVip */
        /** @var StatusVipRepository $statusVipRepository */
        $statusVipRepository = $this->em->getRepository(StatusVip::class);
        $statusVip = $statusVipRepository->find($statusVipId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $statusVip);
        return $statusVip;
    }

    /**
     * Обновление информации о VIP-статусе
     * @param StatusVip $statusVip
     * @param array     $newStatusVipData
     * @return StatusVip
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateStatusVip(StatusVip $statusVip, array $newStatusVipData): StatusVip
    {
        $currentStatusVipData = $this->serializer->toArray($statusVip);

        $form = $this->form->create(StatusVipForm::class, $statusVip);
        $newStatusVipData = array_merge($currentStatusVipData, $newStatusVipData);
        $form->submit($newStatusVipData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $statusVip);

        return $statusVip;
    }

    /**
     * Удаление VIP-статуса
     * @param StatusVip $statusVip
     * @return void
     */
    public function deleteStatusVip(StatusVip $statusVip): void
    {
        $statusVipClone = clone $statusVip;
        $this->em->remove($statusVip);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $statusVipClone);
    }

    /**
     * Получить количество Vip-татусов
     * @return int
     */
    public function totalStatusVip(): int
    {
        /** @var StatusVipRepository $statusVipRepository */
        $statusVipRepository = $this->em->getRepository(StatusVip::class);
        $total = $statusVipRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

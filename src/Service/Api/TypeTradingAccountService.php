<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\TypeTradingAccount;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\FormTradingAccountForm;
use App\Repository\Api\TypeTradingAccountRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class TypeTradingAccountService
 * @package App\Service\Api
 */
final class TypeTradingAccountService extends BaseApiService
{
    /**
     * Получить список типов торговых счетов
     * @param int $page
     * @param int $countPerPage
     * @return TypeTradingAccount[]|null
     */
    public function getTypeTradingAccountList(int $page, int $countPerPage = 25): ?array
    {
        /** @var TypeTradingAccountRepository $typeTradingAccountRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(TypeTradingAccount::class);
        $typeTradingAccountRepository = $this->em->getRepository(TypeTradingAccount::class);
        $typeTradingAccountList = $typeTradingAccountRepository->getList(TypeTradingAccount::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $typeTradingAccountList);
        return $typeTradingAccountList;
    }

    /**
     * Создать новый тип торгового счета
     * @param array $typeTradingAccountData
     * @return TypeTradingAccount
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createTypeTradingAccount(array $typeTradingAccountData): TypeTradingAccount
    {
        $typeTradingAccount = new TypeTradingAccount();
        $form = $this->form->create(FormTradingAccountForm::class, $typeTradingAccount);
        $form->submit($typeTradingAccountData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($typeTradingAccount);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $typeTradingAccount);

        return $typeTradingAccount;
    }

    /**
     * Получить тип торгового счета по ID
     * @param int $typeTradingAccountId
     * @return TypeTradingAccount|null
     */
    public function get(int $typeTradingAccountId): ?TypeTradingAccount
    {
        /** @var TypeTradingAccountRepository $typeTradingAccountRepository */
        /** @var TypeTradingAccount|null $typeTradingAccount */
        $typeTradingAccountRepository = $this->em->getRepository(TypeTradingAccount::class);
        $typeTradingAccount = $typeTradingAccountRepository->find($typeTradingAccountId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $typeTradingAccount);
        return $typeTradingAccount;
    }

    /**
     * Обновление информации о типе торгового счета
     * @param TypeTradingAccount $typeTradingAccount
     * @param array              $newTypeTradingAccountData
     * @return TypeTradingAccount
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateTypeTradingAccount(TypeTradingAccount $typeTradingAccount, array $newTypeTradingAccountData): TypeTradingAccount
    {
        $currentTypeTradingAccountData = $this->serializer->toArray($typeTradingAccount);

        $form = $this->form->create(FormTradingAccountForm::class, $typeTradingAccount);
        $newTypeTradingAccountData = array_merge($currentTypeTradingAccountData, $newTypeTradingAccountData);
        $form->submit($newTypeTradingAccountData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $typeTradingAccount);

        return $typeTradingAccount;
    }

    /**
     * Удаление типа торгового счета
     * @param TypeTradingAccount $typeTradingAccount
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteTypeTradingAccount(TypeTradingAccount $typeTradingAccount): void
    {
        $typeTradingAccountClone = clone $typeTradingAccount;
        $this->em->remove($typeTradingAccount);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $typeTradingAccountClone);
    }

    /**
     * Получить количество типов торговых счетов
     * @return int
     */
    public function totalTypeTradingAccount(): int
    {
        /** @var TypeTradingAccountRepository $typeTradingAccountRepository */
        $typeTradingAccountRepository = $this->em->getRepository(TypeTradingAccount::class);
        $total = $typeTradingAccountRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\SalesDepartment;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\SalesDepartmentForm;
use App\Repository\Api\SalesDepartmentRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class SalesDepartmentService
 * @package App\Service\Api
 */
final class SalesDepartmentService extends BaseApiService
{
    /**
     * Получить список департаментов продаж
     * @param int $page
     * @param int $countPerPage
     * @return SalesDepartment[]|null
     */
    public function getSalesDepartmentList(int $page, int $countPerPage = 25): ?array
    {
        /** @var SalesDepartmentRepository $salesDepartmentRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(SalesDepartment::class);
        $salesDepartmentRepository = $this->em->getRepository(SalesDepartment::class);
        $salesDepartmentList = $salesDepartmentRepository->getList(SalesDepartment::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $salesDepartmentList);
        return $salesDepartmentList;
    }

    /**
     * Создать новый департамент продаж
     * @param array $salesDepartmentData
     * @return SalesDepartment
     * @throws EntityException
     * @throws ORMException
     */
    public function createSalesDepartment(array $salesDepartmentData): SalesDepartment
    {
        $salesDepartment = new SalesDepartment();
        $form = $this->form->create(SalesDepartmentForm::class, $salesDepartment);
        $form->submit($salesDepartmentData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($salesDepartment);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $salesDepartment);

        return $salesDepartment;
    }

    /**
     * Получить департамент продаж по ID
     * @param int $salesDepartmentId
     * @return SalesDepartment|null
     */
    public function get(int $salesDepartmentId): ?SalesDepartment
    {
        /** @var SalesDepartment $salesDepartment */
        /** @var SalesDepartmentRepository $salesDepartmentRepository */
        $salesDepartmentRepository = $this->em->getRepository(SalesDepartment::class);
        $salesDepartment = $salesDepartmentRepository->find($salesDepartmentId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $salesDepartment);
        return $salesDepartment;
    }

    /**
     * Обновление департамента продаж
     * @param SalesDepartment $salesDepartment
     * @param array           $newSalesDepartmentData
     * @return SalesDepartment
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateSalesDepartment(SalesDepartment $salesDepartment, array $newSalesDepartmentData): SalesDepartment
    {
        $currentSalesDepartmentData = $this->serializer->toArray($salesDepartment);

        $form = $this->form->create(SalesDepartmentForm::class, $salesDepartment);
        $newSalesDepartmentData = array_merge($currentSalesDepartmentData, $newSalesDepartmentData);
        $form->submit($newSalesDepartmentData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $salesDepartment);

        return $salesDepartment;
    }

    /**
     * Удаление департамента продаж
     * @param SalesDepartment $salesDepartment
     * @return void
     */
    public function deleteSalesDepartment(SalesDepartment $salesDepartment): void
    {
        $salesDepartmentClone = clone $salesDepartment;
        $this->em->remove($salesDepartment);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $salesDepartmentClone);
    }

    /**
     * Получить количество департаментов продаж
     * @return int
     */
    public function totalSalesDepartment(): int
    {
        /** @var SalesDepartmentRepository $salesDepartmentRepository */
        $salesDepartmentRepository = $this->em->getRepository(SalesDepartment::class);
        $total = $salesDepartmentRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\PersonalArea;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\PersonalAreaForm;
use App\Repository\Api\PersonalAreaRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class PersonalAreaService
 * @package App\Service\Api
 */
final class PersonalAreaService extends BaseApiService
{
    /**
     * Получить список личных кабинетов
     * @param int $page
     * @param int $countPerPage
     * @return PersonalArea[]|null
     */
    public function getPersonalAreaList(int $page, int $countPerPage = 25): ?array
    {
        /** @var PersonalAreaRepository $personalAreaRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(PersonalArea::class);
        $personalAreaRepository = $this->em->getRepository(PersonalArea::class);
        $personalAreaList = $personalAreaRepository->getList(PersonalArea::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $personalAreaList);
        return $personalAreaList;
    }

    /**
     * Создать новый личный кабинет
     * @param array $personalAreaData
     * @return PersonalArea
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createPersonalArea(array $personalAreaData): PersonalArea
    {
        $personalArea = new PersonalArea();
        $form = $this->form->create(PersonalAreaForm::class, $personalArea);
        $form->submit($personalAreaData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($personalArea);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $personalArea);

        return $personalArea;
    }

    /**
     * Получить личный кабинет по ID
     * @param int $personalAreaId
     * @return PersonalArea|null
     */
    public function get(int $personalAreaId): ?PersonalArea
    {
        /** @var PersonalArea $personalArea */
        /** @var PersonalAreaRepository $personalAreaRepository */
        $personalAreaRepository = $this->em->getRepository(PersonalArea::class);
        $personalArea = $personalAreaRepository->find($personalAreaId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $personalArea);
        return $personalArea;
    }

    /**
     * Обновление информации о личном кабинете
     * @param PersonalArea $personalArea
     * @param array        $newPersonalAreaData
     * @return PersonalArea
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updatePersonalArea(PersonalArea $personalArea, array $newPersonalAreaData): PersonalArea
    {
        $currentPersonalAreaData = $this->serializer->toArray($personalArea);

        $form = $this->form->create(PersonalAreaForm::class, $personalArea);
        $newPersonalAreaData = array_merge($currentPersonalAreaData, $newPersonalAreaData);
        $form->submit($newPersonalAreaData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $personalArea);

        return $personalArea;
    }

    /**
     * Удаление личного кабинета
     * @param PersonalArea $personalArea
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deletePersonalArea(PersonalArea $personalArea): void
    {
        $personalAreaClone = clone $personalArea;
        $this->em->remove($personalArea);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $personalAreaClone);
    }

    /**
     * Получить количество личных кабинетов
     * @return int
     */
    public function totalPersonalArea(): int
    {
        /** @var PersonalAreaRepository $personalAreaRepository */
        $personalAreaRepository = $this->em->getRepository(PersonalArea::class);
        $total = $personalAreaRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

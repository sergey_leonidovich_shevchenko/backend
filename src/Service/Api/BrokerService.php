<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\Broker;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\BrokerForm;
use App\Helper\EntityHelper;
use App\Repository\Api\BrokerRepository;
use Symfony\Component\Form\FormError;

/**
 * Class BrokerService
 * @package App\Service\Api
 */
final class BrokerService extends BaseApiService
{
    /**
     * Создать нового брокера
     * @param array $brokerData
     * @return Broker
     * @throws EntityException
     */
    public function createBroker(array $brokerData): Broker
    {
        $broker = new Broker();
        $form = $this->form->create(BrokerForm::class, $broker);
        $form->submit($brokerData);

        if (empty($brokerData['name'])) {
            $error = new FormError('Имя брокера обязательно для заполнения.');
            $form->addError($error);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($broker);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $broker);

        return $broker;
    }

    /**
     * Получить брокера по его ID
     * @param int $brokerId
     * @return Broker|null
     */
    public function get(int $brokerId): ?Broker
    {
        /** @var BrokerRepository $brokerRepository */
        /** @var Broker $broker */
        $brokerRepository = $this->em->getRepository(Broker::class);
        $broker = $brokerRepository->find($brokerId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $broker);

        return $broker;
    }

    /**
     * Получить список брокеров
     * @param int $page
     * @param int $countPerPage
     * @return Broker[]|null
     */
    public function getBrokerList(int $page, int $countPerPage = 25): ?array
    {
        /** @var BrokerRepository $brokerRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(Broker::class);
        $brokerRepository = $this->em->getRepository(Broker::class);
        $brokerList = $brokerRepository->getList(Broker::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $brokerList);

        return $brokerList;
    }

    /**
     * Обновление информации о брокере
     * @param int   $id
     * @param array $newBrokerData
     * @return Broker
     * @throws EntityException
     */
    public function updateBroker(int $id, array $newBrokerData): Broker
    {
        /** @var BrokerRepository $brokerRepository */
        /** @var Broker $broker */
        $brokerRepository = $this->em->getRepository(Broker::class);
        $broker = $brokerRepository->find($id);
        $currentBrokerData = $this->serializer->toArray($broker);

        $form = $this->form->create(BrokerForm::class, $broker);
        $newBrokerData = EntityHelper::privateFieldToPublic($newBrokerData);
        $newBrokerData = array_merge($currentBrokerData, $newBrokerData);
        $form->submit($newBrokerData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $broker);

        return $broker;
    }

    /**
     * Удаление брокера
     * @param Broker $broker
     */
    public function deleteBroker(Broker $broker): void
    {
        $brokerClone = clone $broker;
        $this->em->remove($broker);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $brokerClone);
    }

    /**
     * Получить количество брокеров
     * @return int
     */
    public function totalBroker(): int
    {
        /** @var BrokerRepository $brokerRepository */
        $brokerRepository = $this->em->getRepository(Broker::class);
        $total = $brokerRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

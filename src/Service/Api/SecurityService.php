<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\User as UserEntity;

/**
 * Class SecurityService
 * @package App\Service\Api
 */
final class SecurityService extends BaseApiService
{
    /**
     * @param UserEntity $user
     * @return UserEntity
     */
    private function normalizeAliasesDataUser(UserEntity $user): UserEntity
    {
        if (!$user->getUsername() && $user->getEmail()) {
            $user->setUsername($user->getEmail());
        }

        if (!$user->getUsernameCanonical() && $user->getUsername()) {
            $user->setUsernameCanonical($user->getUsername());
        }

        if (!$user->getEmail() && $user->getUsername()) {
            $user->setEmail($user->getUsername());
        }

        if (!$user->getPlainPassword() && $user->getPassword()) {
            $user->setPlainPassword($user->getPassword());
        }

        $user->setPassword(null);

        return $user;
    }
}

<?php declare(strict_types=1);

namespace App\Service\Api\Security;

use App\Entity\Api\User;
use App\Entity\Api\UserGroup;
use App\Entity\Interfaces\EntityWithGroupsInterface;
use App\Entity\Interfaces\EntityWithRolesInterface;
use App\EventDispatcher\Event\Security\PermissionEvent;
use App\Exception\Security\PermissionException;
use App\Helper\Security\PermissionHelper;
use App\Service\Api\BaseApiService;
use Exception;

/**
 * Class PermissionService
 * @package App\Service\Security
 */
class PermissionService extends BaseApiService
{
    /**
     * Добавление списка ролей пользователю
     * @param int|string $userIdOrName Id или name пользователя
     * @param string[]   $rolesToAdd   Роли на добавление
     * @return User
     * @throws Exception
     */
    public function addRolesToUser($userIdOrName, array $rolesToAdd = []): User
    {
        $newRoles = [];
        try {
            /** @var User $user */
            $user = $this->_getApiEntity(User::class, $userIdOrName);

            $oldRoles = $user->getRoles(false);
            foreach ($rolesToAdd as $role) {
                $user->addRole($role);
            }
            $newRoles = $user->getRoles(false);
            $this->_checkRolesForExistence($user);

            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_EXCEPTION;
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $user,
                'new_roles' => $newRoles,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER;
        $this->dispatchPermissionRoleEvent($eventName, $user, $oldRoles, $newRoles);

        return $user;
    }

    /**
     * Удалить список ролей у пользователя
     * @param int|string $userIdOrName    Id или name пользователя
     * @param string[]   $rolesOnDeletion Роли на удаление
     * @return User
     * @throws PermissionException
     */
    public function deleteRolesFromUser($userIdOrName, array $rolesOnDeletion): User
    {
        $newRoles = [];
        try {
            /** @var User $user */
            $user = $this->_getApiEntity(User::class, $userIdOrName);

            $oldRoles = $user->getRoles(false);
            foreach ($rolesOnDeletion as $role) {
                $user->deleteRole($role);
            }
            $newRoles = $user->getRoles(false);

            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_EXCEPTION;
            $this->dispatchPermissionRoleEvent($eventName, $user, null, $newRoles);
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $user,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER;
        $this->dispatchPermissionRoleEvent($eventName, $user, $oldRoles, $newRoles);

        return $user;
    }

    /**
     * Установка нового списка ролей пользователю
     * @param int|string $userIdOrName Id или name пользователя
     * @param string[]   $rolesToAdd   Роли на добавление
     * @return User
     * @throws PermissionException
     */
    public function setRolesToUser($userIdOrName, array $rolesToAdd = []): User
    {
        $newRoles = [];
        try {
            /** @var User $user */
            $user = $this->_getApiEntity(User::class, $userIdOrName);

            $oldRoles = $user->getRoles(false);
            $user->clearRoles();
            $this->em->flush();
            foreach ($rolesToAdd as $role) {
                $user->addRole($role);
            }
            $newRoles = $user->getRoles(false);
            $this->_checkRolesForExistence($user);

            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_EXCEPTION;
            $this->dispatchPermissionRoleEvent($eventName, $user, null, $newRoles);
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $user,
                'new_roles' => $newRoles,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER;
        $this->dispatchPermissionRoleEvent($eventName, $user, $oldRoles, $newRoles);

        return $user;
    }

    /**
     * Добавление списка ролей пользовательской группе
     * @param int|string $userGroupIdOrName Id или name группы
     * @param string[]   $rolesToAdd        Роли на добавление
     * @return UserGroup
     * @throws PermissionException
     */
    public function addRolesToUserGroup($userGroupIdOrName, array $rolesToAdd = []): UserGroup
    {
        $newRoles = [];
        try {
            /** @var UserGroup $userGroup */
            $userGroup = $this->_getApiEntity(UserGroup::class, $userGroupIdOrName);

            $oldRoles = $userGroup->getRoles(false);
            foreach ($rolesToAdd as $role) {
                $userGroup->addRole($role);
            }
            $newRoles = $userGroup->getRoles(false);
            $this->_checkRolesForExistence($userGroup);

            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP_EXCEPTION;
            $this->dispatchPermissionRoleEvent($eventName, $userGroup, null, $newRoles);
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $userGroup,
                'new_roles' => $newRoles,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP;
        $this->dispatchPermissionRoleEvent($eventName, $userGroup, $oldRoles, $newRoles);

        return $userGroup;
    }

    /**
     * Удаление списка ролей у пользовательской группы
     * @param int|string $userGroupIdOrName Id или name группы
     * @param string[]   $rolesOnDeletion   Роли на удаление
     * @return UserGroup
     * @throws PermissionException
     */
    public function deleteRolesFromUserGroup($userGroupIdOrName, array $rolesOnDeletion): UserGroup
    {
        $newRoles = [];
        try {
            /** @var UserGroup $userGroup */
            $userGroup = $this->_getApiEntity(UserGroup::class, $userGroupIdOrName);

            $oldRoles = $userGroup->getRoles(false);
            foreach ($rolesOnDeletion as $role) {
                $userGroup->deleteRole($role);
            }
            $newRoles = $userGroup->getRoles(false);

            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_GROUP_EXCEPTION;
            $this->dispatchPermissionRoleEvent($eventName, $userGroup, null, $newRoles);
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $userGroup,
                'new_roles' => $newRoles,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_ROLE_FROM_USER_GROUP;
        $this->dispatchPermissionRoleEvent($eventName, $userGroup, $oldRoles, $newRoles);

        return $userGroup;
    }

    /**
     * Установка нового списка ролей пользовательской группе
     * @param int|string $userGroupIdOrName Id или name группы
     * @param string[]   $rolesToAdd        Роли на добавление
     * @return UserGroup
     * @throws PermissionException
     */
    public function setRolesToUserGroup($userGroupIdOrName, array $rolesToAdd = []): UserGroup
    {
        $newRoles = [];
        try {
            /** @var UserGroup $userGroup */
            $userGroup = $this->_getApiEntity(UserGroup::class, $userGroupIdOrName);

            $oldRoles = $userGroup->getRoles(false);
            $userGroup->clearRoles();
            foreach ($rolesToAdd as $role) {
                $userGroup->addRole($role);
            }
            $newRoles = $userGroup->getRoles(false);
            $this->_checkRolesForExistence($userGroup);

            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP_EXCEPTION;
            $this->dispatchPermissionRoleEvent($eventName, $userGroup, null, $newRoles);
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $userGroup,
                'new_roles' => $newRoles,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP;
        $this->dispatchPermissionRoleEvent($eventName, $userGroup, $oldRoles, $newRoles);

        return $userGroup;
    }

    /**
     * Добавление списка групп ролей пользователю
     * @param int|string $userIdOrName Id или name пользователя
     * @param string[]   $groupsToAdd  Группы на добавление
     * @return User
     * @throws PermissionException
     */
    public function addUserGroupToUser($userIdOrName, array $groupsToAdd): User
    {
        $newGroups = [];
        try {
            /** @var User|EntityWithGroupsInterface $user */
            $user = $this->_getApiEntity(User::class, $userIdOrName);
            $userGroupRepository = $this->em->getRepository(UserGroup::class);

            $oldGroups = $user->getGroupNames();
            foreach ($groupsToAdd as $groupIdOrName) {
                $userGroup = is_numeric($groupIdOrName)
                    ? $userGroupRepository->find($groupIdOrName)
                    : $userGroupRepository->findOneBy(['name' => $groupIdOrName]);
                if ($userGroup instanceof UserGroup) {
                    $user->addGroup($userGroup);
                }
            }
            $newGroups = $user->getGroupNames();
            $this->_checkRolesForExistence($user);

            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER_EXCEPTION;
            $this->dispatchPermissionGroupEvent($eventName, $user, null, $newGroups);
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $user,
                'new_groups' => $newGroups,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER;
        $this->dispatchPermissionGroupEvent($eventName, $user, $oldGroups, $newGroups);

        return $user;
    }

    /**
     * Удаление списка групп ролей у пользователя
     * @param int|string $userIdOrName     Id или name пользователя
     * @param string[]   $groupsOnDeletion Группы на удаление
     * @return User
     * @throws PermissionException
     */
    public function deleteUserGroupFromUser($userIdOrName, array $groupsOnDeletion): User
    {
        $user = $userIdOrName;
        $newGroups = [];
        try {
            /** @var User $user */
            $user = $this->_getApiEntity(User::class, $userIdOrName);
            $userGroupRepository = $this->em->getRepository(UserGroup::class);

            $oldGroups = $user->getGroupNames();
            foreach ($groupsOnDeletion as $groupIdOrName) {
                $userGroup = is_numeric($groupIdOrName)
                    ? $userGroupRepository->find($groupIdOrName)
                    : $userGroupRepository->findOneBy(['name' => $groupIdOrName]);
                if ($userGroup instanceof UserGroup) {
                    $user->deleteGroup($userGroup);
                }
            }
            $newGroups = $user->getGroupNames();

            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER_EXCEPTION;
            $this->dispatchPermissionGroupEvent($eventName, $user, null, $newGroups);
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $user,
                'new_groups' => $newGroups,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER;
        $this->dispatchPermissionRoleEvent($eventName, $user, $oldGroups, $newGroups);

        return $user;
    }

    /**
     * Установка нового списка групп ролей пользователю
     * @param int|string $userIdOrName Id или name пользователя
     * @param string[]   $groupsToAdd  Группы на добавление
     * @return User
     * @throws PermissionException
     */
    public function setUserGroupToUser($userIdOrName, array $groupsToAdd): User
    {
        $newGroups = [];
        try {
            /** @var User $user */
            $user = $this->_getApiEntity(User::class, $userIdOrName);
            $userGroupRepository = $this->em->getRepository(UserGroup::class);

            $oldGroups = $user->getGroupNames();
            foreach ($groupsToAdd as $groupIdOrName) {
                $userGroup = is_numeric($groupIdOrName)
                    ? $userGroupRepository->find($groupIdOrName)
                    : $userGroupRepository->findOneBy(['name' => $groupIdOrName]);
                if ($userGroup instanceof UserGroup) {
                    $user->addGroup($userGroup);
                }
            }
            $newGroups = $user->getGroupNames();
            $this->_checkRolesForExistence($user);
            $this->em->flush();
        } catch (Exception $exception) {
            $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER_EXCEPTION;
            $this->dispatchPermissionGroupEvent($eventName, $user, null, $newGroups);
            throw new PermissionException($this->container, $eventName, $exception, [
                'entity' => $user,
                'new_groups' => $newGroups,
            ]);
        }

        $eventName = PermissionEvent::EVENT_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER;
        $this->dispatchPermissionRoleEvent($eventName, $user, $oldGroups, $newGroups);

        return $user;
    }

    /**
     * Проверить добавленные роли у сущности на существование
     * @param EntityWithRolesInterface $entityWithRoles
     */
    private function _checkRolesForExistence(EntityWithRolesInterface $entityWithRoles): void
    {
        /** @var PermissionHelper $roleHelper */
        $roleHelper = $this->container->get(PermissionHelper::class);

        $roleHelper->checkRolesForExistence(
            $entityWithRoles->getRoles(false),
            PermissionHelper::RETURN_EXCEPTION_IF_ERROR,
            );
    }
}

<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\SbCheckStatus;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\SbCheckStatusForm;
use App\Repository\Api\SbCheckStatusRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class SbCheckStatusService
 * @package App\Service\Api
 */
final class SbCheckStatusService extends BaseApiService
{
    /**
     * Получить список статусов проверки СБ
     * @param int $page
     * @param int $countPerPage
     * @return SbCheckStatus[]|null
     */
    public function getSbCheckStatusList(int $page, int $countPerPage = 25): ?array
    {
        /** @var SbCheckStatusRepository $sbCheckStatusRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(SbCheckStatus::class);
        $sbCheckStatusRepository = $this->em->getRepository(SbCheckStatus::class);
        $sbCheckStatusList = $sbCheckStatusRepository->getList(SbCheckStatus::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $sbCheckStatusList);
        return $sbCheckStatusList;
    }

    /**
     * Создать новый статус проверки СБ
     * @param array $sbCheckStatusData
     * @return SbCheckStatus
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createSbCheckStatus(array $sbCheckStatusData): SbCheckStatus
    {
        $sbCheckStatus = new SbCheckStatus();
        $form = $this->form->create(SbCheckStatusForm::class, $sbCheckStatus);
        $form->submit($sbCheckStatusData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($sbCheckStatus);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $sbCheckStatus);

        return $sbCheckStatus;
    }

    /**
     * Получить статус проверки СБ по ID
     * @param int $sbCheckStatusId
     * @return SbCheckStatus|null
     */
    public function get(int $sbCheckStatusId): ?SbCheckStatus
    {
        /** @var SbCheckStatus $sbCheckStatus */
        /** @var SbCheckStatusRepository $sbCheckStatusRepository */
        $sbCheckStatusRepository = $this->em->getRepository(SbCheckStatus::class);
        $sbCheckStatus = $sbCheckStatusRepository->find($sbCheckStatusId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $sbCheckStatus);
        return $sbCheckStatus;
    }

    /**
     * Обновление информации о статусе проверки СБ
     * @param SbCheckStatus $sbCheckStatus
     * @param array         $newSbCheckStatusData
     * @return SbCheckStatus
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateSbCheckStatus(SbCheckStatus $sbCheckStatus, array $newSbCheckStatusData): SbCheckStatus
    {
        $currentSbCheckStatusData = $this->serializer->toArray($sbCheckStatus);

        $form = $this->form->create(SbCheckStatusForm::class, $sbCheckStatus);
        $newSbCheckStatusData = array_merge($currentSbCheckStatusData, $newSbCheckStatusData);
        $form->submit($newSbCheckStatusData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $sbCheckStatus);

        return $sbCheckStatus;
    }

    /**
     * Удаление статуса проверки СБ
     * @param SbCheckStatus $sbCheckStatus
     * @return void
     */
    public function deleteSbCheckStatus(SbCheckStatus $sbCheckStatus): void
    {
        $sbCheckStatusClone = clone $sbCheckStatus;
        $this->em->remove($sbCheckStatus);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $sbCheckStatusClone);
    }

    /**
     * Получить количество статусов проверки СБ
     * @return int
     */
    public function totalSbCheckStatus(): int
    {
        /** @var SbCheckStatusRepository $sbCheckStatusRepository */
        $sbCheckStatusRepository = $this->em->getRepository(SbCheckStatus::class);
        $total = $sbCheckStatusRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

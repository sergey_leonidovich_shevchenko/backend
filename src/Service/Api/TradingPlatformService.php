<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\TradingPlatform;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\TradingPlatformForm;
use App\Repository\Api\TradingPlatformRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Service\MT\BalanceService;

/**
 * Class TradingPlatformService
 * @package App\Service\Api
 */
final class TradingPlatformService extends BaseApiService
{
    /** @var BalanceService */
    private $balanceService;

    /**
     * Получить список торговых платформ
     * @param int $page
     * @param int $countPerPage
     * @return TradingPlatform[]|null
     */
    public function getTradingPlatformList(int $page, int $countPerPage = 25): ?array
    {
        /** @var TradingPlatformRepository $tradingPlatformRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(TradingPlatform::class);
        $tradingPlatformRepository = $this->em->getRepository(TradingPlatform::class);
        $tradingPlatformList = $tradingPlatformRepository->getList(TradingPlatform::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $tradingPlatformList);
        return $tradingPlatformList;
    }

    /**
     * Создать новую торговую платформу
     * @param array $tradingPlatformData
     * @return TradingPlatform
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createTradingPlatform(array $tradingPlatformData): TradingPlatform
    {
        $tradingPlatform = new TradingPlatform();
        $form = $this->form->create(TradingPlatformForm::class, $tradingPlatform);
        $form->submit($tradingPlatformData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($tradingPlatform);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $tradingPlatform);

        return $tradingPlatform;
    }

    /**
     * Получить трейдинговую платформу по ID
     * @param int $tradingPlatformId
     * @return TradingPlatform|null
     */
    public function get(int $tradingPlatformId): ?TradingPlatform
    {
        /** @var TradingPlatform|null $tradingPlatform */
        /** @var TradingPlatformRepository $tradingPlatformRepository */
        $tradingPlatformRepository = $this->em->getRepository(TradingPlatform::class);
        $tradingPlatform = $tradingPlatformRepository->find($tradingPlatformId);
        if ($tradingPlatform) {
            // TODO: Вот здесь пока захардкожу, будем все время получать с сервера баланс когда запрашивается сущность
            $tradingPlatform->uploadBalanceFromServer();
        }
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $tradingPlatform);
        return $tradingPlatform;
    }

    /**
     * Обновление информации о торговой платформе
     * @param TradingPlatform $tradingPlatform
     * @param array           $newTradingPlatformData
     * @return TradingPlatform
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateTradingPlatform(TradingPlatform $tradingPlatform, array $newTradingPlatformData): TradingPlatform
    {
        $currentTradingPlatformData = $this->serializer->toArray($tradingPlatform);

        $form = $this->form->create(TradingPlatformForm::class, $tradingPlatform);
        $newTradingPlatformData = array_merge($currentTradingPlatformData, $newTradingPlatformData);
        $form->submit($newTradingPlatformData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $tradingPlatform);

        return $tradingPlatform;
    }

    /**
     * Удаление торговой платформы
     * @param TradingPlatform $tradingPlatform
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteTradingPlatform(TradingPlatform $tradingPlatform): void
    {
        $tradingPlatformClone = clone $tradingPlatform;
        $this->em->remove($tradingPlatform);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $tradingPlatformClone);
    }

    /**
     * Получить количество торговых платформ
     * @return int
     */
    public function totalTradingPlatform(): int
    {
        /** @var TradingPlatformRepository $tradingPlatformRepository */
        $tradingPlatformRepository = $this->em->getRepository(TradingPlatform::class);
        $total = $tradingPlatformRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\ConsultingCompany;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\ConsultingCompanyForm;
use App\Repository\Api\ConsultingCompanyRepository;

/**
 * Class ConsultingCompanyService
 * @package App\Service\Api
 */
final class ConsultingCompanyService extends BaseApiService
{
    /**
     * Получить список консалтинговых компаний
     * @param int $page
     * @param int $countPerPage
     * @return ConsultingCompany[]|null
     */
    public function getConsultingCompanyList(int $page, int $countPerPage = 25): ?array
    {
        /** @var ConsultingCompanyRepository $consultingCompanyRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(ConsultingCompany::class);
        $consultingCompanyRepository = $this->em->getRepository(ConsultingCompany::class);
        $consultingCompanyList = $consultingCompanyRepository->getList(ConsultingCompany::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $consultingCompanyList);
        return $consultingCompanyList;
    }

    /**
     * Создать новую консалтинговую компанию
     * @param array $consultingCompanyData
     * @return ConsultingCompany
     * @throws EntityException
     */
    public function createConsultingCompany(array $consultingCompanyData): ConsultingCompany
    {
        $consultingCompany = new ConsultingCompany();
        $form = $this->form->create(ConsultingCompanyForm::class, $consultingCompany);
        $form->submit($consultingCompanyData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($consultingCompany);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $consultingCompany);

        return $consultingCompany;
    }

    /**
     * Получить консалтинговую компанию по ID
     * @param int $consultingCompanyId
     * @return ConsultingCompany|null
     */
    public function get(int $consultingCompanyId): ?ConsultingCompany
    {
        /** @var ConsultingCompany $consultingCompany */
        /** @var ConsultingCompanyRepository $consultingCompanyRepository */
        $consultingCompanyRepository = $this->em->getRepository(ConsultingCompany::class);
        $consultingCompany = $consultingCompanyRepository->find($consultingCompanyId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $consultingCompany);
        return $consultingCompany;
    }

    /**
     * Обновление консалтинговой компании
     * @param ConsultingCompany $consultingCompany
     * @param array             $newConsultingCompanyData
     * @return ConsultingCompany
     * @throws EntityException
     */
    public function updateConsultingCompany(ConsultingCompany $consultingCompany, array $newConsultingCompanyData): ConsultingCompany
    {
        $currentConsultingCompanyData = $this->serializer->toArray($consultingCompany);

        $form = $this->form->create(ConsultingCompanyForm::class, $consultingCompany);
        $newConsultingCompanyData = array_merge($currentConsultingCompanyData, $newConsultingCompanyData);
        $form->submit($newConsultingCompanyData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $consultingCompany);

        return $consultingCompany;
    }

    /**
     * Удаление консалтинговой компании
     * @param ConsultingCompany $consultingCompany
     * @return void
     */
    public function deleteConsultingCompany(ConsultingCompany $consultingCompany): void
    {
        $consultingCompanyClone = clone $consultingCompany;
        $this->em->remove($consultingCompany);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $consultingCompanyClone);
    }

    /**
     * Получить количество консалтинговых компаний
     * @return int
     */
    public function totalConsultingCompany(): int
    {
        /** @var ConsultingCompanyRepository $consultingCompanyRepository */
        $consultingCompanyRepository = $this->em->getRepository(ConsultingCompany::class);
        $total = $consultingCompanyRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

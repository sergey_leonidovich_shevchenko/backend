<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\Account;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\AccountForm;
use App\Repository\Api\AccountRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Form\FormError;
use App\Service\MT\AccountService as MtAccountService;
use Throwable;

/**
 * Class AccountService
 * @package App\Service\Api
 */
final class AccountService extends BaseApiService
{
    /**
     * Получить список личных счетов
     * @param int $page
     * @param int $countPerPage
     * @return Account[]|null
     */
    public function getAccountList(int $page, int $countPerPage = 25): ?array
    {
        /** @var AccountRepository $accountRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(Account::class);
        $accountRepository = $this->em->getRepository(Account::class);
        $accountList = $accountRepository->getList(Account::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $accountList);
        return $accountList;
    }

    /**
     * Создать новый личный счет
     * @param array $accountData
     * @return Account Account entity
     * @throws EntityException
     * @throws Throwable
     */
    public function createAccount(array $accountData): Account
    {
        if (empty($accountData['leverage'])) {
            $accountData['leverage'] = Account::BASE_LIVERAGE;
        }

        /** @var MtAccountService $mtAccountService */
        $mtAccountService = $this->container->get(MtAccountService::class);

        $account = new Account();
        $form = $this->form->create(AccountForm::class, $account);
        $form->submit($accountData);

        if (empty($accountData['source_code'])) {
            $message = sprintf(
                'Необходимо указать откуда был создан счет: (%s)',
                implode(',', Account::SOURCE_CODE_LIST)
            );
            $formError = new FormError($message);
            $form->addError($formError);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $mtAccountService->saveAccount($account);
        $this->em->persist($account);
        $this->em->flush();

        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $account);

        return $account;
    }

    /**
     * Получить счет по ID
     * @param int $accountId
     * @return Account|null
     */
    public function get(int $accountId): ?Account
    {
        /** @var Account $account */
        /** @var AccountRepository $accountRepository */
        $accountRepository = $this->em->getRepository(Account::class);
        $account = $accountRepository->find($accountId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $account);
        return $account;
    }

    /**
     * Обновление информации о личном счете
     * @param Account $account
     * @param array   $newAccountData
     * @return Account
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateAccount(Account $account, array $newAccountData): Account
    {
        $form = $this->form->create(AccountForm::class, $account);
        $form->submit($newAccountData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($account);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $account);

        return $account;
    }

    /**
     * Удаление личного счета
     * @param Account $account
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteAccount(Account $account): void
    {
        $accountClone = clone $account;
        $this->em->remove($account);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $accountClone);
    }

    /**
     * Получить количество счетов
     * @return int
     */
    public function totalAccount(): int
    {
        /** @var AccountRepository $accountRepository */
        $accountRepository = $this->em->getRepository(Account::class);
        $total = $accountRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }
}

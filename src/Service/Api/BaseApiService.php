<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Interfaces\EntityInterface;
use App\Entity\Interfaces\EntityWithGroupsInterface;
use App\Entity\Interfaces\EntityWithRolesInterface;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\EventDispatcher\Event\Security\PermissionEvent;
use App\Exception\EntityException;
use App\Service\BaseService as Service;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use JMS\Serializer\Serializer;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityRepository;
use App\Entity\Api\User;
use App\Entity\Interfaces\EntityWithPermission;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class BaseService
 * @package App\Service\Api
 */
abstract class BaseApiService extends Service
{
    private const TYPE_ID = 'TYPE_ID';

    private const TYPE_NAME = 'TYPE_NAME';

    /** @var EntityManagerInterface */
    protected $em;

    /** @var FormFactoryInterface */
    protected $form;

    /** @var Security */
    protected $security;

    /** @var Serializer */
    protected $serializer;

    /**
     * BaseService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->form = $this->container->get('form.factory');
        $this->security = $this->container->get('security.helper');
        $this->serializer = $this->container->get('jms_serializer');
    }

    /**
     * Очистить соединение с БД
     * @throws MappingException
     */
    public function clearEM(): void
    {
        $this->em->clear();
    }

    /**
     * Закрыть соединение с БД
     */
    public function closeEM(): void
    {
        $this->em->close();
    }

    /**
     * @param string          $entityName
     * @param EntityInterface $entity
     * @param array           $newEntityData
     * @return EntityInterface
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateEntity(string $entityName, EntityInterface $entity, array $newEntityData): EntityInterface
    {
        $currentEntityData = $this->serializer->toArray($entity);

        $formClassName = "App\\Form\\Api\\{$entityName}Form";
        $form = $this->form->create($formClassName, $entity);
        $newEntityData = array_merge($currentEntityData, $newEntityData);
        $form->submit($newEntityData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $entity);

        return $entity;
    }

    /**
     * Диспатчит событие произошедшее с сущностью
     * @param string $eventName
     * @param mixed  $data
     */
    protected function dispatchEntityEvent(string $eventName, $data): void
    {
        /** @var EntityEvent $event */
        $event = $this->container->get(EntityEvent::class);

        // Добавляем в событие сущности
        switch ($eventName) {
            case EntityEvent::EVENT_API_ENTITY_LIST:
                $entityList = $data;
                $event->addEntities($entityList);
                break;

            case EntityEvent::EVENT_API_ENTITY_TOTAL:
                $event->setEntityTotal($data);
                break;

            default:
                $entity = $data;
                if ($entity instanceof EntityInterface) {
                    $event->addEntity($entity);
                }
        }

        $this->eventDispatcher->dispatch($event, $eventName);
    }

    /**
     * Диспатчит событие произошедшее с пермишенами
     * @param string                    $eventName
     * @param EntityWithGroupsInterface $entity Сущность
     * @param string[]|null             $oldGroups
     * @param string[]                  $newGroups
     */
    protected function dispatchPermissionGroupEvent(
        string $eventName,
        EntityWithGroupsInterface $entity,
        ?array $oldGroups,
        array $newGroups
    ): void
    {
        /** @var PermissionEvent $event */
        $event = $this->container->get(PermissionEvent::class);
        $entity && $event->setEntityWithGroups($entity);
        if (count($oldGroups)) {
            $oldGroupsCollection = new ArrayCollection($oldGroups);
            $event->setOldGroups($oldGroupsCollection);
        }
        if (count($newGroups)) {
            $newGroupsCollection = new ArrayCollection($newGroups);
            $event->setNewGroups($newGroupsCollection);
        }

        $this->eventDispatcher->dispatch($event, $eventName);
    }

    /**
     * Диспатчит событие произошедшее с пермишенами
     * @param string                   $eventName Название события
     * @param EntityWithRolesInterface $entity    Изменяемая сущность
     * @param string[]|null            $oldRoles  Список старых ролей
     * @param string[]|array           $newRoles  Список новых ролей
     */
    protected function dispatchPermissionRoleEvent(
        string $eventName,
        EntityWithRolesInterface $entity,
        ?array $oldRoles,
        array $newRoles
    ): void
    {
        /** @var PermissionEvent $event */
        $event = $this->container->get(PermissionEvent::class);
        $entity && $event->setEntityWithRoles($entity);
        $oldRoles && $event->setOldRoles($oldRoles);
        $event->setNewRoles($newRoles);

        $this->eventDispatcher->dispatch($event, $eventName);
    }

    /**
     * Имитировать отправку данных через форму
     * @param       $formName
     * @param       $entity
     * @param       $newData
     * @param array $options
     * @throws EntityException
     */
    protected function formSubmitData($formName, $entity, $newData, $options = []): void
    {
        $form = $this->form->create($formName, $entity, $options['form_options']);

        if ($entity instanceof EntityWithPermission) {
            $this->_removeRolesAndGroups($newData);
        }
        $form->submit($newData);

        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }
    }

    /**
     * Получить параметры пагинации
     * @param string $entityName
     * @return mixed
     */
    protected function getPaginationParams(string $entityName)
    {
        $entityPagination = $this->parameterBag->get('app.api.entity.pagination');
        $entityName = str_replace('\\', '/', $entityName);
        if (!empty($entityPagination[$entityName])) {
            return $entityPagination[$entityName];
        }
        return $entityPagination['default'];
    }

    /**
     * Получить API сущность по ее Id или name
     * @param string     $entityClassName Полное название класса (например ClassName::class)
     * @param int|string $entityIdOrName  Id или name сущности
     * @return EntityInterface
     */
    protected function _getApiEntity(string $entityClassName, $entityIdOrName): EntityInterface
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->em->getRepository($entityClassName);
        if (!$entityRepository instanceof EntityRepository) {
            throw new RuntimeException("Entity repository \"{$entityRepository}\" not found!");
        }

        $type = is_numeric($entityIdOrName)
            ? self::TYPE_ID
            : self::TYPE_NAME;
        $keyName = $entityClassName === User::class
            ? 'username'
            : 'name';
        /** @var EntityInterface $entity */
        $entity = $type === self::TYPE_ID
            ? $entityRepository->find($entityIdOrName)
            : $entityRepository->findOneBy([$keyName => $entityIdOrName]);
        if (!$entity instanceof EntityInterface) {
            throw new RuntimeException("Entity \"{$entityClassName}::{$entityIdOrName}\" not found!");
        }

        return $entity;
    }

    /**
     * Удаляем все что связанно с пермишенами,
     * тк их управлением занимается специальный класс управления пермишенами
     * @param array $data
     * @see PermissionController, PermissionService
     */
    private function _removeRolesAndGroups(array $data): void
    {
        if (!empty($data['roles'])) {
            unset($data['roles']);
        }

        if (!empty($data['groups'])) {
            unset($data['groups']);
        }
    }
}

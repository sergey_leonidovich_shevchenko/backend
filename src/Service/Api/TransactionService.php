<?php declare(strict_types=1);

namespace App\Service\Api;

use App\Entity\Api\Transaction;
use App\EventDispatcher\Event\Api\EntityEvent;
use App\Exception\EntityException;
use App\Form\Api\TransactionForm;
use App\Repository\Api\TransactionRepository;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;

/**
 * Class TransactionService
 * @package App\Service\Api
 */
final class TransactionService extends BaseApiService
{
    /**
     * Получить список транзакций
     * @param int $page
     * @param int $countPerPage
     * @return Transaction[]|null
     */
    public function getTransactionList(int $page, int $countPerPage = 25): ?array
    {
        /** @var TransactionRepository $transactionRepository */
        $elementsOnPage = $countPerPage ?: $this->getPaginationParams(Transaction::class);
        $transactionRepository = $this->em->getRepository(Transaction::class);
        $transactionList = $transactionRepository->getList(Transaction::class, $page, $elementsOnPage);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_LIST, $transactionList);
        return $transactionList;
    }

    /**
     * Создать новую транзакцию
     * @param array $transactionData
     * @return Transaction Transaction entity
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createTransaction(array $transactionData): Transaction
    {
        $transaction = new Transaction();
        $form = $this->form->create(TransactionForm::class, $transaction);
        if (!$transactionData['valid_till'] instanceof DateTime) {
            $transactionData['valid_till'] = $this->convertDateToObject($transactionData['valid_till']);
        }
        $form->submit($transactionData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($transaction);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_CREATE, $transaction);

        return $transaction;
    }

    /**
     * Получить транзакцию по ID
     * @param int $transactionId
     * @return Transaction|null
     */
    public function get(int $transactionId): ?Transaction
    {
        /** @var TransactionRepository $transactionRepository */
        /** @var Transaction $transaction */
        $transactionRepository = $this->em->getRepository(Transaction::class);
        $transaction = $transactionRepository->find($transactionId);
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_GET, $transaction);
        return $transaction;
    }

    /**
     * Обновление информации о транзакции
     * @param Transaction $transaction
     * @param array       $newTransactionData
     * @return Transaction
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateTransaction(Transaction $transaction, array $newTransactionData): Transaction
    {
        $form = $this->form->create(TransactionForm::class, $transaction);
        $newTransactionData['valid_till'] = $this->convertDateToObject($newTransactionData['valid_till']);
        $form->submit($newTransactionData);
        if ($form->isSubmitted() && !$form->isValid()) {
            throw new EntityException($form);
        }

        $this->em->persist($transaction);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_UPDATE, $transaction);

        return $transaction;
    }

    /**
     * Удаление транзакции
     * @param Transaction $transaction
     */
    public function deleteTransaction(Transaction $transaction): void
    {
        $transactionClone = clone $transaction;
        $this->em->remove($transaction);
        $this->em->flush();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_DELETE, $transactionClone);
    }

    /**
     * Получить количество транзакций
     * @return int
     */
    public function totalTransaction(): int
    {
        /** @var TransactionRepository $transactionRepository */
        $transactionRepository = $this->em->getRepository(Transaction::class);
        $total = $transactionRepository->count();
        $this->dispatchEntityEvent(EntityEvent::EVENT_API_ENTITY_TOTAL, $total);
        return $total;
    }

    /**
     * @param string $date
     * @return DateTime|null
     */
    private function convertDateToObject(string $date): ?DateTime
    {
        if (!empty($date)) {
            try {
                return new DateTime($date);
            } catch (Exception $e) {
                return null;
            }
        }
    }
}

<?php declare(strict_types=1);

namespace App\Factory;

use App\Service\BaseService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ServiceFactory
 * @package App\Factory
 */
class ServiceFactory
{
    /**
     * @var BaseService[]
     */
    private static $serviceList;

    /**
     * @param string             $serviceClassName
     * @param ContainerInterface $container
     * @return BaseService
     */
    public static function create(string $serviceClassName, ContainerInterface $container): BaseService
    {
        if (self::$serviceList[$serviceClassName] === null) {
            self::$serviceList[$serviceClassName] = new $serviceClassName($container);
        }

        return self::$serviceList[$serviceClassName];
    }

    /**
     * @param string $serviceClassName
     * @return bool
     */
    public function hasService(string $serviceClassName): bool
    {
        return (bool)self::$serviceList[$serviceClassName];
    }

    /**
     * @param string $serviceClassName
     * @return BaseService|null
     */
    public function getService(string $serviceClassName): ?BaseService
    {
        return self::$serviceList[$serviceClassName];
    }
}

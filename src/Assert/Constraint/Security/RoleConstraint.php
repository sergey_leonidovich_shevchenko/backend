<?php declare(strict_types=1);

namespace App\Assert\Constraint\Security;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class RoleConstraint
 * @package App\Assert\Constraint\Security
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class RoleConstraint extends BaseConstraint
{
    public $message = 'Указанной роли "{{ role }}" не нет в системе';
}

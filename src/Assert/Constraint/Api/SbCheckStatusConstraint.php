<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class SbCheckStatusConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class SbCheckStatusConstraint extends BaseConstraint
{
    public $message = 'Указанного статуса проверки СБ не существует или он не активен.';
}
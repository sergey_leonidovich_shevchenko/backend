<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class TypeTradingAccountConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class TypeTradingAccountConstraint extends BaseConstraint
{
    public $message = 'Указанного типа торгового счета не существует или он не активен.';
}
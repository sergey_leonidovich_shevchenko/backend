<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class AccountConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class AccountConstraint extends BaseConstraint
{
    public $message = 'Указанного счета не существует или он не активен.';
}
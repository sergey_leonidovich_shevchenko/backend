<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class UserGroupConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 * @Target("CLASS")
 */
class UserGroupConstraint extends BaseConstraint
{
    public $messageEntityNotFound
        = 'Указанной пользовательской группы не существует или она не активна.';

    public $messageNameAlreadyExists
        = 'Пользовательская группа с названием "{{ name }}" уже зарегистрирована в системе!';

    public $messageNameRusAlreadyExists
        = 'Пользовательская группа с русским названием "{{ nameRus }}" уже зарегистрирована в системе!';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}

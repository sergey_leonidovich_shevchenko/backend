<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class StatusVipConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class StatusVipConstraint extends BaseConstraint
{
    public $message = 'Указанного VIP-статуса не существует или он не активен.';
}
<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class PersonalAreaConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class PersonalAreaConstraint extends BaseConstraint
{
    public $message = 'Указанный личный кабинет не существует или он не активен.';
}
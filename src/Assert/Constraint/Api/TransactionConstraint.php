<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class TransactionConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class TransactionConstraint extends BaseConstraint
{
    public $message = 'Указанной транзакции не существует или она не доступна вам для просмотра.';
}
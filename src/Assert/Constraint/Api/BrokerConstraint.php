<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class BrokerConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class BrokerConstraint extends BaseConstraint
{
    public $message = 'Указанного брокера не существует или он не активен.';
}
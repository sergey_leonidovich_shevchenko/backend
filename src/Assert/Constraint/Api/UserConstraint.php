<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class UserConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class UserConstraint extends BaseConstraint
{
    public $message = 'Указанного пользователя не существует или он не активен.';
}

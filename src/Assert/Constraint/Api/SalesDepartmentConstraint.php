<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class SalesDepartmentConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class SalesDepartmentConstraint extends BaseConstraint
{
    public $message = 'Указанный департамент продаж не существует или он не активен.';
}

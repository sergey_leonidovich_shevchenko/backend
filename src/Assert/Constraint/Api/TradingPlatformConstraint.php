<?php declare(strict_types=1);

namespace App\Assert\Constraint\Api;

use App\Assert\Constraint\BaseConstraint;

/**
 * Class TradingPlatformConstraint
 * @package App\Assert\Constraint\Api
 * @Annotation
 */
class TradingPlatformConstraint extends BaseConstraint
{
    public $message = 'Указанной торговой площадки не существует или она не активна.';
}
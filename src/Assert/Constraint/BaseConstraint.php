<?php declare(strict_types=1);

namespace App\Assert\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Class BaseConstraint
 * @package App\Assert\Constraint
 */
abstract class BaseConstraint extends Constraint
{
    /**
     * @return string
     */
    public function validatedBy(): string
    {
        return str_replace('Constraint', 'Validator', get_class($this));
    }
}

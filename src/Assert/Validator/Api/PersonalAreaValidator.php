<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\PersonalArea;
use Symfony\Component\Validator\Constraint;

/**
 * Class PersonalAreaValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class PersonalAreaValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param PersonalArea $personalArea Сущность личного кабинета
     * @param Constraint   $constraint   The constraint for the validation
     */
    public function validate($personalArea, Constraint $constraint): void
    {
        if (!$personalArea instanceof PersonalArea) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

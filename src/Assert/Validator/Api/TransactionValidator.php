<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\Transaction;
use Symfony\Component\Validator\Constraint;

/**
 * Class TransactionValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class TransactionValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param Transaction $transaction Сущность транзакции
     * @param Constraint  $constraint  The constraint for the validation
     */
    public function validate($transaction, Constraint $constraint): void
    {
        if (!$transaction instanceof Transaction) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

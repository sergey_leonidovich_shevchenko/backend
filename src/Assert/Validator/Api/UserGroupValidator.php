<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Constraint\Api\UserGroupConstraint;
use App\Assert\Validator\BaseValidator;
use App\Entity\Api\UserGroup;
use App\Repository\Api\UserGroupRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Validator\Constraint;

/**
 * Class UserGroupValidator
 * @package App\Assert\Validator\Api
 */
class UserGroupValidator extends BaseValidator
{
    /** @var UserGroupRepository */
    private $userGroupRepository;

    /** @var UserGroupConstraint */
    private $userGroupConstraint;

    /**
     * Checks if the passed value is valid.
     * @param UserGroup                      $userGroup  Сущность пользователя
     * @param UserGroupConstraint|Constraint $constraint The constraint for the validation
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function validate($userGroup, Constraint $constraint): void
    {
        $this->userGroupConstraint = $constraint;
        $this->userGroupRepository = $this->em->getRepository(UserGroup::class);

        // Валидация на корректную сущность переданную для валидации
        if (!$userGroup instanceof UserGroup) {
            $this->context
                ->buildViolation($this->userGroupConstraint->messageEntityNotFound)
                ->addViolation();
            return;
        }

        // Валидация на уникальное название пользовательской группы
        $this->_validationNameUniquenessCheck($userGroup);

        // Валидация на уникальное название русифицированной пользовательской группы
        $this->_validationNameRusUniquenessCheck($userGroup);
    }

    /**
     * Валидация на уникальное название пользовательской группы
     * @param UserGroup $userGroup
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    private function _validationNameUniquenessCheck(UserGroup $userGroup): void
    {
        $userGroupName = $userGroup->getName();
        if ($this->userGroupRepository->countSimilar(UserGroup::class, $userGroup->getId(), ['name' => $userGroupName])) {
            $this->context
                ->buildViolation($this->userGroupConstraint->messageNameAlreadyExists)
                ->setParameter('{{ name }}', $userGroupName)
                ->atPath('name')
                ->addViolation();
        }
    }

    /**
     * Валидация на уникальное название русифицированной пользовательской группы
     * @param UserGroup $userGroup
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    private function _validationNameRusUniquenessCheck(UserGroup $userGroup): void
    {
        $userGroupNameRus = $userGroup->getNameRus();
        if ($this->userGroupRepository->countSimilar(UserGroup::class, $userGroup->getId(), ['nameRus' => $userGroupNameRus])) {
            $this->context
                ->buildViolation($this->userGroupConstraint->messageNameRusAlreadyExists)
                ->setParameter('{{ nameRus }}', $userGroupNameRus)
                ->atPath('nameRus')
                ->addViolation();
        }
    }
}

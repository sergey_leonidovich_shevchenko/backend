<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\SalesDepartment;
use Symfony\Component\Validator\Constraint;

/**
 * Class SalesDepartmentValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class SalesDepartmentValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param SalesDepartment $salesDepartment Сущность департамента продаж
     * @param Constraint      $constraint      The constraint for the validation
     */
    public function validate($salesDepartment, Constraint $constraint): void
    {
        if (!$salesDepartment instanceof SalesDepartment) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\Broker;
use Symfony\Component\Validator\Constraint;

/**
 * Class BrokerValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class BrokerValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param Broker     $broker     Сущность Брокер
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($broker, Constraint $constraint): void
    {
        if (!$broker instanceof Broker) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

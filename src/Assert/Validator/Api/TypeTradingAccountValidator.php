<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\TypeTradingAccount;
use Symfony\Component\Validator\Constraint;

/**
 * Class TypeTradingAccountValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class TypeTradingAccountValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param TypeTradingAccount $typeTradingAccount Сущность типа торгового счета
     * @param Constraint         $constraint         The constraint for the validation
     */
    public function validate($typeTradingAccount, Constraint $constraint): void
    {
        if (!$typeTradingAccount instanceof TypeTradingAccount) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

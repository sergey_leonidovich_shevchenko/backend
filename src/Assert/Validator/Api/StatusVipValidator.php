<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\StatusVip;
use Symfony\Component\Validator\Constraint;

/**
 * Class StatusVipValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class StatusVipValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param StatusVip  $user       Сущность VIP-статуса
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($user, Constraint $constraint): void
    {
        if (!$user instanceof StatusVip) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

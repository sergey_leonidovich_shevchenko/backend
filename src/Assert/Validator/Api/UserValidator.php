<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\User;
use Symfony\Component\Validator\Constraint;

/**
 * Class UserValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class UserValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param User       $user       Сущность пользователя
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($user, Constraint $constraint): void
    {
        if (!$user instanceof User) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

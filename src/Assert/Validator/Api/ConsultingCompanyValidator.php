<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\ConsultingCompany;
use Symfony\Component\Validator\Constraint;

/**
 * Class ConsultingCompanyValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class ConsultingCompanyValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param ConsultingCompany $consultingCompany Сущность консалтинговой компании
     * @param Constraint        $constraint        The constraint for the validation
     */
    public function validate($consultingCompany, Constraint $constraint): void
    {
        if (!$consultingCompany instanceof ConsultingCompany) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\TradingPlatform;
use Symfony\Component\Validator\Constraint;

/**
 * Class TradingPlatformValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class TradingPlatformValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param TradingPlatform $tradingPlatform Сущность торговой площадки
     * @param Constraint      $constraint      The constraint for the validation
     */
    public function validate($tradingPlatform, Constraint $constraint): void
    {
        if (!$tradingPlatform instanceof TradingPlatform) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

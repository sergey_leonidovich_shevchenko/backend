<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\SbCheckStatus;
use Symfony\Component\Validator\Constraint;

/**
 * Class SbCheckStatusValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class SbCheckStatusValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param SbCheckStatus $sbCheckStatus Сущность статуса проверки СБ
     * @param Constraint    $constraint    The constraint for the validation
     * @return void
     */
    public function validate($sbCheckStatus, Constraint $constraint): void
    {
        if (!$sbCheckStatus instanceof SbCheckStatus) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

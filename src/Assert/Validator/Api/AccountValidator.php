<?php declare(strict_types=1);

namespace App\Assert\Validator\Api;

use App\Assert\Validator\BaseValidator;
use App\Entity\Api\Account;
use Symfony\Component\Validator\Constraint;

/**
 * Class AccountValidator
 * @package App\Assert\Validator\Api
 * @Annotation
 */
class AccountValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param Account    $account    Сущность счета
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($account, Constraint $constraint): void
    {
        if (!$account instanceof Account) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

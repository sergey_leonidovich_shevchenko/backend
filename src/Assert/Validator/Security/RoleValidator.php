<?php declare(strict_types=1);

namespace App\Assert\Validator\Security;

use App\Assert\Validator\BaseValidator;
use App\Helper\Security\PermissionHelper;
use Symfony\Component\Validator\Constraint;

/**
 * Class RoleValidator
 * @package App\Assert\Validator\Security
 * @Annotation
 */
class RoleValidator extends BaseValidator
{
    /**
     * Checks if the passed value is valid.
     * @param string[]   $roles      Сущность Брокер
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($roles, Constraint $constraint): void
    {
        if (count($roles)) {
            /** @var PermissionHelper $roleHelper */
            $roleHelper = $this->container->get(PermissionHelper::class);

            foreach ($roles as $role) {
                if (!$roleHelper->checkRolesForExistence($roles)) {
                    $this->context
                        ->buildViolation($constraint->message)
                        ->setParameter('{{ role }}', $role)
                        ->atPath('roles')
                        ->addViolation();
                }
            }
        }
    }
}

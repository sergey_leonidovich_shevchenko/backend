<?php declare(strict_types=1);

namespace App\Assert\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class BaseValidator
 * @package App\Assert\Validator
 */
abstract class BaseValidator extends ConstraintValidator
{
    /** @var ContainerInterface */
    protected $container;

    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->container = $container;
    }
}

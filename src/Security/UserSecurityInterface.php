<?php declare(strict_types=1);


namespace App\Security;

use App\Entity\Api\EntityInterface;

/**
 * Interface UserSecurityInterface
 * @package App\Security
 */
interface UserSecurityInterface extends EntityInterface
{

}
<?php declare(strict_types=1);

namespace App\Security\JWT;

use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;

/**
 * Class JWTEncoder
 * @package App\Security\JWT
 */
class JWTEncoder implements JWTEncoderInterface
{
    private $key;

    public function __construct(string $key = 'super_secret_key')
    {
        $this->key = $key;
    }

    /**
     * @param array $data
     * @return string the encoded token string
     * @throws JWTEncodeFailureException If an error occurred while trying to create
     *                                   the token (invalid crypto key, invalid payload...)
     */
    public function encode(array $data): string
    {
        try {
            return self::encode($data, $this->key);
        } catch (Exception $e) {
            throw new JWTEncodeFailureException(JWTEncodeFailureException::INVALID_CONFIG, 'An error occurred while trying to encode the JWT token.', $e);
        }
    }

    /**
     * @param string $token
     * @return array
     * @throws JWTDecodeFailureException If an error occurred while trying to load the token
     *                                   (invalid signature, invalid crypto key, expired token...)
     */
    public function decode($token): array
    {
        try {
            return (array)self::decode($token, $this->key);
        } catch (Exception $e) {
            throw new JWTDecodeFailureException(JWTDecodeFailureException::INVALID_TOKEN, 'Invalid JWT Token', $e);
        }
    }
}

<?php declare(strict_types=1);

namespace App\Security\JWT;

use App\Security\JWT\Extractor\AuthorizationHeaderTokenExtractor;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator as BaseAuthenticator;

/**
 * Class TokenAuthenticator
 * @package App\Security\JWT
 */
class TokenAuthenticator extends BaseAuthenticator
{
    public const AUTHORIZATION_HEADER_NAME = 'Authorization';
    public const TOKEN_BEARER_HEADER_NAME = 'Bearer';

    /**
     * @return AuthorizationHeaderTokenExtractor
     */
    protected function getTokenExtractor(): AuthorizationHeaderTokenExtractor
    {
        // Return a custom token extractor, no matter of what are configured
        return new AuthorizationHeaderTokenExtractor(
            self::TOKEN_BEARER_HEADER_NAME,
            self::AUTHORIZATION_HEADER_NAME
        );
    }
}
<?php declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Api\Interfaces\Entity;
use App\Entity\Api\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class EntityVoter
 * @package App\Security\Voter
 */
class EntityVoter extends Voter
{
    /**
     * Determines if the attribute and subject are supported by this voter.
     * @param string $access An attribute
     * @param mixed  $entity The subject to secure, e.g. an object the user wants to access or any other PHP type
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($access, $entity): bool
    {
        // only vote on EntityInterface objects inside this voter
        if (!$entity instanceof Entity) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     * @param string         $access
     * @param Entity         $entity
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($access, $entity, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        return in_array($access, $user->getRoles(), true);
    }
}

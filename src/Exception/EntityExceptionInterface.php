<?php declare(strict_types=1);


namespace App\Exception;


use Symfony\Component\Form\Form;
use App\Entity\Interfaces\EntityInterface;

interface EntityExceptionInterface
{
    public function getEntity(): EntityInterface;

    /**
     * Коллекция сущностей
     * @param EntityInterface[] $entityCollection
     * @return mixed
     */
    public function setEntityList(array $entityCollection);

    public function getForm(): Form;

    public function setForm(Form $form);

    public function __construct(EntityInterface $Entity, Form $Form);
}

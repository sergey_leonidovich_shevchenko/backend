<?php declare(strict_types=1);

namespace App\Exception;

use Exception;

/**
 * Class BaseException
 * @package App\Exception
 */
abstract class BaseException extends Exception
{
    protected $logger;

    protected $errorList = [];

    /**
     * @return array
     */
    public function getErrorList(): array
    {
        return $this->errorList;
    }
}

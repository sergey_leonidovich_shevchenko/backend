<?php declare(strict_types=1);

namespace App\Exception\Security;

use App\Entity\Interfaces\EntityWithGroupsInterface;
use App\Entity\Interfaces\EntityWithRolesInterface;
use App\EventDispatcher\Event\Security\PermissionEvent;
use App\Exception\BaseException;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class PermissionException
 * @package App\Exception
 */
class PermissionException extends BaseException
{
    /**
     * PermissionException constructor.
     * @param ContainerInterface $container
     * @param string             $eventName
     * @param Exception          $exception
     * @param array              $data
     */
    public function __construct(ContainerInterface $container, string $eventName, Exception $exception, array $data = [])
    {
        /** @var PermissionEvent $event */
        $event = $container->get(PermissionEvent::class);

        if (array_key_exists('entity', $data)) {
            if ($data['entity'] instanceof EntityWithRolesInterface) {
                $event->setEntityWithRoles($data['entity']);
                !empty($data['old_roles']) && $event->setOldRoles($data['old_roles']);
                !empty($data['new_roles']) && $event->setNewRoles($data['new_roles']);
            }
            if ($data['entity'] instanceof EntityWithGroupsInterface) {
                $event->setEntityWithGroups($data['entity']);
                !empty($data['old_groups']) && $event->setOldGroups($data['old_groups']);
                !empty($data['new_groups']) && $event->setNewGroups($data['new_groups']);
            }
        }

        $event->setMessage($exception->getMessage());

        /** @var EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $container->get('event_dispatcher');
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $eventDispatcher->dispatch($event, $eventName);

        parent::__construct($exception->getMessage());
    }
}


<?php declare(strict_types=1);

namespace App\Exception\Api;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ApiException
 * @package App\Exception\Api
 */
class ApiException extends HttpException
{
    private $apiProblem;

    public function __construct(ApiProblem $apiProblem, Exception $previous = null, array $headers = array(), $code = 0)
    {
        $this->apiProblem = $apiProblem;
        $statusCode = $apiProblem->getStatusCode();
        $message = $apiProblem->getTitle();
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    public function getApiProblem()
    {
        return $this->apiProblem;
    }
}

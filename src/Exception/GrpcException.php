<?php declare(strict_types=1);

namespace App\Exception;

use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\EventDispatcher\Event\MT\MTEvent;

/**
 * Class GrpcException
 * @package App\Exception
 */
class GrpcException extends BaseException
{
    /**
     * GrpcException constructor.
     * @param ContainerInterface $container
     * @param string             $eventName
     * @param Exception          $exception
     * @param array              $data
     */
    public function __construct(ContainerInterface $container, string $eventName, Exception $exception, array $data = [])
    {
        /** @var MtEvent $event */
        $event = $container->get(MTEvent::class);

        if (array_key_exists('user', $data)) {
            $event->setUser($data['user']);
        }

        $event->setMessage('При обращении к MT серверу произошла ошибка. ' . $exception->getMessage());

        /** @var EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $container->get('event_dispatcher');
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $eventDispatcher->dispatch($event, $eventName);

        parent::__construct($event->getMessage());
    }
}


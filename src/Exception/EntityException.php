<?php declare(strict_types=1);

namespace App\Exception;

use App\Helper\FormHelper;
use Symfony\Component\Form\FormInterface;

/**
 * Class EntityException
 * @package App\Exception
 */
class EntityException extends BaseException
{
    /**
     * EntityException constructor.
     * @param FormInterface $form
     */
    public function __construct(FormInterface $form)
    {
        $this->_getAllErrors($form);
        if (!empty($this->getErrorList())) {
            $formDataAsArray = FormHelper::getFormDataAsArray($form);
            $formDataAsString = json_encode($formDataAsArray, JSON_THROW_ON_ERROR, 512);
            $errorListAsString = implode("\n==> ", $this->getErrorList());
            $this->message .= sprintf("\n==> %s", $errorListAsString);
            $this->message .= sprintf("\n==> Сохряняемая сущность: %s", $formDataAsString);
        }

        parent::__construct();
    }

    /**
     * Сформировать список всех ошибок (@param FormInterface $form
     * @see errorList)
     */
    private function _getAllErrors(FormInterface $form): void
    {
        foreach ($form->getErrors(true) as $error) {
            $error = $error->getMessage();
            $this->errorList[] = $error;
        }

        $this->dispatchEntityException($this->errorList);
    }

    /**
     * Событие исключения
     * @param array $errorList
     */
    private function dispatchEntityException(array $errorList): void
    {
        // FIXME: Please uncomment me and register this error =)
//        global $kernel;
//        $container = $kernel->getContainer();
//        if ($container) {
//            $event = $container->get(EntityEvent::class);
//            if ($event) {
//                $event->addExceptions($errorList);
//            }
//        }
    }
}

<?php declare(strict_types=1);

namespace App\Command\Api\UserGroup;

use App\Command\Api\BaseApiCommand;
use App\Entity\Api\UserGroup;
use App\Service\Api\UserGroupService;
use Exception;
use RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class UpdateCommand
 * @package App\Command\Api\UserGroup
 * @author  Serrgey Shevchenko <sergey.leonidovich.shevchenko@gmail.com>
 */
class UpdateCommand extends BaseApiCommand
{
    private const UPDATE_BY_ID = 'ID';

    private const UPDATE_BY_NAME = 'NAME';

    protected static $defaultName = 'api:entity:user-group:update';

    private $entityStructure = ['id', 'name', 'name_rus', 'description', 'roles'];

    /** @var UserGroupService */
    private $_userGroupService;

    /**
     * UserGroupCommand constructor.
     * @param ContainerInterface       $container
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(ContainerInterface $container, EventDispatcherInterface $eventDispatcher, Security $security)
    {
        parent::__construct($container, $eventDispatcher, $security);
        $this->_userGroupService = new UserGroupService($container, $eventDispatcher, $security);
    }

    /**
     * Обновление пользовательской группы.
     * Метод вызывается автоматически в конце команды конструктора.
     * Если ваша команда определяет свой собственный конструктор,
     * сначала установите свойства,
     * а затем вызовите родительский конструктор,
     * чтобы эти свойства были доступны в configure() методе
     */
    protected function configure(): void
    {
        $help = 'The <fg=green>api:entity:user-group:update</> ';
        $help .= "Update user group:\n\n";
        $help .= "  <fg=green>php %command.full_name% group_name.1</>\n";
        $this->setName('api:entity:user-group:update')
            ->setAliases(['group:update', 'fos:group:update'])
            ->addArgument('id', InputArgument::REQUIRED, 'Enter a ID (or name) for the update user group')
            ->addOption('name', 'N', InputOption::VALUE_OPTIONAL, 'Enter a name for the new user group')
            ->addOption('name_rus', 'R', InputOption::VALUE_OPTIONAL, 'Enter a name for the new user group is the Russian language')
            ->addOption('description', 'D', InputOption::VALUE_OPTIONAL, 'Enter a description for the new user group')
            ->addOption('roles', 'P', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, 'A list of roles separated with a space')
            ->setDescription('Update user group')
            ->setHelp($help);
    }

    /**
     * Этот метод выполняется перед interact() и в execute() методах.
     * Его основная цель - инициализировать переменные, используемые в остальных командных методах.
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);
    }

    /**
     * Взаимодействует с пользователем.
     * Этот метод выполняется перед проверкой InputDefinition.
     * Это означает, что это единственное место, где команда может в интерактивном режиме
     * запрашивать значения отсутствующих обязательных аргументов.
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        /** @var UserGroup $userGroup */
        $userGroup = $this->_userGroupService->getUserGroupById((int)$input->getArgument('id'));

        // Id
        if (!$input->getArgument('id')) {
            $questionText = '<fg=cyan>Please enter a group ID and press continue (Enter):</> ';
            $question = new Question($questionText);
            $question->setValidator(static function ($groupName) {
                if (empty($groupName)) {
                    throw new RuntimeException('Group ID can not be empty');
                }
                return $groupName;
            });
            $groupId = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('id', $groupId);
        }

        // Name
        if (!$input->getOption('name')) {
            $questionText = '<fg=cyan>Please enter a new group name (e.g. <fg=yellow>group_name.1</>) '
                . 'or set "null" and press continue (Enter)'
                . PHP_EOL
                . sprintf('(current name: <fg=yellow>%s</>): ', $userGroup->getName() ?? 'null');
            $question = new Question($questionText);
            $groupName = $this->getHelper('question')->ask($input, $output, $question);
            if (!$groupName) {
                $groupName = $userGroup->getName();
            }
            $input->setOption('name', $groupName);
        }

        // Name rus
        if (!$input->getOption('name_rus')) {
            $questionText = '<fg=cyan>Please enter a name for the custom group in the Russian language '
                . '(required for display in the UI) (e.g. <fg=yellow>Моя новая группа</>) or set "null" and press continue (Enter)'
                . PHP_EOL
                . sprintf('(current russian name: <fg=yellow>%s</>): ', $userGroup->getNameRus() ?? 'null');
            $question = new Question($questionText);
            $groupNameRus = $this->getHelper('question')->ask($input, $output, $question);
            if (!$groupNameRus) {
                $groupNameRus = $userGroup->getNameRus();
            }
            $input->setOption('name_rus', $groupNameRus);
        }

        // Description
        if (!$input->getOption('description')) {
            $questionText = '<fg=cyan>Please enter a description for the custom group or set "null" and press continue (Enter)'
                . PHP_EOL
                . sprintf('(current description: <fg=yellow>%s</>): ', $userGroup->getDescription() ?? 'null');
            $question = new Question($questionText);
            $groupDescription = $this->getHelper('question')->ask($input, $output, $question);
            if (!$groupDescription) {
                $groupDescription = $userGroup->getDescription();
            }
            $input->setOption('description', $groupDescription);
        }

        // Roles
        $userGroupRoles = $userGroup->getRoles();
        sort($userGroupRoles);
        if (!$input->getOption('roles')) {
            $userGroupRolesList = !empty($userGroupRoles)
                ? implode(', ', $userGroupRoles)
                : '[]';

            $questionText = '<fg=cyan>Please enter roles for the custom group or set "null" and press continue (Enter)'
                . PHP_EOL . "(current roles: <fg=yellow>{$userGroupRolesList}</>) : ";
            $question = new Question($questionText);
            $userGroupRoles = [];
            while ($role = $this->getHelper('question')->ask($input, $output, $question)) {
                $userGroupRoles[] = $role;
                sort($userGroupRoles);
                $output->writeln(sprintf('Role list: ["%s"]', implode(', ', $userGroupRoles)));
            }
            if (!count($userGroupRoles)) {
                $userGroupRoles = $userGroup->getRoles();
            }
            $input->setOption('roles', $userGroupRoles);
        }
    }

    /**
     * Выполняет текущую команду.
     * Этот метод не является абстрактным,
     * потому что вы можете использовать этот класс в качестве конкретного класса.
     * В этом случае вместо определения метода execute() вы устанавливаете код для выполнения,
     * передавая Closure методу setCode().
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void null or 0 if everything went fine, or an error code
     * @throws Exception
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $idOrName = $input->getArgument('id');

        // Допилим симфонивскую реализацию, что бы можно было задавать пустые значения и сохранять их.
        $newUserGroupData = [];
        foreach (['name', 'name_rus', 'description', 'roles'] as $key) {
            if ($input->getOption($key) !== null) {
                if (is_array($input->getOption($key)) && count($input->getOption($key)) === 1 && $input->getOption($key)[0] === 'null') {
                    $input->setOption($key, 'null');
                }
                $newUserGroupData[$key] = $input->getOption($key) === 'null'
                    ? null
                    : $input->getOption($key);
            }
        }

        $type = is_numeric($idOrName)
            ? self::UPDATE_BY_ID
            : self::UPDATE_BY_NAME;

        // Delete group
        switch ($type) {
            case self::UPDATE_BY_ID:
                $userGroup = $this->_userGroupService->getUserGroupById((int)$idOrName);
                break;

            case self::UPDATE_BY_NAME:
                $userGroup = $this->_userGroupService->getUserGroupByName($idOrName);
                break;

            default:
                $exceptionMessage = 'User group name type is not correct, must be either ID (int) or name (string)';
                throw new RuntimeException($exceptionMessage);
        }

        // Update group
        try {
            if (!$userGroup instanceof UserGroup) {
                throw new RuntimeException("[Error] <fg=red>Group with name:</> <fg=green>{$idOrName}</> <fg=red>not found!</>");
            }
            $this->_confirmMessage($userGroup, $newUserGroupData, $input, $output);
            $this->_userGroupService->updateUserGroup($userGroup, $newUserGroupData);
        } catch (Exception $e) {
            $output->writeln(sprintf('<fg=red>%s</>', $e));
            exit(1);
        }

        // Output the finish message
        $text = '    <fg=black;bg=green> ==> </> User group with ID: "<fg=green>' . $userGroup->getId() . '</>" and name "<fg=green>' . $userGroup->getName() . '</>" successfully updated.' . PHP_EOL;

        $output->writeln($text);
    }

    /**
     * Выводит сообщение с подтверждением на обновление пользовательской группы
     * @param UserGroup       $userGroup
     * @param array           $newData
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    private function _confirmMessage(UserGroup $userGroup, array $newData, InputInterface $input, OutputInterface $output): void
    {
        $text = "          <fg=white>Id: {$userGroup->getId()}</>" . PHP_EOL;

        if (!empty($newData['name']) && $newData['name'] !== $userGroup->getName()) {
            $text .= "    [OLD] Name: <fg=red>{$userGroup->getName()}</>" . PHP_EOL;
            $text .= "    [NEW] Name: <fg=green>{$newData['name']}</>" . PHP_EOL;
        } else {
            $text .= "          <fg=white>Name: {$userGroup->getName()}</>" . PHP_EOL;
        }

        if (!empty($newData['name_rus']) && $newData['name_rus'] !== $userGroup->getNameRus()) {
            $text .= "    <fg=red>[OLD] Name rus: {$userGroup->getNameRus()}</>" . PHP_EOL;
            $text .= "    <fg=green>[NEW] Name rus: {$newData['name_rus']}</>" . PHP_EOL;
        } else {
            $text .= "          <fg=white>Name rus: {$userGroup->getNameRus()}</>" . PHP_EOL;
        }

        if (!empty($newData['description']) && $newData['description'] !== $userGroup->getDescription()) {
            $text .= "    <fg=red>[OLD] Description: {$userGroup->getDescription()}</>" . PHP_EOL;
            $text .= "    <fg=green>[NEW] Description: {$newData['description']}</>" . PHP_EOL;
        } else {
            $text .= "          <fg=white>Description: {$userGroup->getDescription()}</>" . PHP_EOL;
        }

        $userGroupRoles = $userGroup->getRoles();

        sort($userGroupRoles);
        $oldRoleList = !empty($userGroupRoles)
            ? implode(', ', $userGroupRoles)
            : '[]';
        if (!empty($newData['roles']) && $newData['roles']) {
            $newRoleList = !empty($newData['roles'])
                ? implode(', ', $newData['roles'])
                : '[]';
            $text .= "    <fg=red>[OLD] Roles: {$oldRoleList}</>" . PHP_EOL;
            $text .= "    <fg=green>[NEW] Roles: {$newRoleList}</>" . PHP_EOL;
        } else {
            $text .= "          <fg=white>Roles: {$oldRoleList}</>" . PHP_EOL;
        }

        $text .= PHP_EOL . '<fg=cyan>Are you sure you want to update the user group? (yes/No): </>';

        $question = new Question($text);
        $confirm = (string)$this->getHelper('question')->ask($input, $output, $question);
        if (!$input->getOption('no-interaction') && (!$confirm || !in_array(strtolower($confirm[0]), ['y', 'д']))) {
            $output->writeln('<fg=red>Cancel</>' . PHP_EOL);
            exit(0);
        }
    }
}

<?php declare(strict_types=1);

namespace App\Command\Api\UserGroup;

use App\Command\Api\BaseApiCommand;
use App\Entity\Api\UserGroup;
use App\Service\Api\UserGroupService;
use Exception;
use RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class DeleteCommand
 * @package App\Command\Api\UserGroup
 * @author  Serrgey Shevchenko <sergey.leonidovich.shevchenko@gmail.com>
 */
class DeleteCommand extends BaseApiCommand
{
    private const DELETE_BY_ID = 'ID';

    private const DELETE_BY_NAME = 'NAME';

    protected static $defaultName = 'api:entity:user-group:delete';

    /** @var UserGroupService */
    private $_userGroupService;

    /**
     * UserGroupCommand constructor.
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $eventDispatcher
     * @param Security $security
     */
    public function __construct(ContainerInterface $container, EventDispatcherInterface $eventDispatcher, Security $security)
    {
        parent::__construct($container, $eventDispatcher, $security);
        $this->_userGroupService = new UserGroupService($container, $eventDispatcher, $security);
    }

    /**
     * Удаление пользовательской группы.
     * Метод вызывается автоматически в конце команды конструктора.
     * Если ваша команда определяет свой собственный конструктор,
     * сначала установите свойства,
     * а затем вызовите родительский конструктор,
     * чтобы эти свойства были доступны в configure() методе
     */
    protected function configure(): void
    {
        $help = '<fg=green>api:entity:user-group:delete</> command deletes user group: ' . PHP_EOL;
        $help .= 'Remove by ID: <fg=green>php %command.full_name% 777</>' . PHP_EOL;
        $help .= 'Remove by name: <fg=green>php %command.full_name% GROUP_ADMINISTRATORS</>' . PHP_EOL;
        $this->setName('api:entity:user-group:delete')
            ->setAliases(['group:delete', 'fos:group:delete'])
            ->addArgument('id', InputArgument::REQUIRED, 'Enter ID or name to delete user group')
            ->setDescription('Delete user group by ID or name')
            ->setHelp($help);
    }

    /**
     * Выполняет текущую команду.
     * Этот метод не является абстрактным,
     * потому что вы можете использовать этот класс в качестве конкретного класса.
     * В этом случае вместо определения метода execute() вы устанавливаете код для выполнения,
     * передавая Closure методу setCode().
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void null or 0 if everything went fine, or an error code
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $idOrName = $input->getArgument('id');
        $type = is_numeric($idOrName)
            ? self::DELETE_BY_ID
            : self::DELETE_BY_NAME;

        // Delete group
        try {
            switch ($type) {
                case self::DELETE_BY_ID:
                    $userGroup = $this->_userGroupService->getUserGroupById($idOrName);
                    if (!$userGroup instanceof UserGroup) {
                        throw new RuntimeException("<fg=red>[Error] Group with id: <fg=green>{$idOrName}</> not found!</>");
                    }
                    $this->_confirmMessage($userGroup, $input, $output);
                    $this->_userGroupService->deleteUserGroupById($userGroup->getId());
                    break;

                case self::DELETE_BY_NAME:
                    $userGroup = $this->_userGroupService->getUserGroupByName($idOrName);
                    if (!$userGroup instanceof UserGroup) {
                        throw new RuntimeException("<fg=red>Group with name:</> <fg=yellow>{$idOrName}</> <fg=red>not found!</>");
                    }
                    $this->_confirmMessage($userGroup, $input, $output);
                    $this->_userGroupService->deleteUserGroupByName($idOrName);
                    break;

                default:
                    $exceptionMessage = 'User group name type is not correct, must be either ID (int) or name (string)';
                    throw new RuntimeException($exceptionMessage);
            }
        } catch (Exception $e) {
            $output->writeln("<fg=red>{$e->getMessage()}</>");
            exit(1);
        }

        // Output the finish message
        $output->writeln('    <fg=black;bg=green> ==> </> User group successfully deleted.' . PHP_EOL);
    }

    /**
     * Выводит сообщение с подтверждением на удаление пользовательской группы
     * @param UserGroup       $userGroup
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    private function _confirmMessage(UserGroup $userGroup, InputInterface $input, OutputInterface $output): void
    {
        $text = "    ID: <fg=cyan>{$userGroup->getId()}</>" . PHP_EOL;
        $text .= "    Name: <fg=cyan>{$userGroup->getName()}</>" . PHP_EOL;
        $userGroup->getNameRus() && $text .= "    Name rus: <fg=cyan>{$userGroup->getNameRus()}</>" . PHP_EOL;
        $userGroup->getDescription() && $text .= "    Description: <fg=cyan>{$userGroup->getDescription()}</>" . PHP_EOL;
        $roleList = implode('</>", "<fg=cyan>', $userGroup->getRoles());
        $roleList && $text .= "    Roles: \"<fg=cyan>{$roleList}</>\"" . PHP_EOL;
        $text .= '<fg=cyan>Are you sure you want to delete the user group? (yes/No): </>';

        $question = new Question($text);
        $confirm = (string)$this->getHelper('question')->ask($input, $output, $question);
        if (!$confirm || !in_array(strtolower($confirm[0]), ['y', 'д'])) {
            $output->writeln('<fg=red>Cancel</>' . PHP_EOL);
            exit(0);
        }
    }

    /**
     * Взаимодействует с пользователем.
     * Этот метод выполняется перед проверкой InputDefinition.
     * Это означает, что это единственное место, где команда может в интерактивном режиме
     * запрашивать значения отсутствующих обязательных аргументов.
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        // Id
        if (!$input->getArgument('id')) {
            $questionText = '<fg=cyan>Please choose a group ID (or name) for deleting: ';
            $question = new Question($questionText);
            $question->setValidator(static function ($userGroupIdOrName) {
                if (empty($userGroupIdOrName)) {
                    throw new RuntimeException('[Error] User group ID (or name) can not be empty');
                }
                return $userGroupIdOrName;
            });
            $groupName = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('id', $groupName);
        }
    }
}

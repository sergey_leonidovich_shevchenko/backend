<?php declare(strict_types=1);

namespace App\Command\Api\UserGroup;

use App\Command\Api\BaseApiCommand;
use App\Service\Api\UserGroupService;
use Exception;
use RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class CreateCommand
 * @package App\Command\Api\UserGroup
 * @author  Serrgey Shevchenko <sergey.leonidovich.shevchenko@gmail.com>
 */
class CreateCommand extends BaseApiCommand
{
    protected static $defaultName = 'api:entity:user-group:create';

    /** @var UserGroupService */
    private $_userGroupService;

    /**
     * UserGroupCommand constructor.
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $eventDispatcher
     * @param Security $security
     */
    public function __construct(ContainerInterface $container, EventDispatcherInterface $eventDispatcher, Security $security)
    {
        parent::__construct($container, $eventDispatcher, $security);
        $this->_userGroupService = new UserGroupService($container, $eventDispatcher, $security);
    }

    /**
     * Создание пользовательской группы.
     * Метод вызывается автоматически в конце команды конструктора.
     * Если ваша команда определяет свой собственный конструктор,
     * сначала установите свойства,
     * а затем вызовите родительский конструктор,
     * чтобы эти свойства были доступны в configure() методе
     */
    protected function configure(): void
    {
        $help = 'The <fg=green>api:entity:user-group:create</> ';
        $help .= "Create user group:\n\n";
        $help .= "  <fg=green>php %command.full_name% group_name.1</>\n";
        $this->setName('api:entity:user-group:create')
            ->setAliases(['group:create', 'fos:group:create'])
            ->addArgument('name', InputArgument::REQUIRED, 'Enter a name for the new user group')
            ->addArgument('roles', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, 'A list of roles separated with a space')
            ->addOption('name_rus', 'R', InputArgument::OPTIONAL, 'Enter a name for the new user group is the Russian language')
            ->addOption('description', 'D', InputArgument::OPTIONAL, 'Enter a description for the new user group')
            ->setDescription('Create user group')
            ->setHelp($help);
    }

    /**
     * Выполняет текущую команду.
     * Этот метод не является абстрактным,
     * потому что вы можете использовать этот класс в качестве конкретного класса.
     * В этом случае вместо определения метода execute() вы устанавливаете код для выполнения,
     * передавая Closure методу setCode().
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void null or 0 if everything went fine, or an error code
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $name = $input->getArgument('name');
        $roles = $input->getArgument('roles');

        $nameRus = $input->getOption('name_rus');
        $description = $input->getOption('description');

        // Create group
        try {
            $userGroup = $this->_userGroupService->createUserGroup([
                'name' => $name,
                'roles' => $roles,
                'name_rus' => $nameRus,
                'description' => $description,
            ]);
        } catch (Exception $e) {
            $output->writeln("<fg=red>{$e->getMessage()}</>");
            exit(1);
        }

        // Output the finish message
        $text = "    <fg=green> ==> </> User group with name \"<fg=green>{$userGroup->getName()}</>\" successfully created." . PHP_EOL;
        $nameRus && $text .= "    <fg=green> ==> </> Name: \"<fg=green>{$userGroup->getNameRus()}</>\"" . PHP_EOL;
        $description && $text .= "    <fg=green> ==> </> Description: \"<fg=green>{$userGroup->getDescription()}</>\"" . PHP_EOL;
        if (count($roles)) {
            $text .= '    <fg=green> ==> </> Assigned the following roles: ["<fg=green>%s</>"]';
            $text = sprintf($text, implode('</>", "<fg=green>', $userGroup->getRoles()));
        }

        $output->writeln($text);
    }

    /**
     * Взаимодействует с пользователем.
     * Этот метод выполняется перед проверкой InputDefinition.
     * Это означает, что это единственное место, где команда может в интерактивном режиме
     * запрашивать значения отсутствующих обязательных аргументов.
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        // Name
        if (!$input->getArgument('name')) {
            $questionText = '<fg=cyan>Please choose a group name (e.g. <fg=yellow>group_name.1</>) '
                . 'or press continue (Enter):</> ';
            $question = new Question($questionText);
            $question->setValidator(static function ($groupName) {
                if (empty($groupName)) {
                    throw new RuntimeException('Group name can not be empty');
                }
                return $groupName;
            });
            $groupName = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('name', $groupName);
        }

        // Name rus
        if (!$input->getOption('name_rus')) {
            $questionText = '<fg=cyan>Please enter a name for the custom group in the Russian language '
                . '(required for display in the UI) (e.g. <fg=yellow>Моя новая группа</>) or press continue (Enter):</> ';
            $question = new Question($questionText);
            $nameRus = $this->getHelper('question')->ask($input, $output, $question);
            $input->setOption('name_rus', $nameRus);
        }

        // Description
        if (!$input->getOption('description')) {
            $questionText = '<fg=cyan>Please enter a description for the custom group or press continue (Enter):</> ';
            $question = new Question($questionText);
            $description = $this->getHelper('question')->ask($input, $output, $question);
            $input->setOption('description', $description);
        }
    }
}

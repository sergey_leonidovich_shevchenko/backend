<?php declare(strict_types=1);

namespace App\Command\Api;

use App\Command\BaseCommand;

/**
 * Class BaseApiCommand
 * @package App\Command\Api
 */
abstract class BaseApiCommand extends BaseCommand
{

}

<?php declare(strict_types=1);

namespace App\Command\MT;

use App\Command\BaseCommand;

/**
 * Class BaseMTCommand
 * @package App\Command\MT
 */
abstract class BaseMTCommand extends BaseCommand
{

}

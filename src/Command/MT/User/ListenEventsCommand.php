<?php declare(strict_types=1);

namespace App\Command\MT\User;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Command\MT\BaseMTCommand;
use App\Service\MT\Adapter\UserAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Mt5ManagerApi\UserEventResponse;

/**
 * Class ListenEventsCommand
 * @package App\Command\Api\UserGroup
 * @author  Serrgey Shevchenko <sergey.leonidovich.shevchenko@gmail.com>
 */
class ListenEventsCommand extends BaseMTCommand
{
    protected static $defaultName = 'mt:user:listen-events';

    /** @var UserAdapter */
    private $_userAdapter;

    /**
     * UserGroupCommand constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->_userAdapter = $this->container->get(UserAdapter::class);
    }

    /**
     * Слушать события в режиме live на mt сервере
     * Метод вызывается автоматически в конце команды конструктора.
     * Если ваша команда определяет свой собственный конструктор,
     * сначала установите свойства,
     * а затем вызовите родительский конструктор,
     * чтобы эти свойства были доступны в configure() методе
     */
    protected function configure(): void
    {
        $help = 'The <fg=green>mt:user:listen-events</> ';
        $help .= "Listen to live events on the mt server\n\n";
        $this->setName('mt:user:listen-events')
            ->setDescription('Listen to live events on the mt server')
            ->setHelp($help);
    }

    /**
     * Выполняет текущую команду.
     * Этот метод не является абстрактным,
     * потому что вы можете использовать этот класс в качестве конкретного класса.
     * В этом случае вместо определения метода execute() вы устанавливаете код для выполнения,
     * передавая Closure методу setCode().
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void null or 0 if everything went fine, or an error code
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $stream = $this->_userAdapter->subscribeToEvents();
        foreach ($stream->responses() as $response) {
            /** @var UserEventResponse $response */
            $event = $response->getEvent();
            $jsonResponse = $output->writeln(json_decode($response->serializeToJsonString(), true, 512, JSON_THROW_ON_ERROR));
        }

        exit('Поток событий закрылся или оборвался.');
    }
}

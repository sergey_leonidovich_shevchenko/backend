<?php declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class BaseCommand
 * Настройка команды:
 *     configure() - Метод вызывается автоматически в конце конструктора команд.
 *     Если ваша команда определяет свой собственный конструктор, сначала установите свойства,
 *     а затем вызовите родительский конструктор, чтобы сделать эти свойства доступными в методе configure().
 * Команды имеют три метода жизненного цикла, которые вызываются при запуске команды:
 *     initialize() (необязательный) - Этот метод выполняется перед interact() и в execute() методах.
 *         Его основная цель - инициализировать переменные, используемые в остальных командных методах.
 *     interact() (необязательный) - Этот метод выполняется после initialize() и до execute().
 *         Его цель - проверить, отсутствуют ли некоторые параметры/аргументы,
 *         и в интерактивном режиме запросить у пользователя эти значения.
 *         Это последнее место, где вы можете попросить пропустить опции/аргументы.
 *         После этой команды пропущенные параметры/аргументы приведут к ошибке.
 *     execute() (требуется) - Этот метод выполняется после interact() и initialize().
 *         Он содержит логику, которую команду для выполнения, и она должна возвращать целое число,
 *         которое будет использоваться в качестве команды состояния выхода.
 * @package App\Command\Api
 */
abstract class BaseCommand extends Command
{
    /** @var ContainerInterface */
    protected $container;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /** @var Security */
    protected $security;

    /**
     * UserGroupCommand constructor.
     * @param ContainerInterface       $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
        $this->eventDispatcher = $this->container->get('event_dispatcher');
        $this->security = $this->container->get('security.helper');
    }
}

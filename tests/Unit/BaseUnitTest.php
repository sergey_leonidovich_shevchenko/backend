<?php declare(strict_types=1);

namespace App\Tests\Unit;

use App\Tests\BaseTest;

/**
 * Class BaseUnitTest
 * @package App\Tests\Unit
 */
abstract class BaseUnitTest extends BaseTest
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
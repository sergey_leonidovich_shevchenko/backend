<?php declare(strict_types=1);

namespace App\Tests\Unit\MT\Service;

use App\Tests\Unit\BaseUnitTest;
use App\Service\MT\Adapter\UserAdapter;
use App\Helper\DataGenerator\Api\Security\GeneratePassword;
use App\Entity\Api\Account;
use App\Entity\Api\TradingPlatform;
use App\Entity\Api\PersonalArea;
use App\Service\MT\AccountService;
use RuntimeException;
use App\Helper\DataGenerator\Api\Entity\AccountGenerator as AccountGenerator;
use App\Exception\EntityException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class UserServiceTest
 * @package App\Tests\Unit\MT\Service
 */
class UserServiceTest extends BaseUnitTest
{
    /** @var UserAdapter */
    private $_userAdapter;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->_userAdapter = parent::$container->get(UserAdapter::class);
    }

    /**
     * Тестирование добавления счета на MT сервере
     * @group mt
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testAdd(): void
    {
        /** @var GeneratePassword $generatePasswordHelper */
        $generatePasswordHelper = parent::$container->get(GeneratePassword::class);
        $masterPassword = $generatePasswordHelper->generateWithAllSymbols();
        $investorPassword = $generatePasswordHelper->generateWithAllSymbols();

        $accountGenerator = parent::$container->get(AccountGenerator::class);
        $account = $accountGenerator->generateEntity();
        $user = $this->_fillUser($account);
        self::assertTrue($this->_userAdapter->add($user, $masterPassword, $investorPassword));
    }

    /**
     * Собрать структуру пользователя и наполнить данными для отправки на mt сервер
     * @param Account $account
     * @return array
     */
    private function _fillUser(Account $account): array
    {
        $tradingPlatform = $account->getTradingPlatform();
        if (!$tradingPlatform instanceof TradingPlatform) {
            throw new RuntimeException('$tradingPlatform is not instanceof App\\Entity\\Api\\TradingPlatform!');
        }

        $personalArea = $account->getPersonalArea();
        if (!$personalArea instanceof PersonalArea) {
            throw new RuntimeException('$personalArea is not instanceof App\\Entity\\Api\\PersonalArea!');
        }

        return [
            // берётся из сущности платформы (@see TradingPlatform::groups)
            'group' => $tradingPlatform->getGroup() ?? parent::$container->getParameter('mt.user_group.default'),
            'color' => AccountService::DEFAULT_COLOR,
            'rights' => AccountService::USER_RIGHTS_BY_DEFAULT,
            // берётся из  сущности "Личные  кабинеты" обычное ФИО
            'name' => $personalArea->getFullName(),
            // опционально, по умолчанию пусто
            'country' => null,
            // по умолчанию 0
            'language' => null ?? 0,
            // по умолчанию пусто
            'city' => null,
            // по умолчанию пусто
            'address' => null,
            // по умолчанию пусто
            'phone' => null,
            // по умолчанию пусто
            'email' => null,
            // по умолчанию пусто
            'comment' => null,
            // по умолчанию пусто
            'phonePassword' => null,
            // берётся из настроек платформы (обязательное поле)
            'leverage' => 100,
            // берётся поле "code" из сущности "Торговые платформы"
            'leadCampaign' => null,
            // берётся поле "ID" из сущности "Личные  кабинеты"
            'id' => null,
        ];
    }
}

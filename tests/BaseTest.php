<?php declare(strict_types=1);

namespace App\Tests;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class BaseTest
 * @package App\Tests
 */
abstract class BaseTest extends WebTestCase
{
    protected const TERMINAL_TEXT_COLOR_NC = "\e[0m";

    protected const TERMINAL_TEXT_COLOR_RED = "\e[31m";

    protected const TERMINAL_TEXT_COLOR_GREEN = "\e[32m";

    protected const TERMINAL_TEXT_COLOR_YELLOW = "\e[33m";

    protected const TERMINAL_TEXT_COLOR_BLUE = "\e[34m";

    protected const TERMINAL_TEXT_COLOR_MAGENTA = "\e[35m";

    protected const TERMINAL_TEXT_COLOR_CYAN = "\e[36m";

    /** @var EntityManager */
    protected $em;

    /**
     * BaseTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        self::bootKernel();
        $this->em = parent::$container->get('doctrine')->getManager();
    }

    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function tearDown(): void
    {
        // doing this is recommended to avoid memory leaks
        $this->em->close();
        $this->em = null;
        parent::tearDown();
    }

    /**
     * @param                $text
     * @param string         $color
     * @param false|resource $handle
     */
    public static function fwriteColored($text, $color = '', $handle = STDOUT): void
    {
        fwrite(STDOUT, sprintf('%s%s%s', $color, $text, self::TERMINAL_TEXT_COLOR_NC));
    }
}

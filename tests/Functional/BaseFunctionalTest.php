<?php declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\Api\User;
use App\Helper\RoutingHelper;
use App\Tests\BaseTest;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseFunctionalTest
 * @package App\Tests\Functional\Api\Controller
 */
abstract class BaseFunctionalTest extends BaseTest
{
    /** @var KernelBrowser */
    protected $client;

    /** @var string JWT-token */
    protected $jwt;

    /** @var array Актуальные данные сущности (для добавления) */
    protected static $startData = [];

    /** @var array Новые данные сущности (для обновления) */
    protected static $updateData = [];

    /** @var string URN - Unifrorm Resource Name (унифицированное имя ресурса) */
    protected static $urn;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient(['environment' => 'test']);
        $this->client->disableReboot();
    }

    protected function tearDown(): void
    {
        $this->client = null;
        parent::tearDown();
    }

    /**
     * @param string $method Request::METHOD_*
     * @param string $uri    Uri
     * @param array  $data   Data
     * @param array  $headers
     * @return Response
     */
    protected function request(string $method, string $uri, array $data = [], array $headers = []): Response
    {
        if (empty($headers['Content_Type'])) {
            $headers['Content_Type'] = 'application/json';
        }

        if (empty($headers['Authorization']) && $this->jwt) {
            $headers['Authorization'] = "Bearer {$this->jwt}";
        }

        $this->client->request(
            $method,
            $uri,
            $headers,
            [],
            $headers,
            json_encode($data, JSON_THROW_ON_ERROR, 512)
        );

        return $this->client->getResponse();
    }

    /**
     * Get JWT-token (auth)
     * @param string        $username
     * @param string        $password
     * @param string[]|null $setRoles
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function initAuth(string $username, string $password, ?array $setRoles = []): User
    {
        $userRepository = $this->em->getRepository(User::class);

        $user = $userRepository->findOneBy(['username' => $username]);
        if (!$user) {
            $this->registerUser($username, $password, $setRoles);
        }

        $this->_changeUserRoles($user, $setRoles);
        $this->jwt = $this->loginUser($user->getUsername(), $password);

        return $user;
    }

    /**
     * Register new user
     * @param string $username
     * @param string $password
     * @param array  $roles
     */
    private function registerUser(string $username, string $password, $roles = []): void
    {
        $response = $this->request(Request::METHOD_POST, '/security/register', [
            'username' => $username,
            'password' => $password,
            'enabled' => 1,
            'roles' => $roles,
        ]);

        $responseCode = $response->getStatusCode();
        $responseContent = $response->getContent();
        self::assertEquals(Response::HTTP_CREATED, $responseCode, $responseContent);
        $responseContent = json_decode($responseContent, true, 512, JSON_THROW_ON_ERROR);
        $this->assertApiEntity($responseContent);
    }

    /**
     * Auth user
     * @param string $username
     * @param string $password
     * @return string
     */
    private function loginUser(string $username, string $password): string
    {
        $securityLoginRoute = (new RoutingHelper(self::$container))->getPathByName('route.security.security.login');
        $response = $this->request(Request::METHOD_POST, $securityLoginRoute, [
            'username' => $username,
            'password' => $password,
        ]);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 4, JSON_THROW_ON_ERROR);
        self::assertArrayHasKey('token', $responseContent, 'Произошла ошибка при получении JWT-токена');

        return $this->jwt = $responseContent['token'];
    }

    /**
     * Изменить права пользователя с помощью curl-запроса на сервер
     * @param User          $user
     * @param string[]|null $setNewRoles
     */
    private function _changeUserRoles(User $user, ?array $setNewRoles = []): void
    {
        $securityLoginRoute = (new RoutingHelper(self::$container))->getPathByName('route.security.security.login');
        $response = $this->request(Request::METHOD_POST, $securityLoginRoute, [
            'username' => 'admin',
            'password' => 'admin-password',
        ]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 4, JSON_THROW_ON_ERROR);
        self::assertArrayHasKey('token', $responseContent, 'Произошла ошибка при получении JWT-токена');
        $jwt = $responseContent['token'];
        $headers['Authorization'] = "Bearer {$jwt}";

        if ($setNewRoles !== null) {
            $routeSetNewRolesToUser = sprintf('/security/permission/user/%d/role', $user->getId());
            $response = $this->request(
                Request::METHOD_POST,
                $routeSetNewRolesToUser,
                ['roles' => $setNewRoles],
                $headers,
                );
            self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        }
    }

    /**
     * Получить первую, существующую сущность из БД
     * @param string $entityNameToLowerCase Название сущности в нижнем регистре для подстановки в маршрут (напр. account)
     * @param string $errMsg                Текст ошибки если сущность не получена из БД
     * @return array
     */
    protected function getFirstEntityFromDB(string $entityNameToLowerCase, string $errMsg): array
    {
        $urn = sprintf('/api/%s/page/1/count_per_page/1', $entityNameToLowerCase);
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 5, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertCount(1, $responseContent, $errMsg);
        return (array)$responseContent[0];
    }

    /**
     * Проверяем работоспособность роли на добавление
     * @param string $entityNameToLowerCase
     * @param array  $entity
     */
    protected function checkWorkingRolesForAdd(string $entityNameToLowerCase, array $entity): void
    {
        // Проверяем роль добавления сущности (должен запретить сделать этого, тк нету необходимой роли)
        $urn = sprintf('/api/%s/', $entityNameToLowerCase);
        $response = $this->request(Request::METHOD_POST, $urn, $entity);
        self::assertEquals(Response::HTTP_CREATED, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Проверяем работоспособность роли на обновление
     * @param string $entityNameToLowerCase
     * @param array  $entity
     * @param array  $newData
     */
    protected function checkWorkingRolesForUpdate(string $entityNameToLowerCase, array $entity, array $newData): void
    {
        // Проверяем роль обновления сущности (должен разрешить сделать этого, тк есть необходимая роль)
        $urn = sprintf('/api/%s/%d', $entityNameToLowerCase, $entity['id']);
        $response = $this->request(Request::METHOD_PUT, $urn, $newData);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Проверяем работоспособность роли на удаление
     * @param string $entityNameToLowerCase
     * @param array  $entity
     */
    protected function checkWorkingRolesForDelete(string $entityNameToLowerCase, array $entity): void
    {
        // Проверяем роль удаление сущности (должен запретить сделать этого, тк нету необходимой роли)
        $urn = sprintf('/api/%s/%d', $entityNameToLowerCase, $entity['id']);
        $response = $this->request(Request::METHOD_DELETE, $urn);
        self::assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Проверяем не работоспособность на добавление
     * @param string $entityNameToLowerCase
     * @param array  $entity
     */
    protected function checkNotWorkingRolesForAdd(string $entityNameToLowerCase, array $entity): void
    {
        // Проверяем роль добавления сущности (должен запретить сделать этого, тк нету необходимой роли)
        $urn = sprintf('/api/%s/', $entityNameToLowerCase);
        $response = $this->request(Request::METHOD_POST, $urn, $entity);
        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Проверяем не работоспособность на обновление
     * @param string $entityNameToLowerCase
     */
    protected function checkNotWorkingRolesForUpdate(string $entityNameToLowerCase): void
    {
        // Проверяем роль обновления сущности (должен запретить сделать этого, тк нету необходимой роли)
        $urn = sprintf('/api/%s/', $entityNameToLowerCase);
        $response = $this->request(Request::METHOD_POST, $urn);
        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Проверяем не работоспособность на удаление
     * @param string $entityNameToLowerCase
     * @param array  $entity
     */
    protected function checkNotWorkingRolesForDelete(string $entityNameToLowerCase, array $entity): void
    {
        // Проверяем роль удаление сущности (должен запретить сделать этого, тк нету необходимой роли)
        $urn = sprintf('/api/%s/%d', $entityNameToLowerCase, $entity['id']);
        $response = $this->request(Request::METHOD_DELETE, $urn);
        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode(), $response->getContent());
    }
}

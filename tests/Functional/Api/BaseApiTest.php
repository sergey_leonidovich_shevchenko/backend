<?php declare(strict_types=1);

namespace App\Tests\Functional\Api;

use App\Tests\Functional\BaseFunctionalTest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\BaseEntityApi;
use DateTime;
use App\Entity\Api\Transaction;
use Exception;
use App\Entity\Api\User;

/**
 * Class BaseApiTest
 * @package App\Tests\Functional\Api
 */
abstract class BaseApiTest extends BaseFunctionalTest
{
    /** @var string */
    protected static $entityClassName;

    /** @var string */
    protected static $entityNameSnakeCase;

    public static function setUpBeforeClass(): void
    {
        $startText = 'Тестирование сущности "' . static::$entityClassName . '" начата...' . PHP_EOL;
        self::fwriteColored($startText, self::TERMINAL_TEXT_COLOR_CYAN);
        parent::setUpBeforeClass();
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        $finishText = PHP_EOL . 'Тестирование сущности "' . static::$entityClassName . '" закончена.' . PHP_EOL;
        self::fwriteColored($finishText, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование маршрута добавления сущности
     * [POST] /api/{version}/{entity_name}/
     * @throws Exception
     */
    public function addBase(): void
    {
        $response = $this->request(Request::METHOD_POST, static::$urn, self::$startData[self::$entityClassName]);
        self::assertEquals(Response::HTTP_CREATED, $response->getStatusCode(), $response->getContent());
        $this->assertApiEntity(self::$startData[self::$entityClassName]);
    }

    /**
     * Тестирование маршрута получения сущности
     * [GET] /api/{version}/{entity_name}/{id}
     */
    public function getBase(): void
    {
        /** @var BaseEntityApi $entity */
        $entity = $this->em->getRepository(self::$entityClassName)->getLastAdded();
        static::assertInstanceOf(self::$entityClassName, $entity, 'Entity not found!');
        $url = self::$urn . $entity->getId();
        $response = $this->request(Request::METHOD_GET, $url, self::$startData[self::$entityClassName]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Тестирование маршрута получения списка сущностей
     * [GET] /api/{version}/{entity_name}/page/{page}
     * @param int $page
     * @param int $countPerPage
     */
    public function listBase(int $page = 1, $countPerPage = 25): void
    {
        $url = sprintf('%spage/%d/count_per_page/%d', self::$urn, $page, $countPerPage);
        $response = $this->request(Request::METHOD_GET, $url);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Тестирование маршрута обновления сущности
     * [PUT] /api/{version}/{entity_name}/{id}
     * @throws Exception
     */
    public function updateBase(): void
    {
        /** @var BaseEntityApi $entity */
        $entity = $this->em->getRepository(self::$entityClassName)->getLastAdded();
        static::assertInstanceOf(self::$entityClassName, $entity);
        $url = self::$urn . $entity->getId();
        $response = $this->request(Request::METHOD_PUT, $url, self::$updateData[self::$entityClassName]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $this->assertApiEntity(self::$updateData[self::$entityClassName]);
        self::$startData[self::$entityClassName] = self::$updateData[self::$entityClassName];
    }

    /**
     * Тестирование маршрута удаления сущности
     * [DELETE] /api/{version}/{entity_name}/{id}
     */
    public function deleteBase(): void
    {
        /** @var BaseEntityApi $entity */
        $entity = $this->em->getRepository(self::$entityClassName)->getLastAdded();
        static::assertInstanceOf(self::$entityClassName, $entity);
        $url = self::$urn . $entity->getId();
        $response = $this->request(Request::METHOD_DELETE, $url);
        self::assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode(), $response->getContent());
        $response = $this->request(Request::METHOD_GET, $url);
        self::assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Тестирование маршрута получения количество сущностей
     * [GET] /api/{version}/{entity_name}/total
     */
    public function countBase(): void
    {
        $url = sprintf('%stotal', self::$urn);
        $response = $this->request(Request::METHOD_GET, $url);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $content = $response->getContent();
        self::assertJson($content);
        $content = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        self::assertArrayHasKey('total', $content);
    }

    /**
     * Проверка, есть ли в массиве сущность с ожидаемыми значениями
     * @param array $requestEntityData
     * @throws Exception
     */
    protected function assertApiEntity(array $requestEntityData): void
    {
        if (isset($requestEntityData['entity'])) {
            $requestEntityData = $requestEntityData['entity'];
        }

        $responseContent = json_decode($this->client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        self::assertArrayHasKey('entity', $responseContent);
        $responseEntityData = $responseContent['entity'];

        // TODO: Избавится от этого говна и написать уже хелпер приведения полей к единому стилю (Coercion of fields to one type)
        if (self::$entityClassName === Transaction::class) {
            if (isset($responseEntityData['valid_till'])) {
                $datetime = new DateTime($responseEntityData['valid_till']);
                $responseEntityData['valid_till'] = $datetime->format('Y-m-d');
            }
        }

        if (self::$entityClassName === User::class) {
            $removingFieldList = ['password'];
            foreach ($removingFieldList as $removingField) {
                unset($requestEntityData[$removingField]);
            }
        }

        foreach ($requestEntityData as $key => $data) {
            if (is_array($requestEntityData[$key]) && is_array($responseEntityData[$key])) {
                sort($requestEntityData[$key]);
                sort($responseEntityData[$key]);
            }
            self::assertSame($requestEntityData[$key], $responseEntityData[$key], "Key: {$key}");
        }
    }
}

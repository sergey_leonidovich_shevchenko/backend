<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\StatusVip;
use App\Helper\DataGenerator\Api\Entity\StatusVipGenerator as StatusVipDataGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Tests\Functional\Api\BaseApiTest;

/**
 * Class StatusVipTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class StatusVipTest extends BaseApiTest
{
    /**
     * StatusVipTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/status_vip/';
        parent::$entityNameSnakeCase = 'status_vip';
        parent::$entityClassName = StatusVip::class;
        $statusVipDataGenerator = parent::$container->get(StatusVipDataGenerator::class);
        self::$startData[StatusVip::class] = $statusVipDataGenerator->generateData();
        self::$updateData[StatusVip::class] = $statusVipDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления VIP-статуса... ');
        $roles = ['ROLE_ENTITY_API_STATUS_VIP_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения VIP-статуса... ');
        $roles = ['ROLE_ENTITY_API_STATUS_VIP_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления VIP-статуса... ');
        $roles = ['ROLE_ENTITY_API_STATUS_VIP_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка VIP-статусов... ');
        $roles = ['ROLE_ENTITY_API_STATUS_VIP_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления VIP-статуса... ');
        $roles = ['ROLE_ENTITY_API_STATUS_VIP_DELETE', 'ROLE_ENTITY_API_STATUS_VIP_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества VIP-статусов... ');
        $roles = ['ROLE_ENTITY_API_STATUS_VIP_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

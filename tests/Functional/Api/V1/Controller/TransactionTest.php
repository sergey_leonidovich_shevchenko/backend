<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\Transaction;
use App\Helper\DataGenerator\Api\Entity\TransactionGenerator as TransactionDataGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use App\Tests\Functional\Api\BaseApiTest;

/**
 * Class TransactionTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class TransactionTest extends BaseApiTest
{
    /**
     * TransactionTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     * @throws Exception
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/transaction/';
        parent::$entityNameSnakeCase = 'transaction';
        parent::$entityClassName = Transaction::class;
        $transactionDataGenerator = parent::$container->get(TransactionDataGenerator::class);
        self::$startData[Transaction::class] = $transactionDataGenerator->generateData();
        self::$updateData[Transaction::class] = $transactionDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления транзакции... ');
        $roles = ['ROLE_ENTITY_API_TRANSACTION_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения транзакции... ');
        $roles = ['ROLE_ENTITY_API_TRANSACTION_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления транзакции... ');
        $roles = ['ROLE_ENTITY_API_TRANSACTION_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка транзакций... ');
        $roles = ['ROLE_ENTITY_API_TRANSACTION_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления транзакции... ');
        $roles = ['ROLE_ENTITY_API_TRANSACTION_DELETE', 'ROLE_ENTITY_API_TRANSACTION_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества транзакций... ');
        $roles = ['ROLE_ENTITY_API_TRANSACTION_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

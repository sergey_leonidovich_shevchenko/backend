<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\User;
use App\Helper\DataGenerator\Api\Entity\UserGenerator as UserDataGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Tests\Functional\Api\BaseApiTest;
use Exception;

/**
 * Class UserTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class UserTest extends BaseApiTest
{
    /**
     * UserTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/user/';
        parent::$entityNameSnakeCase = 'user';
        parent::$entityClassName = User::class;
        $userDataGenerator = parent::$container->get(UserDataGenerator::class);
        self::$startData[User::class] = $userDataGenerator->generateData();
        self::$updateData[User::class] = $userDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления пользователя... ');
        $roles = ['ROLE_ENTITY_API_USER_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения пользователя... ');
        $roles = ['ROLE_ENTITY_API_USER_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления пользователя... ');
        $roles = ['ROLE_ENTITY_API_USER_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка пользователей... ');
        $roles = ['ROLE_ENTITY_API_USER_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления пользователя... ');
        $roles = ['ROLE_ENTITY_API_USER_DELETE', 'ROLE_ENTITY_API_USER_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества пользователей... ');
        $roles = ['ROLE_ENTITY_API_USER_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\ConsultingCompany;
use App\Helper\DataGenerator\Api\Entity\ConsultingCompanyGenerator as ConsultingCompanyDataGenerator;
use Exception;
use App\Tests\Functional\Api\BaseApiTest;

/**
 * Class ConsultingCompanyTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class ConsultingCompanyTest extends BaseApiTest
{
    /**
     * ConsultingCompanyTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/consulting_company/';
        parent::$entityNameSnakeCase = 'consulting_company';
        parent::$entityClassName = ConsultingCompany::class;
        $consultingCompanyDataGenerator = parent::$container->get(ConsultingCompanyDataGenerator::class);
        self::$startData[ConsultingCompany::class] = $consultingCompanyDataGenerator->generateData();
        self::$updateData[ConsultingCompany::class] = $consultingCompanyDataGenerator->generateData();
    }

    /**
     * @throws Exception
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления консалтинговой компании... ');
        $roles = ['ROLE_ENTITY_API_CONSULTING_COMPANY_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения консалтинговой компании... ');
        $roles = ['ROLE_ENTITY_API_CONSULTING_COMPANY_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления консалтинговой компании... ');
        $roles = ['ROLE_ENTITY_API_CONSULTING_COMPANY_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка консалтинговых компаний... ');
        $roles = ['ROLE_ENTITY_API_CONSULTING_COMPANY_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления консалтинговой компании... ');
        $roles = ['ROLE_ENTITY_API_CONSULTING_COMPANY_DELETE', 'ROLE_ENTITY_API_CONSULTING_COMPANY_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества консалтинговых компаний... ');
        $roles = ['ROLE_ENTITY_API_CONSULTING_COMPANY_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

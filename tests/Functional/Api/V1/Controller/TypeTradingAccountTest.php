<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\TypeTradingAccount;
use App\Helper\DataGenerator\Api\Entity\TypeTradingAccountGenerator as TypeTradingAccountDataGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Tests\Functional\Api\BaseApiTest;

/**
 * Class TypeTradingAccountTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class TypeTradingAccountTest extends BaseApiTest
{
    /**
     * TypeTradingAccountTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/type_trading_account/';
        parent::$entityNameSnakeCase = 'type_trading_account';
        parent::$entityClassName = TypeTradingAccount::class;
        $typeTradingAccountDataGenerator = parent::$container->get(TypeTradingAccountDataGenerator::class);
        self::$startData[TypeTradingAccount::class] = $typeTradingAccountDataGenerator->generateData();
        self::$updateData[TypeTradingAccount::class] = $typeTradingAccountDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления типа торгового счета... ');
        $roles = ['ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения типа торгового счета... ');
        $roles = ['ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления типа торгового счета... ');
        $roles = ['ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка типов торговых счетов... ');
        $roles = ['ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления типа торгового счета... ');
        $roles = ['ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_DELETE', 'ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества типов торговых счетов... ');
        $roles = ['ROLE_ENTITY_API_TYPE_TRADING_ACCOUNT_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

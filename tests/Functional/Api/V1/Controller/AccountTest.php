<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\Account;
use App\Exception\EntityException;
use App\Helper\DataGenerator\Api\Entity\AccountGenerator as AccountDataGenerator;
use Exception;
use App\Tests\Functional\Api\BaseApiTest;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class AccountTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
class AccountTest extends BaseApiTest
{
    /**
     * AccountTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     * @throws EntityException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/account/';
        parent::$entityNameSnakeCase = 'account';
        parent::$entityClassName = Account::class;
        $accountDataGenerator = parent::$container->get(AccountDataGenerator::class);
        self::$startData[Account::class] = $accountDataGenerator->generateData();
        self::$updateData[Account::class] = $accountDataGenerator->generateData();
    }

    /**
     * @group mt
     * @throws Exception
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления счета... ');
        $roles = ['ROLE_ENTITY_API_ACCOUNT_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения счета... ');
        $roles = ['ROLE_ENTITY_API_ACCOUNT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления счета... ');
        $roles = ['ROLE_ENTITY_API_ACCOUNT_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка счетов... ');
        $roles = ['ROLE_ENTITY_API_ACCOUNT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления счета... ');
        $roles = ['ROLE_ENTITY_API_ACCOUNT_DELETE', 'ROLE_ENTITY_API_ACCOUNT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества счетов... ');
        $roles = ['ROLE_ENTITY_API_ACCOUNT_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\SalesDepartment;
use App\Exception\EntityException;
use App\Helper\DataGenerator\Api\Entity\SalesDepartmentGenerator as SalesDepartmentDataGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use App\Tests\Functional\Api\BaseApiTest;

/**
 * Class SalesDepartmentTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class SalesDepartmentTest extends BaseApiTest
{
    /**
     * SalesDepartmentTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     * @throws EntityException
     * @throws Exception
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/sales_department/';
        parent::$entityNameSnakeCase = 'sales_department';
        parent::$entityClassName = SalesDepartment::class;
        $salesDepartmentDataGenerator = parent::$container->get(SalesDepartmentDataGenerator::class);
        self::$startData[SalesDepartment::class] = $salesDepartmentDataGenerator->generateData();
        self::$updateData[SalesDepartment::class] = $salesDepartmentDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления департамента продаж... ');
        $roles = ['ROLE_ENTITY_API_SALES_DEPARTMENT_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения департамента продаж... ');
        $roles = ['ROLE_ENTITY_API_SALES_DEPARTMENT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления департамента продаж... ');
        $roles = ['ROLE_ENTITY_API_SALES_DEPARTMENT_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка департаментов продаж... ');
        $roles = ['ROLE_ENTITY_API_SALES_DEPARTMENT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления департамента продаж... ');
        $roles = ['ROLE_ENTITY_API_SALES_DEPARTMENT_DELETE', 'ROLE_ENTITY_API_SALES_DEPARTMENT_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества департаментов продаж... ');
        $roles = ['ROLE_ENTITY_API_SALES_DEPARTMENT_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

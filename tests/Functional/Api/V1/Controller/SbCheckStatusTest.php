<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\SbCheckStatus;
use App\Helper\DataGenerator\Api\Entity\SbCheckStatusGenerator as SbCheckStatusDataGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Tests\Functional\Api\BaseApiTest;

/**
 * Class SbCheckStatusTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class SbCheckStatusTest extends BaseApiTest
{
    /**
     * SbCheckStatusTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/sb_check_status/';
        parent::$entityNameSnakeCase = 'sb_check_status';
        parent::$entityClassName = SbCheckStatus::class;
        $sbCheckStatusDataGenerator = parent::$container->get(SbCheckStatusDataGenerator::class);
        self::$startData[SbCheckStatus::class] = $sbCheckStatusDataGenerator->generateData();
        self::$updateData[SbCheckStatus::class] = $sbCheckStatusDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления статуса проверки СБ... ');
        $roles = ['ROLE_ENTITY_API_SB_CHECK_STATUS_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения статуса проверки СБ... ');
        $roles = ['ROLE_ENTITY_API_SB_CHECK_STATUS_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления статуса проверки СБ... ');
        $roles = ['ROLE_ENTITY_API_SB_CHECK_STATUS_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка статусов проверки СБ... ');
        $roles = ['ROLE_ENTITY_API_SB_CHECK_STATUS_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления статуса проверки СБ... ');
        $roles = ['ROLE_ENTITY_API_SB_CHECK_STATUS_DELETE', 'ROLE_ENTITY_API_SB_CHECK_STATUS_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества статусов проверки СБ... ');
        $roles = ['ROLE_ENTITY_API_SB_CHECK_STATUS_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

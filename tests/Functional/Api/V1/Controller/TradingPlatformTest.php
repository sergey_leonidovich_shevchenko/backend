<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\TradingPlatform;
use App\Helper\DataGenerator\Api\Entity\TradingPlatformGenerator as TradingPlatformDataGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Tests\Functional\Api\BaseApiTest;
use Exception;

/**
 * Class TradingPlatformTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class TradingPlatformTest extends BaseApiTest
{
    /**
     * TradingPlatformTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/trading_platform/';
        parent::$entityNameSnakeCase = 'trading_platform';
        parent::$entityClassName = TradingPlatform::class;
        $tradingPlatformDataGenerator = parent::$container->get(TradingPlatformDataGenerator::class);
        self::$startData[TradingPlatform::class] = $tradingPlatformDataGenerator->generateData();
        self::$updateData[TradingPlatform::class] = $tradingPlatformDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления торговой платформы... ');
        $roles = ['ROLE_ENTITY_API_TRADING_PLATFORM_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения торговой платформы... ');
        $roles = ['ROLE_ENTITY_API_TRADING_PLATFORM_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления торговой платформы... ');
        $roles = ['ROLE_ENTITY_API_TRADING_PLATFORM_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка торговой платформы... ');
        $roles = ['ROLE_ENTITY_API_TRADING_PLATFORM_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления торговой платформы... ');
        $roles = ['ROLE_ENTITY_API_TRADING_PLATFORM_DELETE', 'ROLE_ENTITY_API_TRADING_PLATFORM_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества торговой платформы... ');
        $roles = ['ROLE_ENTITY_API_TRADING_PLATFORM_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

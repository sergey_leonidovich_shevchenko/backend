<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\PersonalArea;
use App\Exception\EntityException;
use App\Helper\DataGenerator\Api\Entity\PersonalAreaGenerator as PersonalAreaDataGenerator;
use Exception;
use App\Tests\Functional\Api\BaseApiTest;

/**
 * Class PersonalAreaTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class PersonalAreaTest extends BaseApiTest
{
    /**
     * PersonalAreaTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     * @throws EntityException
     * @throws Exception
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/personal_area/';
        parent::$entityNameSnakeCase = 'personal_area';
        parent::$entityClassName = PersonalArea::class;
        $personalAreaDataGenerator = parent::$container->get(PersonalAreaDataGenerator::class);
        self::$startData[PersonalArea::class] = $personalAreaDataGenerator->generateData();
        self::$updateData[PersonalArea::class] = $personalAreaDataGenerator->generateData();
    }

    /**
     * @throws Exception
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления личного кабинета... ');
        $roles = ['ROLE_ENTITY_API_PERSONAL_AREA_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения личного кабинета... ');
        $roles = ['ROLE_ENTITY_API_PERSONAL_AREA_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления личного кабинета... ');
        $roles = ['ROLE_ENTITY_API_PERSONAL_AREA_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка личных кабинетов... ');
        $roles = ['ROLE_ENTITY_API_PERSONAL_AREA_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления личного кабинета... ');
        $roles = ['ROLE_ENTITY_API_PERSONAL_AREA_DELETE', 'ROLE_ENTITY_API_PERSONAL_AREA_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества личных кабинетов... ');
        $roles = ['ROLE_ENTITY_API_PERSONAL_AREA_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

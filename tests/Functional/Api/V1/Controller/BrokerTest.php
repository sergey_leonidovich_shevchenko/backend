<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\Broker;
use App\Helper\DataGenerator\Api\Entity\BrokerGenerator as BrokerDataGenerator;
use Doctrine\ORM\ORMException;
use App\Tests\Functional\Api\BaseApiTest;

/**
 * Class BrokerTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class BrokerTest extends BaseApiTest
{
    /**
     * BrokerTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/broker/';
        parent::$entityClassName = Broker::class;
        parent::$entityNameSnakeCase = 'broker';
        $brokerDataGenerator = parent::$container->get(BrokerDataGenerator::class);
        self::$startData[Broker::class] = $brokerDataGenerator->generateData();
        self::$updateData[Broker::class] = $brokerDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления брокера... ');
        $roles = ['ROLE_ENTITY_API_BROKER_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения одного брокера... ');
        $roles = ['ROLE_ENTITY_API_BROKER_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления брокера... ');
        $roles = ['ROLE_ENTITY_API_BROKER_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка брокеров... ');
        $roles = ['ROLE_ENTITY_API_BROKER_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления брокера... ');
        $roles = ['ROLE_ENTITY_API_BROKER_DELETE', 'ROLE_ENTITY_API_BROKER_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества брокеров... ');
        $roles = ['ROLE_ENTITY_API_BROKER_TOTAL'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

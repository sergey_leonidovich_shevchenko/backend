<?php declare(strict_types=1);

namespace App\Tests\Functional\Api\V1\Controller;

use App\Entity\Api\UserGroup;
use App\Helper\DataGenerator\Api\Entity\UserGroupGenerator as UserGroupDataGenerator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Tests\Functional\Api\BaseApiTest;
use Exception;

/**
 * Class UserGroupTest
 * @package App\Tests\Functional\Api\V1\Controller
 */
final class UserGroupTest extends BaseApiTest
{
    /**
     * UserGroupTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/api/v1/user_group/';
        parent::$entityNameSnakeCase = 'user_group';
        parent::$entityClassName = UserGroup::class;
        $userGroupDataGenerator = parent::$container->get(UserGroupDataGenerator::class);
        self::$startData[UserGroup::class] = $userGroupDataGenerator->generateData();
        self::$updateData[UserGroup::class] = $userGroupDataGenerator->generateData();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function testAPI(): void
    {
        self::fwriteColored('Тестирование добавления пользовательской группы... ');
        $roles = ['ROLE_ENTITY_API_USER_GROUP_ADD'];
        $this->initAuth('test', 'test-password', $roles);
        $this->addBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения пользовательской группы... ');
        $roles = ['ROLE_ENTITY_API_USER_GROUP_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->getBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование обновления пользовательской группы... ');
        $roles = ['ROLE_ENTITY_API_USER_GROUP_UPDATE'];
        $this->initAuth('test', 'test-password', $roles);
        $this->updateBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения списка пользовательских групп... ');
        $roles = ['ROLE_ENTITY_API_USER_GROUP_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->listBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование удаления пользовательской группы... ');
        $roles = ['ROLE_ENTITY_API_USER_GROUP_DELETE', 'ROLE_ENTITY_API_USER_GROUP_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->deleteBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);

        self::fwriteColored('Тестирование получения общего количества пользовательских групп... ');
        $roles = ['ROLE_ENTITY_API_USER_GROUP_TOTAL', 'ROLE_ENTITY_API_USER_GROUP_GET'];
        $this->initAuth('test', 'test-password', $roles);
        $this->countBase();
        self::fwriteColored('Успешно!' . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

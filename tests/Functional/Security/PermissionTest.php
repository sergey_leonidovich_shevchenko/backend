<?php declare(strict_types=1);

namespace App\Tests\Functional\Security;

use App\Tests\Functional\BaseFunctionalTest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Exception;

/**
 * Class PermissionTest
 * @package App\Tests\Functional\Security
 */
class PermissionTest extends BaseFunctionalTest
{
    /**
     * AccountTest constructor.
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        parent::$urn = '/security/permission';
    }

    /**
     * Тестирование добавления списка ролей пользователю
     * маршрут: PUT /security/permission/user/{userIdOrName}/role
     * @throws Exception
     */
    public function testAddRolesToUser(): void
    {
        self::fwriteColored("\tТестирование добавления списка ролей пользователю... ");

        $user = $this->initAuth('test', 'test-password', [
            'ROLE_SECURITY_PERMISSION_ADD_ROLES_TO_USER',
        ]);

        // Добавление ролей пользователю
        $rolesToAdd = [
            'ROLE_ENTITY_API_ACCOUNT_GET',
            'ROLE_ENTITY_API_ACCOUNT_UPDATE',
            'ROLE_ENTITY_API_BROKER_GET',
            'ROLE_ENTITY_API_BROKER_UPDATE',
            // Эта роль нужна, что бы получить пользователя с сервера, для проверки корректности установки ролей
            'ROLE_ENTITY_API_USER_GET',
            // Подсовываем уже существующую роль, система должна это понять и проигнорировать эту роль при добавлении
            'ROLE_SECURITY_PERMISSION_ADD_ROLES_TO_USER',
        ];
        $urn = sprintf('%s/user/%d/role', self::$urn, $user->getId());
        $response = $this->request(Request::METHOD_PUT, $urn, ['roles' => $rolesToAdd]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем добавились ли наши роли
        $urn = sprintf('/api/user/%d', $user->getId());
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 5, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());
        sort($rolesToAdd) && sort($responseContent['entity']['roles']);
        foreach ($rolesToAdd as $roleToAdd) {
            self::assertContains($roleToAdd, $responseContent['entity']['roles'], $response->getContent());
        }

        // Проверяем работоспособность ролей
        $errMsg = 'Для тестирования добавления списка ролей пользователю, вам необходимо иметь хотя-бы один счет в БД.';
        $account = $this->getFirstEntityFromDB('account', $errMsg);
        $this->checkWorkingRolesForUpdate('account', $account, $account);
        $this->checkNotWorkingRolesForAdd('account', $account);
        $this->checkNotWorkingRolesForDelete('account', $account);

        $errMsg = 'Для тестирования добавления списка ролей пользователю, вам необходимо иметь хотя-бы одного брокера в БД.';
        $broker = $this->getFirstEntityFromDB('broker', $errMsg);
        $this->checkWorkingRolesForUpdate('broker', $broker, $broker);
        $this->checkNotWorkingRolesForAdd('broker', $broker);
        $this->checkNotWorkingRolesForDelete('broker', $broker);

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование установки нового списка ролей пользователю
     * маршрут: POST /security/permission/user/{userIdOrName}/role
     * @depends testAddRolesToUser
     * @throws Exception
     */
    public function testSetRolesToUser(): void
    {
        self::fwriteColored("\tТестирование установки нового списка ролей пользователю... ");

        $user = $this->initAuth('test', 'test-password', [
            'ROLE_SECURITY_PERMISSION_SET_ROLES_TO_USER',
        ]);

        // Установка новых ролей пользователю
        $rolesToSet = [
            'ROLE_ENTITY_API_ACCOUNT_DELETE',
            'ROLE_ENTITY_API_BROKER_DELETE',
            // Эта роль нужна, что бы получить пользователя с сервера, для проверки корректности установки ролей
            'ROLE_ENTITY_API_USER_GET',
            'ROLE_ENTITY_API_ACCOUNT_GET',
            'ROLE_ENTITY_API_BROKER_GET',
            // Подсовываем уже существующую роль, система должна это понять и проигнорировать эту роль при добавлении
            'ROLE_SECURITY_PERMISSION_SET_ROLES_TO_USER',
        ];
        $urn = sprintf('%s/user/%d/role', self::$urn, $user->getId());
        $response = $this->request(Request::METHOD_POST, $urn, ['roles' => $rolesToSet]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем добавились ли наши роли
        $urn = sprintf('/api/user/%d', $user->getId());
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 5, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());

        sort($rolesToSet) && sort($responseContent['entity']['roles']);
        foreach ($rolesToSet as $roleToSet) {
            self::assertContains($roleToSet, $responseContent['entity']['roles'], $roleToSet);
        }

        // Проверяем работоспособность ролей
        $errMsg = 'Для тестирования добавления списка ролей пользователю, вам необходимо иметь хотя-бы один счет в БД.';
        $account = $this->getFirstEntityFromDB('account', $errMsg);
        $this->checkNotWorkingRolesForAdd('account', $account);
        $this->checkNotWorkingRolesForUpdate('account');
        $this->checkWorkingRolesForDelete('account', $account);

        $errMsg = 'Для тестирования добавления списка ролей пользователю, вам необходимо иметь хотя-бы одного брокера в БД.';
        $broker = $this->getFirstEntityFromDB('broker', $errMsg);
        $this->checkNotWorkingRolesForAdd('broker', $broker);
        $this->checkNotWorkingRolesForUpdate('broker');
        $this->checkWorkingRolesForDelete('broker', $broker);

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование удаления списка ролей у пользователя
     * маршрут: DELETE /security/permission/user/{userIdOrName}/role
     * @depends testSetRolesToUser
     * @throws Exception
     */
    public function testDeleteRolesFromUser(): void
    {
        self::fwriteColored("\tТестирование удаления списка ролей у пользователя... ");

        $user = $this->initAuth('test', 'test-password', [
            'ROLE_ENTITY_API_ACCOUNT_DELETE',
            'ROLE_ENTITY_API_BROKER_DELETE',
            'ROLE_SECURITY_PERMISSION_DELETE_ROLES_FROM_USER',
            // Эта роль нужна, что бы получить пользователя с сервера, для проверки корректности установки ролей
            'ROLE_ENTITY_API_USER_GET',
        ]);

        // Удаление ролей у пользователя
        $rolesForDeleting = [
            'ROLE_ENTITY_API_ACCOUNT_GET',
            'ROLE_ENTITY_API_ACCOUNT_DELETE',
            'ROLE_ENTITY_API_BROKER_GET',
            'ROLE_ENTITY_API_BROKER_DELETE',
        ];
        $urn = sprintf('%s/user/%d/role', self::$urn, $user->getId());
        $response = $this->request(Request::METHOD_DELETE, $urn, ['roles' => $rolesForDeleting]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем добавились ли наши роли
        $urn = sprintf('/api/user/%d', $user->getId());
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 5, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());
        self::assertNotContains($rolesForDeleting, $responseContent['entity']['roles'], $response->getContent());

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование добавления списка ролей пользовательской группе
     * маршрут: PUT /security/permission/user_group/{userGroupIdOrName}/role
     * @depends testDeleteRolesFromUser
     * @throws Exception
     */
    public function testAddRolesToUserGroup(): void
    {
        self::fwriteColored("\tТестирование добавления списка ролей пользовательской группе... ");

        $this->initAuth('test', 'test-password', [
            // Эта роль нужна, что бы получить пользовательскую группу с сервера, для проверки корректности установки ролей
            'ROLE_ENTITY_API_USER_GROUP_GET',
            'ROLE_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP',
            'ROLE_ENTITY_API_BROKER_GET',
        ]);

        // Добавление ролей пользовательской группе
        $rolesToAdd = [
            'ROLE_ENTITY_API_ACCOUNT_CRUD',
            'ROLE_ENTITY_API_BROKER_GET',
            // Подсовываем уже существующую роль, система должна это понять и проигнорировать эту роль при добавлении
            'ROLE_SECURITY_PERMISSION_ADD_ROLES_TO_USER_GROUP',
        ];
        $urn = sprintf('%s/user_group/%s/role', self::$urn, 'GROUP_TEST');
        $response = $this->request(Request::METHOD_PUT, $urn, ['roles' => $rolesToAdd]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем добавились ли наши роли
        $urn = sprintf('/api/user_group/%s', 'GROUP_TEST');
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 4, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());
        sort($rolesToAdd) && sort($responseContent['entity']['roles']);
        foreach ($rolesToAdd as $roleToAdd) {
            self::assertContains($roleToAdd, $responseContent['entity']['roles'], $response->getContent());
        }

        // Проверяем работоспособность ролей
        $errMsg = 'Для тестирования добавления списка ролей пользователю, вам необходимо иметь хотя-бы одного брокера в БД.';
        $broker = $this->getFirstEntityFromDB('broker', $errMsg);
        $this->checkNotWorkingRolesForAdd('broker', $broker);
        $this->checkNotWorkingRolesForUpdate('broker');
        $this->checkNotWorkingRolesForDelete('broker', $broker);

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование установки нового списка ролей пользовательской группе
     * маршрут: POST /security/permission/user_group/{userGroupIdOrName}/role
     * @depends testAddRolesToUserGroup
     * @throws Exception
     */
    public function testSetRolesToUserGroup(): void
    {
        self::fwriteColored("\tТестирование установки нового списка ролей пользовательской группе... ");

        $this->initAuth('test', 'test-password', [
            'ROLE_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP',
            // Эта роль нужна, что бы получить пользовательскую группу с сервера, для проверки корректности установки ролей
            'ROLE_ENTITY_API_USER_GROUP_GET',
        ]);

        // Установка новых ролей пользователю
        $rolesToSet = [
            'ROLE_ENTITY_API_BROKER_CRUD',
            // Подсовываем уже существующую роль, система должна это понять и проигнорировать эту роль при добавлении
            'ROLE_SECURITY_PERMISSION_SET_ROLES_TO_USER_GROUP',
        ];
        $urn = sprintf('%s/user_group/%s/role', self::$urn, 'GROUP_TEST');
        $response = $this->request(Request::METHOD_POST, $urn, ['roles' => $rolesToSet]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем добавились ли наши роли
        $urn = sprintf('/api/user_group/%s', 'GROUP_TEST');
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 4, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());

        sort($rolesToSet) && sort($responseContent['entity']['roles']);
        foreach ($rolesToSet as $roleToSet) {
            self::assertContains($roleToSet, $responseContent['entity']['roles'], $response->getContent());
        }

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование удаление ролей у пользовательской группы
     * маршрут: DELETE /security/permission/user_group/{userGroupIdOrName}/role
     * @depends testSetRolesToUserGroup
     * @throws Exception
     */
    public function testDeleteRolesFromUserGroup(): void
    {
        self::fwriteColored("\tТестирование удаления списка ролей у пользовательской группы... ");

        $this->initAuth('test', 'test-password', [
            'ROLE_SECURITY_PERMISSION_DELETE_ROLES_FROM_USER_GROUP',
            // Эта роль нужна, что бы получить пользовательскую группу с сервера, для проверки корректности установки ролей
            'ROLE_ENTITY_API_USER_GROUP_GET',
            'ROLE_ENTITY_API_ACCOUNT_GET',
            'ROLE_ENTITY_API_BROKER_GET',
        ]);

        // Удаление ролей у пользователя
        $rolesForDeleting = [
            'ROLE_ENTITY_API_ACCOUNT_GET',
            'ROLE_ENTITY_API_BROKER_GET',
            // Эта роль нужна, для проверки удаления не существующей роли у пользовательской группы (но существующая в системе),
            // система должна это понять и проигнорировать удаление роли, которой у пользовательской группы нет
            'ROLE_ENTITY_API_BROKER_DELETE',
        ];
        $urn = sprintf('%s/user_group/%s/role', self::$urn, 'GROUP_TEST');
        $response = $this->request(Request::METHOD_DELETE, $urn, ['roles' => $rolesForDeleting]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем добавились ли наши роли
        $urn = sprintf('/api/user_group/%s', 'GROUP_TEST');
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 4, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());
        self::assertNotContains($rolesForDeleting, $responseContent['entity']['roles'], $response->getContent());

        // Проверяем работоспособность ролей
        $errMsg = 'Для тестирования добавления списка ролей пользователю, вам необходимо иметь хотя-бы один счет в БД.';
        $account = $this->getFirstEntityFromDB('account', $errMsg);
        $this->checkNotWorkingRolesForAdd('account', $account);
        $this->checkNotWorkingRolesForUpdate('account');
        $this->checkNotWorkingRolesForDelete('account', $account);

        $errMsg = 'Для тестирования добавления списка ролей пользователю, вам необходимо иметь хотя-бы одного брокера в БД.';
        $broker = $this->getFirstEntityFromDB('broker', $errMsg);
        $this->checkNotWorkingRolesForAdd('broker', $broker);
        $this->checkNotWorkingRolesForUpdate('broker');
        $this->checkNotWorkingRolesForDelete('broker', $broker);

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование добавления пользовательских групп пользователю
     * маршрут: PUT /security/permission/user/{userIdOrName}/user_group
     * @depends testDeleteRolesFromUserGroup
     * @throws Exception
     */
    public function testAddUserGroupToUser(): void
    {
        self::fwriteColored("\tТестирование добавления списка групп пользователю... ");

        $user = $this->initAuth('test', 'test-password', [
            'ROLE_SECURITY_PERMISSION_ADD_USER_GROUP_TO_USER',
            // Эта роль нужна, что бы получить пользователя с сервера, для проверки корректности установки ролей
            'ROLE_ENTITY_API_USER_GET',
        ]);

        // Добавление групп ролей пользователю
        $groupsToAdd = [
            'GROUP_TEST',
            // Эта несуществующая в системе группа, система должна это понять и проигнорировать эту группу при добавлении
            'UNDEFINED_GROUP',
        ];
        $urn = sprintf('%s/user/%d/user_group', self::$urn, $user->getId());
        $response = $this->request(Request::METHOD_PUT, $urn, ['groups' => $groupsToAdd]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем добавились ли наши роли
        $urn = sprintf('/api/user/%d', $user->getId());
        $response = $this->request(Request::METHOD_GET, $urn);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 6, JSON_THROW_ON_ERROR);
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());

        if (($key = array_search('UNDEFINED_GROUP', $groupsToAdd, true)) !== false) {
            unset($groupsToAdd[$key]);
        }

        self::assertArrayHasKey('groups', $responseContent['entity']);
        sort($groupsToAdd) && sort($responseContent['entity']['groups']);
        foreach ($groupsToAdd as $groupToAdd) {
            self::assertContains($groupToAdd, $responseContent['entity']['groups']);
        }

        // TODO: проверить работоспособность групп

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование установки нового списка пользовательских групп у пользователя
     * маршрут: POST /security/permission/user/{userIdOrName}/user_group
     * @depends testAddUserGroupToUser
     * @throws Exception
     */
    public function testSetUserGroupToUser(): void
    {
        self::fwriteColored("\tТестирование установки нового списка групп пользователю... ");

        $user = $this->initAuth('test', 'test-password', [
            'ROLE_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER',
            // Эта роль нужна, что бы получить пользователя с сервера, для проверки корректности установки ролей
            'ROLE_ENTITY_API_USER_GET',
        ]);

        // Установка новых групп ролей пользователю
        $groupsToSet = [
            'GROUP_TEST',
            // Эта несуществующая в системе группа, система должна это понять и проигнорировать эту группу при добавлении
            'UNDEFINED_GROUP',
        ];
        $urn = sprintf('%s/user/%d/user_group', self::$urn, $user->getId());
        $response = $this->request(Request::METHOD_POST, $urn, ['groups' => $groupsToSet]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем добавились ли наши группы
        $urn = sprintf('/api/user/%d', $user->getId());
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 6, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());

        if (($key = array_search('UNDEFINED_GROUP', $groupsToSet, true)) !== false) {
            unset($groupsToSet[$key]);
        }

        $userGroups = [];
        foreach ($responseContent['entity']['groups'] as $group) {
            $userGroups[] = $group;
        }

        sort($groupsToSet) && sort($userGroups);
        self::assertEquals($groupsToSet, $userGroups, $response->getContent());

        // TODO: проверить работоспособность групп ролей

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }

    /**
     * Тестирование удаления пользовательских групп у пользователя
     * маршрут: DELETE /security/permission/user/{userIdOrName}/user_group
     * @ depends testSetUserGroupToUser
     * @throws Exception
     */
    public function testDeleteUserGroupFromUser(): void
    {
        self::fwriteColored("\tТестирование удаления списка групп у пользователя... ");

        $user = $this->initAuth('test', 'test-password', [
            'ROLE_SECURITY_PERMISSION_DELETE_USER_GROUP_FROM_USER',
            'ROLE_SECURITY_PERMISSION_SET_USER_GROUP_TO_USER',
            'ROLE_ENTITY_API_USER_GET',
            'ROLE_ENTITY_API_USER_GROUP_GET',
        ]);

        // Установка новых групп ролей пользователю
        $groupsToSet = [
            'GROUP_TEST',
            // Эта несуществующая в системе группа, система должна это понять и проигнорировать эту группу при добавлении
            'UNDEFINED_GROUP',
        ];
        $urn = sprintf('%s/user/%d/user_group', self::$urn, $user->getId());
        $response = $this->request(Request::METHOD_POST, $urn, ['groups' => $groupsToSet]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Удаление групп ролей у пользователя
        $groupsForDeleting = [
            'GROUP_TEST',
            // Эта несуществующая в системе группа, система должна это понять и проигнорировать эту группу при удалении
            'UNDEFINED_GROUP',
        ];
        $urn = sprintf('%s/user/%s/user_group', self::$urn, 'test');
        $response = $this->request(Request::METHOD_DELETE, $urn, ['groups' => $groupsForDeleting]);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        $responseContent = json_decode($response->getContent(), true, 2, JSON_THROW_ON_ERROR);
        self::assertEquals($responseContent, ['status' => 'success'], $response->getContent());

        // Проверяем удалились ли наши группы
        $urn = sprintf('/api/user/%d', $user->getId());
        $response = $this->request(Request::METHOD_GET, $urn);
        $responseContent = json_decode($response->getContent(), true, 6, JSON_THROW_ON_ERROR);
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), $response->getContent());
        self::assertArrayHasKey('status', $responseContent, $response->getContent());
        self::assertEquals('success', $responseContent['status'], $response->getContent());
        self::assertArrayHasKey('entity', $responseContent, $response->getContent());

        if (($key = array_search('UNDEFINED_GROUP', $groupsToSet, true)) !== false) {
            unset($groupsToSet[$key]);
        }

        $userGroups = [];
        foreach ($responseContent['entity']['groups'] as $group) {
            $userGroups[] = $group;
        }

        sort($groupsToSet) && sort($userGroups);
        self::assertNotContains($groupsToSet, $userGroups, $response->getContent());

        // TODO: проверить работоспособность групп ролей

        self::fwriteColored("\tУспешно!" . PHP_EOL, self::TERMINAL_TEXT_COLOR_GREEN);
    }
}

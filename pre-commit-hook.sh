#!/bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Данный файл требуется для PhpStorm плагина (Pre Commit Hook Plugin),  #
#         Данный плагин перед каждым коммитом в этом проекте,           #
#                      будет запускать этот файл                        #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# # # # # # # # # # # # # # #
# Инициализация переменных  #
# # # # # # # # # # # # # # #
PROJECT_DIR=$PWD
RESULT=$? # содержит результат выполнения самой последней команды/функции

# Стиль текста #
COLOR_OFF='\033[0m'   # Text Reset
BOLD='\033[1;0m'      # Bold text
UNDERLINE='\033[4;0m' # Underline text
# Background
BG_GREEN='\033[1;42m'
BG_RED='\033[1;41m'
BG_YELLOW='\033[1;43m'
BG_BLUE='\033[44m'
BG_PURPLE='\033[1;45m'
BG_CYAN='\033[1;46m'
BG_WHITE='\033[1;47m'
# Regular Colors
COLOR_BLACK='\033[0;30m'
COLOR_RED='\033[0;31m'
COLOR_GREEN='\033[0;32m'
COLOR_YELLOW='\033[0;33m'
COLOR_BLUE='\033[0;34m'
COLOR_PURPLE='\033[0;35m'
COLOR_CYAN='\033[0;36m'
COLOR_WHITE='\033[0;37m'

# # # # # # # # # # # # #
# Инициализация функций #
# # # # # # # # # # # # #
sendMessage() {
    case $1 in
        error)
            textColor=${COLOR_RED}
            bgColor=${BG_RED}
            ;;
        info)
            textColor=${COLOR_CYAN}
            bgColor=${BG_CYAN}
            ;;
        success)
            textColor=${COLOR_GREEN}
            bgColor=${BG_GREEN}
            ;;
        warning)
            textColor=${COLOR_YELLOW}
            bgColor=${BG_YELLOW}
            ;;
        *)
            textColor=${COLOR_OFF}
            bgColor=${COLOR_OFF}
            ;;
    esac

    printf "\n${bgColor}==========>${COLOR_OFF} ${textColor}$2${COLOR_OFF} ${bgColor}<==========${COLOR_OFF}\n"
}


# # # # # # # # # # # # # # # # #
# Запуск функциональных тестов  #
# # # # # # # # # # # # # # # # #
sendMessage "info" "Запуск функциональных тестов..."
export SYMFONY_DEPRECATIONS_HELPER=weak
if php "${PROJECT_DIR}/vendor/phpunit/phpunit/phpunit" \
    --bootstrap "${PROJECT_DIR}/config/bootstrap.php"    \
    --configuration "${PROJECT_DIR}/phpunit.xml"         \
    --stderr                                             \
    --stop-on-fail                                       \
    "${PROJECT_DIR}/tests/Functional"; then
    sendMessage "success" "Функциональные тесты успешно пройдены!"
else
    sendMessage "error" "Функциональные тесты не пройдены!"
    exit 1
fi

exit 0

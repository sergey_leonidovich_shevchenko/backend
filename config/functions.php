<?php declare(strict_types=1);

/**
 * @param string $needle     Искомый элемент
 * @param array  $haystack   Массив в котором ищем
 * @param string $currentKey Текущий итерируемый ключ массива
 * @return bool|string
 */
function array_search_recursive(string $needle, array $haystack, string $currentKey = '')
{
    foreach ($haystack as $key => $value) {
        if ($key === $needle) {
            return true;
        }

        if (is_array($value)) {
            $nextKey = array_search_recursive($needle, $value, $currentKey . '[' . $key . ']');
            if ($nextKey) {
                return $nextKey;
            }
        } elseif ($value === $needle) {
            return is_numeric($key) ? $currentKey . '[' . $key . ']' : $currentKey . '["' . $key . '"]';
        }
    }
    return false;
}

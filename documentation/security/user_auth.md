JWT Token авторизация/аутентификация.
========
### На некоторые маршруты не возможно попасть без аутентификации/авторизации.
Для того чтобы авторизоваться к определенному маршруту, нужно получить токен авторизации
- Пример получения токена авторизации:
```shell script
curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/security/login -H "Content-Type: application/json" -d '{
    "username": "admin",
    "password": "admin-password"
}'
```
Если авторизация удачна, то отдают Вам Ваш токен авторизации:
```
Status: 200 OK
Body:
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NzEyNDUxMDgsImV4cCI6MTU3MTI0ODcwOCwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoidGVzdCJ9.U7GFYCBLgAAIMV_Zv9D_GAqAdVUn2cgx0tCMyJU4TW8eWDSU2Rl62PA7NNchrJ7Wq9aUSC7seDX4In1vmFHladiGJiZZh8ZYPpT6iB6w_zqiOObHzD-HLLAOdZ95odVxFogKxZ-01O5l-Hef7593ZTxXiiEL6nVg3y7Pj0yWp93FHGSAqWB2kyoh-I-b0ic8BEKWm9ciYAWyEAlAca6jECWioQaUmxgseeoreXOjE9gM0hTjfCgcjEl5NaE0V0IkJVWPc998t704WQYABHY-wG9vezfZiFXaUqxsfiEBGhqPQc74sGh5JEBTVaHqD8EWFX_2uIDHz4IV_nfg1dQ8DccCTw_EP4DVjkbi5THL_r4VvjCvPN1ux77DlLMhI3OVtOR1TFEmIUZvcMO1udRIVgIg835OvsKzKicWLWNrXDIH6rdzUeEzoVxRw31VS3QdLc6UinM4fSaRxjidZERwkkhRZIiX95NKzus2T3FvsNl4ltj-ctRgCyMwTvsTUDGpUiVianp43HoEOdoiqwJ8kb9A2Y1QnckyosAWgz3QdSBwQ6qpzHffTnd6A-DVtI-mkpbDGjTNEXzfznLjhEi93Lwo9LQfrqFTX5LDbJfpTHb90edxyg4awLnWh5mvdcwc_In3DqiZ58xQWYKq4n-cE2e6mwv9bUZMXAOXDYG91b8"
}
```

Если авторизация неудачна:
```
Status: 401 Unauthorized
Body:
{
    "code": 401,
    "message": "<message>"
}
```
Где <message>:
- Если авторизация неудачна ==> `Bad credentials.`
- Если JWT-токен - "протух" ==> `Expired JWT Token.` 
- Если JWT-токен "битый" или поддельный ==> `Invalid JWT Token.`
- Если JWT-токен пустой ==> `JWT Token not found.`

После того как токен авторизации получен, следуем на маршрут требующую авторизацию:
```
curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/broker/777 -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}"
```
Response:
```
Status: 200 OK
Body
[
    {
        "id": 777,
        "name": "name",
        "setting": {
            "random_text_1": "consequatur et delectus adipisci",
            "random_text_2": "neque eum est illo saepe illo",
            "random_text_3": "velit mollitia fugiat quia est"
        },
        "consulting_company_list": []
    }
]
```


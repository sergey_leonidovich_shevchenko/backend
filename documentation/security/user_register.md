Регистрация нового пользователя
===============================

```shell script
curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/security/register/ -H "Content-Type: application/json" -d '{
    "email": "user@email.ru",
    "password": "password"
}'
```

Response Good:
```
Status: 201 Created
Body:
{
    "status": "success",
    "entity": {
        "id": 81,
        "username": "user1@password1.ru",
        "username_canonical": "user1@password1.ru",
        "email": "user1@password1.ru",
        "email_canonical": "user1@password1.ru",
        "enabled": false,
        "password": "$2y$13$vdlmJAbmjltLgRZ6yC01Qe3V8W59Cg/Ebk3lQ3vqe3j0rKoN3ltUq",
        "roles": []
    }
}
```

**Рассылка Email (Mailer)** - Рассылка сообщений в системе
==========================

#### Рассылка всех писем в системе

В `development` среде можно отлавливать все исходящие из системы email сообщения.
Для этого в Вашем браузере перейдите по адресу `http://127.0.0.1:<IBS_BACKEND_MAILER_WEB_UI_PORT>/`, 
где `IBS_BACKEND_MAILER_WEB_UI_PORT` - порт на котором работает программа Mailhog (стандартный порт - `8025`).


Также во время попытки отсылки сообщения происходят события описанные в классе `App\EventDispatcher\Event\MailerEvent`:
1. `event.email.send` - Событие возникающее при передачи email на отправку
2. `event.email.send.exception` - Событие возникающее во время ошибки при передачи email на отправку

Так же во время исполнения действия после события логируются необходимые данные логгерами:
1. `monolog.logger.mailer` - При успешной отправки email логирует в файл `%kernel.logs_dir%/%kernel.environment%/mailer.log`
2. `monolog.logger.mailer_exception` - При не успешной отправки email логирует в файл `%kernel.logs_dir%/%kernel.environment%/mailer_exception.log` 
 
---


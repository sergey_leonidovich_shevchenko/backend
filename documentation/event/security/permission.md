**События с безопасностью системы** *(Security Event)*
=====================================================

#### Логгеры:
- `monolog.logger.security.permission`
- `monolog.logger.security.permission_exception`

---

### Успешные события
**Логгер:** `monolog.logger.security.permission` 
**Файл:** `./var/log/%env%/security/permission.log`

- Событие возникающее при добавлении списка ролей пользователю `event.security.permission.add.roles_to_user`
- Событие возникающее при добавлении списка ролей пользовательской группе `event.security.permission.add.roles_to_user_group`
- Событие возникающее при добавлении списка групп ролей пользователю `event.permission.add.user_group_to_user`
- Событие возникающее при удалении списка ролей у пользователя `event.security.permission.delete.role_from_user`
- Событие возникающее при удалении списка ролей у пользовательской группы `event.security.permission.delete.role_from_user_group`
- Событие возникающее при удалении списка групп ролей у пользователя `event.permission.delete.user_group_to_user`
- Событие возникающее при установки нового списка ролей пользователю `event.security.permission.set.roles_to_user`
- Событие возникающее при установки нового списка ролей пользовательской группе `event.security.permission.set.roles_to_user_group`
- Событие возникающее при установке нового списка групп ролей пользователю `event.permission.set.user_group_to_user`

Пример:
```json
{
   "message":"Запрос на установку нового списка ролей пользователю",
   "context":{
      "request_id":"000000005774bcbe000000007cc11d8c",
      "event_name":"event.security.permission.set.roles_to_user",
      "old_roles":[
         "ROLE_ENTITY_API_BROKER_ADD",
         "ROLE_ENTITY_API_BROKER_GET",
         "ROLE_ENTITY_API_BROKER_DELETE"
      ],
      "new_roles":[
         "ROLE_ENTITY_API_BROKER_UPDATE"
      ],
      "identity":{
         "user_id":2,
         "is_auth":true
      },
      "entity_info":{
         "entity_name":"App\\Entity\\Api\\User",
         "id":3
      }
   },
   "level":200,
   "level_name":"INFO",
   "channel":"security_permission",
   "datetime":{
      "date":"2020-02-19 19:42:41.433485",
      "timezone_type":3,
      "timezone":"UTC"
   },
   "extra":[

   ]
}
```

---
    
### Не успешные события 
**Логгер:** `monolog.logger.security.permission_exception` 
**Файл:** `./var/log/%env%/security/permission_exception.log`

- Событие возникающее во время ошибки при добавлении списка ролей пользователю `event.security.permission.add.roles_to_user.exception`
- Событие возникающее во время ошибки при добавлении списка ролей пользовательской группе `event.security.permission.add.roles_to_user_group.exception`
- Событие возникающее во время ошибки при добавлении списка групп ролей пользователю `event.permission.add.user_group_to_user.exception`
- Событие возникающее во время ошибки при удалении списка ролей у пользователя `event.security.permission.delete.role_from_user.exception`
- Событие возникающее во время ошибки при удалении списка ролей у пользовательской группы `event.security.permission.delete.role_from_user_group.exception`
- Событие возникающее во время ошибки при удалении списка групп ролей у пользователя `event.permission.delete.user_group_to_user.exception`
- Событие возникающее во время ошибки при установки нового списка ролей пользователю `event.security.permission.set.roles_to_user.exception`
- Событие возникающее во время ошибки при установки нового списка ролей пользовательской группе `event.security.permission.set.roles_to_user_group.exception`
- Событие возникающее во время ошибки при установке нового списка групп ролей пользователю `event.permission.set.user_group_to_user.exception`

Пример:
```json
{
   "message":"Ошибка при запросе на добавление списка ролей пользователю",
   "context":{
      "request_id":"0000000054b2c3b3000000003d7bd95d",
      "event_name":"event.security.permission.add.roles_to_user.exception",
      "message":"Указанной роли \"ROLE_BROKER_ADD\" нет в системе!",
      "identity":{
         "user_id":2,
         "is_auth":true
      }
   },
   "level":400,
   "level_name":"ERROR",
   "channel":"security_permission_exception",
   "datetime":{
      "date":"2020-02-19 19:39:16.202099",
      "timezone_type":3,
      "timezone":"UTC"
   },
   "extra":[

   ]
}
```
**Системные события в системе** *(Kernel Events)*
===================================================

#### Логгеры:
- `monolog.logger.kernel_exception`
- `monolog.logger.kernel_request`
- `monolog.logger.kernel_response`

---

### Kernel request

**Логгер:** `monolog.logger.kernel_request`

**Файл:** `./var/log/%env%/kernel/request.log`

**Listener:** `App\EventDispatcher\Listener\Kernel\RequestListener::onKernelRequest()`

- События возникающее во время запроса пользователя `kernel.request`

Пример одной из всевозможных запросов:
```json
{
   "message":"Входящий запрос",
   "context":{
      "request_id":"0000000043cc02c70000000048300df9",
      "event_name":"kernel.request",
      "request":{
         "method":"POST",
         "format":"json",
         "locale":"en",
         "default_locale":"en",
         "route":"route.api.account.add",
         "route_params":[

         ],
         "firewall_context":"security.firewall.map.context.api",
         "controller":"App\\Controller\\Api\\AccountController::addAction",
         "content":"{\n\t\"user\": 2,\n    \"trading_platform\": 1,\n    \"personal_area\": 1,\n    \"type_trading_account\": 1,\n    \"sales_department\": 1,\n    \"source_code\": \"crm\",\n    \"number\": 1,\n    \"leverage\": 100\n}",
         "media_type":"application/json",
         "uri":"http://192.168.220.4/api/account/?idd=15"
      },
      "identity":{
         "user_id":2,
         "is_auth":true
      }
   },
   "level":200,
   "level_name":"INFO",
   "channel":"request",
   "datetime":{
      "date":"2020-03-05 08:57:54.022083",
      "timezone_type":3,
      "timezone":"UTC"
   },
   "extra":[

   ]
}
```

---

### Kernel response
**Логгер:** `monolog.logger.kernel_response`

**Файл:** `./var/log/%env%/kernel/response.log`

**Listener:** `App\EventDispatcher\Listener\Kernel\ResponseListener::onKernelResponse()`

- События возникающее во время ответа пользователю `kernel.response`

Пример одной из всевозможных ответов:
```json
{
   "message":"Исходящий ответ от сервера",
   "context":{
      "request_id":"0000000005013d09000000002364aca4",
      "event_name":"kernel.response",
      "response":{
         "content":"{\"user\":{\"id\":2,\"username\":\"admin\",\"email\":\"admin@example.com\",\"roles\":[\"ROLE_ADMIN\"]},\"token\":\"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1ODMzOTc0NjUsImV4cCI6MTU4MzQ4Mzg2NSwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6ImFkbWluIn0.SobZz-BwmuUZBxV0eIcpCrWYCQk3fsdbz7CvEz56ap_ccgOyNCco8KZAP3sOaAYiaJIgSJxcIBTx_TzK7EcaQCtcR7WNFyoZcSYTZLN-STsmwEd606WF-2bqYUa4hU5c81Wis-oPg8JH6PRCDdFyrUbwksV0t5Sgig7oADGqx6onjdnD5moJ8_E3VcC5ldwbgwbX_P-guKpmse1LiA5uVjHgBSiGUWgwErpHUEmoondvOaUk3A_gP3lHHEyOfb7D6NfWz6nDowK7ZvMed5cxwCPakt3hTHwZoeppwHgcZeaMUdVJXxpuVfwwcpXRJafIF4DFNGwJjnL5-y9OajOIkLmo5M21tFGYfhG0loSUk4vwPC-9AX9epk-UwR2B7TnuZa5YLvrvULv6H21okg3F197sTxSXzMNdrtBahUC3HPSqP0FZc6-0FPknWSnuIyyE2mpffpjNbp4ustNuIDxq5OFXxZyyZJl10C2qAUi3RtyjrXIV9RBAY-_uzZz_mq8EKtj5aXsbcJjf9owA7KBGfWNsFXEmqUvxIgdYDXP1RQDweXY4x-cHztcPavJnHtEvS-lpg_w-lLUo2f44_JFLiEjYG7yLszknmfAZSx4-WOsOQcgzBbL6_Iln2iT8O2FNfZ5DVsi8HwR6UlMh4i4byLyeYzO-wQoFvSJm1382lH0\"}",
         "status_code":200,
         "headers":{
            "cache-control":[
               "no-cache, private"
            ],
            "date":[
               "Thu, 05 Mar 2020 08:37:45 GMT"
            ],
            "content-type":[
               "application/json"
            ]
         },
         "charset":null
      },
      "identity":{
         "user_id":2,
         "is_auth":true
      }
   },
   "level":200,
   "level_name":"INFO",
   "channel":"response",
   "datetime":{
      "date":"2020-03-05 08:37:45.919457",
      "timezone_type":3,
      "timezone":"UTC"
   },
   "extra":[

   ]
}
```

---

### Kernel exception
**Логгер:** `monolog.logger.kernel_exception`

**Файл:** `./var/log/%env%/kernel_exception.log`

**Listener:** `App\EventDispatcher\Listener\Kernel\ExceptionListener::onKernelException()`

- События возникающее во время любой ошибки в ядре системы `kernel.exception`

Пример одной из всевозможных ошибок:
```json
{
   "message":"No route found for \"GET /api/user-group/7\"",
   "context":{
      "message":"No route found for \"GET /api/user-group/7\"",
      "code":0,
      "file":"/var/www/ibs/backend/vendor/symfony/http-kernel/EventListener/RouterListener.php",
      "trace":[
         {
            "file":"/var/www/ibs/backend/vendor/symfony/event-dispatcher/Debug/WrappedListener.php",
            "line":126,
            "function":"onKernelRequest",
            "class":"Symfony\\Component\\HttpKernel\\EventListener\\RouterListener",
            "type":"->",
            "args":[
               {

               },
               "kernel.request",
               {

               }
            ]
         },
         {
            "file":"/var/www/ibs/backend/vendor/symfony/event-dispatcher/EventDispatcher.php",
            "line":260,
            "function":"__invoke",
            "class":"Symfony\\Component\\EventDispatcher\\Debug\\WrappedListener",
            "type":"->",
            "args":[
               {

               },
               "kernel.request",
               {

               }
            ]
         },
         {
            "file":"/var/www/ibs/backend/vendor/symfony/event-dispatcher/EventDispatcher.php",
            "line":235,
            "function":"doDispatch",
            "class":"Symfony\\Component\\EventDispatcher\\EventDispatcher",
            "type":"->"
         },
         {
            "file":"/var/www/ibs/backend/vendor/symfony/event-dispatcher/EventDispatcher.php",
            "line":73,
            "function":"callListeners",
            "class":"Symfony\\Component\\EventDispatcher\\EventDispatcher",
            "type":"->"
         },
         {
            "file":"/var/www/ibs/backend/vendor/symfony/event-dispatcher/Debug/TraceableEventDispatcher.php",
            "line":168,
            "function":"dispatch",
            "class":"Symfony\\Component\\EventDispatcher\\EventDispatcher",
            "type":"->",
            "args":[
               {

               },
               "kernel.request"
            ]
         },
         {
            "file":"/var/www/ibs/backend/vendor/symfony/http-kernel/HttpKernel.php",
            "line":127,
            "function":"dispatch",
            "class":"Symfony\\Component\\EventDispatcher\\Debug\\TraceableEventDispatcher",
            "type":"->",
            "args":[
               {

               },
               "kernel.request"
            ]
         },
         {
            "file":"/var/www/ibs/backend/vendor/symfony/http-kernel/HttpKernel.php",
            "line":68,
            "function":"handleRaw",
            "class":"Symfony\\Component\\HttpKernel\\HttpKernel",
            "type":"->",
            "args":[
               {
                  "attributes":{

                  },
                  "request":{

                  },
                  "query":{

                  },
                  "server":{

                  },
                  "files":{

                  },
                  "cookies":{

                  },
                  "headers":{

                  }
               },
               1
            ]
         },
         {
            "file":"/var/www/ibs/backend/vendor/symfony/http-kernel/Kernel.php",
            "line":198,
            "function":"handle",
            "class":"Symfony\\Component\\HttpKernel\\HttpKernel",
            "type":"->",
            "args":[
               {
                  "attributes":{

                  },
                  "request":{

                  },
                  "query":{

                  },
                  "server":{

                  },
                  "files":{

                  },
                  "cookies":{

                  },
                  "headers":{

                  }
               },
               1,
               true
            ]
         },
         {
            "file":"/var/www/ibs/backend/public/index.php",
            "line":27,
            "function":"handle",
            "class":"Symfony\\Component\\HttpKernel\\Kernel",
            "type":"->",
            "args":[
               {
                  "attributes":{

                  },
                  "request":{

                  },
                  "query":{

                  },
                  "server":{

                  },
                  "files":{

                  },
                  "cookies":{

                  },
                  "headers":{

                  }
               }
            ]
         }
      ],
      "request_id":"000000000c9b7a1d000000006c231b0f"
   },
   "level":400,
   "level_name":"ERROR",
   "channel":"kernel_exception",
   "datetime":{
      "date":"2020-02-25 21:51:06.401607",
      "timezone_type":3,
      "timezone":"UTC"
   },
   "extra":[

   ]
}
```

---

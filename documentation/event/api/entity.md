**События с сущностями** *(Api Entity Event)*
=============================================

#### Логгеры:
- `monolog.logger.api.entity`
- `monolog.logger.api.entity.exception`

---

### Успешные события (Logger `monolog.logger.api.entity`)
**Логгер:** `monolog.logger.api.entity` 
**Файл:** `./var/log/%env%/api/entity.log`

- События возникающее после создании сущности `event.api.entity.create`
- События возникающее после получении сущности `event.api.entity.get`
- События возникающее после получении списка сущностей `event.api.entity.list`
- События возникающее после удалении сущности `event.api.entity.delete`
- События возникающее после обновлении сущности `event.api.entity.update`
- События возникающее после получении количества сущностей `event.api.entity.total`

Пример:
    ```json
  {
      "message": "Запрос на создание сущности",
      "context": {
          "event_name": "api.entity.create",
          "identity": {
              "user_id": 120,
              "is_auth": true
          },
          "entityList": [
          {
              "entity_name": "App\\Entity\\Api\\User",
              "id": 625
          }
          ]
      },
      "level": 200,
      "level_name": "INFO",
      "channel": "api_entity",
      "datetime": {
          "date": "2019-10-29 13:22:20.591436",
          "timezone_type": 3,
          "timezone": "Europe/Moscow"
      },
      "extra": []
  }
    ```

---
    
### Не успешные события 
_Логгер:_ `monolog.logger.api.entity.exception` 
_Файл:_ `./var/log/%env%/api/entity_exception.log`

- События возникающее во время ошибки при оперировании с сущностью `EntityEvent::EVENT_API_ENTITY_EXCEPTION`

Пример:
```json

```
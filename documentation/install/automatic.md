**Автоматическое разворачивание проекта с помощью Docker**
==========================================================

#### Для автоматического разворачивания проекта нужно выполнить следующие шаги:

1) Зайти в корень текущего проекта
2) Задать следующие переменные окружения (значения можно задать свои):

    |       `Переменная окружения`                |               `Значение`               |                          `Назначение`                          |
    | :------------------------------------------ | :------------------------------------- | :------------------------------------------------------------- |
    | **IBS_BACKEND_APPLICATION_PATH**            | /var/www/ibs/backend                   | Путь к проекту внутри контейнера                               |
    | **IBS_BACKEND_ENV**                         | `development`, `production` или `test` | Среда развертывания                                            |
    | **IBS_BACKEND_APP_ENV**                     | `development`, `production` или `test` | Среда проекта                                                  |
    | **IBS_BACKEND_MYSQL_ROOT_PASSWORD**         | root-password                          | Пароль mysql-пользователя - root                               |
    | **IBS_BACKEND_MYSQL_DATABASE**              | ibs                                    | Название mysql-базы данных                                     |
    | **IBS_BACKEND_MYSQL_USER**                  | ibs                                    | Имя mysql-пользователя                                         |
    | **IBS_BACKEND_MYSQL_PASSWORD**              | ibs-password                           | Пароль mysql-пользователя                                      |
    | **IBS_BACKEND_MYSQL_HOST**                  | ibs-mysql                              | Хост машины с БД подключения                                   |
    | **IBS_BACKEND_MYSQL_HOST_PORT**             | 33060                                  | Порт хостовой машины для подключения к mysql                   |
    | **IBS_BACKEND_NETWORK_SUBNET**              | 192.168.220                            | IP подсети backend докера без 4-ого октета                     |
    | **IBS_FRONTEND_NETWORK**                    | http://192.168.221.2                   | IP подсети frontend докера                                     |
    | **IBS_BACKEND_NGINX_HOST_HTTP_PORT**        | 8081                                   | Порт хостовой машины для обращения к бэкенду через nginx       |
    | **IBS_BACKEND_NGINX_HOST_HTTPS_PORT**       | 4430                                   | Порт хостовой машины для обращения к бэкенду через nginx (SSL) |
    | **IBS_BACKEND_MAILER_DSN**                  | smtp://127.0.0.1:1025                  | DSN почтовой службы для отправки писем                         |
    | **IBS_BACKEND_MAILER_HOST**                 | 127.0.0.1                              | Хост почтовой службы                                           |
    | **IBS_BACKEND_MAILER_PORT**                 | 1025                                   | Порт почтовой службы                                           |
    | **IBS_BACKEND_MAILER_TRANSPORT**            | smtp                                   | Транспорт почтовой службы                                      |
    | **IBS_BACKEND_MAILER_USER**                 |                                        | Пользователь почтовой службы                                   |
    | **IBS_BACKEND_MAILER_PASSWORD**             |                                        | Пароль от почтовой службы                                      |
    | **IBS_BACKEND_MAILER_WEB_UI_PORT**          | 8025                                   | Порт от Web интерфейса почтовой службы                         |
    | **IBS_BACKEND_BLACKFIRE_PORT**              | 8707                                   | Порт на котором работает blackfire                             |
    | **IBS_BACKEND_BLACKFIRE_LOG_LEVEL**         | 4                                      | Уровень логирования blackfire                                  |
    | **IBS_BACKEND_BLACKFIRE_SERVER_ID**         | ~                                      | ID сервера                                                     |
    | **IBS_BACKEND_BLACKFIRE_SERVER_TOKEN**      | ~                                      | Токен сервера                                                  |
    | **IBS_BACKEND_BLACKFIRE_CLIENT_ID**         | ~                                      | ID клиента                                                     |
    | **IBS_BACKEND_BLACKFIRE_CLIENT_TOKEN**      | ~                                      | Токен клиента                                                  |
    | **IBS_BACKEND_GRPC_CERTIFICATE**            | ~                                      | GRPC сертификат для общения с внутренними системами            |
    
3) Запустить команду: `docker-compose up -d --build`
4) Пока устанавливается проект (~5 мин) пойти попить кофе =)
5) После нужно убедиться, что все прошло хорошо, а именно выполнив следующие команды:
    - `docker network ls --filter name=ibs` - мы должны увидеть одну нашу сеть

        |  NETWORK ID  |     NAME    | DRIVER | SCOPE |
        | :----------- | :---------- | :----- | :---- |
        | cd402f354d90 | ibs-network | bridge | local |

    - `docker volume ls --filter name=ibs` - мы должны увидеть один наш volume

        | DRIVER |  VOLUME NAME   |
        | :----- | :------------- |
        | local  | ibs-mysql-data |

    - `docker container ls --filter name=ibs` - мы должны увидеть три наших запущенных контейнера
    
        | CONTAINER ID |        IMAGE        |        COMMAND         |      CREATED      |      STATUS      | PORTS                                    |    NAMES    |
        | :----------- | :------------------ | :--------------------- | :---------------- | :--------------- | :--------------------------------------- | :---------- |
        | 1208105e8ed9 | backend_ibs-nginx   | "nginx -g 'daemon of…" | About an hour ago | Up About an hour | 0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp | ibs-nginx   |
        | f7799d2b476e | backend_ibs-php-fpm | "/entrypoint.sh php-…" | About an hour ago | Up About an hour | 9000/tcp                                 | ibs-php-fpm |
        | e403fdc8a0e1 | backend_ibs-mysql   | "docker-entrypoint.s…" | About an hour ago | Up About an hour | 33060/tcp, 0.0.0.0:33060->3306/tcp       | ibs-mysql   |

6) Настройка Xdebug в PhpStorm:
![Настройка Xdebug в PhpStorm](../settings/phpstorm-xdebug-setting.png)


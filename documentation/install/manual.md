**Ручное разворачивание проекта**
=================================

Для работы приложения вы должны установить следующие пакеты:
  - Apache 2.4
  - PHP 7.2 и выше
  - MySQL 8.0 (создайте пользователя с типом аутентификацией )
  - GRPC 1.25

Для работы приложения вы должны установить следующие расширения:
- **PHP**
    - *bcmath*
    - *ctype*
    - *curl*
    - *grpc*
    - *iconv*
    - *json*
    - *mbstring*
    - *pdo*

Для работы приложения вы должны указать для MySQL следующие настройки
```
[mysqld]
default_authentication_plugin=mysql_native_password
```

И создавйте пользователей с типом пароля `...IDENTIFIED WITH mysql_native_password...` и создайте БД
Например:
```mysql
CREATE USER 'ibs'@'127.0.0.1' IDENTIFIED WITH mysql_native_password BY 'ibs-password';
GRANT ALL PRIVILEGES ON ibs.* TO 'ibs'@'127.0.0.1' WITH GRANT OPTION;
CREATE DATABASE IF NOT EXISTS `ibs` COLLATE 'utf8mb4_general_ci';
FLUSH PRIVILEGES;
```

1) Подтяните все зависимости командой `composer install`
2) Создайте БД и настройте ее в `./.env` и `./.env.test`
3) Добавьте таблицы в свою БД командой `./bin/console doctrine:schema:update --force`
4) Добавьте сертификат GRPC сервера в `./config/grpc/root_mt.cert`
5) В `src/Service/Mt5ManagerApi` добавьте сгенерированные proto файлы для общения с MT5ManagerApi GRPC сервером, 
выполнив команду: `git clone https://<YOUR_LOGIN_FROM_BITBUCKET>@git.gfx.dom/scm/mtpxy/proto.git ./Mt5ManagerApi/`

6) После необходимо указать пространство имен psr-4 в autoload в файле composer.json:
```json
{
    "autoload": {
        "psr-4": {
            "Google\\": "Mt5ManagerApi/php/Google",
            "GPBMetadata\\": "Mt5ManagerApi/php/GPBMetadata",
            "Mt5ManagerApi\\": "Mt5ManagerApi/php/Mt5ManagerApi"
        }
    }
}
```

7) Сгенерируйте ключи авторизации выполнив в корне проекта следующие команды: 
  - `openssl genrsa -out config/jwt/private.pem -aes256 4096` Здесь Вас попросят ввести пароль при генерации, он также должен быть указан в `./env`-->`JWT_PASSPHRASE`
  - `openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem`
  - `sudo chmod 644 config/jwt/private.pem`



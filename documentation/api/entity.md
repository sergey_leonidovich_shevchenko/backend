**Entity** *(Сущности API)*
===========================

[Account](entity/account.md) - Сущность "счета"

[Broker](entity/broker.md) - Сущность "брокер"

[ConsultingCompany](entity/consulting_company.md) - Сущность "консалтинговая компания"

[PersonalArea](entity/personal_area.md) - Сущность "личный кабинет"

[SalesDepartment](entity/sales_department.md) - Сущность "департамент продаж"

[SbCheckStatus](entity/sb_check_status.md) - Сущность "проверка статуса СБ"

[StatusVip](entity/status_vip.md) - Сущность "VIP-статус"

[TradingPlatform](entity/trading_platform.md) - Сущность "торговая платформа"

[Transaction](entity/transaction.md) - Сущность "транзакции"

[TypeTradingAccount](entity/type_trading_account.md) - Сущность "тип торгового счета"

[User](entity/user.md) - Сущность "пользователь"

**TradingPlatform** *(Сущность торговые платформы)*
==================================

- Получить список всех торговых платформ (`ROLE_ENTITY_API_TRADING_PLATFORM_GET`): 
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/trading_platform/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "status": "success",
            "entity": {
                "id": 777,
                "balance": {
                    "server": "balance_server",
                    "port": 3306,
                    "DB": "balance_DB_name",
                    "user": "balance_DB_user",
                    "password": "password_DB_user"
                },
                "code": "trading_platfrom_code_name",
                "crypt": true,
                "group": "trading_platfrom_group_name",
                "login": 777,
                "password": "trading_platfrom_password",
                "port": 3307,
                "server": "trading_platfrom_server",
                "timeout": 500,
                "created_at": "2020-02-25T14:18:04+00:00",
                "updated_at": "2020-02-25T14:18:04+00:00"
            }
        }
    ]
    ```

---

- Получить торговую платформу по ее ID=777 (`ROLE_ENTITY_API_TRADING_PLATFORM_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/trading_platform/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "status": "success",
        "entity": {
            "id": 777,
            "balance": {
                "server": "balance_server",
                "port": 3306,
                "DB": "balance_DB_name",
                "user": "balance_DB_user",
                "password": "password_DB_user"
            },
            "code": "trading_platfrom_code_name",
            "crypt": true,
            "group": "trading_platfrom_group_name",
            "login": 777,
            "password": "trading_platfrom_password",
            "port": 3307,
            "server": "trading_platfrom_server",
            "timeout": 500,
            "created_at": "2020-02-25T14:18:04+00:00",
            "updated_at": "2020-02-25T14:18:04+00:00"
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить новую торговую платформу (`ROLE_TRADING_PLATFORM_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/trading_platform/ -H "Content-Type: application/json" -d '{
        "balance": {
            "server": "balance_server",
            "port": 3306,
            "DB": "balance_DB_name",
            "user": "balance_DB_user",
            "password": "password_DB_user"
        },
        "code": "trading_platfrom_code_name",
        "crypt": true,
        "group": "trading_platfrom_group_name",
        "login": 777,
        "password": "trading_platfrom_password",
        "port": 3307,
        "server": "trading_platfrom_server",
        "timeout": 500
    }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "entity": {
            "id": 777,
            "balance": {
                "server": "balance_server",
                "port": 3306,
                "DB": "balance_DB_name",
                "user": "balance_DB_user",
                "password": "password_DB_user"
            },
            "code": "trading_platfrom_code_name",
            "crypt": true,
            "group": "trading_platfrom_group_name",
            "login": 777,
            "password": "trading_platfrom_password",
            "port": 3307,
            "server": "trading_platfrom_server",
            "timeout": 500,
            "created_at": "2020-02-25T14:18:04+00:00",
            "updated_at": "2020-02-25T14:18:04+00:00"
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В коде торговой платформы допустимы только латинские буквы, цыфры и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Изменить информацию о торговой платформе с ID=777 (`ROLE_ENTITY_API_TRADING_PLATFORM_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/trading_platform/777/ -H "Content-Type: application/json" -d '{
        "balance": {
            "server": "balance_server",
            "port": 3306,
            "DB": "balance_DB_name",
            "user": "balance_DB_user",
            "password": "password_DB_user"
        },
        "code": "trading_platfrom_code_name",
        "crypt": true,
        "group": "trading_platfrom_group_name",
        "login": 777,
        "password": "trading_platfrom_password",
        "port": 3307,
        "server": "trading_platfrom_server",
        "timeout": 500
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация торговой платформы успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В коде торговой платформы допустимы только латинские буквы, цыфры и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить торговую платформу по ее ID=777 (`ROLE_ENTITY_API_TRADING_PLATFORM_DELETE`):
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/trading_platform/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

**SalesDepartment** *(Сущность "департамент продаж")*
==============================================
- Получить список всех департаментов продаж (`ROLE_ENTITY_API_SALES_DEPARTMENT_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sales_department/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "id": 777,
            "code": "codename",
            "consalting_company": 1
        }
    ]
    ```

---

- Получить департамент продаж по его ID=777 (`ROLE_ENTITY_API_SALES_DEPARTMENT_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sales_department/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "id": 777,
        "code": "codename",
        "consalting_company": 1
    }
    ```
    OR
    ```
    Status: 200 OK
    Body:
    {
        "id": 777,
        "code": "codename",
        "consalting_company": 1
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить новый департамент продаж (`ROLE_SALES_DEPARTMENT_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sales_department/ -H "Content-Type: application/json" -d '{
        "code": "codename",
        "consalting_company": 1
    }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Департамент продаж успешно добавлен!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В коде департамента продаж допустимы только латинские буквы, цыфры и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Изменить информацию департамента продаж с ID=777 (`ROLE_ENTITY_API_SALES_DEPARTMENT_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sales_department/777/ -H "Content-Type: application/json" -d '{
        "code": "codename", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_]+$/i
        "consalting_company": 1
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация о департаменте продаж успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В коде департамента продаж допустимы только латинские буквы, цыфры и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить личный кабинет с ID=777 (`ROLE_ENTITY_API_SALES_DEPARTMENT_DELETE`):
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sales_department/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

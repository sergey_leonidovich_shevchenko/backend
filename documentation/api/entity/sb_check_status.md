**SbCheckStatus** *(Сущность статуса проверки СБ)*
==================================================

- Получить список всех статусов проверки СБ (`ROLE_ENTITY_API_SB_CHECK_STATUS_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sb_check_status/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "id": 777,
            "name": "Sb_check_status_name"
        }
    ]
    ```

---

- Получить статус проверки СБ по его ID=777 (`ROLE_ENTITY_API_SB_CHECK_STATUS_GET`): 
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sb_check_status/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "id": 777,
        "name": "Sb_check_status_name"
    }
    ```
    OR
    ```
    Status: 200 OK
    Body:
    {
        "id": 777,
        "name": "Sb_check_status_name"
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить новый статус проверки СБ (`ROLE_SB_CHECK_STATUS_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sb_check_status/ -H "Content-Type: application/json" -d '{
        "name": "Sb_check_status_name" // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
    }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Статус проверки СБ успешно добавлен!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии статуса проверки СБ допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Изменить информацию о статусе проверки СБ с ID=777 (`ROLE_ENTITY_API_SB_CHECK_STATUS_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sb_check_status/777/ -H "Content-Type: application/json" -d '{
        "name": "Sb_check_status_name_new" // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация о статусе проверки СБ успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии статуса проверки СБ допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить статус проверки СБ по его ID=777 (`ROLE_ENTITY_API_SB_CHECK_STATUS_DELETE`):
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/sb_check_status/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

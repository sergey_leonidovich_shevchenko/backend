**PersonalArea** *(Сущность личного кабинета)*
==============================================
- Получить список всех статусов проверки СБ (`ROLE_ENTITY_API_PERSONAL_AREA_GET`): 
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/personal_area/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "id": 777,
            "surname": "Surname",
            "name": "Name",
            "middle_name": "Middle_name",
            "email": "email@mail.ru",
            "phone": "+79998887766",
            "birthday": "2020-12-30",
            "usdt_tether": "usdt_tether",
            "active": "active",
            "broker": 1,
            "sb_check_status": 1,
            "status_vip": 1
        }
    ]
    ```

---

- Получить личный кабинет по его ID=777 (`ROLE_ENTITY_API_PERSONAL_AREA_GET`): 
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/personal_area/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "id": 777,
        "surname": "Surname",
        "name": "Name",
        "middle_name": "Middle_name",
        "email": "email@mail.ru",
        "phone": "+79998887766",
        "birthday": "2020-12-30",
        "usdt_tether": "usdt_tether",
        "active": "active",
        "broker": 1,
        "sb_check_status": 1,
        "status_vip": 1
    }
    ```
    OR
    ```
    Status: 200 OK
    Body:
    {
        "id": 777,
        "surname": "Surname",
        "name": "Name",
        "middle_name": "Middle_name",
        "email": "email@mail.ru",
        "phone": "+79998887766",
        "birthday": "2020-12-30",
        "usdt_tether": "usdt_tether",
        "active": "active",
        "broker": 1,
        "sb_check_status": 1,
        "status_vip": 1
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить новый личный кабинет (`ROLE_ENTITY_API_PERSONAL_AREA_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/personal_area/ -H "Content-Type: application/json" -d '{
        "surname": "Surname",
        "name": "Name",
        "middle_name": "Middle_name",
        "email": "email@mail.ru",
        "phone": "+79998887766",
        "birthday": "2020-12-30",
        "usdt_tether": "usdt_tether",
        "active": "active",
        "broker": 1,
        "sb_check_status": 1,
        "status_vip": 1
    }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Личный кабинет успешно добавлен!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии личного кабинета допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Изменить информацию личного кабинета с ID=777 (`ROLE_ENTITY_API_PERSONAL_AREA_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/personal_area/777/ -H "Content-Type: application/json" -d '{
        "surname": "Surname", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
        "name": "Name", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
        "middle_name": "Middle_name", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
        "email": "email@mail.ru",
        "phone": "+79998887766",
        "birthday": "2020-12-30",
        "usdt_tether": "usdt_tether", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
        "active": "active",
        "broker": 1,
        "sb_check_status": 1,
        "status_vip": 1 
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация о личном кабинете успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии личного кабинета допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить личный кабинет с ID=777 (`ROLE_ENTITY_API_PERSONAL_AREA_DELETE`): 
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/personal_area/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

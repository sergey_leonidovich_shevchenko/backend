**ConsultingCompany** *(Сущность консалтинговая компания)*
==============================================
- Получить список всех консалтинговых компаний (`ROLE_ENTITY_API_CONSULTING_COMPANY_GET`): 
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/consulting_company/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "id": 777,
            "code": "codename",
            "url": "http://url",
            "broker": 1
        }
    ]
    ```

---

- Получить консалтинговую компанию по ее ID=777 (`ROLE_ENTITY_API_CONSULTING_COMPANY_GET`): 
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/consulting_company/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "id": 777,
        "code": "codename",
        "url": "http://url",
        "broker": 1
    }
    ```
    OR
    ```
    Status: 200 OK
    Body:
    {
        "id": 777,
        "code": "codename",
        "url": "http://url",
        "broker": 1
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить новый личный кабинет (`ROLE_CONSULTING_COMPANY_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/consulting_company/ -H "Content-Type: application/json" -d '{
        "code": "codename",
        "url": "http://url",
        "broker": 1
    }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Консалтинговая компания успешно добавлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В коде консалтинговой компании допустимы только латинские буквы, цыфры и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Изменить информацию консалтинговой компании с ID=777 (`ROLE_ENTITY_API_CONSULTING_COMPANY_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/consulting_company/777/ -H "Content-Type: application/json" -d '{
        "code": "codename", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_]+$/i
        "url": "http://url",
        "broker": 1
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация о консалтинговой компании успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В коде консалтинговой компании допустимы только латинские буквы, цыфры и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить личный кабинет с ID=777 (`ROLE_ENTITY_API_CONSULTING_COMPANY_DELETE`):
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/consulting_company/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

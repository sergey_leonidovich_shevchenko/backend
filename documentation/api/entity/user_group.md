**UserGroup** *(Сущность пользовательской группы)*
==================================================

- Получить список всех пользовательских групп (`ROLE_ENTITY_API_USER_GROUP_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/user_group/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "id": 1,
            "name": "User_Group_1",
            "name_rus": "Пользовательская группа №1",
            "description": "Описание пользовательской группы №1",
            "roles": [
                "ROLE_ENTITY_API_BROKER_GET",
                "ROLE_ENTITY_API_BROKER_UPDATE"
            ]
        }
    ]
    ```

---

- Получить пользовательскую группу по ее ID=1 (`ROLE_ENTITY_API_USER_GROUP_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/user_group/1/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "id": 1,
        "name": "User_Group_1",
        "name_rus": "Пользовательская группа №1",
        "description": "Описание пользовательской группы №1",
        "roles": [
            "ROLE_ENTITY_API_BROKER_GET",
            "ROLE_ENTITY_API_BROKER_UPDATE"
        ]
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить новую пользовательскую группу (`ROLE_ENTITY_API_USER_GROUP_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/user_group/ -H "Content-Type: application/json" -d '{
        "name": "User_Group_1",
        "name_rus": "Пользовательская группа №1",
        "description": "Описание пользовательской группы №1",
        "roles": [
            "ROLE_ENTITY_API_BROKER_GET",
            "ROLE_ENTITY_API_BROKER_UPDATE"
        ]
    }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Пользовательская группа успешно добавлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии пользовательской группы допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Изменить информацию о пользователе с ID=1 (`ROLE_ENTITY_API_USER_GROUP_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/user_group/1/ -H "Content-Type: application/json" -d '{
        "name": "User_Group_1",
        "name_rus": "Пользовательская группа №1",
        "description": "Описание пользовательской группы №1",
        "roles": [
            "ROLE_ENTITY_API_BROKER_GET",
            "ROLE_ENTITY_API_BROKER_UPDATE"
        ]
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация о пользовательской группе успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии пользовательской группы допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить пользовательскую группу по ее ID=1 (`ROLE_ENTITY_API_USER_GROUP_DELETE`):
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/user_group/1/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

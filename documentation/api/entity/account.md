**Account** *(Сущность счета)*
==============================

- Получить список всех счетов (`ROLE_ENTITY_API_ACCOUNT_GET`): 
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/account/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "id": 777,
            "user": 1,
            "trading_platform": 1,
            "personal_area": 1,
            "type_trading_account": 1,
            "sales_department": 1,
            "source_code": "crm",
            "number": 1,
            "leverage": 100
        }
    ]
    ```

---

- Получить счет по его ID=777  (`ROLE_ENTITY_API_ACCOUNT_GET`): 
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/account/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "id": 777,
        "user": 1,
        "trading_platform": 1,
        "personal_area": 1,
        "type_trading_account": 1,
        "sales_department": 1,
        "source_code": "crm",
        "number": 1,
        "leverage": 100
    }
    ```
    OR
    ```
    Status: 200 OK
    Body:
    {
        "id": 777,
        "user": 1,
        "trading_platform": 1,
        "personal_area": 1,
        "type_trading_account": 1,
        "sales_department": 1,
        "source_code": "crm",
        "number": 1,
        "leverage": 100
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить новый счет  (`ROLE_ACCOUNT_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/account/ -H "Content-Type: application/json" -d '{
        "user": 777,
        "trading_platform": 1,
        "personal_area": 1,
        "type_trading_account": 1,
        "sales_department": 1,
        "source_code": "crm",
        "number": 1,
        "leverage": 100
     }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Счет успешно добавлен!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В коде счета допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Изменить информацию о счете с ID=777 (`ROLE_ENTITY_API_ACCOUNT_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/account/777/ -H "Content-Type: application/json" -d '{
        "user": 1,
        "trading_platform": 1,
        "personal_area": 1,
        "type_trading_account": 1,
        "sales_department": 1,
        "source_code": "crm",
        "number": 1,
        "leverage": 100
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация о счете успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В коде счета допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить счет по его ID=777 (`ROLE_ENTITY_API_ACCOUNT_DELETE`):
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/account/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

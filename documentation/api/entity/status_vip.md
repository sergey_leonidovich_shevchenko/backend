**StatusVip** *(Сущность VIP-статуса)*
======================================

- Получить список всех VIP-статусов (`ROLE_ENTITY_API_STATUS_VIP_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/status_vip/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "id": 777,
            "name": "Status_vip_name",
            "sum_for_check_status": 10
        }
    ]
    ```

---

- Получить VIP-статус по его ID=777 (`ROLE_ENTITY_API_STATUS_VIP_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/status_vip/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "id": 777,
        "name": "Status_vip_name",
        "sum_for_check_status": 10
    }
    ```
    OR
    ```
    Status: 200 OK
    Body:
    {
        "id": 777,
        "name": "Status_vip_name",
        "sum_for_check_status": 10
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить новый VIP-статус (`ROLE_STATUS_VIP_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/status_vip/ -H "Content-Type: application/json" -d '{
        "name": "Status_vip_name", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
        "sum_for_check_status": 10
    }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "VIP-статус успешно добавлен!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии VIP-статуса допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Изменить информацию о VIP-статусе с ID=777 (`ROLE_ENTITY_API_STATUS_VIP_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/status_vip/777/ -H "Content-Type: application/json" -d '{
        "name": "New username", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
        "sum_for_check_status": 15
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация о VIP-статусе успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии VIP-статуса допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить VIP-статус по его ID=777 (`ROLE_ENTITY_API_STATUS_VIP_DELETE`):
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/status_vip/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

**Broker** *(Сущность брокеров)*
=================================

- Получить список всех брокеров (`ROLE_ENTITY_API_BROKER_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/broker/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    [
        {
            "id": 777,
            "name": "name",
            "setting": [
                "setting_1": "value 1",
                "setting_2": "value 2",
                "setting_3": "value 3"
            ],
            "cosulting_company_list": [1,5,777]
        }
    ]
    ```

---

- Получить брокера по его ID=777 (`ROLE_ENTITY_API_BROKER_GET`):
    ```shell script
    curl -X GET http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/broker/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ``` 
    Status: 200 OK
    Body:
    {
        "id": 777,
        "name": "Broker_name",
        "setting": [
            "setting_1": "value 1",
            "setting_2": "value 2",
            "setting_3": "value 3"
        ],
        "cosulting_company_list": [1,5,777]
    }
    ```
    OR
    ```
    Status: 200 OK
    Body:
    {
        "id": 777,
        "name": "Broker_name",
        "setting": {
            "setting_1": "value 1",
            "setting_2": "value 2"
        },
        "cosulting_company_list": [1,5,777]
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Добавить нового брокера (`ROLE_BROKER_ADD`):
    ```shell script
    curl -X POST http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/broker/ -H "Content-Type: application/json" -d '{
        "name": "Broker_name", // Длинна от 1 до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
        "setting": {
            "setting_1": "value 1",
            "setting_2": "value 2"
        },
        "cosulting_company_list": [1,5,777]
    }'
    ```

    Response Good:
    ``` 
    Status: 201 Created
    Body:
    {
        "status": "success",
        "broker": {
            "id": 777,
            "name": "Broker_name",
            "setting": {
                "setting_1": "value 2",
                "setting_2": "value 3"
            },
            "consultingCompanyList": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии брокера допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

    **[Важно!]** *Обратите внимание, что настройки не могу содержать вложенные массивы 
    (в будующих версиях api возможно это будет исправлено).* 
    
    Например:
    ```
    // GOOD
    "setting": {
       "setting_1": "value 1",
       "setting_2": "value 2"
    }
    
    // BAD
    "setting": {
       "setting_1": [...],
       "setting_2": {...}
    }
    ```

---

- Изменить информацию о брокере с ID=777 (`ROLE_ENTITY_API_BROKER_UPDATE`):
    ```shell script
    curl -X PUT http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/broker/777/ -H "Content-Type: application/json" -d '{
        "name": "Broker_name_new", // Длинна до 255. Должна подходить под регулярное выражение типа /^[\w\d_-]+$/i
        "setting": {
            "setting_2": "value 2",
            "setting_3": "value 3"
        },
        "cosulting_company_list": [1,5,777]
    }'
    ```

    Response Good: 
    ```
    Status: 200 OK
    Body:
    {
        "status": "success",
        "messages": {
            "info": [],
            "success": [
                "Информация о брокере успешно обновлена!"
            ],
            "error": [],
            "warning": []
        }
    }
    ```

    Response Bad:
    ``` 
    Status: 400 Bad Request
    Body:
    {
        "status": "error",
        "messages": {
            "info": [],
            "success": [],
            "error": [
                "В названии брокера допустимы только латинские буквы, цыфры, дефис и знак подчеркивания."
            ],
            "warning": []
        }
    }
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

---

- Удалить брокера по его ID=777 (`ROLE_ENTITY_API_BROKER_DELETE`):
    ```shell script
    curl -X DELETE http://<${IBS_BACKEND_NETWORK_SUBNET}.4>/api/broker/777/ -H "Content-Type: application/json"
    ```

    Response Good:
    ```
    Status: 204 No Content
    Body:
    ```

    Response Bad:
    ```
    Status: 404 Not found
    Body:
    {
        "error": {
            "code": 404,
            "message": "Not Found",
        }
    }
    ```

**Логирование в системе** *(Logger)*
=================================

В системе должно много чего логироваться.
Логирование происходит по принципу отсылки сообщений системы в определенные каналы.
Так же есть обработчики(monolog handlers) которые эти каналы слушают и пишут лог в файл(поток вывода или БД)

Все логи храняться в папке `<project_dir>/%kernel.logs_dir%/%kernel.environment%/`

#### Список пользовательских каналов в системе(monolog channels):

- `monolog.logger.api_entity` - происходит запись лога в файл `api/entity.log`
```json
{
    "message":"Запрос на создание сущности",
    "context":{
        "event_name":"event.api.entity.create",
        "identity":{
            "user_id":2,
            "is_auth":true
        },
        "entity_list":[
            {
                "entity_name":"App\\Entity\\Api\\Account",
                "id":6
            }
        ]
    },
    "level":200,
    "level_name":"INFO",
    "channel":"api_entity",
    "datetime":{
        "date":"2020-03-06 09:04:37.381788",
        "timezone_type":3,
        "timezone":"UTC"
    },
    "extra":[

    ]
}
```
---

- `monolog.logger.api_entity_exception` - происходит запись лога в файл `api/entity_exception.log`
```json

```
--- 

- `monolog.logger.deprecation` - происходит запись лога в файл `deprecatons.log`
```text
[2020-03-04 14:59:55] php.INFO: User Deprecated: The Symfony\Bundle\FrameworkBundle\Templating\EngineInterface interface is deprecated since version 4.3 and will be removed in 5.0; use Twig instead. {"exception":"[object] (ErrorException(code: 0): User Deprecated: The Symfony\\Bundle\\FrameworkBundle\\Templating\\EngineInterface interface is deprecated since version 4.3 and will be removed in 5.0; use Twig instead. at /var/www/ibs/backend/vendor/symfony/framework-bundle/Templating/EngineInterface.php:14)"} []
```
---

- `monolog.logger.kernel_exception` - происходит запись лога в файл `kernel/exception.log`
```json
{
    "message":"You have requested a non-existent service \"App\\Helper\\DataGenerator\\Api\\Security\\GeneratePassword\". Did you mean one of these: \"App\\Helper\\DataGenerator\\Api\\Entity\\AccountGenerator\", \"App\\Helper\\DataGenerator\\Api\\Entity\\BrokerGenerator\", \"App\\Helper\\DataGenerator\\Api\\Entity\\UserGenerator\"?",
    "context":{
        "message":"You have requested a non-existent service \"App\\Helper\\DataGenerator\\Api\\Security\\GeneratePassword\". Did you mean one of these: \"App\\Helper\\DataGenerator\\Api\\Entity\\AccountGenerator\", \"App\\Helper\\DataGenerator\\Api\\Entity\\BrokerGenerator\", \"App\\Helper\\DataGenerator\\Api\\Entity\\UserGenerator\"?",
        "code":0,
        "file":"/var/www/ibs/backend/vendor/symfony/dependency-injection/Container.php",
        "trace":[
            {
                "file":"/var/www/ibs/backend/vendor/symfony/dependency-injection/Container.php",
                "line":225,
                "function":"make",
                "class":"Symfony\\Component\\DependencyInjection\\Container",
                "type":"->",
                "args":[
                    "App\\Helper\\DataGenerator\\Api\\Security\\GeneratePassword",
                    1
                ]
            },
            {
                "file":"/var/www/ibs/backend/src/Service/MT/AccountService.php",
                "line":83,
                "function":"get",
                "class":"Symfony\\Component\\DependencyInjection\\Container",
                "type":"->",
                "args":[
                    "App\\Helper\\DataGenerator\\Api\\Security\\GeneratePassword"
                ]
            },
            {
                "file":"/var/www/ibs/backend/src/Service/Api/AccountService.php",
                "line":71,
                "function":"saveAccount",
                "class":"App\\Service\\MT5\\AccountService",
                "type":"->",
                "args":[
                    {

                    }
                ]
            },
            {
                "file":"/var/www/ibs/backend/src/Controller/Api/AccountController.php",
                "line":87,
                "function":"createAccount",
                "class":"App\\Service\\Api\\AccountService",
                "type":"->",
                "args":[
                    {
                        "user":2,
                        "trading_platform":1,
                        "personal_area":1,
                        "type_trading_account":1,
                        "sales_department":1,
                        "source_code":"crm",
                        "number":1,
                        "leverage":100
                    }
                ]
            },
            {
                "file":"/var/www/ibs/backend/vendor/symfony/http-kernel/HttpKernel.php",
                "line":151,
                "function":"addAction",
                "class":"App\\Controller\\Api\\AccountController",
                "type":"->"
            },
            {
                "file":"/var/www/ibs/backend/vendor/symfony/http-kernel/HttpKernel.php",
                "line":68,
                "function":"handleRaw",
                "class":"Symfony\\Component\\HttpKernel\\HttpKernel",
                "type":"->"
            },
            {
                "file":"/var/www/ibs/backend/vendor/symfony/http-kernel/Kernel.php",
                "line":198,
                "function":"handle",
                "class":"Symfony\\Component\\HttpKernel\\HttpKernel",
                "type":"->"
            },
            {
                "file":"/var/www/ibs/backend/public/index.php",
                "line":27,
                "function":"handle",
                "class":"Symfony\\Component\\HttpKernel\\Kernel",
                "type":"->"
            }
        ],
        "request_id":"000000004bbfb608000000005fe145e9"
    },
    "level":400,
    "level_name":"ERROR",
    "channel":"kernel_exception",
    "datetime":{
        "date":"2020-03-06 09:02:30.308363",
        "timezone_type":3,
        "timezone":"UTC"
    },
    "extra":[

    ]
}
```
---

- `monolog.logger.kernel_request` - происходит запись лога всех запросов к системе в файл `kernel/request.log`
```json
{
    "message":"Входящий запрос",
    "context":{
        "request_id":"000000004cb8e44e0000000009ac7e24",
        "event_name":"kernel.request",
        "request":{
            "method":"POST",
            "format":"json",
            "locale":"en",
            "default_locale":"en",
            "route":"route.api.account.add",
            "route_params":[

            ],
            "firewall_context":"security.firewall.map.context.api",
            "controller":"App\\Controller\\Api\\AccountController::addAction",
            "content":"{\n\t\"user\": 2,\n    \"trading_platform\": 1,\n    \"personal_area\": 1,\n    \"type_trading_account\": 1,\n    \"sales_department\": 1,\n    \"source_code\": \"crm\",\n    \"number\": 1,\n    \"leverage\": 100\n}",
            "media_type":"application/json",
            "uri":"http://192.168.220.4/api/account/?idd=15"
        },
        "identity":{
            "user_id":2,
            "is_auth":true
        }
    },
    "level":200,
    "level_name":"INFO",
    "channel":"kernel_request",
    "datetime":{
        "date":"2020-03-06 09:04:37.485770",
        "timezone_type":3,
        "timezone":"UTC"
    },
    "extra":[

    ]
}
```
---

- `monolog.logger.kernel_response` - происходит запись лога всех ответов из системы в файл `kernel/response.log`
```json
{
    "message":"Исходящий ответ от сервера",
    "context":{
        "request_id":"000000004cb8e44e0000000009ac7e24",
        "event_name":"kernel.response",
        "response":{
            "content":"{\n    \"status\": \"success\",\n    \"entity_name\": \"App\\\\Entity\\\\Api\\\\Account\",\n    \"entity\": {\n        \"id\": 6,\n        \"user\": 2,\n        \"trading_platform\": 1,\n        \"personal_area\": 1,\n        \"type_trading_account\": 1,\n        \"sales_department\": 1,\n        \"number\": 1,\n        \"leverage\": 100,\n        \"source_code\": \"crm\",\n        \"created_at\": \"2020-03-06T09:04:13+00:00\",\n        \"updated_at\": \"2020-03-06T09:04:13+00:00\"\n    }\n}",
            "status_code":201,
            "headers":{
                "cache-control":[
                    "no-cache, private"
                ],
                "date":[
                    "Fri, 06 Mar 2020 09:04:37 GMT"
                ],
                "content-type":[
                    "application/json"
                ]
            },
            "charset":null
        },
        "identity":{
            "user_id":2,
            "is_auth":true
        }
    },
    "level":200,
    "level_name":"INFO",
    "channel":"kernel_response",
    "datetime":{
        "date":"2020-03-06 09:04:37.429490",
        "timezone_type":3,
        "timezone":"UTC"
    },
    "extra":[

    ]
}
```
---

- `monolog.logger.mailer` - происходит запись лога в файл `mailer.log` исходящих email сообщений из системы 
```json
{
    "message":"Email передан на отправку",
    "context":{
        "request_id":"000000004cb8e44e0000000009ac7e24",
        "event_name":"event.email.send",
        "email":{
            "attachments":[

            ],
            "from":[
                "sender@mail.ru"
            ],
            "to":[
                "1_email@email.ru"
            ],
            "subject":null,
            "html_template":"emails/account/save-account.html.twig",
            "text_template":null,
            "html_body":"Hello world!\n<h1>Welcome surname_1 name_1 middle_name_1!</h1>\n\n<p>Login: 201610</p>\n<p>Master password: IO82&lt;0`%1vWf2H</p>\n<p>Investor password: m55F/P*bh{PUE8F&quot;&quot;]</p>\n"
        },
        "identity":{
            "user_id":2,
            "is_auth":true
        }
    },
    "level":200,
    "level_name":"INFO",
    "channel":"mailer",
    "datetime":{
        "date":"2020-03-06 09:04:37.357071",
        "timezone_type":3,
        "timezone":"UTC"
    },
    "extra":[

    ]
}
``` 
---

- `monolog.logger.mailer_exception` - происходит запись лога в файл `mailer_exception.log`
```json

```
---

- `monolog.logger.mt_entity` - происходит запись лога в файл `mt/entity.log`
```json
{
    "message":"Запрос на добавление пользователя на mt сервер",
    "context":{
        "request_id":"000000004cb8e44e0000000009ac7e24",
        "event_name":"event.mt.user.add",
        "identity":{
            "user_id":2,
            "is_auth":true
        },
        "user":{
            "login":"201610",
            "group":"demo\\demoforex",
            "rights":483,
            "registrationTime":"2020-03-06T09:04:46Z",
            "lastAccessTime":"2020-03-06T09:04:46Z",
            "name":"surname_1 name_1 middle_name_1",
            "color":4278190080,
            "leverage":100,
            "lastPasswordChange":"2020-03-06T09:04:46Z"
        },
        "master_password":"**************",
        "investor_password":"******************"
    },
    "level":200,
    "level_name":"INFO",
    "channel":"mt5_entity",
    "datetime":{
        "date":"2020-03-06 09:04:29.451336",
        "timezone_type":3,
        "timezone":"UTC"
    },
    "extra":[

    ]
}
```
---

- `monolog.logger.mt_entity_exception` - происходит запись лога в файл `mt/entity_exception.log`
```json
{
    "message":"Ошибка при добавлении пользователя на mt сервер",
    "context":{
        "request_id":"00000000725605f60000000000e2b80d",
        "event_name":"event.mt.user.add.exception",
        "message":"При обращении к MT5 серверу произошла ошибка. Failed! {\"metadata\":[],\"code\":3,\"details\":\"Invalid parameters 'login' or 'group' value\"}",
        "user":{
            "group":"demo\\forex",
            "color":4278190080,
            "rights":483,
            "name":"surname_1 name_1 middle_name_1",
            "country":null,
            "language":0,
            "city":null,
            "address":null,
            "phone":null,
            "email":null,
            "comment":null,
            "phonePassword":null,
            "leverage":100,
            "leadCampaign":null,
            "id":null
        },
        "identity":{
            "user_id":2,
            "is_auth":true
        }
    },
    "level":400,
    "level_name":"ERROR",
    "channel":"mt5_entity_exception",
    "datetime":{
        "date":"2020-03-05 10:22:32.636238",
        "timezone_type":3,
        "timezone":"UTC"
    },
    "extra":[

    ]
}
```
---

- `monolog.logger.security_permission` - происходит запись лога в файл `security/permission.log`
```json

```
---

- `monolog.logger.security_permission_exception` - происходит запись лога в файл `security/permission_exception.log`
```json

```


